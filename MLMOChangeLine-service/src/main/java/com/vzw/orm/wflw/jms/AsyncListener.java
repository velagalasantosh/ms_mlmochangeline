/*package com.vzw.orm.wflw.jms;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
public class AsyncListener {

	@Value("${orbpm.rabbitmq.startconsumermininterval}")
	private long startconsumermininterval;

	@Value("${orbpm.rabbitmq.stopconsumermininterval}")
	private long stopconsumermininterval;

	@Value("${orbpm.rabbitmq.async.maxconcurrentconsumers}")
	private int maxConcurrentConsumers;
	@Value("${orbpm.rabbitmq.async.minconcurrentconsumers}")
	private int minconcurrentconsumers;
	@Value("${orbpm.rabbitmq.async.prefetchcount}")
	private int prefetchCount;

	@Value("${orbpm.rabbitmq.async.dlqname}")
	private String dlqName;

	@Value("${orbpm.rabbitmq.async.length}")
	private long len;

	@Value("${orbpm.rabbitmq.async.queuemode}")
	private String queueMode;
	
	@Value("${orbpm.rabbitmq.async.queuename}")
	private String asyncQueueName;
	
	@Value("${orbpm.rabbitmq.async.exchangename}")
	private String exchangename;
	
	@Value("${orbpm.rabbitmq.async.routingkey}")
	private String asyncRKey;

	private static final Logger logger = LoggerFactory.getLogger(AsyncListener.class);

	SimpleMessageListenerContainer containerObj = new SimpleMessageListenerContainer();

	@Bean
	Queue asyncQueue() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("x-dead-letter-exchange", dlqName);
		args.put("x-max-length", len);
		args.put("x-queue-mode", queueMode);
		return new Queue(asyncQueueName, true, false, false, args);
	}

	@Bean
	Binding asyncBinding(@Qualifier("asyncQueue") Queue queue) {
		return new Binding(queue.getName(), Binding.DestinationType.QUEUE, exchangename, asyncRKey, null);
	}

	@Bean
	SimpleMessageListenerContainer asyncContainer(ConnectionFactory connectionFactory,
			@Qualifier("asyncListenerAdapter") MessageListenerAdapter listenerAdapter) {
		containerObj.setConnectionFactory(connectionFactory);
		containerObj.setQueueNames(asyncQueueName);
		containerObj.setMessageListener(listenerAdapter);
		containerObj.setConcurrentConsumers(this.minconcurrentconsumers);
		containerObj.setMaxConcurrentConsumers(this.maxConcurrentConsumers);
		containerObj.setStartConsumerMinInterval(this.startconsumermininterval);
		containerObj.setStopConsumerMinInterval(this.stopconsumermininterval);
		containerObj.setDefaultRequeueRejected(false); // Move to DLQ - Since we
														// custom logic; Log it
														// and Move to DLQ.
		containerObj.setPrefetchCount(prefetchCount); // default 250
		return containerObj;
	}

	public void stopContainer() {
		containerObj.stop();
		logger.info("************************* ASYNC Listener Stopped ******************** ");
	}

	public void startContainer() {
		containerObj.start();
		logger.info("************************* ASYNC Listener Started ******************** ");
	}

	public boolean isContainerActive() {
		return containerObj.isActive();
	}

	@Bean
	MessageListenerAdapter asyncListenerAdapter(AsyncExecutor receiver) {
		return new MessageListenerAdapter(receiver, "receiveStartAsyncExecMessage");
	}

}
*/