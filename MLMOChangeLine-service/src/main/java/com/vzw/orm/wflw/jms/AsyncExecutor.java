/*package com.vzw.orm.wflw.jms;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jbpm.services.api.ProcessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Component
public class AsyncExecutor {

	private static final Logger logger = LoggerFactory.getLogger(AsyncExecutor.class);

	@Autowired
	private ProcessService processService;

	public void receiveStartAsyncExecMessage(byte[] message) {
		long start_time = new Date().getTime();
		startProcess(new String(message));
		logger.info("***************** ");
		long end_time = new Date().getTime();
		logger.info("Workflow Processed in :-" + String.valueOf(end_time - start_time));

	}

	public void startProcess(String mqPayload) {

		String deploymentId = null;
		String bpmClusterId = null;
		String processDefId = null;
		String parentprocessinstanceid = null;
		String payload = null;
		//VFWorkflowRequest vfWorkflowRequest = null;
		try {

			ObjectNode node = new ObjectMapper().readValue(mqPayload, ObjectNode.class);

			if (node.has("deploymentId")) {
				deploymentId = node.get("deploymentId").toString().replace("\"", "");
			}

			if (node.has("parentprocessinstanceid")) {
				parentprocessinstanceid = node.get("parentprocessinstanceid").toString().replace("\"", "");
			}

			if (node.has("bpmClusterId")) {
				bpmClusterId = node.get("bpmClusterId").toString().replace("\"", "");
			}

			if (node.has("processDefId")) {
				processDefId = node.get("processDefId").toString().replace("\"", "");
			}

			if (node.has("payload")) {
				payload = node.get("payload").toString();

				logger.info("Full Workflow Payload  :" + payload);

				// Extract VFPayload from workflowpayload

				try {
					ObjectNode workflowRequest = new ObjectMapper().readValue(payload, ObjectNode.class);
					JsonNode objloca = workflowRequest.path("workflowRequest")
							.path("com.vzw.orm.bpmi.domain.objects.subscriptions.VFWorkflowRequest");
					ObjectMapper tmp = new ObjectMapper();
					//vfWorkflowRequest = new ObjectMapper().readValue(tmp.writeValueAsString(objloca),
						//	VFWorkflowRequest.class);

				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			// Mandatory Parameter check
			if ((deploymentId == null || deploymentId.trim().isEmpty())
					|| (bpmClusterId == null || bpmClusterId.trim().isEmpty())
					|| (processDefId == null || processDefId.trim().isEmpty())) {
				logger.error("Mandatory Parameter checks failed.....");
				throw new RuntimeException("Mandatory Parameter checks failed.....");

			}

			// Setting payload param in workflowrequest object.
			Map<String, Object> params = new HashMap<String, Object>();
			//params.put("workflowRequest", vfWorkflowRequest);

			// Start Business process with params
			long processInstanceId = processService.startProcess(deploymentId, processDefId, params);
			logger.info("Workflow sucessfully started ....." + processInstanceId + ".... Parent Processinstance Id : "
					+ parentprocessinstanceid); // If it is "-1" then no parent
												// is associated .

		} catch (Exception e) {
			logger.error("Unable to start business process ......", e);

			// Retry logic = Three Time then move it to failure queue.
		}
	}

}
*/