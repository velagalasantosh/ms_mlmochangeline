package com.vzw.orm.wflw.util;

public class Constants {

	// Exchange Common
	public static final String exchangename = "AM_REQ_EX";

	// SYNC Vs ASYNC
	public static final String asyncRKey = "AM_B_REQ";
	public static final String asyncQueueName = "ORBPM.AM.B.REQ";
	public static final String asyncFailQueueName = "ORBPM.BPM.ASYNC.EXEC.FAIL";
	public static final String asyncFailRKey = "ASYNC_WORKFLOW_FAIL";

}
