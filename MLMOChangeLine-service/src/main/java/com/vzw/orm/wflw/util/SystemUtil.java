package com.vzw.orm.wflw.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.CompositePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.StandardServletEnvironment;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;


@RestController
@RequestMapping("/service")
public class SystemUtil {

	private static final Logger logger = LoggerFactory.getLogger(SystemUtil.class);

	@Autowired
	private EurekaClient discoveryClient;

	@Value("${spring.banner.location}")
	private String value;

	@Autowired
	private Environment environment;

	@Value("${server.port}")
	private String appPort;

	@PostConstruct
	public void setHostPort() {
		System.setProperty("server.port", appPort);
		logger.info(" *****Application Port : " + System.getProperty("server.port"));
	}

	// Getting Host and Port from Eureka CLient
	@RequestMapping("/geteureka/{serviceId}")
	public @ResponseBody String getEureka(@PathVariable String serviceId) {
		InstanceInfo instance = this.discoveryClient.getNextServerFromEureka(serviceId, false);
		return instance.getHostName() + ":" + instance.getPort();
	}

	// Getting all SYSTEM props
	@RequestMapping("/getconfig/systemproperties")
	public @ResponseBody List<PropertyHolder> getAllprop() {
		System.out.println("From value :" + value);

		System.out.println("From system value :" + environment.getProperty("narayana.dbcp.maxTotal"));

		List<PropertyHolder> propertyList = new ArrayList<PropertyHolder>();

		String[] profiles = environment.getActiveProfiles();
		if (profiles != null && profiles.length > 0) {
			for (String profile : profiles) {
				System.out.println("Profile :" + profile);
			}
		} else {
			System.out.println("Setting default profile");
		}

		// Print the profile properties
		if (environment != null && environment instanceof StandardServletEnvironment) {
			StandardServletEnvironment env = (StandardServletEnvironment) environment;
			MutablePropertySources mutablePropertySources = env.getPropertySources();
			if (mutablePropertySources != null) {
				for (PropertySource<?> propertySource : mutablePropertySources) {

					if (propertySource instanceof MapPropertySource) {
						MapPropertySource mapPropertySource = (MapPropertySource) propertySource;
						if (mapPropertySource.getPropertyNames() != null) {
							System.out.println("PropertySource Get name :" + propertySource.getName());
							// if
							// (propertySource.getName().contains("application.properties"))
							// {
							String[] propertyNames = mapPropertySource.getPropertyNames();
							for (String propertyName : propertyNames) {
								Object val = mapPropertySource.getProperty(propertyName);
								System.out.print(propertyName);
								System.out.print(" = " + val);
								System.out.println();

								PropertyHolder obj = new PropertyHolder();
								obj.setKey(propertyName);
								obj.setValue(String.valueOf(val));

								propertyList.add(obj);
							}
						}
						// }
					}
				}
			}
		}
		return propertyList;

	}

	// Getting Application props

	@RequestMapping("/getconfig/appproperties")
	public @ResponseBody Properties applicationProperties() {
		final Properties properties = new Properties();
		for (Iterator it = ((AbstractEnvironment) environment).getPropertySources().iterator(); it.hasNext();) {
			PropertySource propertySource = (PropertySource) it.next();
			if (propertySource instanceof PropertiesPropertySource) {
				System.out.println("Adding all properties contained in " + propertySource.getName());
				properties.putAll(((MapPropertySource) propertySource).getSource());
			}
			if (propertySource instanceof CompositePropertySource) {
				properties.putAll(getPropertiesInCompositePropertySource((CompositePropertySource) propertySource));
			}
		}
		return properties;
	}

	private Properties getPropertiesInCompositePropertySource(CompositePropertySource compositePropertySource) {
		final Properties properties = new Properties();
		compositePropertySource.getPropertySources().forEach(propertySource -> {
			if (propertySource instanceof MapPropertySource) {
				System.out.println("Adding all properties contained in " + propertySource.getName());
				properties.putAll(((MapPropertySource) propertySource).getSource());
			}
			if (propertySource instanceof CompositePropertySource)
				properties.putAll(getPropertiesInCompositePropertySource((CompositePropertySource) propertySource));
		});
		return properties;
	}

}