/**
 * 
 */
package com.vzw.orm.wflw.util;

import java.io.Serializable;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.business.data.update.domain.objects.VboSignalResponse;
import com.vzw.orm.orderfulfillment.domain.objects.vzcare.LineCompleteSignalVO;

/**
 * @author sankuvi
 *
 */
public class BosRewireUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(BosRewireUtil.class);

	/**
	 * This method is used to validate the response received from BDU
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateBosRewireStatusResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validateBosRewireStatusResponse() method ...");

		boolean flag = false;
		try {
			
			VboSignalResponse orderResponse = (VboSignalResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != orderResponse) {
				LOGGER.info("BOS Rewire Status Response is : " + orderResponse + " for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				if (null != orderResponse.getStatus()) {
					if (BPMConstants.SUCCESS.equalsIgnoreCase(orderResponse.getStatus())) {
						LOGGER.info(orderResponse.getStatusDesc());
						flag = true;
						kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
						if ("true".equalsIgnoreCase(orderResponse.getBosFlag())) {
							kcontext.setVariable("isBOSRewire", true);
						} else {
							kcontext.setVariable("isBOSRewire", false);
						}
					}
				}
				if (!flag) {
					LOGGER.info("ErrorCode : " + orderResponse.getStatusCode() + ", Error Source : Line Order Status Update" + ", Error Details : " + orderResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, orderResponse.getStatusCode(), "Line Order Status Update",
							orderResponse.getStatusDesc(), null, BPMConstants.COMP_COUNT);
				}
			} else {
				LOGGER.error("Response Object from BOS Rewire Status Response is NULL for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
						"Response Object from Micro Service is NULL", null, BPMConstants.COMP_COUNT);
			}

		} catch (Exception e) {
			LOGGER.error(
					"Exception while validating response received from BOS Rewire Status Response. Exception : " + e);
			throw new Exception(e);
		}

		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

	public void signalBosRewireLteProcess(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering into signalBosRewireLteProcess() method..");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			
			VboSignalResponse orderResponse = (VboSignalResponse) (kcontext.getVariable(BPMConstants.RESULT));
			
			ServiceProvider bpmUtil = new ServiceProvider();
			String URL = null;
			if(orderResponse.getSite()!=null && !orderResponse.getSite().isEmpty()){
				URL = bpmUtil.getBpmServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BASE_URL.toString(),
					BPMConstants.SERVICE_ID_BPM+"-"+orderResponse.getSite());
			}else{
				URL = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BASE_URL.toString(),
						BPMConstants.SERVICE_ID_BPM);
			}

			URL = URL + "/" + orderResponse.getWfDeploymentId() + "/processes/instances/" + orderResponse.getWfProcessInstId() + "/signal/LTE_SIGNAL";
			LOGGER.info("LTE_SIGNAL URL : " + URL);

			kcontext.setVariable("url", URL);
			kcontext.setVariable("bpmAccessId",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
			kcontext.setVariable("bpmAccessCode",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			ObjectMapper mapper = new ObjectMapper();
			LineCompleteSignalVO lteSignal = new LineCompleteSignalVO();
			lteSignal.setVisionOrderNo(orderResponse.getMasterOrdNo());
			lteSignal.setOrderNo(workFlowRequest.getOrderId());
			lteSignal.setStatus("LC");
			lteSignal.setMtn(workFlowRequest.getMtn());
			lteSignal.setLineItmNo(workFlowRequest.getLineItemNumber());
			LOGGER.info("LTE_SIGNAL Content is:" + mapper.writeValueAsString(lteSignal));
			kcontext.setVariable("payload", mapper.writeValueAsString(lteSignal));

		} catch (Exception e) {
			LOGGER.error("Exception while signaling LTE:" + e.getMessage());
		}
	}
	
	public void validateBosRewireLteSignalResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering into validateBosRewireLteSignalResponse() method..");
		boolean flag = false;
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			int statusCode = (Integer) kcontext.getVariable("status");
			String statusMsg = (String) kcontext.getVariable("statusMsg");
			LOGGER.info("Status : " + statusCode + ". Status Message : " + statusMsg);
			
			if(statusCode>=200 && statusCode<300){
				flag = true;
				LOGGER.info("LTE Process has been successfully signalled");
			}else{
				LOGGER.info("Unable to signal LTE Process");
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR01", "BPM", statusMsg, null, BPMConstants.COMP_COUNT);
			}

		} catch (Exception e) {
			LOGGER.error("Exception while signaling LTE:" + e.getMessage());
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
	
}
