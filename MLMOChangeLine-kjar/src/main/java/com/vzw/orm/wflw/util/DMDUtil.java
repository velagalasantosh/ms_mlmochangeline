package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.KeyValueObject;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.device.domain.DeviceBPMWorkFlow;
import com.vzw.orm.device.domain.DeviceManagerRequest;
import com.vzw.orm.device.domain.DeviceResponse;


/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Joshwa J
 *
 */
public class DMDUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(DMDUtil.class);

	/**
	 * This method is used to set all the variable values required to retrieve
	 * DMD
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateDMDRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiateDMDRequest() method");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			long parentProcessInstanceId = kcontext.getProcessInstance().getParentProcessInstanceId();
			long processInstanceId = kcontext.getProcessInstance().getId();
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:" + processInstanceId + " ParentProcessInstanceId:" + parentProcessInstanceId);
			ServiceProvider bpmUtil = new ServiceProvider();
			String dmdUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.DMD_URL.toString(), BPMConstants.SERVICE_ID_DEVICE_MANAGER);
			String deviceContext= (String)kcontext.getVariable("dmdUpdateValue");
			
			LOGGER.info("DMD URL is: " + dmdUrl);
			kcontext.setVariable(BPMConstants.URL, dmdUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			DeviceBPMWorkFlow bpmWorkflow = new DeviceBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(processInstanceId));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setSignalEventId(null);
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-" + PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}

			DeviceManagerRequest deviceManagerReq = new DeviceManagerRequest();
			deviceManagerReq.setAccountNumber(workFlowRequest.getAccountNumber());
			deviceManagerReq.setActivateMtn(workFlowRequest.getActivateMtn());
			deviceManagerReq.setApiLookupName("DMD");
			deviceManagerReq.setBillingInstance(workFlowRequest.getBillingInstance());
			deviceManagerReq.setChangeDevice(workFlowRequest.getChangeDevice());
			deviceManagerReq.setChangeMtn(workFlowRequest.getChangeMtn());
			deviceManagerReq.setChangePlanFeatures(workFlowRequest.getChangePlanFeatures());
			deviceManagerReq.setCorrelationId(workFlowRequest.getCorrelationId());
			deviceManagerReq.setCustomerId(workFlowRequest.getCustomerId());
			deviceManagerReq.setDeviceTechnology(workFlowRequest.getDeviceTechnology());
			deviceManagerReq.setGlobalTechnology(workFlowRequest.getGlobalTechnology());
			deviceManagerReq.setInstallmentLoan(workFlowRequest.getInstallmentLoan());
			deviceManagerReq.setIpAddressType(workFlowRequest.getIpAddressType());
			deviceManagerReq.setIsSuppOrder("N");
			deviceManagerReq.setLineItemNumber(workFlowRequest.getLineItemNumber());
			deviceManagerReq.setLosIdNoP1(workFlowRequest.getLosIdNoP1());
			deviceManagerReq.setLosIdNoP2(workFlowRequest.getLosIdNoP2());
			deviceManagerReq.setMtn(workFlowRequest.getMtn());
			deviceManagerReq.setOrderDateTime(workFlowRequest.getOrderDateTime());
			deviceManagerReq.setOrderDueDate(workFlowRequest.getOrderDueDate());
			deviceManagerReq.setOrderId(workFlowRequest.getOrderId());
			deviceManagerReq.setOrderSource(workFlowRequest.getOrderSource());
			deviceManagerReq.setOrderType(workFlowRequest.getOrderType());
			deviceManagerReq.setDeviceContext(deviceContext);
			deviceManagerReq.setPortIn(workFlowRequest.getPortIn());
			
			deviceManagerReq.setProcessInsId(String.valueOf(kcontext.getProcessInstance().getId()));
			
			deviceManagerReq.setProductionParallelInd(workFlowRequest.getProductionParallelInd());
			deviceManagerReq.setRequestHost(workFlowRequest.getRequestHost());
			//deviceManagerReq.setRouteApi(workFlowRequest.get);
			deviceManagerReq.setSiteId(PropertyFile.getInstance().getDataCenter());
			//deviceManagerReq.setSuppType(workFlowRequest.get);
			deviceManagerReq.setWfReqid(kcontext.getProcessInstance().getProcessId());
			
			deviceManagerReq.setBpmWorkFlowObject(bpmWorkflow);
			
			
			kcontext.setVariable(BPMConstants.CONTENT, deviceManagerReq);

			ObjectMapper obj = new ObjectMapper();
			LOGGER.info("DMD Request Content for is : " + obj.writeValueAsString(deviceManagerReq));
		} catch (Exception e) {
			LOGGER.error("Error in initiateDMDRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received for DMD response
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateDMDResponse(ProcessContext kcontext) throws Exception {
		LOGGER.debug("Entering validateDMDResponse() method");
		kcontext.setVariable("flag", false);

		try {
			DeviceResponse deviceResponse = (DeviceResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != deviceResponse) {
				LOGGER.info("DMD Response is : " + deviceResponse + " for ProcessInstanceId: "
								+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
								+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = deviceResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
					LOGGER.info("DMD responded with Success");
					kcontext.setVariable("flag", true);

				} else {
					LOGGER.debug("ErrorCode : " + deviceResponse.getStatusCode()
							+ ", Error Source : DMD , Error Details : " + deviceResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, deviceResponse.getStatusCode(), "DMD",
							deviceResponse.getStatusDesc(), null, BPMConstants.COMP_COUNT);
				}
			} else {
				LOGGER.error("Response object validateDMDResponse from BPM Data Update is NULL or unexpected");
				throw new Exception("Response object validateDMDResponse from BPM Data Update is NULL or unexpected");
			}
		} catch (Exception e) {
			LOGGER.error("Exception while validating response received from DMD. Exception : " + e);
			throw new Exception(e);
		}
	}
	
	public void getDmdUpdateFlag(ProcessContext kcontext)
	{
		WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
		boolean dmdUpdateFlag=false;
		
		if(workFlowRequest.getLineLevelInfo() !=null){
			
			
			KeyValueObject keyObject=workFlowRequest.getLineLevelInfo().stream().filter(p -> p.getKey().equalsIgnoreCase("dmdDeviceDataUpdate")).findAny().orElse(null);
			
			if(keyObject!=null && (keyObject.getValue().toLowerCase().contains("imei") || keyObject.getValue().toLowerCase().contains("icc_id")) ){
				
				dmdUpdateFlag=true;
				
				kcontext.setVariable("dmdUpdateValue", keyObject.getValue());
			};
				
		}
		kcontext.setVariable("dmdUpdateFlag", dmdUpdateFlag);

		
	}
	
	
	
}