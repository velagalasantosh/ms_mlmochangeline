package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.beanutils.BeanUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.common.core.util.DateTimeUtils;
import com.vzw.orm.visible.domain.common.VisibleWorkFlowRequest;
import com.vzw.orm.visible.domain.common.VisibleBPMWorkFlow;
import com.vzw.orm.visible.domain.common.VisibleDisconnectRequest;
import com.vzw.orm.visible.domain.common.VisibleInterimRes;
import com.vzw.orm.visible.domain.common.VisibleDisconnectResponse;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Pratyusha Pooskuru
 *
 */
public class VisibleUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(VisibleUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * the Visible Disconnect Request
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateVisibleRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiateVisibleRequest() method ...");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);

			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String visibleUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VISIBLE_URL.toString(),
					BPMConstants.SERVICE_ID_VISIBLE);
			LOGGER.info("Visible URL is : " + visibleUrl);
			kcontext.setVariable(BPMConstants.URL, visibleUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			VisibleDisconnectRequest visibleDisconnectRequest = new VisibleDisconnectRequest();
			
			VisibleWorkFlowRequest visibleWFRequest = new VisibleWorkFlowRequest();
			BeanUtils.copyProperties(visibleWFRequest,workFlowRequest);
			visibleWFRequest.setWfReqid(kcontext.getProcessInstance().getProcessId());
			visibleDisconnectRequest.setWorkFlowRequestObject(visibleWFRequest);
			
			VisibleBPMWorkFlow bpmWorkflow = new VisibleBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_VISIBLE_RESPONSE);
			LOGGER.info("deploymentId-->"
					+ (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			bpmWorkflow.setDeploymentId(
					(String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));

			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(
						PropertyFile.getInstance().getClusterId() + "-" + PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			visibleDisconnectRequest.setBpmWorkFlowObject(bpmWorkflow);
			
			kcontext.setVariable(BPMConstants.CONTENT, visibleDisconnectRequest);
			ObjectMapper mapper = new ObjectMapper();
			LOGGER.info("Visible Request Content is : " + mapper.writeValueAsString(visibleDisconnectRequest));
		} catch (Exception e) {
			LOGGER.error("Error in initiateVisibleRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the sync response received from Visible
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateVisibleResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validateVisibleResponse() method ...");

		boolean flag = false;

		try {
			VisibleInterimRes visibleResponse = (VisibleInterimRes) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != visibleResponse) {
				LOGGER.info("Visible Sync Response is : " + visibleResponse + " for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = visibleResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				} else {
					// Call Compensation
					LOGGER.info("ErrorCode : " + visibleResponse.getStatusCode()
							+ ", Error Source : VISIBLE , Error Details : " + visibleResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, visibleResponse.getStatusCode(), "Visible",
							visibleResponse.getStatusDesc(), null, BPMConstants.COMP_COUNT);
				}
			} else {
				// Call Compensation
				LOGGER.info("Response object from Visible is NULL for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
						"Response Object from Micro Service is NULL", null, BPMConstants.COMP_COUNT);
			}
		} catch (Exception e) {
			flag = false;
			kcontext.setVariable(BPMConstants.FLAG, flag);
			LOGGER.error("Exception while validating sync response received from Visible. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

	/**
	 * This method is used to validate the async response received from Visible
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateVisibleSignalResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validateVisibleSignalResponse() method ...");
		VisibleDisconnectResponse visibleSignalResponse = (VisibleDisconnectResponse) (kcontext.getVariable(BPMConstants.RESULT));

		if (null != visibleSignalResponse) {
			LOGGER.info("Visible Disconnect Signal Response is : " + visibleSignalResponse + " for ProcessInstanceId: "
					+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			String status = visibleSignalResponse.getStatus();
			if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
				kcontext.setVariable("migrationStatus", "AR");
			} else {
				// Failure signal Response
				kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				kcontext.setVariable("migrationStatus", "DF");
			}
		} else {
			// Null Signal Response
			throw new Exception("Unexpected Async signal response received from Visible");
		}
	}
	
	public void closeSignal(ProcessContext kcontext, String signalKey) {
		LOGGER.info("Entering closeSignal() method...");
		try {
			LOGGER.info("De-activating safe point of signalKey : " + signalKey + " and signalId : " + kcontext.getVariable(signalKey));

			if (signalKey != null && kcontext.getVariable(signalKey) != null) {
				kcontext.getKieRuntime().getWorkItemManager().completeWorkItem(Long.parseLong(String.valueOf(kcontext.getVariable(signalKey))), null);
			}
		} catch (Exception e) {
			LOGGER.error("Exception inside closeSignal method. Exception : " + e);
			e.printStackTrace();
		}
	}
}