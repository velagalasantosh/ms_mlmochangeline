package com.vzw.orm.wflw.util;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.common.core.util.BillingSysIdUtil;
import com.vzw.common.core.util.DateTimeUtils;
import com.vzw.common.core.util.InetAddressWrapper;
import com.vzw.orm.bpm.domain.objects.compensation.CompensationRequest;
import com.vzw.orm.bpm.domain.objects.compensation.CompensationRuleResponse;
import com.vzw.orm.bpmi.domain.objects.consumer.KeyValueObject;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.business.data.update.domain.BPMDataUpdateWorkFlow;
import com.vzw.orm.business.data.update.domain.BPMDataUpdateWorkFlowRequest;
import com.vzw.orm.business.data.update.domain.objects.AuditBsXrefTransDomain;
import com.vzw.orm.business.data.update.domain.objects.ExceptionInfo;
import com.vzw.orm.business.data.update.domain.objects.PnoResponseStatus;
import com.vzw.orm.business.data.update.domain.objects.ResponseDomain;
import com.vzw.orm.business.data.update.domain.objects.UpdateLnItemStatCdDomain;
import com.vzw.orm.business.data.update.domain.objects.VisionSaveRecordResponse;
import com.vzw.orm.business.data.update.domain.objects.VisionSyncRequest;
import com.vzw.orm.business.data.update.domain.objects.Wrapper;
import com.vzw.orm.osu.domain.objects.BPMWorkFlow;
import com.vzw.orm.osu.domain.objects.OrderStatusUpdateResponse;
import com.vzw.orm.osu.domain.objects.OrderStatusWorkFlowRequest;
import com.vzw.orm.osu.domain.objects.OrderStatusWrapper;
import com.vzw.orm.osu.domain.objects.StatusData;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class LineOrderStatusUpdateUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(LineOrderStatusUpdateUtil.class);

	/**
	 * This method is used to set all the variable values required to update the
	 * line status as LP/LC
	 * 
	 * @param kcontext
	 * @param eventCode
	 * @param eventDesc
	 * @throws Exception
	 */
	public static void initiateOrderStatusUpdateRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateOrderStatusUpdateRequest() method ...");
		try
		{
            WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			
			kcontext.setVariable(BPMConstants.FLAG, false);
			//This logic will skip MCS update
			if("Y".equalsIgnoreCase(workFlowRequest.getProductionParallelInd())){
				LOGGER.info("OSU for MCS will not be applicable as prod parallel is enabled for OrderID:"+workFlowRequest.getOrderId() +" LineItemNo:" + workFlowRequest.getLineItemNumber());
				kcontext.setVariable("skipMCS", true);
			}
			
			ServiceProvider bpmUtil = new ServiceProvider();
			String daoUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.DAO_URL.toString(), BPMConstants.SERVICE_ID_BPM_DATA_UPD);
			LOGGER.debug("DAO Update URL is : " + daoUrl);
			kcontext.setVariable(BPMConstants.URL, daoUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			UpdateLnItemStatCdDomain updateLnStatus = new UpdateLnItemStatCdDomain();
			String lnItmStatCd = (String)kcontext.getVariable(BPMConstants.STATUS_CODE);
			String migrationStatus = (String)kcontext.getVariable("migrationStatus");
			if (StringUtils.isNotBlank(lnItmStatCd)) {
				updateLnStatus.setLnItmStatCd(lnItmStatCd);
			} else {
				kcontext.setVariable("skipMCS", true);
			}
			
			kcontext.setVariable("triggerCaseManagement", false); 
			if(null != workFlowRequest.getChangeCallingPlan() && "true".equalsIgnoreCase(workFlowRequest.getChangeCallingPlan())) {
				if(StringUtils.isNotBlank(lnItmStatCd) && ("LC".equalsIgnoreCase(lnItmStatCd) || "LD".equalsIgnoreCase(lnItmStatCd))) {
					kcontext.setVariable("triggerCaseManagement", true);
				}
			}
			
			if (StringUtils.isNotBlank(migrationStatus)) {
				updateLnStatus.setPpayEvntStatCd(migrationStatus);
			}
			updateLnStatus.setLnItmNo(workFlowRequest.getLineItemNumber());
			updateLnStatus.setOrdNo(workFlowRequest.getOrderId());
			updateLnStatus.setMtn(workFlowRequest.getMtn());
			updateLnStatus.setBillSysId(Integer.toString(BillingSysIdUtil.getBillSysId(workFlowRequest.getBillingInstance())));
			
			updateLnStatus.setBucket((String)kcontext.getVariable(BPMConstants.BUCKET_DT));
			updateLnStatus.setTranNo(workFlowRequest.getOrderId());
			updateLnStatus.setTranLnItmNo(workFlowRequest.getLineItemNumber());
			updateLnStatus.setTranTyp(workFlowRequest.getOrderType());
			updateLnStatus.setTranSubTyp(workFlowRequest.getOrderSource());
			updateLnStatus.setSourceIp(InetAddressWrapper.getCanonicalHostname());
			updateLnStatus.setEventCd("TRAN_STAT");
			updateLnStatus.setEventDesc((String)kcontext.getVariable(BPMConstants.STATUS_DESC));
			
			ObjectMapper mapper = new ObjectMapper();
			updateLnStatus.setPayload(mapper.writeValueAsString(workFlowRequest));
			updateLnStatus.setPayloadSrc(kcontext.getProcessInstance().getProcessId());
			updateLnStatus.setPayloadTyp(workFlowRequest.getOrderType());
			updateLnStatus.setWfPrcssInstId(String.valueOf(kcontext.getProcessInstance().getId()));
			updateLnStatus.setTranStatCd((String)kcontext.getVariable(BPMConstants.STATUS_CODE));
			updateLnStatus.setUpdateTmstamp(DateTimeUtils.formatCurrentTsBasedOnFormat("yyyy-MM-dd-HH.mm.ss.SSS"));
			updateLnStatus.setInsertDateTime(workFlowRequest.getOrderDateTime());
			updateLnStatus.setCompCnt("0");
			updateLnStatus.setCustIdNo(workFlowRequest.getCustomerId());
			updateLnStatus.setAcctNo(workFlowRequest.getAccountNumber());
			
			List<KeyValueObject> lineLvlInfo = workFlowRequest.getLineLevelInfo();
			if(lineLvlInfo!=null && lineLvlInfo.size() > 0){ 
				for (KeyValueObject info : lineLvlInfo) {
					if ("lnItmTypCd".equalsIgnoreCase(info.getKey())) {
						LOGGER.info("setting " + info.getKey() + " value:" +info.getValue());
						updateLnStatus.setLnItmTypCd(info.getValue());
					}
				}
			}else{
				updateLnStatus.setLnItmTypCd("M");
			}

			if (PropertyFile.getInstance().isConsumerClustered()) {
				updateLnStatus.setDataCenter(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				updateLnStatus.setDataCenter(PropertyFile.getInstance().getDataCenter());
			}

			Wrapper<UpdateLnItemStatCdDomain> businessDomain = new Wrapper<UpdateLnItemStatCdDomain>();
			businessDomain.setService("updateLnItemStatCdDao");
			businessDomain.setServiceMethod("updateLnItmStatCd");
			businessDomain.setDomain(updateLnStatus);
			businessDomain.setSync("Y");
			
			kcontext.setVariable(BPMConstants.CONTENT, businessDomain);
			LOGGER.info("UpdateLnItemStatCdDomain content is : " + mapper.writeValueAsString(businessDomain));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateOrderStatusUpdateRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from Line Item
	 * Order Status Update
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateOrderStatusUpdateResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateOrderStatusUpdateResponse() method ...");

		boolean flag = false;
		try
		{
			ResponseDomain orderResponse = (ResponseDomain) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != orderResponse)
			{
				PnoResponseStatus pnoResponseStatus = orderResponse.getPnoResponseStatus();
				if (null != pnoResponseStatus)
				{
					LOGGER.info("Line Order Status Update Response is : " + pnoResponseStatus+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
							+ kcontext.getProcessInstance().getParentProcessInstanceId());
					String code = pnoResponseStatus.getCode();
					String message = pnoResponseStatus.getMessage();
					if ("200".equalsIgnoreCase(code) && BPMConstants.SUCCESS.equalsIgnoreCase(message))
					{
						flag = true;
						kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					}
				}

				if (!flag)
				{
					ExceptionInfo expInfo = orderResponse.getExceptionInfo();
					LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : " + expInfo.getErrorSource() + ", Error Details : " + expInfo.getErrorDeatils());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), expInfo.getErrorSource(), expInfo.getErrorDeatils(),
							null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				LOGGER.error("Response Object from DAO Update Service is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL", null,
						BPMConstants.COMP_COUNT);
			}

		}
		catch (Exception e)
		{
			LOGGER.error("Exception while validating response received from Line Item Order Status Update. Exception : " + e);
			throw new Exception(e);
		}

		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
	
	/**
	 * This method is used to set all the variable values required to update
	 * Vision Save Order status
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void updateVisionSaveOrderRequest(ProcessContext kcontext) throws Exception
	{
		String processName = kcontext.getProcessInstance().getProcessName();
		LOGGER.info("Entering updateVisionSaveOrderRequest() method for "+processName);
		try
		{
			ServiceProvider bpmUtil = new ServiceProvider();
			String visionSaveOrderUrl = null;
			if("BOSRewire".equalsIgnoreCase(processName)){
				visionSaveOrderUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VBO_STATUS_URL.toString(), BPMConstants.SERVICE_ID_BPM_DATA_UPD);
			}else{
				visionSaveOrderUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VISION_UPDT_STAT_URL.toString(), BPMConstants.SERVICE_ID_BPM_DATA_UPD);
			}

			LOGGER.info("Vision Status Update URL for "+processName+" is : " + visionSaveOrderUrl);
			kcontext.setVariable(BPMConstants.URL, visionSaveOrderUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			
			long parentProcessInstanceId = kcontext.getProcessInstance().getParentProcessInstanceId();
			long processInstanceId = kcontext.getProcessInstance().getId();
			
			BPMDataUpdateWorkFlow bpmWorkflow = new BPMDataUpdateWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(processInstanceId));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_VSO_RESPONSE);
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			
			VisionSyncRequest visionSyncRequest = new VisionSyncRequest();

			LOGGER.info("Constructing request for Vision Update request for "+processName);
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			if(kcontext.getVariable("lineStatus")!=null){
				workFlowRequest.setLnItmStatCd((String) kcontext.getVariable("lineStatus"));
			}
			BPMDataUpdateWorkFlowRequest billingWorkFlowRequest = new BPMDataUpdateWorkFlowRequest();
			//converting workflow object to provisioning workflow object
			BeanUtils.copyProperties(billingWorkFlowRequest, workFlowRequest);

			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:" + processInstanceId + " ParentProcessInstanceId:" + parentProcessInstanceId);
			List<BPMDataUpdateWorkFlowRequest> wfList = new ArrayList<BPMDataUpdateWorkFlowRequest>();
			wfList.add(billingWorkFlowRequest);
			visionSyncRequest.setWorkFlowRequestObject(wfList);

			visionSyncRequest.setBpmWorkFlowObject(bpmWorkflow);
			kcontext.setVariable(BPMConstants.CONTENT, visionSyncRequest);
			
			ObjectMapper obj = new ObjectMapper();
			LOGGER.info("Vision Update Status Request Content  for "+processName+" is : "+ obj.writeValueAsString(visionSyncRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in updateVisionSaveOrderRequest method for "+processName+" . Exception : " + e);
			throw new Exception(e);
		}
	}
	
	/**
	 * This method is used to validate the response received from Vision Save
	 * Order
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateVisionUpdateResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.debug("Entering validateVisionUpdateResponse() method ...");

		try
		{
			VisionSaveRecordResponse visionSaveResponse = (VisionSaveRecordResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != visionSaveResponse)
			{
				LOGGER.info("Vision Update Response is : " + visionSaveResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = visionSaveResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					LOGGER.debug("Vision Update responded with Success");
				}
				else
				{
					LOGGER.debug("ErrorCode : " + visionSaveResponse.getStatusCode() + ", Error Source : VISION , Error Details : " + visionSaveResponse.getStatusDesc());
					ObjectMapper obj = new ObjectMapper();
					throw new Exception("Received failure response from Vision : " + obj.writeValueAsString(visionSaveResponse));
				}
			}
			else
			{
				LOGGER.error("Response object VisionSaveRecordResponse from Vision Update is NULL or unexpected for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				throw new Exception("Response object VisionSaveRecordResponse from Vision Update is NULL or unexpected");
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception while validating response received from Vision Update. Exception : " + e);
			throw new Exception(e);
		}
	}
	
	
	
	public void initiateOrderStatusUpdateMCSRequest(ProcessContext kcontext) throws Exception 
	{
		LOGGER.info("Entering initiateOrderStatusUpdateMCSRequest() method ...");
		try
		{
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ); // BPMI workflow request object
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			
			//kcontext.setVariable(BPMConstants.FLAG, false);

			ServiceProvider bpmUtil = new ServiceProvider();
			String daoUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.ORD_STATUS_MCS_URL.toString(), BPMConstants.SERVICE_ID_BPM_ORD_STATUS_MCS);
			LOGGER.info("Order Status Update MCS URL is : " + daoUrl);
			kcontext.setVariable(BPMConstants.URL, daoUrl);
			
			OrderStatusWorkFlowRequest mcsRequest=new OrderStatusWorkFlowRequest(); // OrderStatus updater workflowRequest Object
			
			mcsRequest.setAccountNumber(workFlowRequest.getAccountNumber());
			mcsRequest.setBillingInstance(workFlowRequest.getBillingInstance());
			mcsRequest.setCorrelationId(workFlowRequest.getCorrelationId());
			mcsRequest.setCustomerId(workFlowRequest.getCustomerId());
			mcsRequest.setLineItemNumber(workFlowRequest.getLineItemNumber());
			mcsRequest.setMtn(workFlowRequest.getMtn());
			mcsRequest.setOrderDateTime(workFlowRequest.getOrderDateTime());
			mcsRequest.setOrderId(workFlowRequest.getOrderId());
			mcsRequest.setOrderSource(workFlowRequest.getOrderSource());
			mcsRequest.setOrderType(workFlowRequest.getOrderType());
			mcsRequest.setProductionParallelInd(workFlowRequest.getProductionParallelInd());
			mcsRequest.setRequestHost(workFlowRequest.getRequestHost());
			
			StatusData statusData=new StatusData();
			String tmp_statuscode=(String)kcontext.getVariable(BPMConstants.STATUS_CODE);
			LOGGER.info("Status Code:" + tmp_statuscode);
			
			LOGGER.info("Previous Status:" + kcontext.getVariable("preStatus"));
			
			if(kcontext.getVariable("preStatus")!=null){
				statusData.setCurrentStatus((String)kcontext.getVariable("preStatus"));
			}
			else{
				statusData.setCurrentStatus(null);
			}
				
			statusData.setNewStatus(tmp_statuscode);
			
			if (kcontext.getVariable(BPMConstants.COMP_RES) != null) {
				
				LOGGER.info("CompResponse is inside MCS OSU: "
						+ (CompensationRuleResponse) kcontext.getVariable(BPMConstants.COMP_RES));
				CompensationRuleResponse compRes = (CompensationRuleResponse) kcontext
						.getVariable(BPMConstants.COMP_RES);
				
				statusData.setErrorCode(compRes.getErrorCode());
				
				if (compRes.getErrorDsc() != null) {
					statusData.setErrorDesc(compRes.getErrorDsc());
				} else {
					CompensationRequest compReq = (CompensationRequest) kcontext.getVariable(BPMConstants.COMP_REQ);
					statusData.setErrorDesc(compReq.getErrorDescirption());
				}
			}else {
				statusData.setErrorCode("");
				statusData.setErrorDesc("");
			}
			
			mcsRequest.setStatusData(statusData);			
			
			BPMWorkFlow bpmWorkFlow=new BPMWorkFlow();
			
			bpmWorkFlow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkFlow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkFlow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			bpmWorkFlow.setSignalEventId("ORD_STAT_UPDATE");
			
			OrderStatusWrapper wrapper=new OrderStatusWrapper();
			wrapper.setOrderStatusWorkFlowRequest(mcsRequest);
			wrapper.setBpmWorkFlowObject(bpmWorkFlow);
			
			
			ObjectMapper mapper = new ObjectMapper();
			kcontext.setVariable(BPMConstants.CONTENT, wrapper);
			LOGGER.info("Domain object for MCS status update content is : " + mapper.writeValueAsString(wrapper));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateOrderStatusUpdateMCSRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}
	

	public void validateOrderStatusUpdateMCSRequest(ProcessContext kcontext) throws Exception 
	{
		LOGGER.info("Entering validateOrderStatusUpdateMCSRequest() method ...");

		boolean _flag = false;
		try
		{
			
			OrderStatusUpdateResponse resp= (OrderStatusUpdateResponse) (kcontext.getVariable(BPMConstants.RESULT));
			
			LOGGER.info("Response object from Order Status Updater MS :" + resp+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			
			if (null != resp)
			{
				
				String status=resp.getStatus();
				if (null != status)
				{
					LOGGER.info("Order Status Update MS Response is : " + status+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
							+ kcontext.getProcessInstance().getParentProcessInstanceId());
					
					if ("200".equalsIgnoreCase(resp.getCode()) && BPMConstants.SUCCESS.equalsIgnoreCase(status))
					{
						_flag = true;					
					}
				}

				if (!_flag)
				{
					
					LOGGER.info("ErrorCode : " + resp.getCode() +  ", Error Details : " + resp.getStatus());					
				}
			}
			else
			{
				LOGGER.error("Response Object from Order Status Updater Service is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());				
			}

		}
		catch (Exception e)
		{
			LOGGER.error("Exception inside validateOrderStatusUpdateMCSRequest method. Exception : " + e);
			throw new Exception(e);
		}

		
	}
	
	
	public static void deriveLineStatusForFailures(ProcessContext kcontext) {

		String statusCode = (String) kcontext.getVariable("statusCode");
		
		if ("SS".equalsIgnoreCase(statusCode) || "LW".equalsIgnoreCase(statusCode)) {
			kcontext.setVariable("lineStatus", "EB");
		} else if ("LN".equalsIgnoreCase(statusCode)) {
			kcontext.setVariable("lineStatus", "LNRF");
			kcontext.setVariable("skipVisionUpd", true);
		} else if ("OC".equalsIgnoreCase(statusCode)) {
			kcontext.setVariable("lineStatus", "LC");
		} else if ("LD".equalsIgnoreCase(statusCode)) {
			kcontext.setVariable("lineStatus", "LDRF");
			kcontext.setVariable("skipVisionUpd", true);
		} else if ("OD".equalsIgnoreCase(statusCode)) {
			kcontext.setVariable("lineStatus", "ODRF");
			kcontext.setVariable("skipVisionUpd", true);
		} else {
			kcontext.setVariable("lineStatus", "EA");
		}

	}
	private static String getPreviousStatusCode(String currentStatus) {

		String newStatus;

		switch (currentStatus) {
		case "LN":
			newStatus = "OK";
			break;
		case "SS":
			newStatus = "OK";
			break;
		case "EB":
			newStatus = "SS";
			break;
		case "ES":
			newStatus = "SS";
			break;
		case "EC":
			newStatus = "SS";
			break;
		case "E9":
			newStatus = "SS";
			break;
		case "EN":
			newStatus = "SS";
			break;
		case "LP":
			newStatus = "SS";
			break;
		case "EA":
			newStatus = "LP";
			break;
		case "LC":
			newStatus = "LP";
			break;
		case "OC":
			newStatus = "LC";
			break;
		case "BANA":
			newStatus = "OC";
			break;
		default:
			newStatus = "OK";
			break;
		}

		return newStatus;
	}

	public static void checkVisionUpdateForSuccess(ProcessContext kcontext) {

		LOGGER.info("inside checkVisionUpdateForSuccess() method..");
		boolean flag = false;
		String statusCode = (String) kcontext.getVariable("statusCode");
		try {
			if ("LN".equalsIgnoreCase(statusCode)) {
				flag = true;
				kcontext.setVariable("lineStatus", statusCode);
				LOGGER.info("Vision Status update required for " + statusCode);
			} else {
				LOGGER.info("Vision Status update not required for " + statusCode);
			}
			LOGGER.info("exiting checkVisionUpdateForSuccess() method..");
		} catch (Exception e) {
			LOGGER.error("Exception in checkVisionUpdateForSuccess method. Exception:" + e.getMessage());
		}
		kcontext.setVariable("flag", flag);
	}
	
	
	public void initiateTranLogBsXrefRequest(ProcessContext kcontext, String isTransLog, String isBsXRef) throws Exception
	{
		String pname = kcontext.getProcessInstance().getProcessName();
		LOGGER.info("Entering initiateTranLogBsXrefRequest() method for " +pname);
		try
		{
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
				
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			
			ServiceProvider bpmUtil = new ServiceProvider();
			String daoUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.DAO_URL.toString(), BPMConstants.SERVICE_ID_BPM_DATA_UPD);
			LOGGER.info("BsXref URL for " +pname+" is : " + daoUrl);
			kcontext.setVariable(BPMConstants.URL, daoUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			
			AuditBsXrefTransDomain logevent = new AuditBsXrefTransDomain();
			
			logevent.setDbUserId("ORBPM");
			logevent.setInsertDateTime(DateTimeUtils.formatCurrentTsBasedOnFormat("yyyy-MM-dd-HH.mm.ss.SSS"));
			logevent.setModifiedBy("BPM");
			logevent.setModifiedTmstamp(DateTimeUtils.formatCurrentTsBasedOnFormat("yyyy-MM-dd-HH.mm.ss.SSS"));
			logevent.setSourceIp(InetAddressWrapper.getCanonicalHostname());
			if ("OrderProvisioningNC".equalsIgnoreCase(pname)) {
				kcontext.setVariable(BPMConstants.NE_TYPE_SET, new HashSet<String>());
				
				logevent.setExtBusReqId(String.format("%010d", Long.parseLong(workFlowRequest.getOrderId()))
						+ String.format("%05d", Long.parseLong(workFlowRequest.getLineItemNumber()))
						+ workFlowRequest.getDeviceTechnology()+ "#MTAS_NC");
			}
			else if(kcontext.getVariable("extBusKey")!=null){
				logevent.setExtBusReqId(workFlowRequest.getOrderId()+"#"+workFlowRequest.getMtn()+ "#" + kcontext.getVariable("extBusKey").toString());
			}
			else if(kcontext.getVariable("isOrder")!=null && (Boolean)kcontext.getVariable("isOrder")){
				logevent.setExtBusReqId(workFlowRequest.getOrderId()+"#ConsumerRelease");
			}
			else if ("CloseOutProcess".equalsIgnoreCase(pname)) {
				logevent.setExtBusReqId(
						workFlowRequest.getOrderId() + "#" + (!"NULL".equalsIgnoreCase(workFlowRequest.getMtn())
								? workFlowRequest.getMtn() : workFlowRequest.getLineItemNumber()) + "#Consumer");
			}
			else {
				logevent.setExtBusReqId(workFlowRequest.getOrderId()+"#"+workFlowRequest.getMtn()+"#Consumer"+pname);
			}
			if(kcontext.getVariable("payload")!=null){
				logevent.setPayload(kcontext.getVariable("payload").toString());
			}
			logevent.setTranNo(workFlowRequest.getOrderId());
			if(kcontext.getVariable("isOrder")!=null || kcontext.getVariable("extBusKey")!=null){
				logevent.setWfPrcssInstId((int) kcontext.getProcessInstance().getParentProcessInstanceId());
			}else {
				logevent.setWfPrcssInstId((int) kcontext.getProcessInstance().getId());
			}
			logevent.setOrdSource(workFlowRequest.getOrderSource());
			logevent.setOrdType(workFlowRequest.getOrderType());
			logevent.setLnItmNo(Integer.parseInt(workFlowRequest.getLineItemNumber()));
			logevent.setWfDeploymentId((String)kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			
			if (PropertyFile.getInstance().isConsumerClustered()) {
				logevent.setDataCenter(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
				logevent.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				logevent.setDataCenter(PropertyFile.getInstance().getDataCenter());
				logevent.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			logevent.setAcctNo(Integer.parseInt(workFlowRequest.getAccountNumber()));
			logevent.setMtn(workFlowRequest.getMtn());
			logevent.setOrdNo(Integer.parseInt(workFlowRequest.getOrderId()));
			logevent.setCustIdNo(Long.parseLong(workFlowRequest.getCustomerId()));
			logevent.setIsBsTransRef(isBsXRef);
			logevent.setIsTranEventLog(isTransLog);
			if(Boolean.parseBoolean(isTransLog)){
				logevent.setEventCd("BOSRWLTERC");
				logevent.setEventDesc("BOS LTE Process has been successfully signalled");
			}
			
			Wrapper<AuditBsXrefTransDomain> businessDomain = new Wrapper<AuditBsXrefTransDomain>();
			businessDomain.setService("bsReqidTranLnEventDao");
			businessDomain.setSync("Y");
			businessDomain.setDomain(logevent);

			kcontext.setVariable(BPMConstants.CONTENT, businessDomain);
			
			ObjectMapper mapper = new ObjectMapper();
			LOGGER.info("Log Event Content for TranLog/BsXref for " +pname+" is : " + mapper.writeValueAsString(businessDomain));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateTranLogBsXrefRequest method for " +pname+". Exception : " + e);
			throw new Exception(e);
		}
	}
}
