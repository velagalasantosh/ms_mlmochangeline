package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ServiceProvider implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceProvider.class);
	
	private String datacenter;
	
	public ServiceProvider() {}
	
	public ServiceProvider(String datacenter) {
		this.datacenter = datacenter;
	}

	/**
	 * This method returns the service url either from eureka if it is enabled or else from project.properties
	 * @param serviceURLName
	 * @param serviceId
	 * @return
	 */
	public String getServiceURL(String serviceURLName, String serviceId)
	{
		LOGGER.info("Entering getServiceURL() method for serviceURLName:"+serviceURLName+" and serviceId:"+serviceId);
		
		String url=null;
		
		url= new EurekaProvider(datacenter).getServerPortFromEureka(serviceURLName, serviceId);
		
		if(url==null) {
		Properties properties = PropertyFile.getInstance().getProjectProperties();
		 url = properties.getProperty(serviceURLName);
		}
		/*if (PropertyFile.getInstance().getIsEurekaEnabled())
		{
			LOGGER.info("Eureka is enabled. Fetching URL from Eureka for Service Id : "+serviceId);
			BpmLoadBalancerClient bpmLoadBalancerClient = new BpmLoadBalancerClient();
			ServerInfo server = null;
			try
			{
				String nativeDataCenter = PropertyFile.getInstance().getDataCenter();
				if (null == nativeDataCenter || "cloud".equalsIgnoreCase(nativeDataCenter))
				{
					LOGGER.info("Data Center value is NULL or is set as cloud. Data Center : " + nativeDataCenter);
					server = bpmLoadBalancerClient.getServerInfo(serviceId);
				}
				else
				{
					LOGGER.info("Fetching values from Eureka for Data Center : " + nativeDataCenter);
					StringBuilder serviceIdBuilder = null;
					if (BPMConstants.SERVICE_ID_BPM.equalsIgnoreCase(serviceId)
							&& PropertyFile.getInstance().isConsumerClustered()) {
						serviceIdBuilder = new StringBuilder(serviceId).append("-")
								.append(PropertyFile.getInstance().getClusterId()).append("-").append(nativeDataCenter);
					} else {
						serviceIdBuilder = new StringBuilder(serviceId).append("-").append(nativeDataCenter);
					}
					LOGGER.info("serviceIdBuilder:"+serviceIdBuilder);
					server = bpmLoadBalancerClient.getServerInfo(serviceIdBuilder.toString());

					if (null == server)
					{
						String neighborDataCenter = properties.getProperty("neighbor.datacenter");
						LOGGER.info("There is no service available for Data Center " + nativeDataCenter + ", Fetching values for neighbour data center : "+neighborDataCenter);
						StringBuilder serviceIdWithNeighbor = null;
						if (BPMConstants.SERVICE_ID_BPM.equalsIgnoreCase(serviceId)
								&& PropertyFile.getInstance().isConsumerClustered()) {
							serviceIdWithNeighbor = new StringBuilder(serviceId).append("-")
									.append(PropertyFile.getInstance().getClusterId()).append("-").append(neighborDataCenter);
						} else {
							serviceIdWithNeighbor = new StringBuilder(serviceId).append("-").append(neighborDataCenter);
						}
						server = bpmLoadBalancerClient.getServerInfo(serviceIdWithNeighbor.toString());
					}
				}
				LOGGER.info("Server details from Eureka, server : " + server);
			}
			catch (Exception exc)
			{
				LOGGER.error("Exception during getting server details..." + exc.getMessage());
			}

			if (null != server)
			{
				try
				{
					URI uri = new URI(urlStr);
					URIBuilder builder = new URIBuilder(uri);
					builder.setHost(server.getHostName());
					builder.setPort(server.getPortNumber());
					urlStr = builder.toString();
					LOGGER.info("URL received from Eureka : "+urlStr);
				}
				catch (URISyntaxException e)
				{
					LOGGER.error("Reached fallback mode to connect default URI defined on consumer.properties", e.getMessage());
				}
			}
		}
		else
		{
			LOGGER.info("Eureka is disabled. Fetching URL from consumer.properties");
		}*/
		/*LOGGER.info("Eureka is disabled. Fetching URL from consumer.properties is >> "+urlStr);
		return urlStr;*/
		
		return url;
	}
	
	public String getBpmServiceURL(String serviceURLName, String serviceId)
	{
		LOGGER.info("Entering getBpmServiceURL() method ...");			
		
		String url=null;
		
		url= new EurekaProvider(datacenter).getServerPortFromEureka(serviceURLName, serviceId);
		
		if(url==null) {
		Properties properties = PropertyFile.getInstance().getProjectProperties();
		 url = properties.getProperty(serviceURLName);
		}
		
		return url;
		/*Properties properties = PropertyFile.getInstance().getProjectProperties();
		String urlStr = properties.getProperty(serviceURLName);*/
		
		/*if (PropertyFile.getInstance().getIsEurekaEnabled())
		{
			LOGGER.info("Eureka is enabled. Fetching URL from Eureka for Service Id : "+serviceId);
			BpmLoadBalancerClient bpmLoadBalancerClient = new BpmLoadBalancerClient();
			ServerInfo server = null;
			try
			{
				server = bpmLoadBalancerClient.getServerInfo(serviceId);
				LOGGER.info("Server details from Eureka, server : " + server);
			}
			catch (Exception exc)
			{
				LOGGER.error("Exception during getting server details..." + exc.getMessage());
			}

			if (null != server)
			{
				try
				{
					URI uri = new URI(urlStr);
					URIBuilder builder = new URIBuilder(uri);
					builder.setHost(server.getHostName());
					builder.setPort(server.getPortNumber());
					urlStr = builder.toString();
					LOGGER.info("URL received from Eureka : "+urlStr);
				}
				catch (URISyntaxException e)
				{
					LOGGER.error("Reached fallback mode to connect default URI defined on consumer.properties", e.getMessage());
				}
			}
		}
		else
		{
			LOGGER.info("Eureka is disabled. Fetching URL from consumer.properties");
		}*/
		
		/*LOGGER.info("Eureka is disabled. Fetching URL from consumer.properties is >> "+urlStr);
		
		return urlStr;*/
	}
}
