package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vzw.orm.billing.domain.BillingBPMWorkFlow;
import com.vzw.orm.billing.datacontract.SimotaOtapaRequest;
import com.vzw.orm.billing.datacontract.SimotaOtapaResponse;
import com.vzw.orm.billing.datacontract.VisionConsumerResponse;
import com.vzw.orm.billing.datacontract.VisionResponseLineItem;
import com.vzw.orm.billing.domain.BillingWorkFlowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class OTAUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(OTAUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * SIMOTA/OTAPA Call
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateOTARequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateOTARequest() method ...");
		try
		{
            WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
            BillingWorkFlowRequest billingWorkFlowRequest = new BillingWorkFlowRequest();
			//converting workflow object to provisioning workflow object
			BeanUtils.copyProperties(billingWorkFlowRequest, workFlowRequest);
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			
			ServiceProvider bpmUtil = new ServiceProvider();
			String otaUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.OTA_URL.toString(), BPMConstants.SERVICE_ID_BILLING);
			LOGGER.info("SIMOTA/OTAPA URL is : " + otaUrl);
			kcontext.setVariable(BPMConstants.URL, otaUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			BillingBPMWorkFlow bpmWorkflow = new BillingBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_OTA_RESPONSE);
			
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			SimotaOtapaRequest otaRequest = new SimotaOtapaRequest();
			otaRequest.setBpmWorkFlowObject(bpmWorkflow);
			otaRequest.setRequestType("OTA");
			otaRequest.setWorkFlowRequestObject(billingWorkFlowRequest);
			kcontext.setVariable(BPMConstants.CONTENT, otaRequest);
			LOGGER.info("OTA Request Content is : " + otaRequest);
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateOTARequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from OTA
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateOTAResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateOTAResponse() method ...");

		boolean flag = false;

		try
		{
			SimotaOtapaResponse otaResponse = (SimotaOtapaResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != otaResponse)
			{
				LOGGER.info("OTA Response is : " + otaResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String deviceProreqStatus = otaResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(deviceProreqStatus))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					LOGGER.info("ErrorCode : " + otaResponse.getStatusCode() + ", Error Source : Billing, Error Details : " + otaResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, otaResponse.getStatusCode(), "Billing",
							otaResponse.getStatusDesc(), null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				LOGGER.info("Response from OTA is null for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL", null,
						BPMConstants.COMP_COUNT);
			}

		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from OTA. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

	/**
	 * This method is used to validate the response received from OTA response
	 * Signal
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateOTASignalResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateOTASignalResponse() method ...");

		boolean flag = false;

		try
		{
			VisionConsumerResponse visionConsumerResponse = (VisionConsumerResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != visionConsumerResponse && visionConsumerResponse.getLineItem() != null && visionConsumerResponse.getLineItem().size() > 0)
			{
				LOGGER.info("SIMOTA/OTAPA Signal Response is : " + visionConsumerResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				// considering only one line item
				VisionResponseLineItem lineItem = visionConsumerResponse.getLineItem().get(0);

				if (BPMConstants.STATUS_CODE_SUCCESS.equalsIgnoreCase(lineItem.getErrCode()))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					// Call Compensation
					LOGGER.info("ErrorCode : " + lineItem.getErrCode() + ", Error Source : Billing , Error Details : " + lineItem.getDescription());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, lineItem.getErrCode(), "Billing", lineItem.getDescription(),
							null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object VisionConsumerResponse  from OTA is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from OTA Signal. Exception : " + e);
			CompensationUtil compUtil = new CompensationUtil();
			compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Onject from Micro Service is either NULL or unexpected",
					null, BPMConstants.COMP_COUNT);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
}