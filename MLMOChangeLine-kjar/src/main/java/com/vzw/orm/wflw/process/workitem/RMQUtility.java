package com.vzw.orm.wflw.process.workitem;


import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.jbpm.process.workitem.core.AbstractLogOrThrowWorkItemHandler;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;


import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.rabbitmq.client.ConnectionFactory;
import com.vzw.orm.wflw.util.BPMConstants;
import com.vzw.orm.wflw.util.PropertyFile;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

/**
 * This class is a Custom WorkItem Handler 
 * which is used to send messages to Rabbit MQ
 * 
 * @author Vinodh Sankuru
 *
 */


public class RMQUtility {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RMQUtility.class);
	private static ConnectionFactory connectionFactory;
	private static Channel channel;
	private static int retryCount;
	
	public RMQUtility(){
		LOGGER.debug("JMS work item handler" );
		this.getClass().getClassLoader();
	}
	
	public static void main(String[] args) {
		executeWorkItem();
	}
	
	public static void executeWorkItem() {
		LOGGER.info("inside executeWorkItem method...");
		long starttime = System.currentTimeMillis();
		try {
			if(connectionFactory==null){
				LOGGER.info("connectionFactory is null, hence creating");
				connectionFactory =  new ConnectionFactory();
				connectionFactory.setHost("mcssrmqwest.ebiz.verizon.com");
				connectionFactory.setPort(5671);
				connectionFactory.setUsername("orbpm");
				connectionFactory.setPassword("orbpm123");
				connectionFactory.setVirtualHost("ORBPM_T2");
			}
			if(channel==null){
				LOGGER.info("channel is null, hence creating");
				channel = getChannel(connectionFactory);
			} else{
				LOGGER.info("Channel:"+channel);
			}
			
			String message = "test message";
			
			String exchange = "BPM_RES_EX";
			String routingKey = "ASYNC_WORKFLOW";
			String type = "x-delayed-message";
			Map<String, Object> args = new HashMap<String, Object>();
			args.put("x-delayed-type", "direct");
			channel.exchangeDeclare(exchange, type, true, false, args);
            channel.basicPublish(exchange, routingKey, null, message.getBytes());
			LOGGER.info("Message Sent!");
			
			Map<String, Object> results = new HashMap<String, Object>();
			//manager.completeWorkItem(workItem.getId(), results);
			retryCount = 0;
			long endtime = System.currentTimeMillis();
			LOGGER.info("Time taken to execute executeWorkItem:" +(endtime-starttime));
		} catch (Exception e) {
			LOGGER.error("Exception while initializing Rabbit MQ SSL : " + e.getMessage());
			LOGGER.info("retryCount:" +retryCount);
			if(retryCount < 3){
				LOGGER.info("making connectionFactory and channel null, as there's some exception");
				connectionFactory = null;
				channel=null;
				retryCount= retryCount+1;
				//executeWorkItem(workItem, manager);
			} else{
				LOGGER.info("Max retry count is reached:" +retryCount);
				connectionFactory = null;
				channel=null;
			}
			e.printStackTrace();
			//handleException(e);
		}
	}
	
	private static Channel getChannel(ConnectionFactory connectionFactory) {
		try {
			Properties props = PropertyFile.getProjectProperties();
			char[] keyPassPhrase = "rmqtest".toString().toCharArray();
			KeyStore ks = KeyStore.getInstance("JKS");
			ks.load(new FileInputStream(System.getProperty(BPMConstants.PROP_FILE_PATH).concat("/").concat(props.get("rabbitmq.ssl.key-store").toString())), keyPassPhrase);
			LOGGER.debug("ks-->" + ks);
			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(ks, keyPassPhrase);
			LOGGER.debug("kmf-->" + kmf);
			
			char[] trustPassPhrase=props.get("rabbitmq.ssl.trust-store-password").toString().toCharArray();
			KeyStore ts = KeyStore.getInstance("JKS");
			ts.load(new FileInputStream(System.getProperty(BPMConstants.PROP_FILE_PATH).concat("/").concat(props.get("rabbitmq.ssl.trust-store").toString())), trustPassPhrase);
			LOGGER.debug("ts-->" + ts);
			TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
			tmf.init(ts);
			LOGGER.debug("tmf-->" + tmf);
			
			SSLContext sslContext = SSLContext.getInstance(props.get("rabbitmq.ssl.algorithm").toString());
			sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
			LOGGER.debug("sslContext-->" + sslContext);
			connectionFactory.useSslProtocol(sslContext);
			LOGGER.debug("connectionFactory-->" + connectionFactory);
			Connection connection = connectionFactory.newConnection();
			LOGGER.debug("connection-->" + connection);
			channel = connection.createChannel();
			LOGGER.info("channel-->" + channel);
		} catch (Exception e) {
			LOGGER.error("Exception inside getChannel method.." + e);
			e.printStackTrace();
		}
		return channel;
	}

	

}
