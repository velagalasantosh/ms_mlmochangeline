package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.accessmanager.domaininteg.AccessManagerRequest;
import com.vzw.orm.accessmanager.domaininteg.AccessManagerResponse;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class AccessManagerOPUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(AccessManagerOPUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * Global Provisioning
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateAccessManagerOPRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateAccessManagerOPRequest() method ...");
		try
		{
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);

			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();

			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			String accessManagerUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.AM_OP_URL.toString(), BPMConstants.SERVICE_ID_AM);
			LOGGER.info("Access Manager URL is : " + accessManagerUrl);
			kcontext.setVariable(BPMConstants.URL, accessManagerUrl);
			AccessManagerRequest amRequest = new AccessManagerRequest();
			amRequest.setOrderId(workFlowRequest.getOrderId());
			amRequest.setLineItemNumber(workFlowRequest.getLineItemNumber());
			amRequest.setGlobalTechnology(workFlowRequest.getGlobalTechnology());
			amRequest.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			amRequest.setCustomerId(workFlowRequest.getCustomerId());
			amRequest.setAccountNumber(workFlowRequest.getAccountNumber());
			amRequest.setBillerId(workFlowRequest.getBillingInstance());
			amRequest.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			amRequest.setMtn(workFlowRequest.getMtn());
			amRequest.setDeviceTechnology(workFlowRequest.getDeviceTechnology());
			amRequest.setProductionParallelInd(workFlowRequest.getProductionParallelInd());
			// amRequest.setCorrelationId(workFlowRequest.getCorrelationId());
			// Jay - with regards to Access Manager changes for new field
			// VZWFB-753
			amRequest.setOrderDate(workFlowRequest.getOrderDateTime());
			kcontext.setVariable(BPMConstants.CONTENT, amRequest);
			ObjectMapper obj = new ObjectMapper();
			LOGGER.info("Access Manager Request Content For OP AM is : " + obj.writeValueAsString(amRequest));

		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateAccessManagerOPRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from Global
	 * Provisioning
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateAccessManagerOPResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateAccessManagerResponse() method ...");

		boolean flag = false;

		try
		{
			AccessManagerResponse amResponse = (AccessManagerResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != amResponse)
			{
				LOGGER.info("Access Manager OP Response is : " + amResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String amResponseStatus = amResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(amResponseStatus))
				{
					LOGGER.info("Access Manager Response is Success.");
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					LOGGER.info("Calling Compensation with statusCode:" + amResponse.getStatusCode() + " and statusDesc :" + amResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, amResponse.getStatusCode(), "BPM", amResponse.getStatusDesc(), null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				LOGGER.info("Response Object from Access Manager OP is NULL for ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId:"
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL", null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from Access Manager OP. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
		LOGGER.info("Exiting validateAccessManagerOPResponse() method ...");
	}

}