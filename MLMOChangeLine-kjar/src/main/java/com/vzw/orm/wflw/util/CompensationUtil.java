package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.process.ProcessContext;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.internal.runtime.manager.RuntimeManagerRegistry;
import org.kie.internal.runtime.manager.context.ProcessInstanceIdContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.common.core.util.BillingSysIdUtil;
import com.vzw.common.core.util.DateTimeUtils;
import com.vzw.common.core.util.InetAddressWrapper;
import com.vzw.orm.provisioning.domain.common.ProvisioningBPMWorkFlow;
import com.vzw.orm.bpmi.domain.objects.consumer.BPMWorkFlow;
import com.vzw.orm.bpmi.domain.objects.consumer.KeyValueObject;
import com.vzw.orm.bpmi.domain.objects.consumer.KieRequestObject;
import com.vzw.orm.provisioning.domain.common.MtasProducerRequest;
import com.vzw.orm.provisioning.domain.common.MtasProducerResponse;
import com.vzw.orm.business.data.update.domain.objects.UpdateLnItemStatCdDomain;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.business.data.update.domain.objects.Wrapper;
import com.vzw.orm.bpm.domain.objects.compensation.CompensationRequest;
import com.vzw.orm.bpm.domain.objects.compensation.CompensationRuleResponse;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class CompensationUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(CompensationUtil.class);

	/**
	 * This method is used to set all the variable values required to execute
	 * Compensation Rule
	 * 
	 * @param kcontext
	 */
	public void initiateCompensationRequest(ProcessContext kcontext)
	{
		LOGGER.info("Entering initiateCompensationRequest() method ...");
		try
		{
			if (null != kcontext.getVariable(BPMConstants.COMP_REQ))
			{
				CompensationRequest compReq = (CompensationRequest) kcontext.getVariable(BPMConstants.COMP_REQ);
				LOGGER.debug("CompensationRequest object value is : " + compReq);

				LOGGER.info("OrderID:" + compReq.getOrderId() + " ProcessInstanceId:"
						+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
						+ kcontext.getProcessInstance().getParentProcessInstanceId());

				CompensationRuleResponse compRuleRes = new CompensationRuleResponse();
				compRuleRes.setErrorCode(compReq.getErrorCode());
				compRuleRes.setErrorSource(compReq.getErrorSource());
				compRuleRes.setErrorDsc(compReq.getErrorDesc());
				compRuleRes.setRetryDelay(300000);
				kcontext.setVariable(BPMConstants.COMP_RES, compRuleRes);
				LOGGER.info("operationType in Before Signal:: "
						+ String.valueOf(kcontext.getVariable(BPMConstants.OPERATION_TYPE)));
				LOGGER.debug("CompensationRuleResponse object value is : " + compRuleRes);
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateCompensationRequest method. Exception : " + e);
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to validate the response received from Compensation
	 * Rule
	 * 
	 * @param kcontext
	 */
	public void validateRuleResponse(ProcessContext kcontext)
	{
		LOGGER.info("Entering validateRuleResponse() method ...");
//		Set<String> autoCorrectionerrorCodeset = null;
		kcontext.setVariable(BPMConstants.AUTO_RETRY, false);
		kcontext.setVariable(BPMConstants.ACTION, "MANUAL");
		kcontext.setVariable(BPMConstants.OPERATION_TYPE, "REFLOW");
		try
		{
			CompensationRuleResponse compRuleResp = (CompensationRuleResponse) kcontext
					.getVariable(BPMConstants.COMP_RES);
			CompensationRequest compReq = (CompensationRequest) kcontext.getVariable(BPMConstants.COMP_REQ);

			LOGGER.debug("CompensationRequest object value is : " + compReq
					+ ", CompensationRuleResponse object value is : " + compRuleResp);

			boolean validResp = (null != compRuleResp ? (null != compReq ? true : false) : false);

			kcontext.setVariable("retryDelay", String.valueOf(compRuleResp.getRetryDelay()) + "ms");
			LOGGER.info("retryDelay -->" + kcontext.getVariable("retryDelay"));
			
			if (validResp)
			{
				if(!"N".equalsIgnoreCase(compRuleResp.getRetryRequired())) {
					if (null != compRuleResp.getActionType() && compRuleResp.getActionType().equalsIgnoreCase("A")
							&& null != compReq.getCompensationCount() && null != compRuleResp.getRetryCount())
					{
						if(compReq.getCompensationCount() < Integer.valueOf(compRuleResp.getRetryCount())) {
							
							kcontext.setVariable(BPMConstants.AUTO_RETRY, true);
							kcontext.setVariable(BPMConstants.ACTION, "AUTO");
							
							if (compRuleResp.getIsAutoCorrect() != null
									&& "Y".equalsIgnoreCase(compRuleResp.getIsAutoCorrect()))
							{
								LOGGER.info("AUTO CORRECTION IS ENABLED");
								kcontext.setVariable("isAutoCorrect",true);
								
								/*autoCorrectionerrorCodeset = ((Set<String>) kcontext.getVariable("errorCodeCollection") != null
										? (Set<String>) kcontext.getVariable("errorCodeCollection") : new HashSet<>());
		
								LOGGER.info("autoCorrectionerrorCodeset-->" + autoCorrectionerrorCodeset);
								if (autoCorrectionerrorCodeset != null && autoCorrectionerrorCodeset
										.contains(compRuleResp.getErrorCode() + "#" + compRuleResp.getErrorSource()))
								{
									kcontext.setVariable(BPMConstants.AUTO_RETRY, false);
								}
								else
								{
									autoCorrectionerrorCodeset
											.add(compRuleResp.getErrorCode() + "#" + compRuleResp.getErrorSource());
									kcontext.setVariable("errorCodeCollection", autoCorrectionerrorCodeset);
		
									kcontext.setVariable("isAutoCorrect",
											("Y".equalsIgnoreCase(compRuleResp.getIsAutoCorrect()) ? true : false));
		
									if (compRuleResp.getDeploymentId() != null && compRuleResp.getProcessDefId() != null)
									{
										kcontext.setVariable("callBpmEH", true);
									}
									else if (compRuleResp.getContextId() != null && compRuleResp.getServiceId() != null
											&& compRuleResp.getRequestPlaceHolder() != null)
									{
										kcontext.setVariable("callBpmEH", false);
									}
									else
									{
										kcontext.setVariable("isAutoCorrect", false);
										LOGGER.info("Auto retrying since there's not sufficient info to call BPM/BPMI EH ");
									}
								}*/
		
							}
							else
							{
								kcontext.setVariable("isAutoCorrect", false);
							}
							
						}else {
							LOGGER.info("Compensation AUTO count has been reached to max retry. Proceeding with MANAUL");
						}
					}
					else
					{
						LOGGER.info("Either Action Type Column in Rule Matrix is null or value is set as M. Action type : "
								+ compRuleResp.getActionType());
					}
				} else{
					LOGGER.info("Retry is not required/configured, hence Skipping the UOW");
					kcontext.setVariable("retryRequired", false);
					kcontext.setVariable(BPMConstants.OPERATION_TYPE, "DISCARD");
				}

			}
			else
			{
				LOGGER.info("CompensationRuleResponse/CompensationRequest object is null. CompensationRuleResponse : "
						+ compRuleResp + ", CompensationRequest : " + compReq);
			}

		}
		catch (Exception e)
		{
			LOGGER.error(
					"Exception while validating response received from Compensation Business Rule. Exception : " + e);
		}

		LOGGER.info("Compensation Workflow automatic retry is : " + kcontext.getVariable(BPMConstants.AUTO_RETRY)
				+ ", So it is " + kcontext.getVariable(BPMConstants.ACTION) + " retry.");
	}

	/**
	 * This method is used to update the table with column value as AUTO\Manual
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateLogEventUpdateRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateLogEventUpdateRequest() method ...");
		try
		{
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			ServiceProvider bpmUtil = new ServiceProvider();
			String url = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.DAO_URL.toString(),
					BPMConstants.SERVICE_ID_BPM_DATA_UPD);
			LOGGER.info("DAO Update URL is : " + url);
			kcontext.setVariable(BPMConstants.URL, url);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			CompensationRequest compReq = (CompensationRequest) kcontext.getVariable(BPMConstants.COMP_REQ);
			LOGGER.debug("CompensationRequest is : " + compReq);
			CompensationRuleResponse compRes = (CompensationRuleResponse) kcontext.getVariable(BPMConstants.COMP_RES);
			LOGGER.debug("CompensationRuleResponse is : " + compRes);
			ObjectMapper mapper = new ObjectMapper();

			UpdateLnItemStatCdDomain updateLnStatus = new UpdateLnItemStatCdDomain();
			updateLnStatus.setLnItmNo(workFlowRequest.getLineItemNumber());
			updateLnStatus.setOrdNo(workFlowRequest.getOrderId());
			updateLnStatus.setMtn(workFlowRequest.getMtn());
			updateLnStatus
					.setBillSysId(String.valueOf(BillingSysIdUtil.getBillSysId(workFlowRequest.getBillingInstance())));

			updateLnStatus.setBucket((String) kcontext.getVariable(BPMConstants.BUCKET_DT));
			updateLnStatus.setEventCd("TRAN_STAT");
			updateLnStatus.setTranNo(workFlowRequest.getOrderId());
			updateLnStatus.setTranLnItmNo(workFlowRequest.getLineItemNumber());
			updateLnStatus.setTranTyp(workFlowRequest.getOrderType());
			updateLnStatus.setTranSubTyp(workFlowRequest.getOrderSource());
			List<KeyValueObject> lineLvlInfo = workFlowRequest.getLineLevelInfo();
			if(lineLvlInfo!=null && lineLvlInfo.size() > 0){ 
				for (KeyValueObject info : lineLvlInfo) {
					if ("lnItmTypCd".equalsIgnoreCase(info.getKey())) {
						LOGGER.info("setting " + info.getKey() + " value:" +info.getValue());
						updateLnStatus.setLnItmTypCd(info.getValue());
					}
				}
			}else{
				updateLnStatus.setLnItmTypCd("M");
			}
			updateLnStatus.setSourceIp(InetAddressWrapper.getCanonicalHostname());
			updateLnStatus.setCompCnt(String.valueOf(compReq.getCompensationCount()));
			if (null != compRes)
			{
				updateLnStatus.setErrSrc(compRes.getErrorSource());
				if (compRes.getErrorDsc() != null)
				{
					updateLnStatus.setErrDesc(compRes.getErrorCode() + "#" + compRes.getErrorDsc());
				}
				else
				{
					updateLnStatus.setErrDesc(compRes.getErrorCode() + "#" + compReq.getErrorDescirption());
				}
				updateLnStatus.setEventDesc(compReq.getErrorDescirption());
				updateLnStatus.setPayload(mapper.writeValueAsString(compReq));
				updateLnStatus.setCompTyp(compRes.getActionType());
			}
			LOGGER.info("line status :" + kcontext.getVariable("lineStatus"));
			if (kcontext.getVariable("lineStatus") != null)
			{

				if ("EB".equalsIgnoreCase((String) kcontext.getVariable("lineStatus")))
				{
					updateLnStatus.setEventDesc("LINE IN ERR BEFORE SWITCH");
				}
				else if ("EA".equalsIgnoreCase((String) kcontext.getVariable("lineStatus")))
				{
					updateLnStatus.setEventDesc("LINE IN ERR AFTER SWITCH");
				}
				else if ("ES".equalsIgnoreCase((String) kcontext.getVariable("lineStatus")))
				{
					updateLnStatus.setEventDesc("ERROR-SIMOTA PROVISIONING");
				}
				else if ("EC".equalsIgnoreCase((String) kcontext.getVariable("lineStatus")))
				{
					updateLnStatus.setEventDesc("ERROR CDMA PROVIS/LTE OK");
				}
				else if ("EN".equalsIgnoreCase((String) kcontext.getVariable("lineStatus")))
				{
					updateLnStatus.setEventDesc("ERROR-NSN PROVIS GATEWAY");
				}
				else if ("E9".equalsIgnoreCase((String) kcontext.getVariable("lineStatus")))
				{
					updateLnStatus.setEventDesc("ERROR IN WIS GATEWAY");
				}
				
				updateLnStatus.setTranStatCd((String) kcontext.getVariable("lineStatus"));
				updateLnStatus.setLnItmStatCd((String) kcontext.getVariable("lineStatus"));
			}
			else
			{
				updateLnStatus.setTranStatCd(compReq.getAppName());
				updateLnStatus.setLnItmStatCd(compReq.getAppName());
			}

			if (kcontext.getVariable("lineStatus") != null) {
				String lineStatus = (String) kcontext.getVariable("lineStatus");
				if (!lineStatus.matches("EB|EA|EC|EN|ES|E9|LC"))
				{
					LOGGER.info("Logging event for evetn_cd:" + lineStatus);
					updateLnStatus.setEventCd(lineStatus);
					updateLnStatus.setEventDesc(updateLnStatus.getErrDesc());
					updateLnStatus.setErrSrc("");
					updateLnStatus.setErrDesc("");
					updateLnStatus.setTranStatCd("");
					updateLnStatus.setLnItmStatCd("NA");
				}
			}

			updateLnStatus.setPayload(mapper.writeValueAsString(workFlowRequest));
			updateLnStatus.setPayloadSrc(kcontext.getProcessInstance().getProcessId());
			updateLnStatus.setPayloadTyp(workFlowRequest.getOrderType());
			updateLnStatus.setWfPrcssInstId(String.valueOf(kcontext.getProcessInstance().getId()));
			updateLnStatus.setUpdateTmstamp(DateTimeUtils.formatCurrentTsBasedOnFormat("yyyy-MM-dd-HH.mm.ss.SSS"));
			updateLnStatus.setInsertDateTime(workFlowRequest.getOrderDateTime());

			updateLnStatus.setCustIdNo(workFlowRequest.getCustomerId());
			updateLnStatus.setAcctNo(workFlowRequest.getAccountNumber());

			if (PropertyFile.getInstance().isConsumerClustered()) {
				updateLnStatus.setDataCenter(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				updateLnStatus.setDataCenter(PropertyFile.getInstance().getDataCenter());
			}

			Wrapper<UpdateLnItemStatCdDomain> businessDomain = new Wrapper<UpdateLnItemStatCdDomain>();
			businessDomain.setService("updateLnItemStatCdDao");
			businessDomain.setServiceMethod("updateLnItmStatCd");
			businessDomain.setDomain(updateLnStatus);
			businessDomain.setSync("Y");

			kcontext.setVariable(BPMConstants.CONTENT, businessDomain);
			LOGGER.info("UpdateLnItemStatCdDomain content for failure status is : "
					+ mapper.writeValueAsString(businessDomain));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateLogEventUpdateRequest method. Exception : " + e);
			LOGGER.error("Setting compensation object to manual retry.");
			kcontext.setVariable(BPMConstants.AUTO_RETRY, false);
			kcontext.setVariable(BPMConstants.ACTION, "MANUAL");
			throw new Exception(e);
		}
	}

	/**
	 * This method adds time delay in the existing compensation process
	 * 
	 * @param kcontext
	 */
	public void threadWait(ProcessContext kcontext)
	{
		LOGGER.info("Entering threadWait() method ...");
		CompensationRuleResponse compRuleResp = (CompensationRuleResponse) kcontext.getVariable(BPMConstants.COMP_RES);
		try
		{
			Long retryDelay = (null == compRuleResp.getRetryDelay() ? 0L : compRuleResp.getRetryDelay());
			LOGGER.info("Adding delay of " + retryDelay + " in Compensation process.");
			Thread.sleep(retryDelay);
		}
		catch (Exception e)
		{
			LOGGER.error("Error in threadWait method. Exception : ", e);
		}
	}

	/**
	 * This method is used to signal back to parent process
	 * 
	 * @param kcontext
	 */
	public void signalBackToParentProcess(ProcessContext kcontext)
	{
		LOGGER.info("Entering signalBackToParentProcess() method ...");
		try
		{
			boolean signalParent = null != kcontext.getVariable("signalParent")
					? (Boolean) kcontext.getVariable("signalParent") : false;

			if (signalParent)
			{
				CompensationRequest compReq = (CompensationRequest) kcontext.getVariable(BPMConstants.COMP_REQ);
				RuntimeManager rm = (RuntimeManager) kcontext.getKieRuntime().getEnvironment().get("RuntimeManager");
				RuntimeEngine runtime = rm
						.getRuntimeEngine(ProcessInstanceIdContext.get(Long.parseLong(compReq.getProcessInstanceId())));
				KieSession ksession = runtime.getKieSession();
				ProcessInstance processInstance = ksession
						.getProcessInstance(Long.parseLong(compReq.getProcessInstanceId()));

				// creating runtime manager and runtime engine for parent
				// process
				rm = RuntimeManagerRegistry.get().getManager(
						runtime.getAuditService().findProcessInstance(processInstance.getId()).getExternalId());
				runtime = rm.getRuntimeEngine(ProcessInstanceIdContext.get(processInstance.getId()));

				runtime.getKieSession().getProcessInstance(processInstance.getId()).signalEvent(compReq.getSignalId(),
						kcontext.getVariable(BPMConstants.OPERATION_TYPE));
				LOGGER.info("Signaled back to the process : " + processInstance.getProcessName()
						+ " with parent process instance id : " + processInstance.getId() + " and signal name is : "
						+ compReq.getSignalId() + "with action : " + kcontext.getVariable(BPMConstants.ACTION));
			}
			else
			{
				LOGGER.info("No need to signal the parent process, since there is no safe point");
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Error in signalBackToParentProcess method. Exception : ", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void validateReflowSignalResponse(ProcessContext kcontext)
	{
		LOGGER.info("Entering in to validateReflowSignalResponse() method..");
		kcontext.setVariable("visionReflow", false);
		kcontext.setVariable("refireRules", false);
		try
		{
			Map<String, Object> resp = (Map<String, Object>) kcontext.getVariable("result");
			LOGGER.info("Signal Data for REFLOW signal is:" + resp);
			if (resp != null && resp.size() > 0)
			{
				if (resp.get("operationType") != null)
				{
					String operationType = resp.get("operationType").toString();

					if ("DISCARD".equalsIgnoreCase(operationType) || "REFLOW".equalsIgnoreCase(operationType)
							|| "REFIRE_RULES".equalsIgnoreCase(operationType))
					{
						kcontext.setVariable("operationType", operationType.toUpperCase());
						kcontext.setVariable("refireRules",
								("REFIRE_RULES".equalsIgnoreCase(operationType) ? true : false));
					}
					else
					{
						LOGGER.info("Unexpected operationType from response. Hence proceeding with REFLOW");
					}
				}
				else
				{
					LOGGER.info("**operationType from response is null. Hence proceeding with REFLOW**");
				}

				if (resp.get("visionReflow") != null && "true".equalsIgnoreCase((String) resp.get("visionReflow")))
				{
					LOGGER.info("Vision Reflow is required, since the data has been modified");
					kcontext.setVariable("visionReflow", true);
				}
				else
				{
					LOGGER.info("Vision Reflow is not required");
				}
			}
			else
			{
				LOGGER.info(
						"Response data for REFLOW signal is NULL. Hence proceeding with REFLOW is required & not invoking visionReflow");
			}
		}
		catch (Exception e)
		{
			LOGGER.info("Exception while validateReflowSignalResponse().. " + e.getMessage()
					+ " Hence proceeding with REFLOW");
		}
	}

	/**
	 * This method is used to set the compensation object
	 * 
	 * @param kcontext
	 * @param errorCode
	 * @param errorSource
	 * @param errorDetails
	 * @param signalId
	 * @param compCount
	 * @return
	 */
	public void setCompensationObject(ProcessContext kcontext, String errorCode, String errorSource,
			String errorDetails, String signalId, String countVarName)
	{
		LOGGER.info("Entering setCompensationObject() method ...");

		int compCount = getCompensationCount(kcontext, countVarName);

		LOGGER.info("Setting compensation object. It receives signal back from compensation at signalId : " + signalId);
		CompensationRequest compReq = new CompensationRequest();
		compReq.setCompensationCount(Integer.valueOf(String.valueOf(compCount)));
		compReq.setErrorCode(errorCode);
		compReq.setErrorSource(errorSource);
		compReq.setErrorDescirption(errorDetails);
		compReq.setSignalId(signalId);
		compReq.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
		compReq.setAppName(kcontext.getProcessInstance().getProcessId());

		WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
		compReq.setBpmCorrId(workFlowRequest.getCorrelationId());
		compReq.setLnItmNo(workFlowRequest.getLineItemNumber());
		compReq.setOrderId(workFlowRequest.getOrderId());
		compReq.setDispatchDt(workFlowRequest.getOrderDateTime());

		LOGGER.info("Compensation Request Content is : " + compReq);

		kcontext.setVariable(BPMConstants.CONTENT, compReq);
		kcontext.setVariable(BPMConstants.COMP_REQ, compReq);
	}

	/**
	 * This method returns the compensation count (number of times compensation
	 * being called).
	 * 
	 * @param kcontext
	 * @param countVarName
	 * @return
	 */
	public int getCompensationCount(ProcessContext kcontext, String countVarName)
	{
		LOGGER.info("Entering getCompensationCount() method ...");

		int compCount = 0;
		if (null != kcontext.getVariable(countVarName))
		{
			compCount = Integer.valueOf(String.valueOf(kcontext.getVariable(countVarName)));
			kcontext.setVariable(countVarName, compCount + 1);
		}
		else
		{
			kcontext.setVariable(countVarName, 1);
		}

		LOGGER.info("Compensation Count is : " + compCount);
		LOGGER.info("kcontext.getVariable(countVarName) -->"
				+ Integer.valueOf(String.valueOf(kcontext.getVariable(countVarName))));

		return compCount;
	}

	public void initiateErrorCorrectionProcess(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering into initiateErrorCorrectionProcess().. method");
		try
		{
			ServiceProvider bpmUtil = new ServiceProvider();
			String URL = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_EH_URL.toString(),
					BPMConstants.SERVICE_ID_BPM);

			CompensationRuleResponse compRes = (CompensationRuleResponse) kcontext.getVariable(BPMConstants.COMP_RES);

			URL = URL.replace("{container_id}", compRes.getDeploymentId()).replace("{process_id}",
					compRes.getProcessDefId());
			LOGGER.info(" kieURL--> " + URL);

			kcontext.setVariable("kieURL", URL);
			kcontext.setVariable("username",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
			kcontext.setVariable("password",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));

			// WorkFlowRequest workFlowRequest = (WorkFlowRequest)
			// kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			BPMWorkFlow bpmWorkflow = new BPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setDeploymentId(
					(String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));

			KieRequestObject obj = new KieRequestObject();
			// obj.setWorkflowRequest(workFlowRequest);
			obj.setBpmWorkFlowObject(bpmWorkflow);
			kcontext.setVariable(BPMConstants.CONTENT, obj);
			ObjectMapper objectMapper = new ObjectMapper();
			LOGGER.info("KieRequestObject is -->" + objectMapper.writeValueAsString(obj));

			// kcontext.setVariable("workflowRequest", workFlowRequest);
			// LOGGER.info(" updated workflowRequest --> " + (WorkFlowRequest)
			// kcontext.getVariable("workflowRequest"));

			// kcontext.setVariable("processInstanceId",
			// String.valueOf(kcontext.getProcessInstance().getId()));

		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateErrorCorrectionProcess method. Exception : ", e);
			throw new Exception(e);
		}
	}

	public void initiateRuleMatrix(ProcessContext kcontext) throws Exception
	{

		try
		{
			LOGGER.info("Inside onEntry of CustomRuleTask");

			ServiceProvider bpmUtil = new ServiceProvider();
			String url = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BRMS_URL.toString(),
					BPMConstants.SERVICE_ID_BPM_BRMS);
			LOGGER.info("BRMS URL is : " + url);
			kcontext.setVariable("brmsURL", url);
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			kcontext.setVariable("username",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
			kcontext.setVariable("password",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));

			CompensationRequest compReq = (CompensationRequest) kcontext.getVariable(BPMConstants.COMP_REQ);
			LOGGER.debug("CompensationRequest object value is : " + compReq);

			CompensationRuleResponse compRuleRes = new CompensationRuleResponse();
			if(BPMConstants.GUI_ERR.equalsIgnoreCase(compReq.getErrorCode())){
				kcontext.setVariable("isGuiErr", true);
			}else{
				kcontext.setVariable("isGuiErr", false);
			}
			kcontext.setVariable("retryRequired", true);
			compRuleRes.setErrorCode(compReq.getErrorCode());
			compRuleRes.setErrorSource(compReq.getErrorSource());
			compRuleRes.setRetryDelay(300000);
			
			kcontext.setVariable(BPMConstants.COMP_RES, compRuleRes);
			/*LOGGER.info("operationType in Before Signal:: "
					+ String.valueOf(kcontext.getVariable(BPMConstants.OPERATION_TYPE)));*/
			LOGGER.info("CompensationRuleResponse object value is : " + compRuleRes);

			String request = "{ \"lookup\": \"ks-comp\", \"commands\": [{ \"insert\" : { \"object\": { \"com.vzw.orm.bpm.domain.objects.compensation.CompensationRuleResponse\": { \"errorCode\": \"{ERROR_CODE}\", \"retryDelay\": 300000, \"errorSource\": \"{ERROR_SOURCE}\" } }, \"out-identifier\": \"compensationRuleResponseObject\" } },{ \"fire-all-rules\":\"\" }] }";

			request = request.replace("{ERROR_CODE}", compReq.getErrorCode()).replace("{ERROR_SOURCE}",
					compReq.getErrorSource());
			LOGGER.info("Request --->" + request);
			kcontext.setVariable("brmsRequest", request);
			LOGGER.info("*********Exiting onEntry in CustomRuleTask*********");

		}
		catch (Exception e)
		{
			LOGGER.error("Error in signalBackToParentProcess method. Exception : ", e);
			throw new Exception(e);
		}
	}

	public void callBPMIErrorCorrectionLogic(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering callBPMIErrorCorrectionLogic() method ...");
		try
		{
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			CompensationRuleResponse compRes = (CompensationRuleResponse) kcontext.getVariable(BPMConstants.COMP_RES);
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String mtasUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.BPMI_ERR_CORR_URL.toString(),
					compRes.getServiceId());
			mtasUrl = mtasUrl.replace("{contextId}", compRes.getContextId());
			LOGGER.info("BPMI Error Correction URL is : " + mtasUrl);
			kcontext.setVariable(BPMConstants.URL, mtasUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			kcontext.setVariable("resultClass", "com.vzw.orm.business.domain.objects.MtasProducerResponse");

			ProvisioningBPMWorkFlow bpmWorkflow = new ProvisioningBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			// bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_MTAS);
			bpmWorkflow.setDeploymentId(
					(String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));

			MtasProducerRequest mtasRequest = new MtasProducerRequest();
			mtasRequest.setBpmWorkFlowObject(bpmWorkflow);
			mtasRequest.setProvisioningRequestType("dataUpdate");
			// mtasRequest.setWorkFlowRequestObject(workFlowRequest);

			kcontext.setVariable(BPMConstants.CONTENT, mtasRequest);

			ObjectMapper obj = new ObjectMapper();
			LOGGER.info("Data Update Content for BPMI is : " + obj.writeValueAsString(mtasRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in callBPMIErrorCorrectionLogic method. Exception : " + e);
			throw new Exception(e);
		}
	}

	public void validateBPMIEHResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateBPMIEHResponse() method ...");
		try
		{
			if (null != kcontext.getVariable(BPMConstants.RESULT))
			{
				MtasProducerResponse mtasProducerResp = (MtasProducerResponse) (kcontext
						.getVariable(BPMConstants.RESULT));
				LOGGER.info("BPMI Error Correction Logic Response is: " + mtasProducerResp);

				if (null != mtasProducerResp && BPMConstants.SUCCESS.equalsIgnoreCase(mtasProducerResp.getStatus()))
				{
					LOGGER.info("BPMI Error Correction Logic responded with SUCCESS");
				}
				else
				{
					LOGGER.error("BPMI Error Correction Logic responded with NULL or Error response");
					throw new Exception("BPMI Error Correction Logic responded with NULL or Error response");
				}
			}
			else
			{
				LOGGER.error("Exception while Invoking BPMI Error Correction Logic");
				throw new Exception("Exception while Invoking BPMI Error Correction Logic");
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Error in validateBPMIEHResponse method. Exception : " + e);
			throw new Exception(e);
		}
	}

	public void checkVisionupdate(ProcessContext kcontext)
	{
		LOGGER.info("Entering checkVisionupdate() method ...");
		kcontext.setVariable("updateVision", false);
		kcontext.setVariable("updateMCS", false);
		try
		{
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			kcontext.setVariable("orderKey", "orderId,"+workFlowRequest.getOrderId());
			
			if (kcontext.getVariable("lineStatus") != null)
			{
				String lineStatus = (String) kcontext.getVariable("lineStatus");
				kcontext.setVariable(BPMConstants.STATUS_CODE, lineStatus);
				
				if (lineStatus.matches("EB|EA|EC|EN|ES|E9"))
				{
					LOGGER.info("skipVisionUpd-->" + kcontext.getVariable("skipVisionUpd"));
					
					if(kcontext.getVariable("skipVisionUpd") == null ||  !(Boolean) kcontext.getVariable("skipVisionUpd"))
					{
						kcontext.setVariable("updateVision", true);
						LOGGER.info("Vision Status Update required for status: " + lineStatus);
					}else 
					{
						LOGGER.info("Skipping Vision Update for EA/EB as this is VRO Acc Level");
					}
					
					if("Y".equalsIgnoreCase(workFlowRequest.getProductionParallelInd())){
						LOGGER.info("OSU for MCS will not be applicable as it is prod parallel, though lineStatus is:"+lineStatus);
					}else {
						if(kcontext.getVariable("skipMCS") == null ||  !(Boolean) kcontext.getVariable("skipMCS")) {
							LOGGER.info("MCS Data Update required, since the status is: " + lineStatus);
							kcontext.setVariable("updateMCS", true);
						}else {
							LOGGER.info("MCS Data Update not required");
						}
					}
					
				} 
				else 
				{
					LOGGER.info("Vision Status Update not required, since the status is: "+lineStatus);
					LOGGER.info("MCS Data Update not required, since the status is: " + lineStatus);
				}
			}
			else
			{
				LOGGER.info("lineStatus is null");
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception in checkVisionUpdate method :" + e);
		}
	}

}