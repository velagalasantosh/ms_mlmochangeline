package com.vzw.orm.wflw.util;

import java.io.Serializable;
import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.common.core.util.DateTimeUtils;
import com.vzw.orm.tnip.domain.TnipBPMWorkFlow;
import com.vzw.orm.tnip.domain.EtniConsumerResponse;
import com.vzw.orm.tnip.domain.EtniRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.tnip.domain.EtniResponse;
import com.vzw.orm.tnip.domain.ExceptionInfo;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Vinodh Sankuru
 *
 */
public class TNIPUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(TNIPUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate in TNIP
	 * cancel_DACM Deactivate MTN If true, trigger the TN-IP Manager
	 * Microservice for DACM; otherwise skip trigger. 
	 * cancel_DACP Deactivate Ported-Out MTN If true, trigger the TN-IP Manager Microservice for DACP;
	 * otherwise skip trigger. 
	 * cancel_DAHA Deactivate Enterprise Home Agent
	 * (EHA) If true, trigger the TN-IP Manager Microservice for DAHA; otherwise skip trigger. 
	 * cancel_PRQ2 Port In Composite for Port In TNC (handles both
	 * port in and cancelation of port in) If true, trigger the TN-IP Manager
	 * Microservice for PRQ2; otherwise, skip trigger. NOTE: Follow the Port In TNC consent == �N� trigger for PRQ2. 
	 * cancel_RHNA 
	 * cancel_RRDN Release
	 * Reserve Port-In MTN If true, trigger the TN-IP Manager Microservice for RRDN; otherwise, skip trigger. 
	 * cancel_RRIP Release Reserve IP Address
	 * cancel_RRSV Release Reserve MDN If true, trigger the TN-IP Manager
	 * Microservice for RRSV; otherwise, skip trigger. 
	 * cancel_RSIM Release SIM Card
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateTNIPRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateTNIPRequest() method ...");
		try
		{
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			String etniUrl;
			ServiceProvider bpmUtil = new ServiceProvider();
			
			if("cancel_DACM".equalsIgnoreCase(workFlowRequest.getCancelContext())) {
				LOGGER.info("TNIP MANAGER call to Deactivate MTN");
				etniUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.TNIP_DACM_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			} else if("cancel_DACP".equalsIgnoreCase(workFlowRequest.getCancelContext())) {
				LOGGER.info("TNIP MANAGER call to Deactivate Ported-Out MTN");
				etniUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.TNIP_DACP_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			} else if("cancel_DAHA".equalsIgnoreCase(workFlowRequest.getCancelContext())) {
				LOGGER.info("TNIP MANAGER call to Deactivate Enterprise Home Agent (EHA)");
				etniUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.TNIP_DAHA_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			} else if("cancel_PRQ2".equalsIgnoreCase(workFlowRequest.getCancelContext())) {
				LOGGER.info("TNIP MANAGER call to Port In Composite for Port In TNC (handles both port in and cancelation of port in)");
				etniUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.PRQ2_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			} else if("cancel_RHNA".equalsIgnoreCase(workFlowRequest.getCancelContext())) {
				LOGGER.info("TNIP MANAGER about call to RHNA");
				etniUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.TNIP_RHNA_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			} else if("cancel_RRDN".equalsIgnoreCase(workFlowRequest.getCancelContext())) {
				LOGGER.info("TNIP MANAGER to call Release Reserve Port-In MTN");
				etniUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.TNIP_RRDN_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			} else if("cancel_RRIP".equalsIgnoreCase(workFlowRequest.getCancelContext())) {
				LOGGER.info("TNIP MANAGER to call Release Reserve IP Address");
				etniUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.TNIP_RRIP_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			} else if("cancel_RRSV".equalsIgnoreCase(workFlowRequest.getCancelContext())) {
				LOGGER.info("TNIP MANAGER to Release Reserve MDN");
				etniUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.TNIP_RRSV_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			}  else if("cancel_RSIM".equalsIgnoreCase(workFlowRequest.getCancelContext())) {
				LOGGER.info("TNIP MANAGER to Release SIM Card");
				etniUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.TNIP_RSIM_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			} else {
				LOGGER.error("Undefined Action For TNIP " +workFlowRequest.getCancelContext());
				throw new Exception("Undefined Action For TNIP");
			}
				
			LOGGER.info("TNIP URL for {}, is : " +workFlowRequest.getCancelContext(), etniUrl);
			kcontext.setVariable(BPMConstants.URL, etniUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			
			TnipBPMWorkFlow bpmWorkflow = new TnipBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_TNIP_CANCEL);
			LOGGER.info("deploymentId-->" + (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			EtniRequest etniRequest = new EtniRequest();
			BeanUtils.copyProperties(etniRequest, workFlowRequest);
			etniRequest.setOrderDueDate(DateTimeUtils.formatCurrentTsBasedOnFormat("yyyy-MM-dd-HH.mm.ss.SSS"));
			etniRequest.setWfReqid(kcontext.getProcessInstance().getProcessId());
			etniRequest.setProcessInsId(String.valueOf(kcontext.getProcessInstance().getId()));
			etniRequest.setBpmWorkFlowObject(bpmWorkflow);
			
			kcontext.setVariable(BPMConstants.CONTENT, etniRequest);
			ObjectMapper mapper = new ObjectMapper();
			LOGGER.info("TNIP Request Content is : " + mapper.writeValueAsString(etniRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateTNIPRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}
	/**
	 * This method is used to validate the response received from ETNI Activation
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateTNIPResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateTNIPResponse() method ...");

		boolean flag = false;

		try
		{
			EtniResponse etniResponse = (EtniResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != etniResponse)
			{
				LOGGER.info("TNIP Response is : " + etniResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = etniResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					/*if(kcontext.getVariable("lineStatus") != null) 
					{
						kcontext.setVariable("preStatus", (String) kcontext.getVariable("lineStatus"));
						LOGGER.info("Prestatus: "+(String) kcontext.getVariable("preStatus"));

					}else{
						kcontext.setVariable("preStatus", "OK");
					}*/
				}
				else
				{
					// Call Compensation
					LOGGER.info("ErrorCode : " + etniResponse.getStatusCode() + ", Error Source : ETNI , Error Details : " + etniResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, etniResponse.getStatusCode(), "ETNI", etniResponse.getStatusDesc(),
							null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object EtniResponseObject from ETNI is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from TNIP. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
	
	/**
	 * This method is used to validate the response received from TNIP CANCEL 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateTNIPCancelSignalResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateTNIPCancelSignalResponse() method ...");

		boolean flag = false;

		try
		{
			EtniConsumerResponse etniSignalResponse = (EtniConsumerResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != etniSignalResponse)
			{
				LOGGER.info("TNIP CANCEL Signal Response is : " + etniSignalResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = etniSignalResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					
					/*if(kcontext.getVariable("lineStatus") != null) {
						if("GUIETNIRFL"
								.equalsIgnoreCase(kcontext.getVariable("lineStatus").toString())){
							LOGGER.info("Prestatus when GUIERR01: "+(String) kcontext.getVariable("preStatus"));
							kcontext.setVariable("preStatus", (String) kcontext.getVariable("preStatus"));
						} else{
							kcontext.setVariable("preStatus", (String) kcontext.getVariable("lineStatus"));
							LOGGER.info("Prestatus: "+(String) kcontext.getVariable("preStatus"));
						}
					}else{
						kcontext.setVariable("preStatus", "LP");
					}*/
					
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					ExceptionInfo expInfo = etniSignalResponse.getExceptionInfo();
					
					CompensationUtil compUtil = new CompensationUtil();
					if(BPMConstants.GUI_ERR.equalsIgnoreCase(expInfo.getErrorCode()))
					{
						kcontext.setVariable("isGuiErr", true);
						LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : GUI , Error Details : " + expInfo.getErrorDeatils());
						compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), BPMConstants.GUI, expInfo.getErrorDeatils(),
								null, BPMConstants.COMP_COUNT);
						
					}else{
						LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : ETNI , Error Details : " + expInfo.getErrorDeatils());
						compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), "ETNI", expInfo.getErrorDeatils(),
							null, BPMConstants.COMP_COUNT);
					}
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object EtniConsumerResponse from TNIP is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception while validating response received from TNIP. Exception : " + e);
			CompensationUtil compUtil = new CompensationUtil();
			compUtil.setCompensationObject(kcontext, "BPMERR03", "BPM", "Exception while validating response received from TNIP",
					null, BPMConstants.COMP_COUNT);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
}