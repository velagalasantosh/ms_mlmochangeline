package com.vzw.orm.wflw.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EurekaProvider {

	private static final Logger logger = LoggerFactory.getLogger(EurekaProvider.class);

	private static EurekaProvider INSTANCE = new EurekaProvider(null);

	private String datacenter;

	private String nativeDataCenter;

	public EurekaProvider(String datacenter) {
		this.datacenter = datacenter;
	}

	private static EurekaProvider getInstance() {
		return INSTANCE;
	}

	public String getServerPortFromEureka(String serviceURLName, String serviceId) {
		logger.info("Enter into getServerPortFromEureka() for serviceURLName:" + serviceURLName + " and serviceId:"
				+ serviceId);

		StringBuilder serviceIdBuilder = null;
		String dc = null;
		String serverResponse = null;

		Properties properties = PropertyFile.getInstance().getProjectProperties();
		String urlStr = properties.getProperty(serviceURLName); // Default URL

		nativeDataCenter = PropertyFile.getInstance().getDataCenter();

		// Either datacenter (or) native data center value has to populate other
		// wise get it from property as a default url. UrlStr variable holds
		// default value from property.

		if (datacenter != null) {
			dc = datacenter;
		} else if (nativeDataCenter != null) {
			dc = nativeDataCenter;
		}

		if (dc != null) {
			if (BPMConstants.SERVICE_ID_BPM.equalsIgnoreCase(serviceId) && PropertyFile.getInstance().isClustered()) {
				serviceIdBuilder = new StringBuilder(serviceId).append("-")
						.append(PropertyFile.getInstance().getClusterId()).append("-").append(dc);
				// For Ex :- BPM-Addline-AWS-EAST1
			} else {
				serviceIdBuilder = new StringBuilder(serviceId).append("-").append(dc);
				// For Ex:- Billing-AWS-EAS1
			}
			// Go lookup for Eureka
			serverResponse = HttpUtlity.getEurekaInfo(serviceIdBuilder.toString());
			// If native DC eureka response is null go for neighbor data center
			if (serverResponse == null) {

				dc = properties.getProperty("neighbor.datacenter");

				if (BPMConstants.SERVICE_ID_BPM.equalsIgnoreCase(serviceId)
						&& PropertyFile.getInstance().isClustered()) {
					serviceIdBuilder = new StringBuilder(serviceId).append("-")
							.append(PropertyFile.getInstance().getClusterId()).append("-").append(dc);
					// For Ex :- BPM-Addline-AWS-EAST1
				} else {
					serviceIdBuilder = new StringBuilder(serviceId).append("-").append(dc);
					// For Ex:- Billing-AWS-EAS1
				}
				// Go lookup for Eureka
				serverResponse = HttpUtlity.getEurekaInfo(serviceIdBuilder.toString());
			}

			if (null != serverResponse) {
				if (2 == serverResponse.split(":").length) {
					try {
						URI uri = new URI(urlStr);
						URIBuilder builder = new URIBuilder(uri);
						builder.setHost(serverResponse.split(":")[0]);
						builder.setPort(Integer.valueOf(serverResponse.split(":")[1]));
						urlStr = builder.toString();
						logger.info("Fully Framed URL : " + urlStr);
					} catch (URISyntaxException e) {
						logger.error("Reached fallback mode to connect default URI defined on project.properties",
								e.getMessage());
					}
				}

			}

			logger.info("Url Retrived from DC :" + dc + " and Url  :" + urlStr);
		}

		return urlStr;

	}
	
	public static void main(String[] args) {

		System.setProperty("prop.path", "/D:/git/ms_properties/ms_kieserver/perf");

		System.setProperty("hostname", "mq-perf-1.orbpm.ebiz.verizon.com");

		EurekaProvider obj = EurekaProvider.getInstance();
	/*	System.out.println(obj.getServerPortFromEureka(PropertyFile.PROP_CONST.SM_CLEANUP_DAO_URL.toString(),
				"MOCKINTERFACE"));
		System.out.println(obj.getServerPortFromEureka(PropertyFile.PROP_CONST.SM_CLEANUP_DAO_URL.toString(),
				"MOCKINTERFACE"));
		System.out.println(obj.getServerPortFromEureka(PropertyFile.PROP_CONST.SM_CLEANUP_DAO_URL.toString(),
				"MOCKINTERFACE"));
		System.out.println(obj.getServerPortFromEureka(PropertyFile.PROP_CONST.SM_CLEANUP_DAO_URL.toString(),
				"MOCKINTERFACE"));*/

	}

}
