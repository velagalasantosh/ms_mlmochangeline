package com.vzw.orm.wflw.util;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpUtlity {

	private static final Logger logger = LoggerFactory.getLogger(HttpUtlity.class);

	private static final CloseableHttpClient httpClient = HttpClients.createDefault();

	public static String getEurekaInfo(String ServiceId) {

		String httpResponse = null;

		try {
			logger.info("hostname:"+System.getProperty("hostname"));
			logger.info("port:"+System.getProperty("server.port"));
			logger.info("ServiceId:"+System.getProperty("ServiceId"));

			HttpGet request = new HttpGet(
					"https://" + System.getProperty("hostname") + ":"+System.getProperty("server.port")+"/service/geteureka/" + ServiceId);
			CloseableHttpResponse response = httpClient.execute(request);
			try {
				// Get HttpResponse Status
				logger.info("Endpoint HTTP Code :" + response.getStatusLine().getStatusCode()); // 200

				HttpEntity entity = response.getEntity();
				if (entity != null && response.getStatusLine().getStatusCode() == 200) {
					httpResponse = EntityUtils.toString(entity);
					logger.info("Response from Service Layer :" + httpResponse);
				} else {
					logger.error("Server Error : " + EntityUtils.toString(entity));
				}

			} finally {
				response.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return httpResponse;

	}
	
	public static String getApplicationProperties() {

		String httpResponse = null;

		try {
			logger.info("App Properties URL is : https://" + System.getProperty("hostname") + ":"+System.getProperty("server.http.port")+"/service/getconfig/appproperties");
			HttpGet request = new HttpGet(
					"https://" + System.getProperty("hostname") + ":"+System.getProperty("server.port")+"/service/getconfig/appproperties");
			CloseableHttpResponse response = httpClient.execute(request);
			try {
				// Get HttpResponse Status
				logger.info("Endpoint HTTP Code :" + response.getStatusLine().getStatusCode()); // 200

				HttpEntity entity = response.getEntity();
				if (entity != null && response.getStatusLine().getStatusCode() == 200) {
					httpResponse = EntityUtils.toString(entity);
					logger.info("Response from Service Layer :" + httpResponse);
				} else {
					logger.error("Server Error : " + EntityUtils.toString(entity));
				}

			} finally {
				response.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return httpResponse;

	}
	
	public static String configRefresh() {
        String httpResponse = null;

        try {
            HttpPost request = new HttpPost("https://" +
                    System.getProperty("hostname") + ":" +
                    System.getProperty("server.port") + "/refresh");
            CloseableHttpResponse response = httpClient.execute(request);

            try {
                // Get HttpResponse Status
                logger.info("Endpoint HTTP Code :" +
                    response.getStatusLine().getStatusCode()); // 200

                HttpEntity entity = response.getEntity();

                if ((entity != null) &&
                        (response.getStatusLine().getStatusCode() == 200)) {
                    httpResponse = EntityUtils.toString(entity);
                    logger.info("Response from Service Layer :" + httpResponse);
                } else {
                    logger.error("Server Error : " +
                        EntityUtils.toString(entity));
                }
            } finally {
                response.close();
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return httpResponse;
    }
}
