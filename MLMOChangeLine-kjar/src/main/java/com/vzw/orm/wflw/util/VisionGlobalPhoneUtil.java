package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.billing.domain.BillingBPMWorkFlow;
import com.vzw.orm.billing.datacontract.VisionSaveRecordResponse;
import com.vzw.orm.billing.datacontract.VisionSaveRecordSignalResponse;
import com.vzw.orm.billing.datacontract.VisionSyncRequest;
import com.vzw.orm.billing.domain.BillingWorkFlowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Vinodh Sankuru
 *
 */
public class VisionGlobalPhoneUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(VisionGlobalPhoneUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * Vision Global Phone Call
	 * 
	 * @param kcontext
	 * @throws Exception 
	 */
	public void initiateVisionGPRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateVisionGPRequest() method ...");
		try
		{
			WorkFlowRequest workflowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			BillingWorkFlowRequest billingWorkFlowRequest = new BillingWorkFlowRequest();
			//converting workflow object to provisioning workflow object
			BeanUtils.copyProperties(billingWorkFlowRequest, workflowRequest);
			
			LOGGER.info("OrderID:" +workflowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String visionGPUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VISION_GP_URL.toString(), BPMConstants.SERVICE_ID_BILLING);
			LOGGER.info("Vision Global Phone Provisioning URL is : " + visionGPUrl);
			kcontext.setVariable(BPMConstants.URL, visionGPUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			BillingBPMWorkFlow bpmWorkflow = new BillingBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_VGP_RESPONSE);
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			VisionSyncRequest visionSyncRequest = new VisionSyncRequest();
			List<BillingWorkFlowRequest> wfRequestList = new ArrayList<BillingWorkFlowRequest>();
			wfRequestList.add(billingWorkFlowRequest);
			
			visionSyncRequest.setWorkFlowRequestObject(wfRequestList);
			visionSyncRequest.setBpmWorkFlowObject(bpmWorkflow);
			kcontext.setVariable(BPMConstants.CONTENT, visionSyncRequest);
			ObjectMapper obj = new ObjectMapper();
			
			LOGGER.info("Vision Global Phone Provisioning Request Content is : " + obj.writeValueAsString(visionSyncRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateVisionGPRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from 
	 * Vision Global Provisioning
	 * 
	 * @param kcontext
	 * @throws Exception 
	 */
	public void validateVisionGPResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateVisionGPResponse() method ...");

		boolean flag = false;

		try
		{
			VisionSaveRecordResponse visionGPResponse = (VisionSaveRecordResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != visionGPResponse)
			{
				LOGGER.info("Vision Global Phone Provisioning Response is : " + visionGPResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = visionGPResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					LOGGER.info("ErrorCode : " + visionGPResponse.getStatusCode() + ", Error Source : VISION , Error Details : " + visionGPResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, visionGPResponse.getStatusCode(), "VISION", visionGPResponse.getStatusDesc(),
							null, BPMConstants.COMP_COUNT);
					
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object VisionSaveRecordResponse from Vision Global Phone Provisioning is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}
			
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from Vision Global Phone Provisioning . Exception : " + e);
			throw new Exception(e);
		}
		
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

	/**
	 * This method is used to validate the response received from 
	 * Vision Global Phone Provisioning Signal
	 * 
	 * @param kcontext
	 * @throws Exception 
	 */
	public void validateVisionGPSignalResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateVisionGPSignalResponse() method ...");

		boolean flag = false;

		try
		{
			VisionSaveRecordSignalResponse visionGPSingalResponse = (VisionSaveRecordSignalResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != visionGPSingalResponse)
			{
				LOGGER.info("Vision Global Phone Provisioning Signal Response is : " + visionGPSingalResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = visionGPSingalResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					// Call Compensation
					LOGGER.info("ErrorCode : " + visionGPSingalResponse.getStatusCode() + ", Error Source : VISION , Error Details : " + visionGPSingalResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, visionGPSingalResponse.getStatusCode(), "VISION", visionGPSingalResponse.getStatusDesc(),
							null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object VisionSaveRecordSignalResponse from Vision Global Phone Provisioning is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from Vision Global Phone Provisioning Signal. Exception : " + e);
			LOGGER.info("Response object VisionSaveRecordSignalResponse from Vision Global Phone Provisioning is NULL.");
			CompensationUtil compUtil = new CompensationUtil();
			compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Onject from Micro Service is either NULL or unexpected",
					null, BPMConstants.COMP_COUNT);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

}