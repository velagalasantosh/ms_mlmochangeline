package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.ConsumerWorkflowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.KeyValueObject;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.business.data.update.domain.objects.FiveGRewireRequest;
import com.vzw.orm.business.data.update.domain.objects.FiveGRewireResponse;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Pratyusha Pooskuru
 *
 */
public class FiveGRewireUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(FiveGRewireUtil.class);

	public void updateDeviceTechnology(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering updateDeviceTechnology() method ...");
		try {
			ServiceProvider bpmUtil = new ServiceProvider();
			String url = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.UPD_DEV_TECH_URL.toString(), BPMConstants.SERVICE_ID_BPM_DATA_UPD);
			LOGGER.info("UPDATE DEVICE TECHNOLOGY URL is : " + url);
			kcontext.setVariable(BPMConstants.URL, url);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			
			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext.getVariable("consumerWorkflowRequest");
			WorkFlowRequest workFlowRequest = new WorkFlowRequest();
			BeanUtils.copyProperties(workFlowRequest, consumerWFRequest.getLstOfWorkflowRequest().get(0));
			kcontext.setVariable(BPMConstants.WORKFLOW_REQ, workFlowRequest);
			
			kcontext.setVariable("orderKey", "orderId," + consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderId());
			com.vzw.orm.business.data.update.domain.objects.FiveGRewireRequest updateWFRequest =  new FiveGRewireRequest();
			List<com.vzw.orm.business.data.update.domain.objects.WorkFlowRequest> lstOfWorkflowRequest
				= new ArrayList<com.vzw.orm.business.data.update.domain.objects.WorkFlowRequest>();
			
			//CONSTRUCTING REQUEST
			BeanUtils.copyProperties(updateWFRequest,consumerWFRequest);
			for (WorkFlowRequest wfRequest : consumerWFRequest.getLstOfWorkflowRequest()) {
				com.vzw.orm.business.data.update.domain.objects.WorkFlowRequest updWfReq = new com.vzw.orm.business.data.update.domain.objects.WorkFlowRequest();
				BeanUtils.copyProperties(updWfReq, wfRequest);
				lstOfWorkflowRequest.add(updWfReq);
			}
			updateWFRequest.setLstOfWorkflowRequest(lstOfWorkflowRequest);
			
			ObjectMapper obj = new ObjectMapper();		
			kcontext.setVariable(BPMConstants.CONTENT, updateWFRequest);
			LOGGER.info("Update Device Technology Content is :" + obj.writeValueAsString(updateWFRequest));	
		} catch (Exception e) {
			kcontext.setVariable("flag", false);
			LOGGER.error("Error in updateDeviceTechnology() method. Exception: " + e);
			throw new Exception("Error in updateDeviceTechnology() method " + e);
		}
	}
		
	public void validateUpdateDevTechResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validateUpdateDevTechResponse() method ...");
		kcontext.setVariable("flag", false);
		try {
			FiveGRewireResponse fivegRewireResp = (FiveGRewireResponse) kcontext.getVariable("fivegRewireResp");
			LOGGER.info("FiveGRewireResponse is:" + fivegRewireResp+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId()
					+ " and ParentProcessInstanceId: " + kcontext.getProcessInstance().getParentProcessInstanceId()
					+ " and OrderId: " + kcontext.getVariable("orderKey"));
			
			//PARSE RESPONSE
			if(fivegRewireResp != null) {
				if("200".equalsIgnoreCase(fivegRewireResp.getStatusCode()) && "Success".equalsIgnoreCase(fivegRewireResp.getStatus())) {
					if (fivegRewireResp.getLstOfWorkflowResponse() != null && fivegRewireResp.getLstOfWorkflowResponse().size() > 0) {
						
						LOGGER.info("Success response from BPM Data Update MS");
						
						// PARSING RESPONSE
						ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext.getVariable("consumerWorkflowRequest");
						ConsumerWorkflowRequest consumerWFRequestTemp = new ConsumerWorkflowRequest();
						BeanUtils.copyProperties(consumerWFRequestTemp,consumerWFRequest);
						List<WorkFlowRequest> lstOfWorkflowRequest
							= new ArrayList<WorkFlowRequest>();
						for (com.vzw.orm.business.data.update.domain.objects.WorkFlowRequest wfReq : fivegRewireResp.getLstOfWorkflowResponse()) {
							WorkFlowRequest workflowRequest = new WorkFlowRequest();
							BeanUtils.copyProperties(workflowRequest,wfReq);
							if(wfReq.getLineLevelInfo()!=null){
								List<KeyValueObject> kvList = new ArrayList<KeyValueObject>();
								for(com.vzw.orm.business.data.update.domain.objects.KeyValueObject kv : wfReq.getLineLevelInfo()) {
									KeyValueObject kvo = new KeyValueObject();
									BeanUtils.copyProperties(kvo, kv);
									kvList.add(kvo);
								}
								workflowRequest.setLineLevelInfo(kvList);
							}
							lstOfWorkflowRequest.add(workflowRequest);
						}
						consumerWFRequestTemp.setLstOfWorkflowRequest(lstOfWorkflowRequest);
						kcontext.setVariable("consumerWorkflowRequest", consumerWFRequestTemp);
						LOGGER.info("Updated ConsumerWorkflowRequest is:" + consumerWFRequestTemp + " for ProcessInstanceId: " + kcontext.getProcessInstance().getId()
								+ " and ParentProcessInstanceId: " + kcontext.getProcessInstance().getParentProcessInstanceId()
								+ " and " + kcontext.getVariable("orderKey"));
						
						
						kcontext.setVariable("flag", true);
						kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					} else {
						LOGGER.info("LineItems in FiveGRewireResponse is NULL");
						throw new Exception("LineItems in FiveGRewireResponse is NULL");
					}
				} else {
					LOGGER.info("ErrorCode : " + fivegRewireResp.getStatusCode() + ", Error Details : " + fivegRewireResp.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, fivegRewireResp.getStatusCode(), "BPM", fivegRewireResp.getStatusDesc(),
							null, BPMConstants.COMP_COUNT);
				}
			} else {
				LOGGER.info("FiveGRewireResponse is NULL or Unexpected");
				throw new Exception("FiveGRewireResponse is NULL or Unexpected");
			}
		} catch (Exception e) {
			LOGGER.error("Entering validateUpdateDevTechResponse() method. Exception: " + e);
			throw new Exception("Entering validateUpdateDevTechResponse() method " + e);
		}
	}
}
