package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.BPMIServiceRequest;
import com.vzw.orm.bpmi.domain.objects.BPMIServiceResponse;
import com.vzw.orm.bpmi.domain.objects.consumer.ConsumerWorkflowRequest;

import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.bpmi.domain.objects.subscriptions.LineItem;
import com.vzw.orm.bpmi.domain.objects.subscriptions.Spo;
import com.vzw.orm.bpmi.domain.objects.vzcare.VendorFulfillmentRequest;
import com.vzw.orm.consumer.domain.objects.ConsumerFeatureListResponse;
import com.vzw.orm.consumer.domain.objects.LineItems;
import com.vzw.orm.consumer.domain.objects.SPOArray;


/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Joshwa J
 *
 */
public class BPMIUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(BPMIUtil.class);

	/**
	 * This method is used to set all the variable values required to retrieve
	 * DMD
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void initiateBPMIRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiateBPMIRequest() method");
		try {
			ServiceProvider bpmUtil = new ServiceProvider();
			String BPMIUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.SM_BPMI_URL.toString(), BPMConstants.SERVICE_ID_BPMI);
			
			LOGGER.info("BPMI URL is: " + BPMIUrl);
			kcontext.setVariable(BPMConstants.URL, BPMIUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			
			ConsumerWorkflowRequest consumerWorkflowReq = (ConsumerWorkflowRequest) kcontext.getVariable("consumerWorkflowRequest");
			WorkFlowRequest workFlowRequest = new WorkFlowRequest();
			BeanUtils.copyProperties(workFlowRequest, consumerWorkflowReq.getLstOfWorkflowRequest().get(0));
			
			BPMIServiceRequest bpmiServiceRequest = new BPMIServiceRequest();
			bpmiServiceRequest.setOrderType("NEW");
			bpmiServiceRequest.setOrderSource("Subscription");
			bpmiServiceRequest.setMessageType("Initiation");
			
			VendorFulfillmentRequest vendorFulfillmentReq = new VendorFulfillmentRequest();
			vendorFulfillmentReq.setAccountNumber(workFlowRequest.getAccountNumber());
			vendorFulfillmentReq.setBillingInstance(workFlowRequest.getBillingInstance());
			vendorFulfillmentReq.setCorrelationId(workFlowRequest.getCorrelationId());
			vendorFulfillmentReq.setCustomerId(workFlowRequest.getCustomerId());
			vendorFulfillmentReq.setVisonOrderNumber(workFlowRequest.getOrderId());
			vendorFulfillmentReq.setBillingInstance(workFlowRequest.getBillingInstance());
			vendorFulfillmentReq.setOrderDateTime(workFlowRequest.getOrderDateTime());
			vendorFulfillmentReq.setRequestHost(workFlowRequest.getRequestHost());
			vendorFulfillmentReq.setClientId("VZW-IVRPOS");
			ConsumerFeatureListResponse consumerFeatureResponse = (ConsumerFeatureListResponse) kcontext.getVariable("consumerFeatureResp");
			LOGGER.info("consumerFeatureResponse before initiating SM: "+consumerFeatureResponse);
			
			List<LineItem> lineItemList = new ArrayList<LineItem>();
			LOGGER.info("ConsumerWFREQ: " + consumerWorkflowReq.getLstOfWorkflowRequest().size());
			if(consumerFeatureResponse!=null&&consumerFeatureResponse.getLineItems()!=null) {
				for(LineItems lineItms: consumerFeatureResponse.getLineItems()) {
					LOGGER.info("LineItems: " );
					LineItem lineItem = new LineItem();
					lineItem.setLineItemNumber(lineItms.getLineItemNumber());
					for(WorkFlowRequest wfr : consumerWorkflowReq.getLstOfWorkflowRequest()) {
						if(lineItms.getLineItemNumber().equalsIgnoreCase(wfr.getLineItemNumber())){
							lineItem.setIdNoP1(wfr.getLosIdNoP1());
							lineItem.setIdNoP2(wfr.getLosIdNoP2());
							lineItem.setMtn(wfr.getMtn());
						}
					}
					
					List<Spo> spoList = new ArrayList<Spo>();
					if(lineItms.getSpoArray()!=null && lineItms.getSpoArray().size() > 0) {
						LOGGER.info("getSpoArray: " );
						for(SPOArray spoArray : lineItms.getSpoArray()) {
							LOGGER.info("getSpoArray lineItem: " );
							Spo spo = new Spo();
							spo.setSpoAction(spoArray.getSpoAction());
							spo.setSpoId(spoArray.getSpoId());
							spo.setSpoUniqueId(spoArray.getSpoUniqueId());
							spo.setProductLevelCode(spoArray.getProductLevelCode());
							spo.setSpoCategoryCodes(spoArray.getSpoCategoryCodes());
							spoList.add(spo);
						}
					}
					
				lineItem.setSpoArray(spoList);
				lineItemList.add(lineItem);
				
			}
		}
			vendorFulfillmentReq.setLineItems(lineItemList);
			bpmiServiceRequest.setTransPayload(vendorFulfillmentReq);
			
			kcontext.setVariable(BPMConstants.CONTENT, bpmiServiceRequest);

			ObjectMapper obj = new ObjectMapper();
			LOGGER.info("BPMI Request Content for SM is : " + obj.writeValueAsString(bpmiServiceRequest));
		} catch (Exception e) {
			LOGGER.error("Error in initiateBPMIRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received for BPMI response
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateBPMIResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validateBPMIResponse() method");
		kcontext.setVariable("flag", false);

		try {
			BPMIServiceResponse bpmiServiceResponse = (BPMIServiceResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != bpmiServiceResponse) {
				LOGGER.info("BPMI Response is : " + bpmiServiceResponse + " for ProcessInstanceId: "
								+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
								+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = bpmiServiceResponse.getStatus();
				if (BPMConstants.BPMI_STATUS_CODE.equalsIgnoreCase(status)) {
					LOGGER.info("BPMI responded with Success");
					LOGGER.info("BPMI BpmInstanceId is:"+bpmiServiceResponse.getBpmInstanceId());
					kcontext.setVariable("flag", true);

				} else {
					LOGGER.debug("ErrorCode : " + bpmiServiceResponse.getCode()
							+ ", Error Source : BPMI , Error Details : " + bpmiServiceResponse.getStatus());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, bpmiServiceResponse.getCode(), "BPMI",
							bpmiServiceResponse.getStatus(), null, BPMConstants.COMP_COUNT);
				}
			} else {
				LOGGER.error("Response object from BPMI is NULL or unexpected");
				throw new Exception("Response object from BPMI is NULL or unexpected");
			}
		} catch (Exception e) {
			LOGGER.error("Exception while validating response received from BPMI. Exception : " + e);
			throw new Exception(e);
		}
	}
}