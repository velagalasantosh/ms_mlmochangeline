package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class is used to load the property file and read these properties in the
 * process model
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class PropertyFile implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(PropertyFile.class);

	private static PropertyFile instance;
	private static Properties _projectProperties = null;
	private static Boolean isEurekaEnabled = null;
	private static Boolean isClustered = null;
	private static String clusterId = null;
	
	private static String connectionTimeOut = null;
	private static String readTimeOut = null;
	private static String dataCenter = null;
	private static String msAccessId = null;
	private static String msAccessCode = null;
	private static Connection connection = null;
	private static String configRefreshResponse = null;
	
	static
	{
		_projectProperties = new Properties();
		
		/*StringBuffer proj_home = new StringBuffer();
		proj_home.append(System.getProperty(BPMConstants.CONSUMER_HOME));
		proj_home.append(File.separator);
		proj_home.append(BPMConstants.FILE_NAME);
		LOGGER.info("proj_home:" + proj_home.toString());
		try (InputStream inputStream = new FileInputStream(FilenameUtils.normalize(proj_home.toString())))
		{
			_projectProperties.load(inputStream);
			LOGGER.info("Properties loaded from static block " + proj_home.toString());
		}
		catch (Exception ex)
		{
			LOGGER.error("Problem occurs when reading file :" + BPMConstants.FILE_NAME + " from path : " + System.getProperty(BPMConstants.CONSUMER_HOME));
			ex.printStackTrace();
		}*/
		
		// convert JSON string to Properties		
		try{
			ObjectMapper mapper = new ObjectMapper();
			_projectProperties = mapper.readValue(HttpUtlity.getApplicationProperties(), Properties.class);
		}catch(Exception e){
			LOGGER.error("Unable to load from config server :",e);			
		}

	}

	private PropertyFile()
	{

	}
	// Variable names declared in property file
	public enum PROP_CONST
	{
		VISION_UPDT_STAT_URL,FD_URL, DMD_URL, SM_BPMI_URL ,VISION_SAVORDER_URL, VISION_SAV_ASYNC_URL, VISION_SAV_CANCEL_URL, CASE_MGMT_URL, PORT_IN_CASE_MGMT_URL, BPMI_ERR_CORR_URL, KIE_SERVER_EH_URL, KIE_SERVER_BASE_URL,KIE_SERVER_SYNC_URL, KIE_SERVER_SYNC_SM_URL, KIE_SERVER_BRMS_URL, KIE_SERVER_BPM_DEP_ID_URL, IP_URL, MTAS_URL, AM_OP_URL, AM_VP_URL, DAO_URL, GP_URL, OTA_URL,ORD_STATUS_MCS_URL,VISION_SAV_URL,VISION_SAV_ACC_LVL_RFLW_URL, VISION_SAV_RFLW_URL,VISION_SEED_DATA_URL, VISION_ORD_REC_URL, VISION_LN_REC_URL, VISION_GP_URL, ETNI_URL, ETNI_CHPP_URL,ETNI_CHANGE_URL,SPO_FEATURES_URL, PTIN_URL, PREQ_URL, PRQ2_URL, TNIP_DACM_URL, TNIP_DACP_URL, TNIP_DAHA_URL, TNIP_RHNA_URL, TNIP_RRDN_URL, TNIP_RRIP_URL, TNIP_RRSV_URL, TNIP_RSIM_URL, LOAN_CREATION_URL, VW_UPD_ORD_URL, VIP_INSRT_CRD_URL, VP_URL, CONTRACT_CREATION_URL,CONTRACT_COMPLETION_URL, VP_IND_URL, VP_ACC_URL, RPA_URL, UPD_DEV_TECH_URL, MIGRATION_URL, VISIBLE_URL, DVS_ACCESS_URL, CONNECTIONTIMEOUT, READTIMEOUT, WAITLIMIT, BILLING_ANA_URL, VBO_STATUS_URL, MS_ACCESS_ID, MS_ACCESS_CODE,VISION_UNGRAFT_UPDATE_URL,MTAS_UNGRAFT, CHUNK_SIZE_VALUE_VISION, CAL_BPMI_URL,IS_SM_BPMI_FLOW_ENABLED,IS_MLMO_CAL_BPMI_FLOW_ENABLED
	};

	public static PropertyFile getInstance()
	{
		if (instance == null) {
			LOGGER.info("instance is null, hence creating it");
			instance = new PropertyFile();
		}
		return instance;
	}

	public static Properties getProjectProperties()
	{
		if (null == _projectProperties || (_projectProperties != null && _projectProperties.isEmpty()))
		{
			LOGGER.info("Entering getProjectProperties() method for first time...");
			_projectProperties = new Properties();
			
			/*StringBuffer proj_home = new StringBuffer();
			proj_home.append(System.getProperty(BPMConstants.CONSUMER_HOME));
			proj_home.append(File.separator);
			proj_home.append(BPMConstants.FILE_NAME);
			LOGGER.info("proj_home:" + proj_home.toString());
			
			try (InputStream inputStream = new FileInputStream(FilenameUtils.normalize(proj_home.toString())))
			{
				_projectProperties.load(inputStream);
				LOGGER.info("Properties loaded from " + proj_home.toString());
			}
			catch (Exception ex)
			{
				LOGGER.error("Problem occurs when reading file :" + BPMConstants.FILE_NAME + " from path : " + System.getProperty(BPMConstants.CONSUMER_HOME));
				ex.printStackTrace();
			}*/
			
			// convert JSON string to Properties
				try{
					ObjectMapper mapper = new ObjectMapper();
					_projectProperties = mapper.readValue(HttpUtlity.getApplicationProperties(), Properties.class);
					//_projectProperties = HttpUtlity.getApplicationProperties();
				}catch(Exception e){
					LOGGER.error("Unable to load from config server :",e);			
				}

		}
		
		return _projectProperties;
	}
	
	/**
	 * This method resets all the properties upon 
	 * initiating ResetProcess
	 * 
	 * @return
	 */
	public static void resetProperties()
	{
		LOGGER.info("inside resetProperties() method..");
		instance = null;
		_projectProperties = null;
		isEurekaEnabled = null;
		isClustered = null;
		clusterId = null;
		connectionTimeOut = null;
		readTimeOut = null;
		dataCenter = null;
		msAccessId = null;
		msAccessCode = null;
		connection = null;
		configRefreshResponse = HttpUtlity.configRefresh();
		LOGGER.info("exiting resetProperties() method..");
	}
	
	/**
	 * This method returns true if eureka is integrated and enabled in the
	 * project
	 * 
	 * @return
	 */
	public Boolean getIsEurekaEnabled()
	{
		if (null == isEurekaEnabled)
		{
			LOGGER.info("isEurekaEnabled paramaeter value is null. Loading it from " + BPMConstants.FILE_NAME +" file.");
			Properties properties = PropertyFile.getProjectProperties();
			isEurekaEnabled = null == properties.getProperty(BPMConstants.EUREKA_FLAG) ? false : Boolean.parseBoolean(properties.getProperty(BPMConstants.EUREKA_FLAG).toString().trim());
		}
		LOGGER.info("Is Eureka enabled : " + isEurekaEnabled);
		return isEurekaEnabled;
	}
	
	/*
	 * public static Connection getPostgresConnection() { if (null == connection) {
	 * LOGGER.info("connection is null. Hence Creating it."); Properties
	 * projectProps = PropertyFile.getProjectProperties(); String url =
	 * PropertyFile.getProjectProperties().get("resource.ds1.driverProperties.url").
	 * toString(); try { connection = DriverManager.getConnection(url,
	 * projectProps.get("resource.ds1.driverProperties.accessid").toString(),
	 * projectProps.get("resource.ds1.driverProperties.accesscode").toString()); }
	 * catch (Exception e) {
	 * LOGGER.error("Exception inside getPostgresConnection() method. Exception:" +
	 * e); e.printStackTrace(); }
	 * 
	 * LOGGER.info("connection" + connection); } return connection; }
	 */

	public static Connection getPostgresConnection() {
		if (null == connection) {
			createDBConnection();
		} else {
			if (!isDbConnected()) {
				try {
					if (connection.isClosed()) {
						LOGGER.info("connection is closed . will be Creating soon.");
					}
					connection = null;
					createDBConnection();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					LOGGER.error("Exception inside getPostgresConnection() method. Exception:" + e);
					e.printStackTrace();
				}
			}
		}
		return connection;
	}

	private static void createDBConnection() {
		LOGGER.info("connection is null. Hence Creating it.");
		Properties projectProps = PropertyFile.getProjectProperties();
		String url = PropertyFile.getProjectProperties().get("resource.ds1.driverProperties.url").toString();
		try {
			connection = DriverManager.getConnection(url,
					projectProps.get("resource.ds1.driverProperties.accessid").toString(),
					projectProps.get("resource.ds1.driverProperties.accesscode").toString());
			LOGGER.info("connection" + connection);
		} catch (Exception e) {
			LOGGER.error("Exception inside createDBConnection() method. Exception:" + e);
			e.printStackTrace();
		}
	}

	private static boolean isDbConnected() {
		final String CHECK_SQL_QUERY = "SELECT 1";
		boolean isConnected = false;
		try {
			PreparedStatement statement = connection.prepareStatement(CHECK_SQL_QUERY);
			LOGGER.error(" statement.getFetchSize() : {} " + statement.getFetchSize());
			isConnected = true;
		} catch (SQLException | NullPointerException e) {
			// handle SQL error here!
			LOGGER.error("Exception inside isDbConnected() method. Exception:" + e);
			e.printStackTrace();
		}
		return isConnected;
	}

	public Boolean isConsumerClustered()
	{
		if (null == isClustered)
		{
			LOGGER.info("isClustered paramaeter value is null. Loading it from " + BPMConstants.FILE_NAME +" file.");
			Properties properties = PropertyFile.getProjectProperties();
			isClustered = null == properties.getProperty(BPMConstants.IS_CLUSTERED) ? false : Boolean.parseBoolean(properties.getProperty(BPMConstants.IS_CLUSTERED).toString().trim());
		}
		LOGGER.info("Is Consumer Clustered : " + isClustered);
		return isClustered;
	}
	
	public String getClusterId()
	{
		if (null == clusterId)
		{
			LOGGER.info("clusterId paramaeter value is null. Loading it from " + BPMConstants.FILE_NAME +" file.");
			Properties properties = PropertyFile.getProjectProperties();
			clusterId = properties.getProperty("consumer.cluster.id").toString();
		}

		return clusterId;
	}
	
	/**
	 * This method reads the value of connectionTimeOut parameter from
	 * consumer.properties file and returns it
	 * 
	 * @return
	 */
	public String getConnectionTimeOut()
	{
		if (null == connectionTimeOut)
		{
			LOGGER.info("connectionTimeOut paramaeter value is null. Loading it from " + BPMConstants.FILE_NAME +" file.");
			Properties properties = PropertyFile.getProjectProperties();
			connectionTimeOut = properties.getProperty(PROP_CONST.CONNECTIONTIMEOUT.toString());
		}

		return connectionTimeOut;
	}

	/**
	 * This method reads the value of readTimeOut parameter from
	 * consumer.properties file and returns it
	 * 
	 * @return
	 */
	public String getReadTimeOut()
	{
		if (null == readTimeOut)
		{
			LOGGER.info("readTimeOut paramaeter value is null. Loading it from " + BPMConstants.FILE_NAME +" file.");
			Properties properties = PropertyFile.getProjectProperties();
			readTimeOut = properties.getProperty(PROP_CONST.READTIMEOUT.toString());
		}

		return readTimeOut;
	}
	
	/**
	 * This method reads the value of dataCenter parameter from
	 * consumer.properties file and returns it
	 * 
	 * @return
	 */
	public String getDataCenter()
	{
		if (null == dataCenter)
		{
			LOGGER.info("dataCenter paramaeter value is null. Loading it from " + BPMConstants.FILE_NAME +" file.");
			Properties properties = PropertyFile.getProjectProperties();
			dataCenter = properties.getProperty("native.datacenter");
		}

		return dataCenter;
	}

	/**
	 * This method reads the value of msAccessId parameter from
	 * consumer.properties file and returns it
	 * 
	 * @return
	 */
	public String getMSAccessId()
	{
		if (null == msAccessId)
		{
			LOGGER.info("msAccessId paramaeter value is null. Loading it from " + BPMConstants.FILE_NAME +" file.");
			Properties properties = PropertyFile.getProjectProperties();
			msAccessId = properties.getProperty(PROP_CONST.MS_ACCESS_ID.toString());
		}

		return msAccessId;
	}
	
	/**
	 * This method reads the value of msAccessCode parameter from
	 * consumer.properties file and returns it
	 * 
	 * @return
	 */
	public String getMSAccessCode()
	{
		if (null == msAccessCode)
		{
			LOGGER.info("msAccessCode paramaeter value is null. Loading it from " + BPMConstants.FILE_NAME +" file.");
			Properties properties = PropertyFile.getProjectProperties();
			msAccessCode = properties.getProperty(PROP_CONST.MS_ACCESS_CODE.toString());
		}

		return msAccessCode;
	}
	
	public Boolean isClustered()
	{
		if (null == isClustered)
		{
			LOGGER.info("isClustered paramaeter value is null. Loading it from config server file.");
			Properties properties = PropertyFile.getInstance().getProjectProperties();
			isClustered = null == properties.getProperty(BPMConstants.IS_CLUSTERED) ? false : Boolean.parseBoolean(properties.getProperty(BPMConstants.IS_CLUSTERED).toString().trim());
		}
		LOGGER.info("Is MLMOChangeLine Clustered : " + isClustered);
		return isClustered;
	}
}
