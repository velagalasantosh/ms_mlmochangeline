package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vzw.orm.tnip.domain.TnipBPMWorkFlow;
import com.vzw.orm.tnip.domain.ExceptionInfo;
import com.vzw.orm.tnip.domain.VipInsertEcreditNoteRequest;
import com.vzw.orm.tnip.domain.VipInsertEcreditNoteResponse;
import com.vzw.orm.tnip.domain.TnipWorkFlowRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class VIPInsertECreditUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(VIPInsertECreditUtil.class);

	/**
	 * This method is used to set all the variable values required to call VIP
	 * Insert E Credit Note MS
	 * 
	 * @param kcontext
	 * @param eventCode
	 * @param eventDesc
	 * @throws Exception
	 */
	public void initiateInsertECreditRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateInsertECreditRequest() method ...");
		try
		{
            WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
            TnipWorkFlowRequest omniWorkFlowRequest = new TnipWorkFlowRequest();
			//converting workflow object to provisioning workflow object
			BeanUtils.copyProperties(omniWorkFlowRequest, workFlowRequest);
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			kcontext.setVariable(BPMConstants.FLAG, false);

			ServiceProvider bpmUtil = new ServiceProvider();
			String vipUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VIP_INSRT_CRD_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			LOGGER.debug("VIP Insert E Credit Note URL is : " + vipUrl);
			kcontext.setVariable(BPMConstants.URL, vipUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			VipInsertEcreditNoteRequest vipCreditRequest = new VipInsertEcreditNoteRequest();

			TnipBPMWorkFlow bpmWorkflow = new TnipBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			vipCreditRequest.setBpmWorkFlowObject(bpmWorkflow);
			vipCreditRequest.setWorkFlowRequestObject(omniWorkFlowRequest);

			kcontext.setVariable(BPMConstants.CONTENT, vipCreditRequest);
			ObjectMapper obj = new ObjectMapper();
			LOGGER.info("VIP Insert E Credit Note Request Content is : " + obj.writeValueAsString(vipCreditRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateInsertECreditRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from VIP Insert E
	 * Credit Note Request
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateInsertECreditResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateInsertECreditResponse() method ...");

		boolean flag = false;
		try
		{
			VipInsertEcreditNoteResponse vipCreditResponse = (VipInsertEcreditNoteResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != vipCreditResponse)
			{
				LOGGER.info("VIP Insert E Credit Note Response is : " + vipCreditResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = vipCreditResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					// Call Compensation
					ExceptionInfo expInfo = vipCreditResponse.getExceptionInfo();
					LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : " + expInfo.getErrorSource() + ", Error Details : " + expInfo.getErrorDeatils());
					if("99".equalsIgnoreCase(expInfo.getErrorCode()))
					{
						CompensationUtil compUtil = new CompensationUtil();
						compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), "OMNI", expInfo.getErrorDeatils(),
								null, BPMConstants.COMP_COUNT);
					}
					else
					{
						flag = true;
						kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					}
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object VipInsertEcreditNoteResponse from VIP Insert E Credit Note API is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}

		}
		catch (Exception e)
		{
			LOGGER.error("Exception while validating response received from VIP Insert E Credit Note. Exception : " + e);
			throw new Exception(e);
		}

		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

}