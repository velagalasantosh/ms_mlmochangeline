package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.common.core.util.BillingSysIdUtil;
import com.vzw.common.core.util.DateTimeUtils;
import com.vzw.common.core.util.StringUtils;
import com.vzw.orm.billing.datacontract.VisionSaveRecordResponse;
import com.vzw.orm.billing.datacontract.VisionSyncRequest;
import com.vzw.orm.billing.domain.BillingBPMWorkFlow;
import com.vzw.orm.billing.domain.BillingWorkFlowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.ConsumerWorkflowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.KeyValueObject;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.business.data.update.domain.objects.FetchOrderRefDetails;
import com.vzw.orm.provisioning.domain.common.MdnStatus;
import com.vzw.orm.provisioning.domain.common.MtasConsumerRequest;
import com.vzw.orm.provisioning.domain.common.MtasConsumerRequestGrafting;
import com.vzw.orm.provisioning.domain.common.MtasGraftingProducerRequest;
import com.vzw.orm.provisioning.domain.common.MtasProducerRequest;
import com.vzw.orm.provisioning.domain.common.MtasProducerResponse;
import com.vzw.orm.provisioning.domain.common.ProvisioningBPMWorkFlow;
import com.vzw.orm.provisioning.domain.common.ProvisioningWorkFlowRequest;
import com.vzw.orm.visible.domain.common.VisibleBPMWorkFlow;
import com.vzw.orm.business.data.update.domain.objects.Wrapper;
import com.vzw.orm.business.data.update.domain.objects.PnoResponseStatus;
import com.vzw.orm.business.data.update.domain.objects.ResponseDomain;
import com.vzw.orm.business.data.update.domain.objects.ExceptionInfo;


public class NumberShareUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(NumberShareUtil.class);

	public void splitBranchlines(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering splitBranchlines() method ...");
		try {
			LOGGER.info("isNSBranchLinePresent -->" + kcontext.getVariable("isNSBranchLinePresent"));
			Set<String> filteredMtnListBranch = new HashSet<String>();
			List<WorkFlowRequest> lstOfWorkflowRequestChangeLineForLineLevelFinalBranch = new ArrayList<>();
			List<WorkFlowRequest> lstOfWorkflowRequestAddlineForLineLevelFinalBranch = new ArrayList<>();
			Set<String> filteredMtnListtAll = (Set<String>) kcontext.getVariable("filteredMtnList");
			List<WorkFlowRequest> lstOfWorkflowRequestChangeLineForLineLevelFinalAll = (List<WorkFlowRequest>) kcontext
					.getVariable("filteredLstOfChangeLineWorkflow");
			List<WorkFlowRequest> lstOfWorkflowRequestAddlineForLineLevelFinalAll = (List<WorkFlowRequest>) kcontext
					.getVariable("filteredLstOfAddLineWorkflow");
			Set<String> collectedMtnsAll = (Set<String>) kcontext.getVariable("mtnList");
			// Number shared - remove branch lines from work flow request line
			// level and add into branch list\
			LOGGER.info("isBranchLinePending -->" + kcontext.getVariable("isBranchLinePending"));
			List<WorkFlowRequest> Copylst = new ArrayList<>();
			Copylst.addAll(lstOfWorkflowRequestChangeLineForLineLevelFinalAll);

			if (null ==kcontext.getVariable("isBranchLinePending")) {

				LOGGER.info("change line sizes  {} , add line sizes  {} " ,lstOfWorkflowRequestChangeLineForLineLevelFinalAll.size(),lstOfWorkflowRequestAddlineForLineLevelFinalAll.size());	
				for (WorkFlowRequest wfrtemp : Copylst) {
					List<KeyValueObject> lstlineLevelInfo = wfrtemp.getLineLevelInfo();
					if (lstlineLevelInfo != null && lstlineLevelInfo.size() > 0) {
						for (KeyValueObject keyValueObject : lstlineLevelInfo) {
							if (!StringUtils.isEmpty(keyValueObject.getKey())
									&& !StringUtils.isEmpty(keyValueObject.getValue())) {
								if (keyValueObject.getKey().equalsIgnoreCase("isBranchLine")
										&& Boolean.parseBoolean(keyValueObject.getValue().toString())) {
									lstOfWorkflowRequestChangeLineForLineLevelFinalBranch.add(wfrtemp);
									lstOfWorkflowRequestChangeLineForLineLevelFinalAll.remove(wfrtemp);
									LOGGER.info(" remove Change line due to branch inditicator present  {}" , wfrtemp.getLineItemNumber());
								}
							}
						}
					}

				}
				for (WorkFlowRequest wfr : lstOfWorkflowRequestChangeLineForLineLevelFinalBranch) {
					filteredMtnListtAll.remove(wfr.getMtn());
					filteredMtnListBranch.add(wfr.getMtn());
				}
				Copylst = new ArrayList<>();
				Copylst.addAll(lstOfWorkflowRequestAddlineForLineLevelFinalAll);
				for (WorkFlowRequest wfrtemp : Copylst) {
					List<KeyValueObject> lstlineLevelInfo = wfrtemp.getLineLevelInfo();
					if (lstlineLevelInfo != null && lstlineLevelInfo.size() > 0) {
						for (KeyValueObject keyValueObject : lstlineLevelInfo) {
							if (!StringUtils.isEmpty(keyValueObject.getKey())
									&& !StringUtils.isEmpty(keyValueObject.getValue())) {
								if (keyValueObject.getKey().equalsIgnoreCase("isBranchLine")
										&& Boolean.parseBoolean(keyValueObject.getValue().toString())) {
									lstOfWorkflowRequestAddlineForLineLevelFinalBranch.add(wfrtemp);
									lstOfWorkflowRequestAddlineForLineLevelFinalAll.remove(wfrtemp);
									LOGGER.info(" remove Add line due to branch inditicator present  {}" , wfrtemp.getLineItemNumber());
								}
							}
						}
					}

				}
				for (WorkFlowRequest wfr : lstOfWorkflowRequestAddlineForLineLevelFinalBranch) {
					filteredMtnListtAll.remove(wfr.getMtn());
					filteredMtnListBranch.add(wfr.getMtn());

				}
				LOGGER.info("after remove change line sizes  {} , add line sizes  {} " ,lstOfWorkflowRequestChangeLineForLineLevelFinalAll.size(),lstOfWorkflowRequestAddlineForLineLevelFinalAll.size());
				kcontext.setVariable("filteredBranchMtnList", filteredMtnListBranch);
				kcontext.setVariable("filteredChangelineBranchLstOfWorkflow", lstOfWorkflowRequestChangeLineForLineLevelFinalBranch);
				kcontext.setVariable("filteredAddlineBranchLstOfWorkflow", lstOfWorkflowRequestAddlineForLineLevelFinalBranch);
				kcontext.setVariable("mtnList", filteredMtnListtAll);
				kcontext.setVariable("isBranchLinePending", true);
			} else if (Boolean.parseBoolean(String.valueOf(kcontext.getVariable("isBranchLinePending")))) {

				LOGGER.info("after trunk lines completed , mtn size {} ");
				kcontext.setVariable("filteredMtnList", kcontext.getVariable("filteredBranchMtnList"));
				kcontext.setVariable("filteredLstOfChangeLineWorkflow", kcontext.getVariable("filteredChangelineBranchLstOfWorkflow"));
				kcontext.setVariable("filteredLstOfAddLineWorkflow", kcontext.getVariable("filteredAddlineBranchLstOfWorkflow"));
				kcontext.setVariable("mtnList", kcontext.getVariable("filteredBranchMtnList"));
				kcontext.setVariable("isBranchLinePending", false);

			}


		} catch (Exception e) {
			LOGGER.info("Exception in Number Share Util --> splitBranchlines()" + e);
		}
	}

	/**
	 * This method is used to set all the variable values required to update for
	 * Ungrafting Vision Seeding Data
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void updateUngraftingStatustoVision(ProcessContext kcontext, String status) throws Exception {
		LOGGER.info("Entering updateUngraftingStatustoVision() method");

		try {
			LOGGER.info("Status value is" + status);
			LOGGER.info("Kcontext value is" + kcontext);
			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
					.getVariable(BPMConstants.CONSUMERWFLREQUEST);

			LOGGER.info("OrderID:" + consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderId()
					+ " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			kcontext.setVariable(BPMConstants.WORKFLOW_REQ, consumerWFRequest.getLstOfWorkflowRequest().get(0));
			WorkFlowRequest workFlowRequest =  consumerWFRequest.getLstOfWorkflowRequest().get(0);
			long parentProcessInstanceId = kcontext.getProcessInstance().getParentProcessInstanceId();
			long processInstanceId = kcontext.getProcessInstance().getId();
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:" + processInstanceId
					+ " ParentProcessInstanceId:" + parentProcessInstanceId);
			ServiceProvider bpmUtil = new ServiceProvider();
			String billingDataUrl = bpmUtil.getServiceURL(
					PropertyFile.PROP_CONST.VISION_UNGRAFT_UPDATE_URL.toString(), BPMConstants.SERVICE_ID_BILLING);

			LOGGER.info("Ungrafting Data URL is : " + billingDataUrl);
			kcontext.setVariable(BPMConstants.URL, billingDataUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			List<BillingWorkFlowRequest> wfList = new ArrayList<BillingWorkFlowRequest>();

			if (consumerWFRequest.getLstOfWorkflowRequest().size() > 0) {
				LOGGER.info("consumerWorkflowRequest value is not null");
				LOGGER.info(consumerWFRequest.getLstOfWorkflowRequest().size() + "size of consumerWorkFlow LstOfWorkflowRequest");
				//setting workflowRequest as this is mandatory if any failure occurs
				kcontext.setVariable(BPMConstants.WORKFLOW_REQ, consumerWFRequest.getLstOfWorkflowRequest().get(0));
				boolean flagCheck = false;
				for (WorkFlowRequest lstOfWorkflowRequest : consumerWFRequest.getLstOfWorkflowRequest()) {
					List<KeyValueObject> lstlineLevelInfo = lstOfWorkflowRequest.getLineLevelInfo();

					if (lstlineLevelInfo != null && lstlineLevelInfo.size() > 0) {
						LOGGER.info("Inside main loop for lstOfWorkflowRequest");
						boolean isActivepair = false;
						boolean isNsSfoFound = false;
						for (Iterator<KeyValueObject> iterator = lstlineLevelInfo.iterator(); iterator.hasNext();  ) {
							KeyValueObject keyValueObject = iterator.next();

							if (!StringUtils.isEmpty(keyValueObject.getKey())
									&& !StringUtils.isEmpty(keyValueObject.getValue())) {
								if (keyValueObject.getKey().equalsIgnoreCase("activePair")
										&& Boolean.parseBoolean(keyValueObject.getValue().toString())) {
									isActivepair = true;
								} else if (keyValueObject.getKey().equalsIgnoreCase("isNsSfoFound")
										&& Boolean.parseBoolean(keyValueObject.getValue().toString())) {
									isNsSfoFound = true;
								}
								else if (keyValueObject.getKey().equalsIgnoreCase("ungraftStatus")
										&& keyValueObject.getValue()!= null){

									iterator.remove();
								}
							}
						}
						List<KeyValueObject> cpylstlineLevelInfo = new ArrayList<KeyValueObject>();
						cpylstlineLevelInfo.addAll(lstlineLevelInfo);
						for (KeyValueObject keyValueObject : cpylstlineLevelInfo) {

							if (Integer.parseInt(lstOfWorkflowRequest.getLineItemNumber()) < 100
									&& keyValueObject.getKey().equalsIgnoreCase("numberShareIndicator")
									&& lstOfWorkflowRequest.getOrderSource().toString().equalsIgnoreCase("Modify") && !Boolean.parseBoolean(lstOfWorkflowRequest.getActivateMtn())
									&& (keyValueObject.getValue().equalsIgnoreCase("U")
											|| keyValueObject.getValue().equalsIgnoreCase("D") 
											|| keyValueObject.getValue().equalsIgnoreCase("T")
											|| keyValueObject.getValue().equalsIgnoreCase("X")
											|| keyValueObject.getValue().equalsIgnoreCase("G")
											|| keyValueObject.getValue().equalsIgnoreCase("B"))){

								if(isActivepair && isNsSfoFound && Boolean.parseBoolean(lstOfWorkflowRequest.getChangePlanFeatures().toString()) ==true) {
									KeyValueObject keyvalue = new KeyValueObject();
									keyvalue.setKey("ungraftStatus");
									keyvalue.setValue(status);

									BillingWorkFlowRequest billingWorkFlowRequest = new BillingWorkFlowRequest();
									lstOfWorkflowRequest.getLineLevelInfo().add(keyvalue);
									BeanUtils.copyProperties(billingWorkFlowRequest, lstOfWorkflowRequest);
									wfList.add(billingWorkFlowRequest);
									LOGGER.info("Setting wflist item first parameter");

								}else if ( isActivepair && (Boolean.parseBoolean(lstOfWorkflowRequest.getChangeDevice().toString()) ==true  ||
										Boolean.parseBoolean(lstOfWorkflowRequest.getChangeMtn().toString()) ==true)) {
									KeyValueObject keyvalue = new KeyValueObject();
									keyvalue.setKey("ungraftStatus");
									keyvalue.setValue(status);

									BillingWorkFlowRequest billingWorkFlowRequest = new BillingWorkFlowRequest();
									lstOfWorkflowRequest.getLineLevelInfo().add(keyvalue);
									BeanUtils.copyProperties(billingWorkFlowRequest, lstOfWorkflowRequest);
									wfList.add(billingWorkFlowRequest);
									LOGGER.info("Setting wflist item second parameter");
								}

							}
						}
					}
				}

			}

			BillingBPMWorkFlow bpmWorkflow = new BillingBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(processInstanceId));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setDeploymentId(
					(String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(
						PropertyFile.getInstance().getClusterId() + "-" + PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}

			VisionSyncRequest visionSyncRequest = new VisionSyncRequest();
			visionSyncRequest.setWorkFlowRequestObject(wfList);
			visionSyncRequest.setBpmWorkFlowObject(bpmWorkflow);
			kcontext.setVariable(BPMConstants.CONTENT, visionSyncRequest);

			ObjectMapper obj = new ObjectMapper();
			LOGGER.info(obj.writeValueAsString(wfList));
			LOGGER.info("Ungraft Request Content is : " + obj.writeValueAsString(visionSyncRequest));
		} catch (Exception e) {
			LOGGER.error("Error in initiate Ungrafting status to vision Request method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from Billing for
	 * Ungrafting Vision Seed Data Request
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateUngraftingResponse(ProcessContext kcontext) throws Exception {
		LOGGER.debug("Entering validateUngraftingResponse() method ");
		kcontext.setVariable("flag", false);
		try {
			VisionSaveRecordResponse visionSeedDataResponse = (VisionSaveRecordResponse) (kcontext
					.getVariable(BPMConstants.RESULT));

			if (null != visionSeedDataResponse) {
				LOGGER.info("validateUngraftingResponse is : " + visionSeedDataResponse + " for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = visionSeedDataResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
					LOGGER.info("validateUngraftingResponse was successful");
					kcontext.setVariable("flag", true);
				} else {
					LOGGER.debug("ErrorCode : " + visionSeedDataResponse.getStatusCode()
					+ ", Error Source : VISION , Error Details : " + visionSeedDataResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, visionSeedDataResponse.getStatusCode(), "VISION",
							visionSeedDataResponse.getStatusDesc(), null, BPMConstants.COMP_COUNT);
				}
			} else {
				LOGGER.error(
						"Response object VisionSaveRecordResponse for Vision Number Share request is NULL or unexpected");
				throw new Exception(
						"Response object VisionSaveRecordResponse for Vision Number Share is NULL or unexpected");
			}
		} catch (Exception e) {
			LOGGER.error(
					"Exception while validating response received for Ungrafting Vision request. Exception : " + e);
			throw new Exception(e);
		}
	}

	public void initiateMTASRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiateMTASRequest() method for Ungraft");

		try {

			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
					.getVariable(BPMConstants.CONSUMERWFLREQUEST);
			LOGGER.info("consumerWorkflowRequest value is-->" + consumerWFRequest);
			List<String> Ungraftbranchlines = new ArrayList<String>();
			List<ProvisioningWorkFlowRequest> lstProvisioningWorkFlowRequest = new ArrayList<ProvisioningWorkFlowRequest>();

			if (consumerWFRequest.getLstOfWorkflowRequest().size() > 0) {
				LOGGER.info("consumerWorkflowRequest value is not null");
				//setting workflowrequest as this is mandatory if any failure occurs
				kcontext.setVariable(BPMConstants.WORKFLOW_REQ, consumerWFRequest.getLstOfWorkflowRequest().get(0));
				boolean flagCheck = false;
				for (WorkFlowRequest lstOfWorkflowRequest : consumerWFRequest.getLstOfWorkflowRequest()) {
					List<KeyValueObject> lstlineLevelInfo = lstOfWorkflowRequest.getLineLevelInfo();
					if (lstlineLevelInfo != null && lstlineLevelInfo.size() > 0) {

						boolean isActivepair = false;
						boolean isNsSfoFound = false;
						for (KeyValueObject keyValueObject : lstlineLevelInfo) {

							if (!StringUtils.isEmpty(keyValueObject.getKey())
									&& !StringUtils.isEmpty(keyValueObject.getValue())) {
								if (keyValueObject.getKey().equalsIgnoreCase("activePair")
										&& Boolean.parseBoolean(keyValueObject.getValue().toString())) {
									isActivepair = true;
								} else if (keyValueObject.getKey().equalsIgnoreCase("isNsSfoFound")
										&& Boolean.parseBoolean(keyValueObject.getValue().toString())) {
									isNsSfoFound = true;
								}
							}

						}
						for (KeyValueObject keyValueObject : lstlineLevelInfo) {
							ProvisioningWorkFlowRequest provisioningWorkFlowRequest = new ProvisioningWorkFlowRequest();
							BeanUtils.copyProperties(provisioningWorkFlowRequest, lstOfWorkflowRequest);
							if (Integer.parseInt(lstOfWorkflowRequest.getLineItemNumber()) < 100
									&& keyValueObject.getKey().equalsIgnoreCase("numberShareIndicator")
									&& (keyValueObject.getValue().equalsIgnoreCase("D")
											|| keyValueObject.getValue().equalsIgnoreCase("T"))
									&& lstOfWorkflowRequest.getOrderSource().toString().equalsIgnoreCase("Modify") && !Boolean.parseBoolean(lstOfWorkflowRequest.getActivateMtn())) {
								LOGGER.info("Trunk line present so ingore branch line {} ",
										provisioningWorkFlowRequest.getMtn());

								if (isActivepair && isNsSfoFound && Boolean.parseBoolean(
										provisioningWorkFlowRequest.getChangePlanFeatures().toString()) == true) {

									Ungraftbranchlines.clear();
									Ungraftbranchlines.add(provisioningWorkFlowRequest.getMtn());
									lstProvisioningWorkFlowRequest.clear(); 
									lstProvisioningWorkFlowRequest.add(provisioningWorkFlowRequest);
									break;
								}
								else if ( isActivepair && (Boolean.parseBoolean(provisioningWorkFlowRequest.getChangeDevice().toString()) ==true  ||
										Boolean.parseBoolean(provisioningWorkFlowRequest.getChangeMtn().toString()) ==true)) {

									Ungraftbranchlines.clear();
									Ungraftbranchlines.add(provisioningWorkFlowRequest.getMtn());
									lstProvisioningWorkFlowRequest.clear(); 
									lstProvisioningWorkFlowRequest.add(provisioningWorkFlowRequest);
									break;
								}
							} else if (Integer.parseInt(lstOfWorkflowRequest.getLineItemNumber()) < 100
									&& keyValueObject.getKey().equalsIgnoreCase("numberShareIndicator")
									&& (keyValueObject.getValue().equalsIgnoreCase("U")
											|| keyValueObject.getValue().equalsIgnoreCase("X")
											|| keyValueObject.getValue().equalsIgnoreCase("G")
											|| keyValueObject.getValue().equalsIgnoreCase("B"))
									&& lstOfWorkflowRequest.getOrderSource().toString().equalsIgnoreCase("Modify") && !Boolean.parseBoolean(lstOfWorkflowRequest.getActivateMtn())) {
								BeanUtils.copyProperties(provisioningWorkFlowRequest, lstOfWorkflowRequest);
								LOGGER.info("Branch line ungraft present : {} ", provisioningWorkFlowRequest.getMtn());

								if (isActivepair && isNsSfoFound && Boolean.parseBoolean(
										provisioningWorkFlowRequest.getChangePlanFeatures().toString()) == true) {
									Ungraftbranchlines.add(provisioningWorkFlowRequest.getMtn());
									lstProvisioningWorkFlowRequest.add(provisioningWorkFlowRequest);
								}else if ( isActivepair && (Boolean.parseBoolean(provisioningWorkFlowRequest.getChangeDevice().toString()) ==true  ||
										Boolean.parseBoolean(provisioningWorkFlowRequest.getChangeMtn().toString()) ==true)) {
									Ungraftbranchlines.add(provisioningWorkFlowRequest.getMtn());
									lstProvisioningWorkFlowRequest.add(provisioningWorkFlowRequest);
								}

							}
						}

					}
					LOGGER.info("Break the loop and Branch Lines Data: " + Ungraftbranchlines);
				}

			}

			LOGGER.info("OrderID:" + lstProvisioningWorkFlowRequest.get(0).getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			kcontext.setVariable(BPMConstants.FLAG, false);
			ServiceProvider bpmUtil = new ServiceProvider();
			String mtasUrlUngraft = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.MTAS_UNGRAFT.toString(),
					BPMConstants.SERVICE_ID_PROV);

			LOGGER.info("MTAS URL for Ungraft is : " + mtasUrlUngraft);
			kcontext.setVariable(BPMConstants.URL, mtasUrlUngraft);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			kcontext.setVariable("ungraftBranchlines", Ungraftbranchlines);

			ProvisioningBPMWorkFlow bpmWorkflow = new ProvisioningBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setSignalEventId("");

			LOGGER.info("deploymentId-->"
					+ (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			bpmWorkflow.setDeploymentId(
					(String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(
						PropertyFile.getInstance().getClusterId() + "-" + PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}

			MtasGraftingProducerRequest mtasRequest = new MtasGraftingProducerRequest();

			mtasRequest.setBpmWorkFlowObject(bpmWorkflow);
			mtasRequest.setProvisioningRequestType("NS");
			mtasRequest.setWorkFlowRequestList(lstProvisioningWorkFlowRequest);
			kcontext.setVariable(BPMConstants.CONTENT, mtasRequest);

			ObjectMapper obj = new ObjectMapper();
			LOGGER.info("MTAS Content for Ungraft is : " + obj.writeValueAsString(mtasRequest));
		} catch (Exception e) {
			LOGGER.error("Error in initiateMTASRequest method for Ungraft Exception : " + e);
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	public void validateMTASRequestsResponse(ProcessContext kcontext) throws Exception {

		LOGGER.info("Entering validateMTASRequestsResponse() method for Ungraft");
		try {
			kcontext.setVariable("skipUGMTASSignal", false);
			if (null != kcontext.getVariable(BPMConstants.RESULT)) {
				MtasProducerResponse mtasProducerResp = (MtasProducerResponse) (kcontext
						.getVariable(BPMConstants.RESULT));
				LOGGER.info("MtasProducerResponse for Ungraft: " + mtasProducerResp + " for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());

				if (null != mtasProducerResp) {
					if (BPMConstants.SUCCESS.equalsIgnoreCase(mtasProducerResp.getStatus())) {
						kcontext.setVariable(BPMConstants.FLAG, true);
						if ("Y".equalsIgnoreCase(mtasProducerResp.getSwitchBypassInd())) {
							kcontext.setVariable("skipUGMTASSignal", true);
						}
					} else {
						kcontext.setVariable(BPMConstants.FLAG, false);
						CompensationUtil compUtil = new CompensationUtil();
						if (mtasProducerResp.getExceptionInfo() != null) {
							compUtil.setCompensationObject(kcontext, mtasProducerResp.getExceptionInfo().getErrorCode(),
									mtasProducerResp.getExceptionInfo().getErrorSource(),
									mtasProducerResp.getExceptionInfo().getErrorDeatils(), "MTAS_RES", "compCount");
						} else {
							compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
									"Response Object from Micro Service is either NULL or unexpected", "MTAS_RES",
									"compCount");
						}
					}
				} else {
					LOGGER.info("Mtas Ungraft responded with NULL or Error response");
					kcontext.setVariable(BPMConstants.FLAG, false);
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
							"Response Object from Micro Service is either NULL or unexpected", "MTAS_RES", "compCount");
				}
			} else {
				LOGGER.error("Exception while Invoking MTAS Service");
				kcontext.setVariable(BPMConstants.FLAG, false);
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Exception while Invoking Micro Service",
						"MTAS_RES", "compCount");
			}
		} catch (Exception e) {
			LOGGER.error("Error in validateMTASRequestsResponse method for Ungraft Exception : " + e);
			throw new Exception(e);
		}
	}

	public void validateMTASUngraftSignalResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validateMTASUngraftSignalResponse() method for Ungraft");

		kcontext.setVariable(BPMConstants.FLAG, false);
		kcontext.setVariable(BPMConstants.MTAS_SUCCESS, false);

		try {
			kcontext.setVariable(BPMConstants.MTAS_SUCCESS, false);
			LOGGER.info("Results from BPMConstants" + kcontext.getVariable(BPMConstants.RESULT));

			if (null != kcontext.getVariable(BPMConstants.RESULT)) {
				MtasConsumerRequestGrafting mtasRespData = (MtasConsumerRequestGrafting) (kcontext
						.getVariable(BPMConstants.RESULT));
				LOGGER.info("MtasConsumerRequestGrafting for Ungraft : " + mtasRespData);

				String status = mtasRespData.getStatus();
				if (null != status && !status.isEmpty() && BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
					// Getting branch lines from kcontext
					LOGGER.info("Response is success");
					List<String> ungraftBranchlines = (List<String>) kcontext.getVariable("ungraftBranchlines");

					List<String> receivedBranchlines = (List<String>) kcontext
							.getVariable("ReceivedUngraftBranchlines");

					if (null == receivedBranchlines) {
						receivedBranchlines = new ArrayList<>();
					}
					if (mtasRespData.getTrunk() != null && mtasRespData.getTrunk().getAction() != null
							&& mtasRespData.getTrunk().getAction().equalsIgnoreCase("SEPARATE")) {
						receivedBranchlines.add(mtasRespData.getTrunk().getMdn());
					}

					for (MdnStatus mdnStatus : mtasRespData.getBranchList()) {

						if (mdnStatus.getAction() != null && mdnStatus.getAction().equalsIgnoreCase("SEPARATE")) {
							receivedBranchlines.add(mdnStatus.getMdn());
						}
					}
					kcontext.setVariable("ReceivedUngraftBranchlines", receivedBranchlines);
					LOGGER.info("ReceivedUngraftBranchlines is " + receivedBranchlines + " ungraftBranchlines "
							+ ungraftBranchlines);
					if (receivedBranchlines.size() == ungraftBranchlines.size()) {
						kcontext.setVariable(BPMConstants.FLAG, true);
						kcontext.setVariable(BPMConstants.MTAS_SUCCESS, true);

					}

				} else {
					LOGGER.info("Error response received from MTAS Signal for Ungraft with errorCode : ["
							+ mtasRespData.getStatus() + "] and errorDesc : [" + mtasRespData.getStatusDesc() + "]");

				}
			} else {
				LOGGER.error(
						"Received NULL response from MTAS Signal for Ungraft . Deactivating safe point and calling compensation.");
				discardLine(kcontext, BPMConstants.SIGNAL_UNGRAFT_DEVICE);
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
						"Response Object from Micro Service is NULL for ", null, BPMConstants.COMP_COUNT);
			}

		} catch (Exception e) {
			LOGGER.error("Exception while validating response received from MTAS signal for Ungraft. Exception : " + e);
			discardLine(kcontext, BPMConstants.SIGNAL_UNGRAFT_DEVICE);
			CompensationUtil compUtil = new CompensationUtil();
			compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
					"Exception while validating response received from MTAS signal for Ungraft ", null,
					BPMConstants.COMP_COUNT);
			kcontext.setVariable(BPMConstants.MTAS_SUCCESS, false);
		}

	}

	public void deriveLineStatus(ProcessContext kcontext) {

		LOGGER.info("Entering deriveLineStatus() method ... NumberShareUtil.java");
		try {
			if (kcontext.getVariable(BPMConstants.RESULT) != null) {
				if (kcontext.getVariable(BPMConstants.RESULT) instanceof MtasConsumerRequest) {
					MtasConsumerRequest mtasRespData = (MtasConsumerRequest) (kcontext
							.getVariable(BPMConstants.RESULT));
					if (mtasRespData != null && mtasRespData.getNeType() != null) {

						String neType = mtasRespData.getNeType();
						if (neType.equalsIgnoreCase("AUTO1000")) {
							kcontext.setVariable("lineStatus", "EC");
						} else if (neType.equalsIgnoreCase("SIMOTA")) {
							kcontext.setVariable("lineStatus", "ES");
						} else if (neType.equalsIgnoreCase("WIS")) {
							kcontext.setVariable("lineStatus", "E9");
						} else if (neType.equalsIgnoreCase("TASPI")) {
							kcontext.setVariable("lineStatus", "EN");
						} else {
							if (kcontext.getVariable("lineStatus") != null) {

								String lineStatus = kcontext.getVariable("lineStatus").toString();
								LOGGER.info("lineStatus-->" + lineStatus);
								if (lineStatus.matches("EC|EN|ES|E9|EW")) {
									LOGGER.info("the previous line status is: " + lineStatus);
								} else {
									LOGGER.info(
											"Previous line status does not match with any of the list: " + lineStatus);
									kcontext.setVariable("lineStatus", "EB");
								}
							} else {
								LOGGER.info("lineStatus is null, hence setting to EB");
								kcontext.setVariable("lineStatus", "EB");
							}

						}
					} else {
						LOGGER.info("MtasConsumerRequest is null or neType is null");
						kcontext.setVariable("lineStatus", "EB");
					}
				} else if (kcontext.getVariable(BPMConstants.RESULT) instanceof MtasProducerRequest) {

					LOGGER.info("Setting lineStatus to EB as this is mtas sync failure");
					kcontext.setVariable("lineStatus", "EB");
				} else {

					LOGGER.info("Response is null or unexpected. Hence setting line_status to EB");
					kcontext.setVariable("lineStatus", "EB");
				}
			} else {

				LOGGER.info("Result is null or unexpected. Hence setting line_status to EB");
				kcontext.setVariable("lineStatus", "EB");
			}
		} catch (Exception e) {
			kcontext.setVariable("lineStatus", "EB");
			LOGGER.error("Exception inside deriveLineStatus() method. Exception : " + e);
			e.printStackTrace();
		}

		LOGGER.info("final lineStatus for MTAS failure:" + kcontext.getVariable("lineStatus"));
	}

	public void discardLine(ProcessContext kcontext, String signalKey) {
		LOGGER.info("Entering discardLine() method ...");
		try {
			kcontext.setVariable(BPMConstants.OPERATION_TYPE, "REFLOW");
			kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
			LOGGER.info("De-activating safe point of signalKey : " + signalKey + " and signalId : "
					+ kcontext.getVariable(signalKey));

			if (signalKey != null && kcontext.getVariable(signalKey) != null) {
				kcontext.getKieRuntime().getWorkItemManager()
				.completeWorkItem(Long.parseLong(String.valueOf(kcontext.getVariable(signalKey))), null);
			}

		} catch (Exception e) {
			LOGGER.error("Exception while validating response received from Line Item Order Status Update. Exception : "
					+ e);
			e.printStackTrace();
		}

	}

	public static void initiateTrunkCheckRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateTrunkCheckRequest() method ...");

		try {
			ConsumerWorkflowRequest consumerWorkFlowRequest = (ConsumerWorkflowRequest) kcontext
					.getVariable(BPMConstants.CONSUMERWFLREQUEST);
			LOGGER.info("consumerWorkflowRequest value is-->" + consumerWorkFlowRequest );
			WorkFlowRequest workFlowRequest = consumerWorkFlowRequest.getLstOfWorkflowRequest().get(0);

			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String daoUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.DAO_URL.toString(),
					BPMConstants.SERVICE_ID_BPM_DATA_UPD);
			LOGGER.debug("DAO Update URL is : " + daoUrl);
			kcontext.setVariable(BPMConstants.URL, daoUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			String timeStamp = DateTimeUtils.geCurrenttDateTime();

			FetchOrderRefDetails updateTrunkStatus = new FetchOrderRefDetails();
			long parentProcessInstanceId = kcontext.getProcessInstance().getParentProcessInstanceId();
			updateTrunkStatus.setCustIdNo(workFlowRequest.getCustomerId());
			updateTrunkStatus.setAcctNo(workFlowRequest.getAccountNumber());
			updateTrunkStatus.setBillSysId(BillingSysIdUtil.getBillSysId(workFlowRequest.getBillingInstance()));
			updateTrunkStatus.setOrdNo(workFlowRequest.getOrderId());
			updateTrunkStatus.setOrdSource(workFlowRequest.getOrderSource());
			updateTrunkStatus.setOrdType(workFlowRequest.getOrderType());
			updateTrunkStatus.setParentPrcssInstId(String.valueOf(parentProcessInstanceId));
			updateTrunkStatus.setSite(PropertyFile.getInstance().getDataCenter());
			updateTrunkStatus.setStatus("OK");
			updateTrunkStatus.setWfDeploymentId( (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			updateTrunkStatus.setCurrentTimeStamp(timeStamp); 
			updateTrunkStatus.setWfPrcssInstId(String.valueOf(parentProcessInstanceId));

			String mtns = "";
			String lnItmTypCds = "";
			String lnItmNo = "";
			for (WorkFlowRequest obj : consumerWorkFlowRequest.getLstOfWorkflowRequest()) {
				List<KeyValueObject> lstlineLevelInfo = obj.getLineLevelInfo();
				if (lstlineLevelInfo != null && lstlineLevelInfo.size() > 0) {
					for (KeyValueObject keyValueObject : lstlineLevelInfo) {
						if (!StringUtils.isEmpty(keyValueObject.getKey())
								&& !StringUtils.isEmpty(keyValueObject.getValue())) {
							if (keyValueObject.getKey().equalsIgnoreCase("isNSTrunkInOrder")
									&& Boolean.parseBoolean(keyValueObject.getValue().toString())) {
								mtns += (obj.getMtn() + ",");
								lnItmNo += (obj.getLineItemNumber() + ",");
								lnItmTypCds += (getLnItmTypCd(obj) + ",");
							}
						}
					}
				}
			}

			updateTrunkStatus
			.setMtns(mtns.substring(0, (mtns.length() == 0 ? mtns.length() : (mtns.length() - 1))));
			updateTrunkStatus.setLnItmTypCds(lnItmTypCds.substring(0,
					(lnItmTypCds.length() == 0 ? lnItmTypCds.length() : (lnItmTypCds.length() - 1))));
			updateTrunkStatus
			.setLnItmNos(lnItmNo.substring(0, (lnItmNo.length() == 0 ? lnItmNo.length() : (lnItmNo.length() - 1))));
			Wrapper<FetchOrderRefDetails> businessDomain = new Wrapper<FetchOrderRefDetails>();
			businessDomain.setService("NumberShareDao");
			businessDomain.setServiceMethod("enrichContent");
			businessDomain.setDomain(updateTrunkStatus);
			businessDomain.setSync("Y");
			ObjectMapper mapper = new ObjectMapper();
			kcontext.setVariable(BPMConstants.CONTENT, mapper.writeValueAsString(businessDomain));

			LOGGER.info("FetchOrderRefDetails content is : " + mapper.writeValueAsString(businessDomain));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateTrunkCheckRequest method. Exception : " + e);
			throw new Exception(e);
		} 
	}

	private static String getLnItmTypCd(WorkFlowRequest workFlowRequest) {
		String lnItmTypCd = "A";

		if (workFlowRequest != null) {

			if ((workFlowRequest.getOrderSource().equalsIgnoreCase("Activation")
					|| ((!StringUtils.isEmpty(workFlowRequest.getActivateMtn()))
							&& (workFlowRequest.getActivateMtn()).equalsIgnoreCase("true")))) {
				return lnItmTypCd = "A";
			} else if (((!StringUtils.isEmpty(workFlowRequest.getChangeMtn()))
					&& (workFlowRequest.getChangeMtn()).equalsIgnoreCase("true"))
					|| ((!StringUtils.isEmpty(workFlowRequest.getChangeDevice()))
							&& (workFlowRequest.getChangeDevice()).equalsIgnoreCase("true"))
					|| ((!StringUtils.isEmpty(workFlowRequest.getChangePlanFeatures()))
							&& (workFlowRequest.getChangePlanFeatures()).equalsIgnoreCase("true"))) {
				return lnItmTypCd = "M";
			}
		}

		return lnItmTypCd;

	}

	public static void validateTrunkCheckResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validateTrunkCheckResponse() method ...");

		boolean flag = false;
		try
		{
			ResponseDomain orderResponse = (ResponseDomain) (kcontext.getVariable(BPMConstants.RESULT));
			kcontext.setVariable("isParentSignalSkip", true);
			kcontext.setVariable("isTrunkCompleted", false);
			LOGGER.info("orderResponse is -->"+orderResponse.toString());
			if (null != orderResponse && orderResponse.getPnoResponseStatus() !=null) {
				PnoResponseStatus pnoResponseStatus = orderResponse.getPnoResponseStatus();
				if (null != pnoResponseStatus) {
					LOGGER.info("Trunk Order Check Response is : " + pnoResponseStatus + " for ProcessInstanceId: "
							+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
							+ kcontext.getProcessInstance().getParentProcessInstanceId());
					String code = pnoResponseStatus.getCode();
					String message = pnoResponseStatus.getMessage();
					if ("200".equalsIgnoreCase(code) && BPMConstants.SUCCESS.equalsIgnoreCase(message))
					{
						flag = true;
						kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					}
				}

				if (!flag)
				{
					ExceptionInfo expInfo = orderResponse.getExceptionInfo();
					LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : " + expInfo.getErrorSource()
					+ ", Error Details : " + expInfo.getErrorDeatils());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), expInfo.getErrorSource(),
							expInfo.getErrorDeatils(), null, BPMConstants.COMP_COUNT);
				} else {
					// checking the Trunk Status
					ObjectMapper objMapper = new ObjectMapper();
					String getResultJson = objMapper.writeValueAsString(orderResponse.getResults());
					HashMap<String, Object> responseResult = objMapper.readValue(getResultJson, HashMap.class);
					Boolean isTrunkCompleted = (Boolean) responseResult.get("isTrunkCompleted");
					LOGGER.info("Trunk Status from response domian: " + isTrunkCompleted);
					kcontext.setVariable("isTrunkCompleted", isTrunkCompleted);
					List<HashMap<String, Object>> listprocessData = (List<HashMap<String, Object>>) responseResult
							.get("trunkPendingORPrcss");
					List<HashMap<String, Object>> retrieveOrderStatus = (List<HashMap<String, Object>>) responseResult
							.get("retrieveOrderStatus");
					if (listprocessData != null && listprocessData.size() > 0) {
						HashMap<String, Object> processData = listprocessData.get(0);
						if (processData != null && processData.get("wfPrcssInstId") != null) {
							kcontext.setVariable("trunkInstanceId", processData.get("wfPrcssInstId"));
							kcontext.setVariable("trunkDeploymentId", processData.get("wfDeploymentId"));
							kcontext.setVariable("isParentSignalSkip", false);
							LOGGER.info("isParentSignalSkip was set to true, "+ processData.get("wfPrcssInstId") + "<--wfPrcssInstId , wfDeploymentId:"+processData.get("wfDeploymentId"));

						}
					}
					if (retrieveOrderStatus != null && retrieveOrderStatus.size() > 0) {
						HashMap<String, Object> processData = retrieveOrderStatus.get(0);
						if (processData != null && processData.get("lnItmStatCd") != null) {
							kcontext.setVariable("trunkOrderStatus", processData.get("lnItmStatCd"));
							LOGGER.info("trunkOrderStatus"+processData.get("lnItmStatCd"));

						}
					}
				}
			} else {
				LOGGER.error("Response Object from DAO Update Service is NULL for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
						"Response Object from Micro Service is NULL", null, BPMConstants.COMP_COUNT);
			}

		} catch (Exception e) {
			LOGGER.error(
					"Exception while validating response received from Trunk Check Status Update. Exception : " + e);
			throw new Exception(e);
		}

		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

	public static void signalPendingProcess(ProcessContext kcontext) throws Exception {

		LOGGER.info("Entering into signalParentProcess() method..");

		try {
			Boolean isTrunkRequest = Boolean.parseBoolean(String.valueOf(kcontext.getVariable("isTrunkRequest")));
			if (isTrunkRequest == true) {

				LOGGER.info(
						" ProcessInstanceId:" + kcontext.getProcessInstance() + " ParentProcessInstanceId:"
								+ kcontext.getVariable("trunkInstanceId"),
								"WfDeploymentId:" + kcontext.getVariable("trunkDeploymentId"));

				ServiceProvider bpmUtil = new ServiceProvider();
				String URL = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BASE_URL.toString(),
						BPMConstants.SERVICE_ID_BPM);
				String signal="OC_SIGNAL";  
				if(null!=kcontext.getVariable("trunkOrderStatus")&& kcontext.getVariable("trunkOrderStatus").toString().equalsIgnoreCase("OK")){  
					signal="UPDATE_ORDER"  ;
				}  
				URL = URL + "/" + kcontext.getVariable("trunkDeploymentId") + "/processes/instances/"
						+ kcontext.getVariable("trunkInstanceId") + "/signal/"+signal;
				LOGGER.info("OC_SIGNAL URL : " + URL);

				kcontext.setVariable("url", URL);
				kcontext.setVariable("bpmAccessKey",
						PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
				kcontext.setVariable("bpmAccessCode",
						PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));
				kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT,
						PropertyFile.getInstance().getConnectionTimeOut());
				kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
				ObjectMapper mapper = new ObjectMapper();

				VisibleBPMWorkFlow bpmWorkflow = new VisibleBPMWorkFlow();
				bpmWorkflow.setProcessInstanceId(
						String.valueOf(kcontext.getProcessInstance().getParentProcessInstanceId()));
				bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
				bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_TRUNK_SIGNAL);
				LOGGER.info("deploymentId-->"
						+ (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
				bpmWorkflow.setDeploymentId(
						(String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));

				if (PropertyFile.getInstance().isConsumerClustered()) {
					bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
							+ PropertyFile.getInstance().getDataCenter());
				} else {
					bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
				}
				String request = mapper.writeValueAsString(bpmWorkflow) ;
				LOGGER.info("BPMWorkflow is: " + mapper.writeValueAsString(bpmWorkflow));
				kcontext.setVariable(BPMConstants.CONTENT, request);
				LOGGER.info("branch order signal to trunk order -OC SIGNAL Content is:" + request);

			} else {
				VisibleBPMWorkFlow visibleBPMWorkFlow = (VisibleBPMWorkFlow) kcontext.getVariable("branchBPMWorkFlow");
				if (visibleBPMWorkFlow != null) {
					LOGGER.info(
							" ProcessInstanceId:" + kcontext.getProcessInstance() + " ParentProcessInstanceId:"
									+ kcontext.getVariable("trunkInstanceId"),
									"WfDeploymentId:" + kcontext.getVariable("trunkDeploymentId"));
					ServiceProvider bpmUtil = new ServiceProvider();
					String URL = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BASE_URL.toString(),
							BPMConstants.SERVICE_ID_BPM);
					String depId = (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID);
					URL = URL + "/" + visibleBPMWorkFlow.getDeploymentId() + "/processes/instances/"
							+ visibleBPMWorkFlow.getProcessInstanceId() + "/signal/"
							+ visibleBPMWorkFlow.getSignalEventId();
					LOGGER.info("TRUNK_SIGNAL URL : " + URL);

					kcontext.setVariable("url", URL);
					kcontext.setVariable("bpmAccessKey",
							PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
					kcontext.setVariable("bpmAccessCode",
							PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));
					kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT,
							PropertyFile.getInstance().getConnectionTimeOut());
					kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
					String request = "{ \"requestType \": \"TRUNK_COMPLETED\"}";
					kcontext.setVariable(BPMConstants.CONTENT, request);
					LOGGER.info("trunk signal to branch as completed  is:" + request);
				} else {
					LOGGER.info("trunk signal to branch as incompleted   request , please check kcontext:");
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception while signaling OC_SIGNAL AND TRUNK_SIGNAL in NumberShareUtil" + e.getMessage());
		}
	}


	public static void validateBranchSignal(ProcessContext kcontext) throws Exception {

		LOGGER.info("Entering into validateBranchSignal() method..");

		try {
			VisibleBPMWorkFlow visibleBPMWorkFlow = (VisibleBPMWorkFlow) kcontext.getVariable("branchBPMWorkFlow");
			LOGGER.info(kcontext.getVariable("branchBPMWorkFlow").toString());
			if (visibleBPMWorkFlow.getProcessInstanceId() != null) {
				LOGGER.info("validateBranchSignal()  -  process instance id  {}",
						visibleBPMWorkFlow.getProcessInstanceId());
			}
			if (visibleBPMWorkFlow.getDeploymentId() != null) {
				LOGGER.info("validateBranchSignal()  -  process getDeployment id  {}",
						visibleBPMWorkFlow.getDeploymentId());
			}

		} catch (Exception e) {
			LOGGER.error("Exception while signaling OC_SIGNAL AND TRUNK_SIGNAL in NumberShareUtil" + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public void validateTrunkCompletion(ProcessContext kcontext) {

		LOGGER.info("Entering validateTrunkCompletion() method..");

		try {

			Map<String, Object> resp = (Map<String, Object>) kcontext.getVariable("result");
			LOGGER.info("Signal from Result "+kcontext.getVariable("result").toString());
			LOGGER.info("trunk pending  signal received from order  : {} ",resp.get("requestType"));
			String requestType =(String) resp.get("requestType");
			if(requestType !=null && requestType.equalsIgnoreCase("TRUNK_COMPLETED")) {
				kcontext.setVariable("isTrunkCompleted", true);
				// close trunk signal 
				String signalKey="TRUNK_RESPONSE";
				LOGGER.info("De-activating safe point of signalKey : " + signalKey + " and signalId : "
						+ kcontext.getVariable(signalKey));

				if (signalKey != null && kcontext.getVariable(signalKey) != null) {
					kcontext.getKieRuntime().getWorkItemManager()
					.completeWorkItem(Long.parseLong(String.valueOf(kcontext.getVariable(signalKey))), null);
				}
			}


		} catch (Exception e) {
			LOGGER.error("Exception inside validateTrunkCompletion() method :" + e.getMessage());
		}
	}
}
