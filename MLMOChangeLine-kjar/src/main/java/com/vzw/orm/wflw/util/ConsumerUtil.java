package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.BeanUtils;
import org.jbpm.process.instance.command.UpdateTimerCommand;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.process.ProcessContext;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.internal.runtime.manager.RuntimeManagerRegistry;
import org.kie.internal.runtime.manager.context.ProcessInstanceIdContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.common.core.util.DateTimeUtils;
import com.vzw.common.core.util.StringUtils;
import com.vzw.orm.bpmi.domain.objects.consumer.ConsumerWorkflowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.KeyValueObject;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.visible.domain.common.VisibleBPMWorkFlow;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class ConsumerUtil implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerUtil.class);

	/**
	 * This method is used to set all the variable values required to call MTAS
	 * in Consumer Workflow
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateConsumerRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiateConsumerRequest() method ...");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);

			boolean prodParallelFlag = false;
			if ("Y".equalsIgnoreCase(workFlowRequest.getProductionParallelInd())) {
				prodParallelFlag = true;
			}
			kcontext.setVariable("productionParallelInd", prodParallelFlag);
			kcontext.setVariable("orderKey", "orderId," + workFlowRequest.getOrderId());

			boolean is911 = false;
			if (workFlowRequest.getDeviceTechnology() != null
					&& "9".equalsIgnoreCase(workFlowRequest.getDeviceTechnology())) {
				is911 = true;
			}
			kcontext.setVariable("is911", is911);
			LOGGER.info("is911 line-->" + kcontext.getVariable("is911"));
			boolean is911sParent = false;
			if (workFlowRequest.getE911LineItemNumber() != null && !workFlowRequest.getE911LineItemNumber().isEmpty()) {
				is911sParent = true;
			}
			kcontext.setVariable("is911sParent", is911sParent);
			LOGGER.info("is911sParent-->" + kcontext.getVariable("is911sParent"));
			LOGGER.info("LineItemNo: " + workFlowRequest.getLineItemNumber() + " has is911 -->" + is911);
			kcontext.setVariable("callLoanCreation",
					"true".equalsIgnoreCase(workFlowRequest.getInstallmentLoan()) ? true : false);

			LOGGER.info("callLoanCreation:" + kcontext.getVariable("callLoanCreation"));

			if (is911sParent) {
				kcontext.setVariable("workflowRequestMain", workFlowRequest);
				WorkFlowRequest wfr = new WorkFlowRequest();
				BeanUtils.copyProperties(wfr, workFlowRequest);
				wfr.setDeviceTechnology("9");
				wfr.setLineItemNumber(workFlowRequest.getE911LineItemNumber());
				wfr.setE911LineItemNumber("");
				kcontext.setVariable("workflowRequest911", wfr);
			}
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId() + " ProductionParallelInd:"
					+ prodParallelFlag);

			kcontext.setVariable(BPMConstants.BUCKET_DT, DateTimeUtils.formatCurrentTsBasedOnFormat("yyyy-MM-dd"));

			kcontext.setVariable("pid", String.valueOf(kcontext.getProcessInstance().getParentProcessInstanceId()));
			kcontext.setVariable("ocSignalPid",
					String.valueOf(kcontext.getProcessInstance().getParentProcessInstanceId()));
			LOGGER.info("ocSignalPid:" + kcontext.getVariable("ocSignalPid"));
			kcontext.setVariable("cpid", String.valueOf(kcontext.getProcessInstance().getId()));
			kcontext.setVariable("portIn", workFlowRequest.getPortIn());
			kcontext.setVariable("isPortIn",
					("PI".equalsIgnoreCase(workFlowRequest.getPortIn())
							|| "PR".equalsIgnoreCase(workFlowRequest.getPortIn())
							|| "FV".equalsIgnoreCase(workFlowRequest.getPortIn()) || "PIT".equalsIgnoreCase(workFlowRequest.getPortIn()) || "PIR".equalsIgnoreCase(workFlowRequest.getPortIn())) ? true : false);
			kcontext.setVariable("isLNStatusUpdate", ("FV".equalsIgnoreCase(workFlowRequest.getPortIn())
					|| "PR".equalsIgnoreCase(workFlowRequest.getPortIn()) || "PIT".equalsIgnoreCase(workFlowRequest.getPortIn()) || "PIR".equalsIgnoreCase(workFlowRequest.getPortIn())) ? true : false);
			kcontext.setVariable("isTNC", ("PIT".equalsIgnoreCase(workFlowRequest.getPortIn()) || "PIR".equalsIgnoreCase(workFlowRequest.getPortIn())) ? true : false);
			kcontext.setVariable("isVisibleMigration",
					"FV".equalsIgnoreCase(workFlowRequest.getPortIn()) ? true : false);
			kcontext.setVariable("is5gHome", ("5GI".equalsIgnoreCase(workFlowRequest.getDeviceTechnology())
					|| "5GF".equalsIgnoreCase(workFlowRequest.getDeviceTechnology())) ? true : false);
			boolean trackMtasNC = PropertyFile.getProjectProperties().get("TRACK_NON_CRITICAL_ELEMENTS") != null
					? Boolean.parseBoolean(
							PropertyFile.getProjectProperties().get("TRACK_NON_CRITICAL_ELEMENTS").toString())
							: false;
			LOGGER.info("trackMtasNC:" + trackMtasNC);
			kcontext.setVariable("enableNC", trackMtasNC);
			
			boolean enableRmqLineProcessing = PropertyFile.getProjectProperties()
					.get("ENABLE_RMQ_LINE_PROCESSING_FOR_MLMO") != null
							? Boolean.parseBoolean(PropertyFile.getProjectProperties()
									.get("ENABLE_RMQ_LINE_PROCESSING_FOR_MLMO").toString())
							: false;
			LOGGER.info("enableRmqLineProcessing:" + enableRmqLineProcessing);
			kcontext.setVariable("enableRmqLineProcessing", enableRmqLineProcessing);
			
			boolean etni_device = false;
			if ("true".equalsIgnoreCase(workFlowRequest.getChangeDevice())) {
				etni_device = true;
			}

			kcontext.setVariable("is5gFcl",
					("5GF".equalsIgnoreCase(workFlowRequest.getDeviceTechnology()) && etni_device) ? true : false);


			kcontext.setVariable("callNSP",
					"true".equalsIgnoreCase(workFlowRequest.getChangePlanFeatures()) ? true : false);

			kcontext.setVariable("isFutureDated", false);
			SimpleDateFormat dateFormat = workFlowRequest.getOrderDueDate().length() > 10
					? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS") : new SimpleDateFormat("yyyy-MM-dd");
			if (dateFormat.parse(workFlowRequest.getOrderDueDate())
					.compareTo(dateFormat.parse(workFlowRequest.getOrderDateTime())) > 0) {
				kcontext.setVariable("isFutureDated", true);
			}
			LOGGER.info("isFutureDated:" + kcontext.getVariable("isFutureDated"));

			kcontext.setVariable("isPortInPegaNeeded", false);
			if(workFlowRequest.getPortIn()!= null && !"".equalsIgnoreCase(workFlowRequest.getPortIn())) {
				kcontext.setVariable("isPortInPegaNeeded", true);
			}

			LOGGER.info("isPortInPegaNeeded:" + kcontext.getVariable("isPortInPegaNeeded"));
			kcontext.setVariable("etnichangedevice", etni_device);
			kcontext.setVariable("asyncSaveOrderInd",
					"true".equalsIgnoreCase(workFlowRequest.getAsyncSaveOrderInd()) ? true : false);

		} catch (Exception e) {
			LOGGER.error("Error in initiateConsumerRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	public void getWorkflowRequestList(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering getWorkflowRequestList() method ...");
		try {
			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
					.getVariable("consumerWorkflowRequest");
			LOGGER.info("consumerWFRequest--> " + consumerWFRequest);
			String ppid = String.valueOf(kcontext.getProcessInstance().getId());
			if (kcontext.getVariable("isModifyPending") == null) {
				kcontext.setVariable("isModifyPending", false);
				LOGGER.info("set ModifyPending--> {} ", kcontext.getVariable("isModifyPending"));
			}
			consumerWFRequest.getLstOfWorkflowRequest().forEach(wrkflw -> wrkflw.setParentProcessInstanceId(ppid));
			LOGGER.info("consumerWFRequest after ppid added--> " + consumerWFRequest);
			kcontext.setVariable("consumerWorkflowRequest", consumerWFRequest);
			ConsumerWorkflowRequest consumerWFRequestUpdated = new ConsumerWorkflowRequest();
			List<WorkFlowRequest> lstOfWorkflowRequestTemp = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> lstOfWorkflowRequestUpdated = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> lstOfWorkflowRequestForLineLevel = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> filteredListForAccountLevel = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> lstOfE911ParentWorkflowRequest = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> lstOfAddLineWorkflowRequestForLineLevel = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> lstOfChangeLineWorkflowRequestForLineLevel = new ArrayList<WorkFlowRequest>();

			lstOfWorkflowRequestTemp = consumerWFRequest.getLstOfWorkflowRequest();
			for (WorkFlowRequest workflowRequest : lstOfWorkflowRequestTemp) {
				LOGGER.info("adding line -->" + workflowRequest.getLineItemNumber());
				if (Integer.parseInt(workflowRequest.getLineItemNumber()) < 101) {
					lstOfWorkflowRequestForLineLevel.add(workflowRequest);
				} else {
					filteredListForAccountLevel.add(workflowRequest);
				}
			}

			boolean isAddLineExist = false;
			boolean isChangeLineExist = false;
			for (WorkFlowRequest workflowRequest : lstOfWorkflowRequestForLineLevel) {
				LOGGER.info("adding line -->" + workflowRequest.getLineItemNumber());
				if (workflowRequest.getActivateMtn() != null
						&& workflowRequest.getActivateMtn().equalsIgnoreCase("true")) {
					lstOfAddLineWorkflowRequestForLineLevel.add(workflowRequest);
					isAddLineExist = true;
				} else {
					lstOfChangeLineWorkflowRequestForLineLevel.add(workflowRequest);
					isChangeLineExist = true;
				}
			}

			kcontext.setVariable("isAddLineExist", isAddLineExist);
			kcontext.setVariable("isChangeLineExist", isChangeLineExist);
			
			if(PropertyFile.getProjectProperties().get("IS_MLMO_CAL_BPMI_FLOW_ENABLED")!=null 
					&& "true".equalsIgnoreCase(PropertyFile.getProjectProperties().get("IS_MLMO_CAL_BPMI_FLOW_ENABLED").toString())){
				kcontext.setVariable("isCABpmiFlowEnabled", true);
			} else {
				kcontext.setVariable("isCABpmiFlowEnabled", false);
			}
			
			LOGGER.info("lstOfAddLineWorkflowRequestForLineLevel before-->" + lstOfAddLineWorkflowRequestForLineLevel);
			LOGGER.info("filteredListForAccountLevel -->" + filteredListForAccountLevel);
			lstOfWorkflowRequestUpdated.addAll(filteredListForAccountLevel);
			Map<String, String> mtnLnItmCombo = new HashMap<String, String>();
			Set<String> e911MtnList = new HashSet<String>();
			for (WorkFlowRequest wfr : consumerWFRequest.getLstOfWorkflowRequest()) {
				if ("9".equalsIgnoreCase(wfr.getDeviceTechnology())) {
					mtnLnItmCombo.put(wfr.getMtn(), wfr.getLineItemNumber());
					e911MtnList.add(wfr.getMtn());
					lstOfAddLineWorkflowRequestForLineLevel.remove(wfr);
					lstOfChangeLineWorkflowRequestForLineLevel.remove(wfr);

				}
			}
			LOGGER.info("mtnLnItmCombo:" + mtnLnItmCombo);
			kcontext.setVariable("mtnLnItmCombo", mtnLnItmCombo);
			kcontext.setVariable("e911MtnList", e911MtnList);
			LOGGER.info("e911MtnList-->" + e911MtnList);
			LOGGER.info("lstOfAddLineWorkflowRequestForLineLevel after 911 removal-->"
					+ lstOfAddLineWorkflowRequestForLineLevel);
			LOGGER.info("lstOfChangeLineWorkflowRequestForLineLevel after 911 removal-->"
					+ lstOfChangeLineWorkflowRequestForLineLevel);
			for (String mtn : e911MtnList) {
				for (WorkFlowRequest wfr : consumerWFRequest.getLstOfWorkflowRequest()) {
					if (mtn.equalsIgnoreCase(wfr.getMtn()) && !"9".equalsIgnoreCase(wfr.getDeviceTechnology())) {
						WorkFlowRequest wfr911 = wfr;
						wfr911.setE911LineItemNumber(mtnLnItmCombo.get(mtn).toString());
						lstOfE911ParentWorkflowRequest.add(wfr911);
						lstOfAddLineWorkflowRequestForLineLevel.remove(wfr);
						lstOfChangeLineWorkflowRequestForLineLevel.remove(wfr);
					}
				}
			}
			LOGGER.info("lstOfE911ParentWorkflowRequest ->" + lstOfE911ParentWorkflowRequest);

			for (WorkFlowRequest wfr :lstOfE911ParentWorkflowRequest){
				if (wfr.getActivateMtn() != null
						&& wfr.getActivateMtn().equalsIgnoreCase("true")) {
					lstOfAddLineWorkflowRequestForLineLevel.add(wfr);
				} else {
					lstOfChangeLineWorkflowRequestForLineLevel.add(wfr);
				}

			}



			LOGGER.info("lstOfAddLineWorkflowRequestForLineLevel after major dependent lines removal->"
					+ lstOfAddLineWorkflowRequestForLineLevel);
			LOGGER.info("lstOfChangeLineWorkflowRequestForLineLevel after major dependent lines removal->"
					+ lstOfChangeLineWorkflowRequestForLineLevel);

			kcontext.setVariable("filteredLstOfAddLineWorkflow", lstOfAddLineWorkflowRequestForLineLevel);

			if (consumerWFRequest.isOrderLevel()) {
				kcontext.setVariable("lstOfAccLevelLines", filteredListForAccountLevel);
			}

			kcontext.setVariable("lstOfWorkflowRequest", lstOfWorkflowRequestTemp);

			List<WorkFlowRequest> lstOfAddLineWorkflowRequestForLineLevelFinal = new ArrayList<WorkFlowRequest>();
			for (WorkFlowRequest wfReq : lstOfAddLineWorkflowRequestForLineLevel) {
				lstOfAddLineWorkflowRequestForLineLevelFinal.add(wfReq); // recheck
			}

			LOGGER.info("lstOfAddLineWorkflowRequestForLineLevelFinal after separation-->"
					+ lstOfAddLineWorkflowRequestForLineLevelFinal);
			//COMMENTED AS we are taking care of the 5G and 4G back in consumeraddline project
			/*for (WorkFlowRequest wfReq : lstOfAddLineWorkflowRequestForLineLevel) {
				if (wfReq != null && "4GI".equalsIgnoreCase(wfReq.getDeviceTechnology())) {
					kcontext.setVariable("isFiveGICL", true);
					WorkFlowRequest fourgBackupWFReq = new WorkFlowRequest();
					BeanUtils.copyProperties(fourgBackupWFReq, wfReq);
					kcontext.setVariable("workflowRequest4G", fourgBackupWFReq);
					lstOfAddLineWorkflowRequestForLineLevelFinal.remove(wfReq);
				}
			}

			LOGGER.info("isFiveGICL-->" + kcontext.getVariable("isFiveGICL"));
			LOGGER.info("lstOfAddLineWorkflowRequestForLineLevelFinal after-->"
					+ lstOfAddLineWorkflowRequestForLineLevelFinal);
*/ // COMMENT END
			Set<String> filteredMtnList = new HashSet<>();
			for (WorkFlowRequest wfr : lstOfAddLineWorkflowRequestForLineLevelFinal) {
				filteredMtnList.add(wfr.getMtn());
			}
			for (WorkFlowRequest wfr : lstOfChangeLineWorkflowRequestForLineLevel) {
				filteredMtnList.add(wfr.getMtn());
			}
			LOGGER.info("filteredMtnList-->" + filteredMtnList);
			kcontext.setVariable("filteredMtnList", filteredMtnList);
			
			Set<String> filteredLnItmList = new HashSet<>();
			for (WorkFlowRequest wfr : filteredListForAccountLevel) {
				filteredLnItmList.add(wfr.getLineItemNumber());
			}
			LOGGER.info("filteredLnItmList-->" + filteredLnItmList);
			kcontext.setVariable("filteredLnItmList", filteredLnItmList);
			
			kcontext.setVariable("filteredLstOfAddLineWorkflow", lstOfAddLineWorkflowRequestForLineLevelFinal);
			kcontext.setVariable("filteredLstOfChangeLineWorkflow", lstOfChangeLineWorkflowRequestForLineLevel);
			lstOfWorkflowRequestUpdated.addAll(lstOfAddLineWorkflowRequestForLineLevelFinal);
			lstOfWorkflowRequestUpdated.addAll(lstOfChangeLineWorkflowRequestForLineLevel);
			LOGGER.info("lstOfWorkflowRequestUpdated-->" + lstOfWorkflowRequestUpdated);
			if (kcontext.getVariable(BPMConstants.WORKFLOW_REQ) == null) {
				kcontext.setVariable(BPMConstants.WORKFLOW_REQ, lstOfChangeLineWorkflowRequestForLineLevel.get(0));
			}
			boolean accVPFlag = false;
			if (PropertyFile.getProjectProperties().get("ACC_LEVEL_VP_REQ_PP") != null) {
				accVPFlag = Boolean
						.parseBoolean(PropertyFile.getProjectProperties().get("ACC_LEVEL_VP_REQ_PP").toString());
			}
			LOGGER.info("Account Level VP Flag-->" + accVPFlag);

			boolean isQuartzTimerEnable = true;
			if (PropertyFile.getProjectProperties().get("IS_MLMO_QUARTZ_TIMER_FLOW_ENABLE") != null) {
				isQuartzTimerEnable = Boolean.parseBoolean(
						PropertyFile.getProjectProperties().get("IS_MLMO_QUARTZ_TIMER_FLOW_ENABLE").toString());
			}
			kcontext.setVariable("isQuartzTimerEnable", isQuartzTimerEnable);
			LOGGER.info("isQuartzTimerEnable  Flag--> {} ", isQuartzTimerEnable);

			boolean prodParallelFlag = false;
			boolean triggerALVP = false;

			for (WorkFlowRequest workFlowReqTemp : consumerWFRequest.getLstOfWorkflowRequest()) {
				if ("Y".equalsIgnoreCase(workFlowReqTemp.getProductionParallelInd())) {
					prodParallelFlag = true;
					if (accVPFlag && Integer.parseInt(workFlowReqTemp.getLineItemNumber()) > 100) {
						kcontext.setVariable("workflowRequestALVP", workFlowReqTemp);
						triggerALVP = true;
						break;
					}
				}
			}
			LOGGER.info("triggerALVP-->" + triggerALVP);
			kcontext.setVariable("triggerALVP", triggerALVP);
			kcontext.setVariable("productionParallelInd", prodParallelFlag);

			LOGGER.info("Checking if Subscription Manager to be invoked");
			boolean smRequired = false;
			for (WorkFlowRequest wfr : consumerWFRequest.getLstOfWorkflowRequest()) {
				if (wfr.getSpoCategoryCodes() != null && wfr.getSpoCategoryCodes().length > 0) {
					smRequired = true;
					LOGGER.info("Subscription Manager needs to be invoked");
					break;
				}
			}
			kcontext.setVariable("callSubsManager", smRequired);

			LOGGER.info("OrderID:" + consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderId()
					+ " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId() + " ProductionParallelInd:"
					+ prodParallelFlag);

			kcontext.setVariable("orderLevel", consumerWFRequest.isOrderLevel());
			kcontext.setVariable("correlationId",
					consumerWFRequest.getLstOfWorkflowRequest().get(0).getCorrelationId());
			if (kcontext.getVariable("orderKey") == null) {
				kcontext.setVariable("orderKey",
						"orderId," + consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderId());
			}
			LOGGER.info("Request type orderLevel:" + kcontext.getVariable("orderLevel"));

			BeanUtils.copyProperties(consumerWFRequestUpdated, consumerWFRequest);
			consumerWFRequestUpdated.setLstOfWorkflowRequest(null);
			consumerWFRequestUpdated.setLstOfWorkflowRequest(lstOfWorkflowRequestUpdated);
			LOGGER.info("consumerWFRequestUpdated-->" + consumerWFRequestUpdated);
			kcontext.setVariable("consumerWorkflowRequest", consumerWFRequestUpdated);
			kcontext.setVariable("pid", String.valueOf(kcontext.getProcessInstance().getId()));
			kcontext.setVariable(BPMConstants.BUCKET_DT, DateTimeUtils.formatCurrentTsBasedOnFormat("yyyy-MM-dd"));

			kcontext.setVariable("ocSignalLaunched", false);
			kcontext.setVariable("is911SignalLaunched", false);
			kcontext.setVariable("vroAccTriggered", false);
			kcontext.setVariable("alvpAlreadyTriggered", false);
			kcontext.setVariable("smTriggered", false);
			kcontext.setVariable("orderDelete", false);
			kcontext.setVariable("allReceived", false);
			kcontext.setVariable("orderCompleted", false);

			// Due Date Calculation Here
			calculateDueDate(kcontext);

		} catch (Exception e) {
			LOGGER.error("Error in getWorkflowRequestList method. Exception : " + e);
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	public void signalOrderCompletion(ProcessContext kcontext, String signalId, long processInstanceId) {
		LOGGER.info("Entering signalAccountOrLineForVRO() method to signal: " + signalId + " for processInstanceId:"
				+ processInstanceId);
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);

			RuntimeManager rm = (RuntimeManager) kcontext.getKieRuntime().getEnvironment().get("RuntimeManager");
			RuntimeEngine runtime = rm.getRuntimeEngine(ProcessInstanceIdContext.get(Long.valueOf(processInstanceId)));
			KieSession ksession = runtime.getKieSession();
			ProcessInstance processInstance = ksession.getProcessInstance(Long.valueOf(processInstanceId), true);

			// creating runtime manager and runtime engine for parent process
			rm = RuntimeManagerRegistry.get()
					.getManager(runtime.getAuditService().findProcessInstance(processInstance.getId()).getExternalId());
			runtime = rm.getRuntimeEngine(ProcessInstanceIdContext.get(processInstance.getId()));

			runtime.getKieSession().getProcessInstance(processInstance.getId(), true).signalEvent(signalId,
					workFlowRequest);

			LOGGER.info("Signalled to the process : " + processInstance.getProcessName()
			+ " with process instance id : " + processInstance.getId() + " and signal name is : " + signalId);

		} catch (Exception e) {
			LOGGER.error("Exception while signalling " + signalId + ": " + e.getMessage());
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void calculateDueDate(ProcessContext kcontext) {
		LOGGER.info("Inside calculateDueDate() method.....");
		kcontext.setVariable("isOrderDue", false);
		kcontext.setVariable("isCancel", false);
		kcontext.setVariable("releasedEarlyInd", false);
		kcontext.setVariable("firstLineInd", false);
		try {
			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
					.getVariable("consumerWorkflowRequest");
			kcontext.setVariable("lstOfWorkflowRequest", consumerWFRequest.getLstOfWorkflowRequest());
			//Commented orderDateTime as we are going to consider currentDateTime from FEB ER
//			String orderDateTime = consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderDateTime();
			String orderDueDate = consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderDueDate();
			kcontext.setVariable("dueDate", orderDueDate);
			SimpleDateFormat dateFormat = orderDueDate.length() > 10 ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
					: new SimpleDateFormat("yyyy-MM-dd");
			Date cd = dateFormat.parse(dateFormat.format(new Date()));
			Date dd = dateFormat.parse(orderDueDate);
			int value = dd.compareTo(cd);
			if (value == 0) {
				LOGGER.info("Due Date is equal to Order Date");
			} else if (value > 0) {
				LOGGER.info("Due Date is greater than Order Date");
				kcontext.setVariable("isOrderDue", true);
				kcontext.setVariable("firstLineInd", true);

				// Below code is for Testing env-start

				Date dueDate = dateFormat.parse(orderDueDate);
				LOGGER.info("dueDate after parsing-->" + dueDate);
				SimpleDateFormat dateToFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
				String duedate = dateToFormat.format(dueDate);
				LOGGER.info("duedate after formatting-->" + duedate);
				LocalDateTime dueLocalDateTime = LocalDateTime.parse(duedate).withHour(0).withMinute(0).withSecond(0);
				// end

				// add the below 2 lines to test in local and comment above
				// lines
				/*
				 * DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
				 * "yyyy-MM-dd HH:mm:ss.SSSSSS"); LocalDateTime dueLocalDateTime
				 * = LocalDateTime.parse(orderDueDate, formatter);
				 */
				// end
				LOGGER.info("dueLocalDateTime-->" + dueLocalDateTime);
				LocalDateTime currentDate = null;
				String billingInstance = consumerWFRequest.getLstOfWorkflowRequest().get(0).getBillingInstance();
				if (billingInstance != null) {
					if ("E".equalsIgnoreCase(billingInstance) || "N".equalsIgnoreCase(billingInstance)) {
						LOGGER.info("Eastern Timezone as billingInstance is " + billingInstance);
						currentDate = LocalDateTime.now(ZoneId.of("America/New_York"));
					} else if ("W".equalsIgnoreCase(billingInstance) || "B".equalsIgnoreCase(billingInstance)) {
						currentDate = LocalDateTime.now(ZoneId.of("America/Los_Angeles"));
						LOGGER.info("Pacific Timezone as billingInstance is " + billingInstance);
					} else {
						LOGGER.info("Considering the default Timezone as billingInstance is " + billingInstance);
						currentDate = LocalDateTime.now(ZoneId.systemDefault());
					}
				} else {
					currentDate = LocalDateTime.now(ZoneId.systemDefault());
				}
				LOGGER.info("currentDate-->" + currentDate);
				Duration duration = Duration.between(currentDate, dueLocalDateTime);
				LOGGER.info("duration-->" + duration);
				LOGGER.info("duration in seconds-->" + duration.getSeconds());
				kcontext.setVariable("orderDueDate", String.valueOf(duration.getSeconds()) + "s");

				List<WorkFlowRequest> wfrList = new ArrayList<>();
				wfrList = (List<WorkFlowRequest>) kcontext.getVariable("filteredLstOfAddLineWorkflow");
				for (WorkFlowRequest wfr : wfrList) {
					wfr.setReleasedEarlyInd("false");
				}
				kcontext.setVariable("filteredLstOfAddLineWorkflow", wfrList);
				wfrList = (List<WorkFlowRequest>) kcontext.getVariable("filteredLstOfChangeLineWorkflow");
				for (WorkFlowRequest wfr : wfrList) {
					wfr.setReleasedEarlyInd("false");
				}
				kcontext.setVariable("filteredLstOfChangeLineWorkflow", wfrList);

				List<WorkFlowRequest> filteredListForAccountLevel = (List<WorkFlowRequest>) kcontext.getVariable("lstOfAccLevelLines");
				for (WorkFlowRequest wfr : filteredListForAccountLevel) {
					wfr.setReleasedEarlyInd("false");
				}
				kcontext.setVariable("lstOfAccLevelLines",filteredListForAccountLevel);
			} else {
				LOGGER.info("Due Date is less than Order Date");
			}
		} catch (Exception e) {
			LOGGER.error("Exception while calculating due date" + e);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void validateSignalAndUpdateTimer(String timerName, long timerValue, ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validateSignalAndUpdateTimer() method for processInstanceId:"
				+ kcontext.getProcessInstance().getId());
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestType = mapper.writeValueAsString(kcontext.getVariable("result"));
			Map<String, Object> resp =null ;
			if(!requestType.contains("Visible")){
				resp = (Map<String, Object>) kcontext.getVariable("result");
			}
			LOGGER.info("Signal Data for UPDATE ORDER signal is:" + resp);
			kcontext.setVariable("isModifyPending", false);
			kcontext.setVariable("isBranchNotification", false);
			if (resp != null && resp.size() > 0) {
				if (resp.get("action") != null) {
					ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
							.getVariable("consumerWorkflowRequest");
					Boolean firsLineInd = (Boolean) kcontext.getVariable("firstLineInd");
					if ("RELEASE".equalsIgnoreCase(resp.get("action").toString())) {
						kcontext.setVariable("isCancel", false);
						kcontext.setVariable("orderDueDate", "1ms");
						timerValue = 1;
						List<WorkFlowRequest> wfrAddLineList = new ArrayList<>();
						List<WorkFlowRequest> wfrChangeLineList = new ArrayList<>();

						Set<String> releasedMtnsFromBpmi = new HashSet<String>();
						if (resp.get("mtn") != null) {
							for (WorkFlowRequest wfr : consumerWFRequest.getLstOfWorkflowRequest()) {
								if (resp.get("mtn").toString().contains(wfr.getMtn())) {
									LOGGER.info("About to release lineItmNo [" + wfr.getLineItemNumber()
									+ "] in order :" + wfr.getOrderId());
									if (firsLineInd != null) {
										wfr.setFirstLineInd(firsLineInd.toString());
									} else {
										wfr.setFirstLineInd("true");
									}
									wfr.setReleasedEarlyInd("true");
									if (wfr.getActivateMtn().equalsIgnoreCase("true")) {
										wfrAddLineList.add(wfr);
									} else {
										wfrChangeLineList.add(wfr);
									}
									releasedMtnsFromBpmi.add(wfr.getMtn());
								}
							}

							List<WorkFlowRequest> filteredListForAccountLevel = (List<WorkFlowRequest>) kcontext.getVariable("lstOfAccLevelLines");
							if (filteredListForAccountLevel != null && filteredListForAccountLevel.size() > 0) {
								for (WorkFlowRequest wfr : filteredListForAccountLevel) {
									wfr.setReleasedEarlyInd("true");
									if (firsLineInd != null) {
										wfr.setFirstLineInd(firsLineInd.toString());
									} else {
										wfr.setFirstLineInd("true");
									}
								}
							}
							kcontext.setVariable("lstOfAccLevelLines",filteredListForAccountLevel);

							kcontext.setVariable("firstLineInd", false);
							if (kcontext.getVariable("releaseMtnList") != null) {
								Set<String> releaseMtnList = (Set<String>) kcontext.getVariable("releaseMtnList");
								releaseMtnList.addAll(releasedMtnsFromBpmi);
							} else {
								Set<String> releaseMtnList = new HashSet<String>();
								releaseMtnList.addAll(releasedMtnsFromBpmi);
								kcontext.setVariable("releaseMtnList", releaseMtnList);
							}

							LOGGER.info("releaseMtnList-->" + kcontext.getVariable("releaseMtnList"));
							//ACCCC
							if (kcontext.getVariable("releaseMtnList") != null) {
								Set<String> releaseMtnList = (Set<String>) kcontext.getVariable("releaseMtnList");
								releaseMtnList.addAll(releasedMtnsFromBpmi);
							} else {
								Set<String> releaseMtnList = new HashSet<String>();
								releaseMtnList.addAll(releasedMtnsFromBpmi);
								kcontext.setVariable("releaseMtnList", releaseMtnList);
							}

							LOGGER.info("releaseMtnList-->" + kcontext.getVariable("releaseMtnList"));
							if (wfrAddLineList != null && wfrAddLineList.size() > 0) {
								kcontext.setVariable("filteredLstOfAddLineWorkflow", wfrAddLineList);
								LOGGER.info("filteredLstOfAddLineWorkflow -->"
										+ kcontext.getVariable("filteredLstOfAddLineWorkflow"));
							} else if (wfrChangeLineList != null && wfrChangeLineList.size() > 0) {
								kcontext.setVariable("filteredLstOfChangeLineWorkflow", wfrChangeLineList);
								LOGGER.info("filteredLstOfChangeLineWorkflow -->"
										+ kcontext.getVariable("filteredLstOfChangeLineWorkflow"));
							}
						} else {
							LOGGER.info("Release all MTNs");
							List<WorkFlowRequest> filteredList = consumerWFRequest.getLstOfWorkflowRequest().stream()
									.filter(wrkflw -> Integer.parseInt((wrkflw.getLineItemNumber())) < 101)
									.collect(Collectors.toList());

							Set<String> mtnsListFromReq = filteredList.stream().map(WorkFlowRequest::getMtn)
									.collect(Collectors.toSet());
							if (kcontext.getVariable("releaseMtnList") != null) {
								Set<String> releaseMtnList = (Set<String>) kcontext.getVariable("releaseMtnList");
								releaseMtnList.addAll(mtnsListFromReq);
							} else {
								Set<String> releaseMtnList = new HashSet<String>();
								releaseMtnList.addAll(mtnsListFromReq);
								kcontext.setVariable("releaseMtnList", releaseMtnList);
							}
							List<WorkFlowRequest> wfrAddLineListUpd = (ArrayList<WorkFlowRequest>) kcontext
									.getVariable("filteredLstOfAddLineWorkflow");
							List<WorkFlowRequest> wfrChangeLineListUpd = (ArrayList<WorkFlowRequest>) kcontext
									.getVariable("filteredLstOfChangeLineWorkflow");
							for (WorkFlowRequest wfr : wfrAddLineListUpd) {
								if (firsLineInd != null) {
									wfr.setFirstLineInd(firsLineInd.toString());
								} else {
									wfr.setFirstLineInd("true");
								}
								wfr.setReleasedEarlyInd("true");
								wfrAddLineListUpd.add(wfr);
							}
							for (WorkFlowRequest wfr : wfrChangeLineListUpd) {
								if (firsLineInd != null) {
									wfr.setFirstLineInd(firsLineInd.toString());
								} else {
									wfr.setFirstLineInd("true");
								}
								wfr.setReleasedEarlyInd("true");
								wfrChangeLineListUpd.add(wfr);
							}

							List<WorkFlowRequest> filteredListForAccountLevel = (List<WorkFlowRequest>) kcontext.getVariable("lstOfAccLevelLines");
							if (filteredListForAccountLevel != null && filteredListForAccountLevel.size() > 0) {
								for (WorkFlowRequest wfr : filteredListForAccountLevel) {
									wfr.setReleasedEarlyInd("true");
									if (firsLineInd != null) {
										wfr.setFirstLineInd(firsLineInd.toString());
									} else {
										wfr.setFirstLineInd("true");
									}
								}
							}
							kcontext.setVariable("lstOfAccLevelLines",filteredListForAccountLevel);

							kcontext.setVariable("firstLineInd", false);
							kcontext.setVariable("filteredLstOfAddLineWorkflow", wfrAddLineListUpd);
							kcontext.setVariable("filteredLstOfChangeLineWorkflow", wfrChangeLineListUpd);
							LOGGER.info("releaseMtnList-->" + kcontext.getVariable("releaseMtnList"));
							LOGGER.info("filteredLstOfAddLineWorkflow -->"
									+ kcontext.getVariable("filteredLstOfAddLineWorkflow"));
							LOGGER.info("filteredLstOfChangeLineWorkflow -->"
									+ kcontext.getVariable("filteredLstOfChangeLineWorkflow"));
						}
					} else if ("CANCEL".equalsIgnoreCase(resp.get("action").toString())) {
						LOGGER.info("entering cancel block..");
						kcontext.setVariable("isCancel", true);
						kcontext.setVariable("orderDueDate", "1ms");
//						WorkFlowRequest releaseWfr = new WorkFlowRequest();
						List<WorkFlowRequest> lineDeleteWorkflowRequestList = new ArrayList<WorkFlowRequest>();
						
						Set<String> mtnList = (Set<String>) kcontext.getVariable("filteredMtnList");
						Set<String> lnItmList = (Set<String>) kcontext.getVariable("filteredLnItmList");
						Set<String> cancelMtnList;
						if (kcontext.getVariable("cancelMtnList") != null) {
							cancelMtnList = (Set) kcontext.getVariable("cancelMtnList");
						} else {
							cancelMtnList = new HashSet<>();
						}
						Set<String> cancelLnItmList;
						if (kcontext.getVariable("cancelLnItmList") != null) {
							cancelLnItmList = (Set) kcontext.getVariable("cancelLnItmList");
						} else {
							cancelLnItmList = new HashSet<>();
						}
						List<WorkFlowRequest> omsCancelWFRListSOV = null;
						if(kcontext.getVariable("omsCancelWFRListSOV") == null){
							omsCancelWFRListSOV = new ArrayList<>();
						}else {
							omsCancelWFRListSOV = (List<WorkFlowRequest>)kcontext.getVariable("omsCancelWFRListSOV");
						}
						if (resp.get("lineItemNumber") != null) {
							for(String lineItmNo : resp.get("lineItemNumber").toString().split(",")){
								for (WorkFlowRequest wfr : consumerWFRequest.getLstOfWorkflowRequest()) {
									if (wfr.getLineItemNumber().equalsIgnoreCase(lineItmNo)) {
										LOGGER.info("About to cancel lineItmNo [" + wfr.getLineItemNumber() + "] in order :"
												+ wfr.getOrderId());
										lineDeleteWorkflowRequestList.add(wfr);
										if(Integer.parseInt(lineItmNo) > 100) {
											lnItmList.remove(lineItmNo);
											cancelLnItmList.add(lineItmNo);
										} else {
											mtnList.remove(wfr.getMtn());
											cancelMtnList.add(wfr.getMtn());
										}
										if (resp.get("cancelOrderWorkflowRequest") != null) {
											omsCancelWFRListSOV.add(wfr);
											LOGGER.info("added lineItem to omsCancelWFRListSOV in validateSignalAndUpdateTimer() method:" + wfr.getLineItemNumber());
										}
									}
								}
							}
							kcontext.setVariable("omsCancelWFRListSOV", omsCancelWFRListSOV);
							LOGGER.info("omsCancelWFRListSOV-->"+omsCancelWFRListSOV);
							kcontext.setVariable("lineDeleteWorkflowRequestList", lineDeleteWorkflowRequestList);
							LOGGER.info("lineDeleteWorkflowRequestList-->"+lineDeleteWorkflowRequestList);
							kcontext.setVariable("cancelMtnList", cancelMtnList);
							LOGGER.info("cancelMtnList-->" + kcontext.getVariable("cancelMtnList"));
							kcontext.setVariable("cancelLnItmList", cancelLnItmList);
							LOGGER.info("cancelLnItmList-->" + kcontext.getVariable("cancelLnItmList"));
							kcontext.setVariable("filteredMtnList", mtnList);
							LOGGER.info("mtnList after removing cancelled lines-->" + kcontext.getVariable("filteredMtnList"));
							kcontext.setVariable("filteredLnItmList", lnItmList);
							LOGGER.info("filteredLnItmList after removing cancelled lines-->" + kcontext.getVariable("filteredLnItmList"));
						}
						else {
							throw new Exception("mtn or lineItemNumber is missing in the response.");
						}
						
						boolean isTNIPCancelFD = false;
						
						if (resp.get("cancelOrderWorkflowRequest") != null) {
							ConsumerWorkflowRequest wfrList = (ConsumerWorkflowRequest) resp.get("cancelOrderWorkflowRequest");
							List<WorkFlowRequest> tnipWorkflowReqList = new ArrayList<>();
							for(WorkFlowRequest wfr : wfrList.getLstOfWorkflowRequest()) {
								LOGGER.info("wfr in cancel-->" +wfr);
								List<KeyValueObject> lineLvlInfo = wfr.getLineLevelInfo();
								if(lineLvlInfo!=null && lineLvlInfo.size() > 0){ 
									for (KeyValueObject info : lineLvlInfo) {
										LOGGER.info("validating key:" + info.getKey() + " and value:" +info.getValue());
										if (info.getKey() != null && info.getKey().toUpperCase().contains("CANCEL") && "true".equalsIgnoreCase(info.getValue())) {
											WorkFlowRequest wfrToList = new WorkFlowRequest();
											wfr.setCancelContext(info.getKey());
											BeanUtils.copyProperties(wfrToList, wfr);
											wfrToList.setCancelContext(info.getKey());
											tnipWorkflowReqList.add(wfrToList);
											LOGGER.info("Added key to wfrList:" + info.getKey());
											isTNIPCancelFD = true;
										}
									}
								}
							}
							kcontext.setVariable("tnipWorkflowReqList", tnipWorkflowReqList);
							LOGGER.info("tnipWorkflowReqList-->" +tnipWorkflowReqList);
							LOGGER.info("added mtn to omsCancelWFRListSOV in validateSignalAndUpdateTimer() method:" + resp.get("mtn"));
							kcontext.setVariable("omsCancelWFRListSOV", omsCancelWFRListSOV);
							LOGGER.info("omsCancelWFRListSOV-->"+omsCancelWFRListSOV);
							kcontext.setVariable("sovCancelReq", true);
						}
						kcontext.setVariable("isTNIPCancelFD", isTNIPCancelFD);
						
						if (omsCancelWFRListSOV != null && omsCancelWFRListSOV.size() > 0) {
							ConsumerWorkflowRequest consumerWFReq = new ConsumerWorkflowRequest();
							consumerWFReq.setLstOfWorkflowRequest(omsCancelWFRListSOV);
							kcontext.setVariable("cancelWorkflowRequest", consumerWFReq);
							LOGGER.info("About to vision save order for cancelled lines-->" + consumerWFReq);
						}
						
						if (mtnList != null && mtnList.size() == 0 && lnItmList != null && lnItmList.size() == 0) {
							LOGGER.info(
									"mtnList/lnItmList is null. Proceeding to delete the order as all lines received cancel.");

							kcontext.setVariable("orderDelete", true);
							kcontext.setVariable("statusCode", "OD");
							kcontext.setVariable("statusDesc", "Order Deleted");
							kcontext.setVariable("preStatus", "LD");
							kcontext.setVariable("allReceived", true);
							kcontext.setVariable("workflowRequest", lineDeleteWorkflowRequestList.get(0));
						}
						
						List<WorkFlowRequest> wfrAddLineListUpd = (ArrayList<WorkFlowRequest>) kcontext
								.getVariable("filteredLstOfAddLineWorkflow");

						List<WorkFlowRequest> wfrChangeLineListUpd = (ArrayList<WorkFlowRequest>) kcontext
								.getVariable("filteredLstOfChangeLineWorkflow");

//						Set<String> cancelMtnListToRemove = (Set) kcontext.getVariable("cancelMtnList");
						// MODIFIED THE LOGIC TO REMOVE LINEITEMNUMNERS INSTEAD OF MTNS
						if(resp.get("lineItemNumber")!= null){

							for (String lineitmno : resp.get("lineItemNumber").toString().split(",")){

								LOGGER.info("Before Removed WorkFLow Change Line List Upd {}:" + wfrChangeLineListUpd.size());
								Iterator<WorkFlowRequest> itrWfrChangeLineList  = wfrChangeLineListUpd.iterator();

								while(itrWfrChangeLineList.hasNext()){

									WorkFlowRequest workFlowRequest  = itrWfrChangeLineList.next();

									if(workFlowRequest.getLineItemNumber().equalsIgnoreCase(lineitmno)){

										itrWfrChangeLineList.remove();
										LOGGER.info("After Removed WorkFLow Change Line List Upd {}:" + wfrChangeLineListUpd.size());
									}

								}
								LOGGER.info("Finally Found after Removed WorkFLow Change Line List Upd {}:" + wfrChangeLineListUpd.size());

								LOGGER.info("Before Removed WorkFLow Add LineList {}:" + wfrAddLineListUpd.size());
								Iterator<WorkFlowRequest> iterWfrAddLineList  = wfrAddLineListUpd.iterator();

								while(iterWfrAddLineList.hasNext()){

									WorkFlowRequest workFlowRequest = iterWfrAddLineList.next();

									if(workFlowRequest.getLineItemNumber().equalsIgnoreCase(lineitmno)){

										iterWfrAddLineList.remove();
										LOGGER.info("After Removed  WorkFLow Add LineList {}:" + wfrAddLineListUpd.size());
									}

								}
								LOGGER.info("Finally Found after Removed  WorkFLow Add LineList {}:" + wfrAddLineListUpd.size());
							}
						}

						kcontext.setVariable("filteredLstOfAddLineWorkflow", wfrAddLineListUpd);
						kcontext.setVariable("filteredLstOfChangeLineWorkflow", wfrChangeLineListUpd);

						timerValue = 1;
					} else if ("MODIFYPENDINGORDER".equalsIgnoreCase(resp.get("action").toString()) 
							|| "MODIFYPENDINGORDERWITHACCOUNT".equalsIgnoreCase(resp.get("action").toString())) {

						ConsumerWorkflowRequest consumerWorkflowRequest = (ConsumerWorkflowRequest) resp
								.get("consumerWorkflowRequest");
						kcontext.setVariable("consumerWorkflowRequest", consumerWorkflowRequest);
						reSetWorkflowRequestList(kcontext);
						timerValue = recalculateDueDate(consumerWorkflowRequest,kcontext);
						LOGGER.info("DYMER is: " + timerValue);
						kcontext.setVariable("isModifyPending", true);
						String quartzenable = String.valueOf(kcontext.getVariable("isQuartzTimerEnable"));
						if (quartzenable !=null && quartzenable !="" && Boolean.parseBoolean(quartzenable)){
							timerValue = recalculateDueDate(consumerWorkflowRequest,kcontext);
							LOGGER.info("timerValue when we recieve modify/pending order -->" + timerValue);
							RuntimeManager rm = (RuntimeManager) kcontext.getKieRuntime().getEnvironment()
									.get("RuntimeManager");
							RuntimeEngine runtime = rm
									.getRuntimeEngine(ProcessInstanceIdContext.get(kcontext.getProcessInstance().getId()));
							KieSession ksession = runtime.getKieSession();
							ksession.execute(
									new UpdateTimerCommand(kcontext.getProcessInstance().getId(), timerName, timerValue));
						}
						LOGGER.info("DYMER value has been updated");
						LOGGER.info("Modify Pending Order Flag-->" + kcontext.getVariable("isModifyPending"));

					} else {
						LOGGER.info("Received invalid action for Signal: "+resp.get("action"));
						throw new Exception("Received invalid action for Signal: " +resp.get("action"));
					}

					// if((Integer)kcontext.getVariable("noOfLines") == )

					/*
					 * LOGGER.info("timerValue-->" + timerValue); RuntimeManager
					 * rm = (RuntimeManager)
					 * kcontext.getKieRuntime().getEnvironment()
					 * .get("RuntimeManager"); RuntimeEngine runtime = rm
					 * .getRuntimeEngine(ProcessInstanceIdContext.get(kcontext.
					 * getProcessInstance().getId())); KieSession ksession =
					 * runtime.getKieSession(); ksession.execute( new
					 * UpdateTimerCommand(kcontext.getProcessInstance().getId(),
					 * timerName, timerValue)); LOGGER.info(
					 * "DYMER value has been updated");
					 * rm.disposeRuntimeEngine(runtime);
					 */

				} else {
					LOGGER.info("No need to update DYMER as there's no action defined in the payload");
					throw new Exception("No action defined in the UPDATE_ORDER Signal Response: "+resp);
				}

			}else {
				LOGGER.info("requestType for branchWaiting--> " + requestType);
				LOGGER.info("branchBPMWorkFlow " + kcontext.getVariable("branchBPMWorkFlow"));
				LOGGER.info("result object "+ mapper.writeValueAsString(kcontext.getVariable("result")));

				if(requestType !=null && requestType.contains("Visible")) {
					LOGGER.info("Inside the visible status check for Signal Dymer");
					VisibleBPMWorkFlow  branchBPMWorkFlow =  (VisibleBPMWorkFlow) kcontext.getVariable("result");
					kcontext.setVariable("branchBPMWorkFlow",branchBPMWorkFlow);
					kcontext.setVariable("isBranchOrderWaiting", true);
					kcontext.setVariable("isBranchNotification", true);
				}
			}

		} catch (Exception e) {
			LOGGER.error("Exception inside validateSignalAndUpdateTimer method: " + e.getMessage());
			e.printStackTrace();
			throw new Exception("Exception inside validateSignalAndUpdateTimer method: " + e.getMessage());
		}
	}

	private  void reSetWorkflowRequestList(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering reSettWorkflowRequestList() method ...");
		try {
			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
					.getVariable("consumerWorkflowRequest");
			LOGGER.info("re-set consumerWFRequest--> " + consumerWFRequest);
			String ppid = String.valueOf(kcontext.getProcessInstance().getId());
			consumerWFRequest.getLstOfWorkflowRequest().forEach(wrkflw -> wrkflw.setParentProcessInstanceId(ppid));
			LOGGER.info("re-set consumerWFRequest after ppid added--> " + consumerWFRequest);
			kcontext.setVariable("consumerWorkflowRequest", consumerWFRequest);
			ConsumerWorkflowRequest consumerWFRequestUpdated = new ConsumerWorkflowRequest();
			List<WorkFlowRequest> lstOfWorkflowRequestTemp = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> lstOfWorkflowRequestUpdated = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> lstOfWorkflowRequestForLineLevel = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> filteredListForAccountLevel = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> lstOfE911ParentWorkflowRequest = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> lstOfAddLineWorkflowRequestForLineLevel = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> lstOfChangeLineWorkflowRequestForLineLevel = new ArrayList<WorkFlowRequest>();

			lstOfWorkflowRequestTemp = consumerWFRequest.getLstOfWorkflowRequest();
			for (WorkFlowRequest workflowRequest : lstOfWorkflowRequestTemp) {
				LOGGER.info("re-set adding line -->" + workflowRequest.getLineItemNumber());
				if (Integer.parseInt(workflowRequest.getLineItemNumber()) < 101) {
					lstOfWorkflowRequestForLineLevel.add(workflowRequest);
				} else {
					filteredListForAccountLevel.add(workflowRequest);
				}
			}

			boolean isAddLineExist = false;
			boolean isChangeLineExist = false;
			for (WorkFlowRequest workflowRequest : lstOfWorkflowRequestForLineLevel) {
				LOGGER.info("re-set adding line -->" + workflowRequest.getLineItemNumber());
				if (workflowRequest.getActivateMtn() != null
						&& workflowRequest.getActivateMtn().equalsIgnoreCase("true")) {
					lstOfAddLineWorkflowRequestForLineLevel.add(workflowRequest);
					isAddLineExist = true;
				} else {
					lstOfChangeLineWorkflowRequestForLineLevel.add(workflowRequest);
					isChangeLineExist = true;
				}
			}

			kcontext.setVariable("isAddLineExist", isAddLineExist);
			kcontext.setVariable("isChangeLineExist", isChangeLineExist);

			LOGGER.info("re-set lstOfAddLineWorkflowRequestForLineLevel before-->" + lstOfAddLineWorkflowRequestForLineLevel);
			LOGGER.info("re-set filteredListForAccountLevel -->" + filteredListForAccountLevel);
			lstOfWorkflowRequestUpdated.addAll(filteredListForAccountLevel);
			Map<String, String> mtnLnItmCombo = new HashMap<String, String>();
			Set<String> e911MtnList = new HashSet<String>();
			for (WorkFlowRequest wfr : consumerWFRequest.getLstOfWorkflowRequest()) {
				if ("9".equalsIgnoreCase(wfr.getDeviceTechnology())) {
					mtnLnItmCombo.put(wfr.getMtn(), wfr.getLineItemNumber());
					e911MtnList.add(wfr.getMtn());
					lstOfAddLineWorkflowRequestForLineLevel.remove(wfr);
				}
			}
			LOGGER.info("re-set mtnLnItmCombo:" + mtnLnItmCombo);
			kcontext.setVariable("mtnLnItmCombo", mtnLnItmCombo);
			kcontext.setVariable("e911MtnList", e911MtnList);
			LOGGER.info("re-set e911MtnList-->" + e911MtnList);
			LOGGER.info("re-set lstOfAddLineWorkflowRequestForLineLevel after 911 removal-->"
					+ lstOfAddLineWorkflowRequestForLineLevel);
			for (String mtn : e911MtnList) {
				for (WorkFlowRequest wfr : consumerWFRequest.getLstOfWorkflowRequest()) {
					if (mtn.equalsIgnoreCase(wfr.getMtn()) && !"9".equalsIgnoreCase(wfr.getDeviceTechnology())) {
						WorkFlowRequest wfr911 = wfr;
						wfr911.setE911LineItemNumber(mtnLnItmCombo.get(mtn).toString());
						lstOfE911ParentWorkflowRequest.add(wfr911);
						lstOfAddLineWorkflowRequestForLineLevel.remove(wfr);
					}
				}
			}
			LOGGER.info("re-set lstOfE911ParentWorkflowRequest ->" + lstOfE911ParentWorkflowRequest);
			lstOfAddLineWorkflowRequestForLineLevel.addAll(lstOfE911ParentWorkflowRequest);
			LOGGER.info("re-set lstOfAddLineWorkflowRequestForLineLevel after major dependent lines removal->"
					+ lstOfAddLineWorkflowRequestForLineLevel);

			kcontext.setVariable("filteredLstOfAddLineWorkflow", lstOfAddLineWorkflowRequestForLineLevel);

			if (consumerWFRequest.isOrderLevel()) {
				kcontext.setVariable("lstOfAccLevelLines", filteredListForAccountLevel);
			}

			kcontext.setVariable("lstOfWorkflowRequest", lstOfWorkflowRequestTemp);

			List<WorkFlowRequest> lstOfAddLineWorkflowRequestForLineLevelFinal = new ArrayList<WorkFlowRequest>();
			for (WorkFlowRequest wfReq : lstOfAddLineWorkflowRequestForLineLevel) {
				lstOfAddLineWorkflowRequestForLineLevelFinal.add(wfReq); // recheck
			}

			LOGGER.info("re-set lstOfAddLineWorkflowRequestForLineLevelFinal before-->"
					+ lstOfAddLineWorkflowRequestForLineLevelFinal);
			for (WorkFlowRequest wfReq : lstOfAddLineWorkflowRequestForLineLevel) {
				if (wfReq != null && "4GI".equalsIgnoreCase(wfReq.getDeviceTechnology())) {
					kcontext.setVariable("isFiveGICL", true);
					WorkFlowRequest fourgBackupWFReq = new WorkFlowRequest();
					BeanUtils.copyProperties(fourgBackupWFReq, wfReq);
					kcontext.setVariable("workflowRequest4G", fourgBackupWFReq);
					lstOfAddLineWorkflowRequestForLineLevelFinal.remove(wfReq);
				}
			}

			LOGGER.info("re-set isFiveGICL-->" + kcontext.getVariable("isFiveGICL"));
			LOGGER.info("re-set lstOfAddLineWorkflowRequestForLineLevelFinal after-->"
					+ lstOfAddLineWorkflowRequestForLineLevelFinal);

			Set<String> filteredMtnList = new HashSet<>();
			for (WorkFlowRequest wfr : lstOfAddLineWorkflowRequestForLineLevelFinal) {
				filteredMtnList.add(wfr.getMtn());
			}
			for (WorkFlowRequest wfr : lstOfChangeLineWorkflowRequestForLineLevel) {
				filteredMtnList.add(wfr.getMtn());
			}
			LOGGER.info("re-set filteredMtnList-->" + filteredMtnList);
			kcontext.setVariable("filteredMtnList", filteredMtnList);
			kcontext.setVariable("filteredLstOfAddLineWorkflow", lstOfAddLineWorkflowRequestForLineLevelFinal);
			kcontext.setVariable("filteredLstOfChangeLineWorkflow", lstOfChangeLineWorkflowRequestForLineLevel);
			lstOfWorkflowRequestUpdated.addAll(lstOfAddLineWorkflowRequestForLineLevelFinal);
			lstOfWorkflowRequestUpdated.addAll(lstOfChangeLineWorkflowRequestForLineLevel);
			LOGGER.info("re-set lstOfWorkflowRequestUpdated-->" + lstOfWorkflowRequestUpdated);
			kcontext.setVariable(BPMConstants.WORKFLOW_REQ, lstOfChangeLineWorkflowRequestForLineLevel.get(0));

			boolean accVPFlag = false;
			if (PropertyFile.getProjectProperties().get("ACC_LEVEL_VP_REQ_PP") != null) {
				accVPFlag = Boolean
						.parseBoolean(PropertyFile.getProjectProperties().get("ACC_LEVEL_VP_REQ_PP").toString());
			}
			LOGGER.info("re-set Account Level VP Flag-->" + accVPFlag);

			boolean prodParallelFlag = false;
			boolean triggerALVP = false;

			for (WorkFlowRequest workFlowReqTemp : consumerWFRequest.getLstOfWorkflowRequest()) {
				if ("Y".equalsIgnoreCase(workFlowReqTemp.getProductionParallelInd())) {
					prodParallelFlag = true;
					if (accVPFlag && Integer.parseInt(workFlowReqTemp.getLineItemNumber()) > 100) {
						kcontext.setVariable("workflowRequestALVP", workFlowReqTemp);
						triggerALVP = true;
						break;
					}
				}
			}
			LOGGER.info("re-set triggerALVP-->" + triggerALVP);
			kcontext.setVariable("triggerALVP", triggerALVP);
			kcontext.setVariable("productionParallelInd", prodParallelFlag);

			LOGGER.info("re-set Checking if Subscription Manager to be invoked");
			boolean smRequired = false;
			for (WorkFlowRequest wfr : consumerWFRequest.getLstOfWorkflowRequest()) {
				if (wfr.getSpoCategoryCodes() != null && wfr.getSpoCategoryCodes().length > 0) {
					smRequired = true;
					LOGGER.info("Subscription Manager needs to be invoked");
					break;
				}
			}
			kcontext.setVariable("callSubsManager", smRequired);

			LOGGER.info("OrderID:" + consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderId()
					+ " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId() + " ProductionParallelInd:"
					+ prodParallelFlag);

			kcontext.setVariable("orderLevel", consumerWFRequest.isOrderLevel());
			kcontext.setVariable("correlationId",
					consumerWFRequest.getLstOfWorkflowRequest().get(0).getCorrelationId());
			if (kcontext.getVariable("orderKey") == null) {
				kcontext.setVariable("orderKey",
						"orderId," + consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderId());
			}
			LOGGER.info("re-set Request type orderLevel:" + kcontext.getVariable("orderLevel"));

			BeanUtils.copyProperties(consumerWFRequestUpdated, consumerWFRequest);
			consumerWFRequestUpdated.setLstOfWorkflowRequest(null);
			consumerWFRequestUpdated.setLstOfWorkflowRequest(lstOfWorkflowRequestUpdated);
			LOGGER.info("reflow consumerWFRequestUpdated-->" + consumerWFRequestUpdated);
			kcontext.setVariable("consumerWorkflowRequest", consumerWFRequestUpdated);
			kcontext.setVariable("pid", String.valueOf(kcontext.getProcessInstance().getId()));
			kcontext.setVariable(BPMConstants.BUCKET_DT, DateTimeUtils.formatCurrentTsBasedOnFormat("yyyy-MM-dd"));

			kcontext.setVariable("ocSignalLaunched", false);
			kcontext.setVariable("is911SignalLaunched", false);
			kcontext.setVariable("vroAccTriggered", false);
			kcontext.setVariable("alvpAlreadyTriggered", false);
			kcontext.setVariable("smTriggered", false);
			kcontext.setVariable("orderDelete", false);
			kcontext.setVariable("allReceived", false);
			kcontext.setVariable("orderCompleted", false);

		} catch (Exception e) {
			LOGGER.error("Error in reSetWorkflowRequestList method. Exception : " + e);
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void verifyOCForLastCancel(ProcessContext kcontext) {
		LOGGER.info("Entering verifyOCForLastCancel() method..");
		try {
			Set<String> mtnList = (Set) kcontext.getVariable("filteredMtnList");
			LOGGER.info("filteredMtnList inside verifyOCForLastCancel:" + mtnList);
			if (kcontext.getVariable("receivedMtnList") != null) {
				Set<String> completedMtns = (Set) kcontext.getVariable("receivedMtnList");
				LOGGER.info("completedMtns inside verifyOCForLastCancel:" + completedMtns);
				if (mtnList != null && mtnList.size() > 0 && completedMtns != null && completedMtns.size() > 0
						&& mtnList.containsAll(completedMtns)) {
					LOGGER.info(completedMtns.size() + " lines already LC and other lines got LD. Proceeding to OC.");
					kcontext.setVariable("orderCompleted", true);
					kcontext.setVariable("statusCode", "OC");
					kcontext.setVariable("statusDesc", "Order Completed");
					kcontext.setVariable("preStatus", "LC");
					ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
							.getVariable("consumerWorkflowRequest");
					for (WorkFlowRequest wfreq : consumerWFRequest.getLstOfWorkflowRequest()) {
						if (mtnList.contains(wfreq.getMtn())) {
							kcontext.setVariable(BPMConstants.WORKFLOW_REQ, wfreq);
							break;
						}
					}
					//TN-IP CANCEL LINES to SOV
					if(kcontext.getVariable("omsCancelWFRListSOV")!=null){
						List<WorkFlowRequest> omsCancelWFRListSOV = (List<WorkFlowRequest>)kcontext.getVariable("omsCancelWFRListSOV");
						if(omsCancelWFRListSOV!=null && omsCancelWFRListSOV.size() > 0) {
							ConsumerWorkflowRequest consumerWFReq = new ConsumerWorkflowRequest();
							consumerWFReq.setLstOfWorkflowRequest(omsCancelWFRListSOV);
							kcontext.setVariable("cancelWorkflowRequest", consumerWFReq);
							LOGGER.info("About to vision save order for oms cancelled lines in verifyOCForLastCancel() method-->" +consumerWFReq);
						}
					}

				} else {
					LOGGER.info("Other lines yet to be released/provisioned");
				}
			}

		} catch (Exception e) {
			LOGGER.info("Exception inside verifyOCForLastCancel method: " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public void closeUpdateOrderSignal(ProcessContext kcontext, String signalKey) {
		LOGGER.info("Entering closeUpdateOrderSignal() method after DYMER expiration...");
		try {

			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
					.getVariable("consumerWorkflowRequest");
			kcontext.setVariable("isCancel", false);
			LOGGER.info("filteredMtnList:" + kcontext.getVariable("filteredMtnList"));
			LOGGER.info("releaseMtnList:" + kcontext.getVariable("releaseMtnList"));
			LOGGER.info("cancelMtnList:" + kcontext.getVariable("cancelMtnList"));
			List<WorkFlowRequest> wfrListToLineLevel = new ArrayList<WorkFlowRequest>();
			List<WorkFlowRequest> filteredList = consumerWFRequest.getLstOfWorkflowRequest().stream()
					.filter(wrkflw -> Integer.parseInt((wrkflw.getLineItemNumber())) < 101)
					.collect(Collectors.toList());
			Set<String> allMtns = new HashSet<String>();

			Set<String> releaseMtnList = (Set<String>) kcontext.getVariable("releaseMtnList");
			if (releaseMtnList != null && releaseMtnList.size() > 0) {
				allMtns.addAll(releaseMtnList);
			}

			Set<String> cancelMtnList = (Set<String>) kcontext.getVariable("cancelMtnList");
			if (cancelMtnList != null && cancelMtnList.size() > 0) {
				allMtns.addAll(cancelMtnList);
			}

			LOGGER.info("allMtns that are already released/canceled-->" + allMtns);
			/*
			 * Set<String> mtns = (Set<String>)kcontext.getVariable("mtnList");
			 * mtns.removeAll(allMtns); LOGGER.info(
			 * "mtnList to be provisioned after removing release/cancel mtns-->"
			 * +mtns);
			 */

			if (allMtns != null && allMtns.size() > 0) {
				for (WorkFlowRequest wfr : filteredList) {
					if (!allMtns.contains(wfr.getMtn())) {
						wfrListToLineLevel.add(wfr);
					}
				}
				LOGGER.info("wfrListToLineLevel after removing release/canceled lines-->" + wfrListToLineLevel);
				kcontext.setVariable("filteredLstOfWorkflow", wfrListToLineLevel);
				if (wfrListToLineLevel.size() == 0) {
					kcontext.setVariable("allReceived", true);
					LOGGER.info("All Lines Received Release/Cancel. Proceeding to End :"
							+ kcontext.getVariable("allReceived"));
				}
			} else {
				kcontext.setVariable("filteredLstOfWorkflow", filteredList);
			}
			LOGGER.info("filteredMtnList:" + kcontext.getVariable("filteredMtnList"));
			LOGGER.info("filteredLstOfWorkflow-->" + kcontext.getVariable("filteredLstOfWorkflow"));

			LOGGER.info("De-activating safe point of signalKey : " + signalKey + " and signalId : "
					+ kcontext.getVariable(signalKey));

			if (signalKey != null && kcontext.getVariable(signalKey) != null) {
				kcontext.getKieRuntime().getWorkItemManager()
				.completeWorkItem(Long.parseLong(String.valueOf(kcontext.getVariable(signalKey))), null);
			}

		} catch (Exception e) {
			LOGGER.error("Exception inside closeUpdateOrderSignal method. Exception : " + e);
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void checkLineCompletion(ProcessContext kcontext) {

		LOGGER.info("Entering checkLineCompletion() method..");
		kcontext.setVariable("allLinesCompleted", false);
		kcontext.setVariable("isBranchLineProcess", false);
		try {

			// Trunk order signal back to branch if branch is waiting
			//String  requestType = (String) kcontext.getVariable("requestType");
			if(kcontext.getVariable("isBranchOrderWaiting")==null){
				kcontext.setVariable("isBranchOrderWaiting", false);
			}
			ObjectMapper mapper = new ObjectMapper();
			String requestType = mapper.writeValueAsString(kcontext.getVariable("workflowRequest"));
			LOGGER.info("requestType --> " + requestType);
			LOGGER.info("workflow object "+ mapper.writeValueAsString(kcontext.getVariable("workflowRequest")));

			if(requestType !=null && requestType.contains("Visible")) {
				LOGGER.info("Inside the visible status check");
				VisibleBPMWorkFlow  branchBPMWorkFlow =  (VisibleBPMWorkFlow) kcontext.getVariable("workflowRequest");
				kcontext.setVariable("branchBPMWorkFlow",branchBPMWorkFlow);
				kcontext.setVariable("isBranchOrderWaiting", true);
			} else {

				WorkFlowRequest workflowRequest = (WorkFlowRequest) kcontext.getVariable("workflowRequest");
				LOGGER.info("workFlowRequest signal for MTN -->" + workflowRequest.getMtn());

				Set<String> mtnList = (Set<String>) kcontext.getVariable("filteredMtnList");
				LOGGER.info("filteredMtnList :" + mtnList);

				Set<String> completedMtnList = null;
				Set<String> completedMtnListDummy = new HashSet<String>();
				if (kcontext.getVariable("receivedMtnList") != null) {
					completedMtnList = (Set<String>) kcontext.getVariable("receivedMtnList");
				} else {
					completedMtnList = new HashSet<String>();
				}

				LOGGER.info("completedMtnList before-->" + completedMtnList);
				if(!"NULL".equalsIgnoreCase(workflowRequest.getMtn())){
					completedMtnList.add(workflowRequest.getMtn());
				} else {
					kcontext.setVariable("isAccountCompleted", true);
				}
				
				for (String mtn : completedMtnList) {
					completedMtnListDummy.add(mtn);
				}
				
				LOGGER.info("completedMtnList after-->" + completedMtnList);
				LOGGER.info("completedMtnListDummy-->" + completedMtnListDummy);
				kcontext.setVariable("receivedMtnList", completedMtnListDummy);
				LOGGER.info("kcontext.getVariable(\"receivedMtnList\")-->" + kcontext.getVariable("receivedMtnList"));
				
				List<WorkFlowRequest> omsCancelWFRListSOV = null;
				if(kcontext.getVariable("omsCancelWFRListSOV") == null){
					omsCancelWFRListSOV = new ArrayList<>();
				}else {
					omsCancelWFRListSOV = (List<WorkFlowRequest>)kcontext.getVariable("omsCancelWFRListSOV");
				}
//				if(workflowRequest.getCancelContext()!=null && workflowRequest.getCancelContext().contains("cancel")){
				if("Cancel".equalsIgnoreCase(workflowRequest.getOrderSource())){
					kcontext.setVariable("sovCancelReq", true);
					omsCancelWFRListSOV.add(workflowRequest);
					LOGGER.info("added mtn to omsCancelWFRListSOV in checkLineCompletion() method:" + workflowRequest.getMtn());
					kcontext.setVariable("omsCancelWFRListSOV", omsCancelWFRListSOV);
				}
				
				if (mtnList.size() == completedMtnListDummy.size()) {
	
					if((Boolean)kcontext.getVariable("orderLevel") && kcontext.getVariable("isAccountCompleted")==null && !(Boolean)kcontext.getVariable("productionParallelInd")){
						kcontext.setVariable("allLinesCompleted", false);
						LOGGER.info("All Lines Completed Successfully, but Account Level Line Items(s)");
					} else {
						kcontext.setVariable("allLinesCompleted", true);
						kcontext.setVariable("statusCode", "OC");
						kcontext.setVariable("statusDesc", "Order Completed");
						kcontext.setVariable("preStatus", "LC");
						LOGGER.info("All Lines Completed Successfully");
					}
					
					if(kcontext.getVariable("omsCancelWFRListSOV") == null){
						if (kcontext.getVariable("isBranchLinePending") !=null && kcontext.getVariable("isBranchLinePending") !="" && Boolean.parseBoolean(String.valueOf(kcontext.getVariable("isBranchLinePending")))) {
							// set true if branch line is not pending
							kcontext.setVariable("isBranchLineProcess", true);
							kcontext.setVariable("receivedMtnList", null);
							kcontext.setVariable("allLinesCompleted", false);
							LOGGER.info("All trunk lines are completed Successfully , Branch lines will start processing");
						}
					} else {
						ConsumerWorkflowRequest consumerWFReq = new ConsumerWorkflowRequest();
						consumerWFReq.setLstOfWorkflowRequest(omsCancelWFRListSOV);
						kcontext.setVariable("cancelWorkflowRequest", consumerWFReq);
						LOGGER.info("Vision save order for cancelled lines-->" +consumerWFReq);
					}
				}
			}
			/*
			 * Boolean isFiveGICL = (Boolean)
			 * kcontext.getVariable("isFiveGICL"); if (isFiveGICL != null &&
			 * isFiveGICL) { if
			 * ("5GI".equalsIgnoreCase(workflowRequest.getDeviceTechnology())) {
			 * kcontext.setVariable("allLinesCompleted", null); } }
			 */
		} catch (Exception e) {
			LOGGER.error("Exception inside checkLineCompletion() method :" + e.getMessage());
		}
	}

	public void signalParentProcess(ProcessContext kcontext, String ocPid) throws Exception {
		LOGGER.info("Entering into signalParentProcess() method..");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String URL = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BASE_URL.toString(),
					BPMConstants.SERVICE_ID_BPM);

			String depId = (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID);
			URL = URL + "/" + depId + "/processes/instances/" + workFlowRequest.getParentProcessInstanceId()
			+ "/signal/OC_SIGNAL";
			LOGGER.info("OC_SIGNAL URL : " + URL);

			kcontext.setVariable("url", URL);
			kcontext.setVariable("bpmAccessKey",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
			kcontext.setVariable("bpmAccessCode",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			ObjectMapper mapper = new ObjectMapper();
			String request = "{\"com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest\": "
					+ mapper.writeValueAsString(workFlowRequest) + "}";
			kcontext.setVariable(BPMConstants.CONTENT, request);
			LOGGER.info("OC SIGNAL Content is:" + request);

		} catch (Exception e) {
			LOGGER.error("Exception while signaling OC_SIGNAL" + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public void signalChildProcess(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering into signalChildProcess() method..");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String URL = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BASE_URL.toString(),
					BPMConstants.SERVICE_ID_BPM);

			CloseOutProcessUtil cop = new CloseOutProcessUtil();
			cop.getChildProcessesToAbort(kcontext, "parent");

			List<String> pidList = (List<String>) kcontext.getVariable("pidList");
			if (pidList != null && pidList.size() > 0) {
				String pid = pidList.get(0);

				String depId = (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID);
				URL = URL + "/" + depId + "/processes/instances/" + pid + "/signal/LCD_SIGNAL";
				LOGGER.info("LCD_SIGNAL URL : " + URL);

				kcontext.setVariable("url", URL);
				kcontext.setVariable("bpmAccessKey",
						PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
				kcontext.setVariable("bpmAccessCode",
						PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));
				kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT,
						PropertyFile.getInstance().getConnectionTimeOut());
				kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
				ObjectMapper mapper = new ObjectMapper();
				Map<String, Object> request = new HashMap<String, Object>();
				if(kcontext.getVariable("lcdAction")!=null) {
					request.put("action", kcontext.getVariable("lcdAction").toString());
				} else {
					request.put("action", "CLOSE");
				}
				request.put("mtn", workFlowRequest.getMtn());
				LOGGER.info("LCD_SIGNAL Content is:" + mapper.writeValueAsString(request));
				kcontext.setVariable(BPMConstants.CONTENT, mapper.writeValueAsString(request));
			} else {
				LOGGER.info("pidList is null for ProcessInstanceId" + kcontext.getProcessInstance().getId());
			}
		} catch (Exception e) {
			LOGGER.error("Exception while signaling:" + e.getMessage());
		}
	}

	public void checkForFiveGLines(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering checkForFiveGLines() method ...");
		try {
			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
					.getVariable("consumerWorkflowRequest");
			kcontext.setVariable("consumerWorkflowRequestMain", consumerWFRequest);
			boolean fivegFlag = false;
			Set<String> collectedMtns = new HashSet<String>();
			for (WorkFlowRequest wfr : consumerWFRequest.getLstOfWorkflowRequest()) {
				if (Integer.parseInt(wfr.getLineItemNumber()) < 101) {
					collectedMtns.add(wfr.getMtn());
				}
				if (wfr.getDeviceTechnology() != null && "5".equalsIgnoreCase(wfr.getDeviceTechnology())
						&& !fivegFlag) {
					fivegFlag = true;
					LOGGER.info("5G line exists. BPM Data Update MS needs to be invoked to update device Technology");
				}
			}
			LOGGER.info("collectedMtns -->" + collectedMtns);
			kcontext.setVariable("mtnList", collectedMtns);
			LOGGER.info("fivegFlag -->" + fivegFlag);
			kcontext.setVariable("fivegFlag", fivegFlag);
			checkNumberShare(kcontext,consumerWFRequest);
		} catch (Exception e) {
			LOGGER.error("Error in checkForFiveGLines method. Exception : " + e);
			throw new Exception(e);
		}
	}

	private void checkNumberShare(ProcessContext kcontext, ConsumerWorkflowRequest consumerWFRequest) throws Exception {
		LOGGER.info("Entering checkNumberShare() method ...");
		try {

			kcontext.setVariable("isNSTrunkPending", false);
			kcontext.setVariable("isNSUngraftRequire", false);
			kcontext.setVariable("isNSBranchLinePresent", false);
			boolean isTrunkPresent =false;
			boolean isBranchPresent =false;
			if (consumerWFRequest.getLstOfWorkflowRequest().size() > 0) {
				List<WorkFlowRequest> lstwfr = consumerWFRequest.getLstOfWorkflowRequest();
				for (WorkFlowRequest wfr : lstwfr) {
					List<KeyValueObject> lstlineLevelInfo = wfr.getLineLevelInfo();
					if (lstlineLevelInfo != null && lstlineLevelInfo.size() > 0) {

						boolean isActivepair =false ;
						boolean isNsSfoFound =false ;
						for (KeyValueObject keyValueObject : lstlineLevelInfo) {

							if (!StringUtils.isEmpty(keyValueObject.getKey())
									&& !StringUtils.isEmpty(keyValueObject.getValue())) {
								if (keyValueObject.getKey().equalsIgnoreCase("activePair")
										&& Boolean.parseBoolean(keyValueObject.getValue().toString())) {
									isActivepair=true;
								} else if (keyValueObject.getKey().equalsIgnoreCase("isNsSfoFound")
										&& Boolean.parseBoolean(keyValueObject.getValue().toString())) {
									isNsSfoFound=true;
								}
							}

						}
						for (KeyValueObject keyValueObject : lstlineLevelInfo) {
							if (!StringUtils.isEmpty(keyValueObject.getKey())
									&& !StringUtils.isEmpty(keyValueObject.getValue())) {
								if (keyValueObject.getKey().equalsIgnoreCase("isTrunkPending")
										&& Boolean.parseBoolean(keyValueObject.getValue().toString())) {
									kcontext.setVariable("isNSTrunkPending", true);
								} else if (keyValueObject.getKey().equalsIgnoreCase("numberShareIndicator")
										&& (keyValueObject.getValue().equalsIgnoreCase("U")
												|| keyValueObject.getValue().equalsIgnoreCase("D")
												|| keyValueObject.getValue().equalsIgnoreCase("T")
												|| keyValueObject.getValue().equalsIgnoreCase("X")
												|| keyValueObject.getValue().equalsIgnoreCase("G")
												|| keyValueObject.getValue().equalsIgnoreCase("B"))
										&& wfr.getOrderSource().toString().equalsIgnoreCase("Modify")) {

									if(isActivepair && isNsSfoFound && Boolean.parseBoolean(wfr.getChangePlanFeatures().toString()) ==true) {
										kcontext.setVariable("isNSUngraftRequire", true);
									}else if ( isActivepair && (Boolean.parseBoolean(wfr.getChangeDevice().toString()) ==true  ||
											Boolean.parseBoolean(wfr.getChangeMtn().toString()) ==true)) {
										kcontext.setVariable("isNSUngraftRequire", true);
									}
								} else if (keyValueObject.getKey().equalsIgnoreCase("isBranchLine")
										&& Boolean.parseBoolean(keyValueObject.getValue().toString())) {
									isBranchPresent=true;
								}
								else if (keyValueObject.getKey().equalsIgnoreCase("isTrunkLine")
										&& Boolean.parseBoolean(keyValueObject.getValue().toString())) {
									isTrunkPresent=true;
								}
							}
						}
					}
				}

			}
			if(isTrunkPresent && isBranchPresent ){
				kcontext.setVariable("isNSBranchLinePresent", true);

			}
			LOGGER.info("isNSBranchLinePresent --> {}   " + kcontext.getVariable("isNSBranchLinePresent"));
			LOGGER.info("isNSUngraftRequire --> {}   " + kcontext.getVariable("isNSUngraftRequire"));
			LOGGER.info("isNSTrunkPending --> {}   " + kcontext.getVariable("isNSTrunkPending"));
		} catch (Exception e) {
			LOGGER.error("Error in checkNumberShare method. Exception : " + e);
		}
	}

	/*
	 * @SuppressWarnings("unchecked") public void
	 * release911Parent(ProcessContext kcontext) throws Exception { LOGGER.info(
	 * "inside release911Parent() method"); List<WorkFlowRequest> filteredList =
	 * new ArrayList<WorkFlowRequest>(); try{ ConsumerWorkflowRequest
	 * consumerWFRequest = (ConsumerWorkflowRequest) kcontext
	 * .getVariable("consumerWorkflowRequest");
	 * 
	 * Map<String, Object> resp = (Map<String, Object>)
	 * kcontext.getVariable("result"); String e911MtnLc =
	 * (String)resp.get("mtnLC"); LOGGER.info("e911MtnLc-->"+e911MtnLc);
	 * kcontext.setVariable("e911Status", resp.get("statusType").toString());
	 * if("LC".equalsIgnoreCase(resp.get("statusType").toString())){
	 * for(WorkFlowRequest wfr : consumerWFRequest.getLstOfWorkflowRequest()) {
	 * if(e911MtnLc.equalsIgnoreCase(wfr.getMtn()) &&
	 * !"9".equalsIgnoreCase(wfr.getDeviceTechnology())){ WorkFlowRequest
	 * wfrToRelease = new WorkFlowRequest(); Map<String,String> map =
	 * (HashMap<String,String>)kcontext.getVariable("mtnLnItmCombo");
	 * LOGGER.info("LineItem to release-->" +wfr.getLineItemNumber() +
	 * " and corresponding e911LineItmNo:"+map.get(wfr.getMtn()));
	 * BeanUtils.copyProperties(wfr, wfrToRelease);
	 * wfrToRelease.setE911LineItemNumber(map.get(wfr.getMtn()));
	 * filteredList.add(wfrToRelease); break; } } }else{ LOGGER.info(
	 * "Not initating parent as the line is already "+resp.get("statusType")); }
	 * LOGGER.info("filteredList to release-->"+filteredList); }catch(Exception
	 * e){ LOGGER.error("Exception while release911Parent " +e.getMessage()); }
	 * kcontext.setVariable("filteredLstOfWorkflow", filteredList);
	 * 
	 * }
	 * 
	 * public void e911CompletionParams(ProcessContext kcontext, String type)
	 * throws Exception { LOGGER.info("inside e911CompletionParams() method");
	 * try{ ObjectMapper mapper = new ObjectMapper(); WorkFlowRequest wfr =
	 * (WorkFlowRequest) kcontext.getVariable("workflowRequest"); Map<String,
	 * Object> resp = new HashMap<String, Object>(); resp.put("statusType",
	 * type); resp.put("mtnLC", wfr.getMtn());
	 * kcontext.setVariable("e911SignalResp", resp);
	 * LOGGER.info("e911SignalResp:" +mapper.writeValueAsString(resp));
	 * }catch(Exception e){ LOGGER.error("Exception while e911CompletionParams "
	 * +e.getMessage()); } }
	 */

	public void signalProcess(ProcessContext kcontext, String pid, String sid, Object payload) throws Exception {
		LOGGER.info("Entering into signalProcess() method..");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String URL = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BASE_URL.toString(),
					BPMConstants.SERVICE_ID_BPM);

			String depId = (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID);
			URL = URL + "/" + depId + "/processes/instances/" + pid + "/signal/" + sid;
			LOGGER.info(sid + " SIGNAL URL : " + URL);

			kcontext.setVariable("url", URL);
			kcontext.setVariable("bpmAccessKey",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
			kcontext.setVariable("bpmAccessCode",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			ObjectMapper mapper = new ObjectMapper();
			LOGGER.info(sid + " SIGNAL Content is:" + mapper.writeValueAsString(payload));
			kcontext.setVariable(BPMConstants.CONTENT, mapper.writeValueAsString(payload));

		} catch (Exception e) {
			LOGGER.error("Exception while signaling:" + e.getMessage());
		}
	}

	public void initiateConsumerAddLineProcess(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering into initiateConsumerAddLineProcess() method..");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String URL = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BASE_URL.toString(),
					BPMConstants.SERVICE_ID_BPM);

			String deplomentId = (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID);
			String processId = "MLMOChangeLine.ChangeLine";
			URL = URL + "/" + deplomentId + "/processes/" + processId + "/instances";
			LOGGER.info("MLMOChangeLine Process Initiation URL : " + URL);

			kcontext.setVariable("url", URL);
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT,
					PropertyFile.getProjectProperties().get(BPMConstants.CAL_CONNECTIONTIMEOUT));
			kcontext.setVariable(BPMConstants.READ_TIME_OUT,
					PropertyFile.getProjectProperties().get(BPMConstants.CAL_READTIMEOUT));
			kcontext.setVariable("bpmAccessKey",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
			kcontext.setVariable("bpmAccessCode",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));
			/*
			 * KieRequestObject kieReqObj = new KieRequestObject();
			 * kieReqObj.setWorkflowRequest(workFlowRequest);
			 */

			ObjectMapper obj = new ObjectMapper();
			String request = "{\"workflowRequest\":{\"com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest\": "
					+ obj.writeValueAsString(workFlowRequest) + "}}";
			kcontext.setVariable(BPMConstants.CONTENT, request);
			LOGGER.info("ConsumerAddLine Content is : " + request);

		} catch (Exception e) {
			LOGGER.error("Exception while intiating ConsumerAddLine Process. Exception:" + e);
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	public void validateConsumerAddLineResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering into validateConsumerAddLineResponse() method..");
		kcontext.setVariable("flag", false);
		try {
			String processinsatnceid = (String) kcontext.getVariable("result");
			if (processinsatnceid != null) {
				LOGGER.info("ConsumerAddLine Process Instance id ->" + processinsatnceid);
				kcontext.setVariable("flag", true);
			} else {
				LOGGER.info("Unable to initiate ConsumerAddLine process");
				throw new Exception("Unable to initiate ConsumerAddLine process");
			}

		} catch (Exception e) {
			LOGGER.error("Exceptiom inside validateConsumerAddLineResponse() method.." + e.getMessage());
			throw new Exception("Exceptiom inside validateConsumerAddLineResponse() method.." + e.getMessage());
		}
	}

	public void updateParentProcessInstanceId(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering updateParentProcessInstanceId() method..");
		boolean flag = false;
		Connection connection = null;
		PreparedStatement preparedStmt = null;
		try{
			String pid = (String) kcontext.getVariable("processInstanceId");
			String ppid = (String) kcontext.getVariable("parentProcessInstanceId");
			
			WorkFlowRequest workflowRequest = null;
			if (kcontext.getVariable("consumerWorkflowRequest") != null) {
				ConsumerWorkflowRequest cwfReq = (ConsumerWorkflowRequest) kcontext
						.getVariable("consumerWorkflowRequest");
				workflowRequest = cwfReq.getLstOfWorkflowRequest().get(0);
				kcontext.setVariable("workflowRequest", workflowRequest);
			} else if (kcontext.getVariable("workflowRequest") != null) {
				workflowRequest = (WorkFlowRequest) kcontext.getVariable("workflowRequest");
			} else {
				workflowRequest = new WorkFlowRequest(); 
			}
			LOGGER.info("Updating PPID for orderId:" +workflowRequest.getOrderId());
			LOGGER.info("PPID:"+ppid);
			LOGGER.info("PID:"+pid);
			String query = "update processinstancelog set parentprocessinstanceid = ? where processinstanceid = ?";
			connection = PropertyFile.getPostgresConnection();
			preparedStmt = connection.prepareStatement(query);
			
			preparedStmt.setLong(1, Long.parseLong(ppid));
			preparedStmt.setLong(2, Long.parseLong(pid));
			
			preparedStmt.executeUpdate();
		    LOGGER.info("ParentProcessInstanceId had been updated for "+pid);
		    flag = true;
		} catch(Exception e){
			LOGGER.error("Exception inside updateParentProcessInstanceId() method. Exception:" +e);
			e.printStackTrace();
			CompensationUtil compUtil = new CompensationUtil();
			compUtil.setCompensationObject(kcontext, "BPMPPIDERR01", "BPM", "Exception while Updating PPID for MLMO", "BPM", "compCount");
		}
		kcontext.setVariable("flag", flag);
	}

	/**
	 * This method is used to set all the variable values required
	 * in Consumer POST LC Workflow
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateLinePostLCRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiateLinePostLCRequest() method ...");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			Boolean prodParallelFlag = (Boolean) kcontext.getVariable("productionParallelInd");
			kcontext.setVariable("orderKey", "orderId," + workFlowRequest.getOrderId());

			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId() + " ProductionParallelInd:"
					+ prodParallelFlag);
		} catch (Exception e) {
			LOGGER.error("Error in initiateLinePostLCRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	private long recalculateDueDate(ConsumerWorkflowRequest consumerWFRequest,ProcessContext kcontext) {
		LOGGER.info("Inside re-calculateDueDate() method.....");
		long reCalTimer = 1;
		try {

			String orderDueDate = consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderDueDate();
			SimpleDateFormat dateFormat = orderDueDate.length() > 10 ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
					: new SimpleDateFormat("yyyy-MM-dd");

			//Commented orderDateTime as we are going to consider currentDateTime from FEB ER
			//String orderDateTime = consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderDateTime();
			kcontext.setVariable("dueDate", orderDueDate);
			Date cd = dateFormat.parse(dateFormat.format(new Date()));
			Date dd = dateFormat.parse(orderDueDate);
			int value = dd.compareTo(cd);
			if (value == 0) {
				// default timer 1ms 
				LOGGER.info("Recal -Due Date is equal to Order Date");
			}else {
				// Below code is for Testing env-start
				LOGGER.info("Recal -Due Date is greater than Order Date");
				Date dueDate = dateFormat.parse(orderDueDate);
				LOGGER.info("dueDate after parsing-->" + dueDate);
				SimpleDateFormat dateToFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
				String duedate = dateToFormat.format(dueDate);
				LOGGER.info("duedate after formatting-->" + duedate);
				LocalDateTime dueLocalDateTime = LocalDateTime.parse(duedate).withHour(0).withMinute(0).withSecond(0);
				// end

				// add the below 2 lines to test in local and comment above
				//
				// lines
				/*
				 * DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
				 * "yyyy-MM-dd HH:mm:ss.SSSSSS"); LocalDateTime dueLocalDateTime =
				 * LocalDateTime.parse(orderDueDate, formatter);
				 */
				// end
				LOGGER.info("dueLocalDateTime-->" + dueLocalDateTime);
				LocalDateTime currentDate = null;
				String billingInstance = consumerWFRequest.getLstOfWorkflowRequest().get(0).getBillingInstance();
				if (billingInstance != null) {
					if ("E".equalsIgnoreCase(billingInstance) || "N".equalsIgnoreCase(billingInstance)) {
						LOGGER.info("Eastern Timezone as billingInstance is " + billingInstance);
						currentDate = LocalDateTime.now(ZoneId.of("America/New_York"));
					} else if ("W".equalsIgnoreCase(billingInstance) || "B".equalsIgnoreCase(billingInstance)) {
						currentDate = LocalDateTime.now(ZoneId.of("America/Los_Angeles"));
						LOGGER.info("Pacific Timezone as billingInstance is " + billingInstance);
					} else {
						LOGGER.info("Considering the default Timezone as billingInstance is " + billingInstance);
						currentDate = LocalDateTime.now(ZoneId.systemDefault());
					}
				} else {
					currentDate = LocalDateTime.now(ZoneId.systemDefault());
				}
				LOGGER.info("recal -currentDate -->" + currentDate);
				Duration duration = Duration.between(currentDate, dueLocalDateTime);
				LOGGER.info("recal - duration -->" + duration);
				LOGGER.info("recal - duration in seconds -->" + duration.getSeconds());
				reCalTimer = duration.getSeconds();
			}

		} catch (Exception e) {
			LOGGER.error("Exception while re calculating due date" + e);
		}

		return reCalTimer;
	}

}
