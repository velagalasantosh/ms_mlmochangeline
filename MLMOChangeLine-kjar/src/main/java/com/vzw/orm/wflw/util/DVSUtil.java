package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.beanutils.BeanUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.ordvsacc.domain.request.DvsBPMWorkFlow;
import com.vzw.orm.ordvsacc.domain.request.DvsLineItemRequest;
import com.vzw.orm.ordvsacc.domain.request.DvsWorkFlowRequest;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Pratyusha Pooskuru
 *
 */
public class DVSUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(DVSUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * the Visible Disconnect Request
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateDvsRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiateDvsRequest() method ...");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);

			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String dvsAccessUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.DVS_ACCESS_URL.toString(),
					BPMConstants.SERVICE_ID_DVS_ACCESS);
			LOGGER.info("DVS Access MS URL is : " + dvsAccessUrl);
			kcontext.setVariable(BPMConstants.URL, dvsAccessUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			DvsLineItemRequest dvsLineItemRequest = new DvsLineItemRequest();

			DvsWorkFlowRequest dvsWorkFlowRequest = new DvsWorkFlowRequest();
			BeanUtils.copyProperties(dvsWorkFlowRequest, workFlowRequest);
			dvsWorkFlowRequest.setWfReqid(kcontext.getProcessInstance().getProcessId());
			
			dvsLineItemRequest.setWorkFlowRequestObject(dvsWorkFlowRequest);
			String serviceName = (String) kcontext.getVariable("dvsApiServiceName");
			String subServiceName = (String) kcontext.getVariable("dvsApiSubServiceName");
			dvsLineItemRequest.setServiceName(serviceName);
			dvsLineItemRequest.setSubServiceName(subServiceName);;
			DvsBPMWorkFlow bpmWorkflow = new DvsBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			LOGGER.info("deploymentId-->"
					+ (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			bpmWorkflow.setDeploymentId(
					(String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));

			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(
						PropertyFile.getInstance().getClusterId() + "-" + PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			dvsLineItemRequest.setBpmWorkFlowObject(bpmWorkflow);

			kcontext.setVariable(BPMConstants.CONTENT, dvsLineItemRequest);
			ObjectMapper mapper = new ObjectMapper();
			LOGGER.info("DVS Access Request Content is : " + mapper.writeValueAsString(dvsLineItemRequest));
		} catch (Exception e) {
			LOGGER.error("Error in initiateDvsRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the sync response received from Visible
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateDvsAccessResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validateDvsAccessResponse() method ...");

		boolean flag = false;

		try {
//			VisibleInterimRes visibleResponse = (VisibleInterimRes) (kcontext.getVariable(BPMConstants.RESULT));
//
//			if (null != visibleResponse) {
//				LOGGER.info("Visible Sync Response is : " + visibleResponse + " for ProcessInstanceId: "
//						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
//						+ kcontext.getProcessInstance().getParentProcessInstanceId());
//				String status = visibleResponse.getStatus();
//				if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
//					flag = true;
//					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
//				} else {
//					// Call Compensation
//					LOGGER.info("ErrorCode : " + visibleResponse.getStatusCode()
//							+ ", Error Source : VISIBLE , Error Details : " + visibleResponse.getStatusDesc());
//					CompensationUtil compUtil = new CompensationUtil();
//					compUtil.setCompensationObject(kcontext, visibleResponse.getStatusCode(), "Visible",
//							visibleResponse.getStatusDesc(), null, BPMConstants.COMP_COUNT);
//				}
//			} else {
//				// Call Compensation
//				LOGGER.info("Response object from Visible is NULL for ProcessInstanceId: "
//						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
//						+ kcontext.getProcessInstance().getParentProcessInstanceId());
//				CompensationUtil compUtil = new CompensationUtil();
//				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
//						"Response Object from Micro Service is NULL", null, BPMConstants.COMP_COUNT);
//			}
		} catch (Exception e) {
			flag = false;
			kcontext.setVariable(BPMConstants.FLAG, flag);
			LOGGER.error("Exception while validating response from DVS Access MS call. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
}
