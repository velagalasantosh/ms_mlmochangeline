package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.apache.commons.beanutils.BeanUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.tnip.domain.CustomerViewUpdateOrderRequest;
import com.vzw.orm.tnip.domain.CustomerViewUpdateOrderResponse;
import com.vzw.orm.tnip.domain.TnipBPMWorkFlow;
import com.vzw.orm.tnip.domain.TnipWorkFlowRequest;
import com.vzw.orm.tnip.domain.ExceptionInfo;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class ViewUpdateOrderUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ViewUpdateOrderUtil.class);

	/**
	 * This method is used to set all the variable values required to call Customer View Update Order Request
	 * 
	 * @param kcontext
	 * @param eventCode
	 * @param eventDesc
	 * @throws Exception
	 */
	public void initiateViewUpdateOrderRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateViewUpdateOrderRequest() method ...");
		try
		{
            WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
            TnipWorkFlowRequest omniWorkFlowRequest = new TnipWorkFlowRequest();
			//converting workflow object to provisioning workflow object
			BeanUtils.copyProperties(omniWorkFlowRequest, workFlowRequest);
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			
			kcontext.setVariable(BPMConstants.FLAG, false);

			ServiceProvider bpmUtil = new ServiceProvider();
			String viewOrderUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VW_UPD_ORD_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			LOGGER.debug("Customer View Update Order URL is : " + viewOrderUrl);
			kcontext.setVariable(BPMConstants.URL, viewOrderUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			CustomerViewUpdateOrderRequest viewOrderRequest = new CustomerViewUpdateOrderRequest();

			TnipBPMWorkFlow bpmWorkflow = new TnipBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			viewOrderRequest.setBpmWorkFlowObject(bpmWorkflow);
			viewOrderRequest.setWorkFlowRequestObject(omniWorkFlowRequest);
			ObjectMapper obj = new ObjectMapper();
			kcontext.setVariable(BPMConstants.CONTENT, viewOrderRequest);
			LOGGER.info("Customer View Update Order Request Content is : " +obj.writeValueAsString(viewOrderRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateViewUpdateOrderRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from Customer View Update Order API
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateViewUpdateOrderResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateViewUpdateOrderResponse() method ...");

		boolean flag = false;
		try
		{
			CustomerViewUpdateOrderResponse viewOrderResponse = (CustomerViewUpdateOrderResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != viewOrderResponse)
			{
				LOGGER.info("Customer View Update Order Response is : " + viewOrderResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = viewOrderResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					// Call Compensation
					ExceptionInfo expInfo = viewOrderResponse.getExceptionInfo();
					LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : " + expInfo.getErrorSource() + ", Error Details : " + expInfo.getErrorDeatils());
					if("99".equalsIgnoreCase(expInfo.getErrorCode()))
					{
						CompensationUtil compUtil = new CompensationUtil();
						compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), "OMNI", expInfo.getErrorDeatils(),
								null, BPMConstants.COMP_COUNT);
					}
					else
					{
						flag = true;
						kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					}
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object VipInsertEcreditNoteResponse from Customer View Update Order API is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}

		}
		catch (Exception e)
		{
			LOGGER.error("Exception while validating response received from Customer View Update Order. Exception : " + e);
			throw new Exception(e);
		}

		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

}