/**
 * 
 */
package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.vendorprovisioning.domain.common.VpBPMWorkFlow;
import com.vzw.orm.vendorprovisioning.domain.common.VpExceptionInfo;
import com.vzw.orm.vendorprovisioning.domain.common.VpMtasProducerRequest;
import com.vzw.orm.vendorprovisioning.domain.common.VpMtasProducerResponse;
import com.vzw.orm.vendorprovisioning.domain.common.VpWorkFlowRequest;

/**
 * @author sankuvi
 *
 */
public class VpIndicatorUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(VpIndicatorUtil.class);

	/**
	 * This method is used to set all the variable values required to call
	 * Vendor Provisioning MS to get the VP Indicator whether to call VP or not
	 * in Consumer Workflow
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateVpIndRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateVpIndRequest() method ...");
		try
		{
			kcontext.setVariable("callVp", false);
			kcontext.setVariable("callAmVp", false);

			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			// setting features to workflowRequest object as part of V-P
			VpWorkFlowRequest vpWorkFlowRequest = new VpWorkFlowRequest();
			// converting workflow object to provisioning workflow object
			BeanUtils.copyProperties(vpWorkFlowRequest, workFlowRequest);
			vpWorkFlowRequest.setFeatures((String) kcontext.getVariable("features"));

			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			kcontext.setVariable(BPMConstants.FLAG, false);

			ServiceProvider bpmUtil = new ServiceProvider();
			String url = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VP_IND_URL.toString(),
					BPMConstants.SERVICE_ID_VENDOR_PROV);
			LOGGER.debug("VENDOR-PROVISIONING URL is : " + url);
			kcontext.setVariable(BPMConstants.URL, url);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			VpBPMWorkFlow bpmWorkflow = new VpBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setDeploymentId(
					(String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			VpMtasProducerRequest vpRequest = new VpMtasProducerRequest();
			vpRequest.setBpmWorkFlowObject(bpmWorkflow);
			vpRequest.setProvisioningRequestType("Vendor Provisioning");
			vpRequest.setWorkFlowRequestObject(vpWorkFlowRequest);
			kcontext.setVariable(BPMConstants.CONTENT, vpRequest);
			ObjectMapper obj = new ObjectMapper();
			LOGGER.info("VENDOR PROVISIONING Request Content is : " + obj.writeValueAsString(vpRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateVpIndRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from Vendor
	 * Provisioning
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateVpIndResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateVpIndResponse() method ...");
		boolean flag = false;
		boolean callVp = false;
		boolean callAmVp = false;
		try
		{
			VpMtasProducerResponse vpResponse = (VpMtasProducerResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != vpResponse)
			{
				LOGGER.info("VENDOR PROVISIONING Response is : " + vpResponse + " for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String deviceProreqStatus = vpResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(deviceProreqStatus))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					LOGGER.info("VpIndicator is : " + vpResponse.getVpInd());
					if (vpResponse.getVpInd() != null && vpResponse.getVpInd().toUpperCase().contains("VP"))
					{
						String[] splitList = vpResponse.getVpInd().split(",");
						for (String ind : splitList)
						{
							if (ind.equalsIgnoreCase("MVP"))
							{
								callVp = true;
								LOGGER.info("VP is enabled");
							}
							else if (ind.equalsIgnoreCase("AMVP"))
							{
								callAmVp = true;
								LOGGER.info("AMVP is enabled");
							}
						}
					}
					kcontext.setVariable("callVp", callVp);
					kcontext.setVariable("callAmVp", callAmVp);
				}
				else
				{
					VpExceptionInfo expInfo = vpResponse.getExceptionInfo();
					LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : " + expInfo.getErrorSource()
							+ ", Error Details : " + expInfo.getErrorDeatils());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), expInfo.getErrorSource(),
							expInfo.getErrorDeatils(), null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				LOGGER.info("Response Object from VpIndicator is NULL for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
						"Response Object from Micro Service is NULL", null, BPMConstants.COMP_COUNT);
			}

		}
		catch (Exception e)
		{
			LOGGER.error(
					"Exception while validating response received from VENDOR PROVISIONING VpIndicator. Exception : "
							+ e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

}