package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.ConsumerWorkflowRequest;

/**
 * This class is used to make rest call to put the future dated or wait timers
 * 
 * @author Vinodh Sankuru
 *
 */
public class FDTimerUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(FDTimerUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * Future Dated Timer
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateFDTimerRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiateFDTimerRequest() method ...");
		try {
			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
					.getVariable(BPMConstants.CONSUMERWFLREQUEST);
			
			kcontext.setVariable(BPMConstants.WORKFLOW_REQ, consumerWFRequest.getLstOfWorkflowRequest().get(0));

			LOGGER.info("OrderID:" + consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderId()
					+ " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();

			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			getPayloadBatchTrigger(kcontext, consumerWFRequest);
			
			String fdUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.FD_URL.toString(),
					BPMConstants.SERVICE_ID_SPRING_BATCH);
			LOGGER.info("Future Dated Timer URL " + fdUrl);
			kcontext.setVariable(BPMConstants.URL, fdUrl);
			kcontext.setVariable(BPMConstants.REST_METHOD, "POST");
			String orderType=consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderType();
			kcontext.setVariable("FDListener_skip" ,false);
	            if(orderType !=null && orderType !="" && ( orderType.equalsIgnoreCase("ModifyPendingOrder") || orderType.equalsIgnoreCase("ModifyPendingOrderWithAccount"))){
	            kcontext.setVariable("FDListener_skip" ,true);
	        }
		} catch (Exception e) {
			LOGGER.error("Error in initiateFDTimerRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}


	private void getPayloadBatchTrigger(ProcessContext kcontext, ConsumerWorkflowRequest consumerWFRequest)
			throws JsonProcessingException {
		Map<String, Object> inputRequest = new HashMap<>();
		inputRequest.put("orderId", consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderId());
		inputRequest.put("deploymentId", kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
		inputRequest.put("processInstanceId", kcontext.getProcessInstance().getId());
		inputRequest.put("BillingInstance", consumerWFRequest.getLstOfWorkflowRequest().get(0).getBillingInstance());
		inputRequest.put("processDefId", kcontext.getProcessInstance().getProcessId());
		inputRequest.put("parentProcessDefId", kcontext.getProcessInstance().getParentProcessInstanceId());

		if (PropertyFile.getInstance().isConsumerClustered()) {
			inputRequest.put("bpmClusterId", PropertyFile.getInstance().getClusterId() + "-"
					+ PropertyFile.getInstance().getDataCenter());
		} else {
			inputRequest.put("bpmClusterId", PropertyFile.getInstance().getDataCenter());
		}
		
		List<Map<String, Object>> lstSgnals = new ArrayList<>();
		Map<String, Object> signal = new HashMap<>();

		signal.put("signalId", "FD_SIGNAL");
		signal.put("start", consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderDueDate());
		signal.put("triggerType", "day");
		lstSgnals.add(signal);
		inputRequest.put("signals", lstSgnals);
		ObjectMapper obj = new ObjectMapper();
		kcontext.setVariable(BPMConstants.CONTENT, obj.writeValueAsString(inputRequest));
		LOGGER.info("Future Dated Timer Request Content is : " + obj.writeValueAsString(inputRequest));

	}

	/**
	 * This method is used to validate the response received from Future Dated
	 * Timer Process
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void validateFDTimerResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validateFDTimerResponse() method ...{}", kcontext.getVariable(BPMConstants.RESULT));
		boolean flag = false;
		try {
			Map<String, Object> qout = (Map<String, Object>) (kcontext.getVariable(BPMConstants.RESULT));
			LOGGER.info("Future Dated Response is : " + qout + " for ProcessInstanceId: "
					+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			if (null != qout) {
				LOGGER.info("Future Dated Response is : " + qout + " for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String qoutStatus = (String) qout.get("statusCode");
				if ("00".equalsIgnoreCase(qoutStatus)) {
					LOGGER.info("Future Dated Timer Response is Success.");
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				} else {
					LOGGER.info("Calling Compensation with statusCode:" + (String) qout.get("statusCode")
							+ " and statusDesc :" + (String) qout.get("statusMessage"));
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, (String) qout.get("statusCode"), "BPM",
							(String) qout.get("statusMessage"), null, BPMConstants.COMP_COUNT);
				}
			} else {
				LOGGER.info("Response Object from Future Dated Timer is NULL for ProcessInstanceId:"
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId:"
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR01", "BPM",
						"Response Object from Future Dated Process is NULL", null, BPMConstants.COMP_COUNT);
			}
		} catch (Exception e) {
			flag = false;
			LOGGER.error("Exception while validating response from FDTimer Rest Call. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
		LOGGER.info("Exiting validateFDTimerResponse() method ...");
	}

}