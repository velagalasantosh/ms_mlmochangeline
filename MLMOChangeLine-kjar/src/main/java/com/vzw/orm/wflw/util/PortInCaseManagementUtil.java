package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.beanutils.BeanUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.common.core.util.BillingSysIdUtil;
import com.vzw.common.core.util.StringUtils;
import com.vzw.common.core.util.BillingSysIdUtil.BillSysId;
import com.vzw.orm.bpmi.domain.objects.consumer.KeyValueObject;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.bpmi.domain.objects.fiveg.FiveGFCLWorkflowRequest;
import com.vzw.orm.bpmi.domain.objects.fiveg.LineItems;
import com.vzw.orm.peganotifier.domain.common.FiveGDisconnect;
import com.vzw.orm.peganotifier.domain.common.Notification5GRequest;
import com.vzw.orm.peganotifier.domain.common.Notification5GResponseDomain;
import com.vzw.orm.peganotifier.domain.common.OrderLineItem;
import com.vzw.orm.peganotifier.domain.common.PegaNotifRequest;
import com.vzw.orm.peganotifier.domain.common.PegaNotiflResponseDomain;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Pratyusha Pooskuru
 *
 */
public class PortInCaseManagementUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(PortInCaseManagementUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * the Port In Case Management Request
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiatePortInCaseMgmtRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiatePortInCaseMgmtRequest() method ...");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			LOGGER.info("OrderNumber:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			//LineItems lineItem = (LineItems) kcontext.getVariable("lineItem");
			
			ServiceProvider bpmUtil = new ServiceProvider();
			String caseManagementUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.PORT_IN_CASE_MGMT_URL.toString(),
					BPMConstants.SERVICE_ID_PORT_IN_CASE_MGMT);
			LOGGER.info("PORT IN CASE MGMT MS URL is : " + caseManagementUrl);
			kcontext.setVariable(BPMConstants.URL, caseManagementUrl);
			kcontext.setVariable(BPMConstants.RESULT_CLASS, "com.vzw.orm.peganotifier.domain.common.Notification5GResponseDomain");
			
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			Notification5GRequest notification5GRequest = new Notification5GRequest();
			FiveGDisconnect fiveGDisconnect = new FiveGDisconnect();
			BeanUtils.copyProperties(fiveGDisconnect, workFlowRequest);
			fiveGDisconnect.setOrderNumber(workFlowRequest.getOrderId());
			fiveGDisconnect.setAcctNo(workFlowRequest.getAccountNumber()); // MS will trim to remove the 0000s
			fiveGDisconnect.setSource("OMS");
			fiveGDisconnect.setDvsClientId(""); // For Actual Value Case Management will make the PNO Call
			fiveGDisconnect.setBillSysId(BillingSysIdUtil.getBillSysId(workFlowRequest.getBillingInstance()).toString());
			
			List<OrderLineItem> requestLineItems = new ArrayList<>();
			OrderLineItem  orderLineItem = new OrderLineItem();
			orderLineItem.setAcctNo(workFlowRequest.getAccountNumber()); // MS will trim to remove the 0000s
			if(null!=kcontext.getVariable("triggerCaseManagement") && true == (Boolean)kcontext.getVariable("triggerCaseManagement")) {
				orderLineItem.setOrderType("CPC");
				orderLineItem.setSubServiceName("");
			} else {
				orderLineItem.setOrderType(BPMConstants.PORT_IN_ORDER_TYPE);
				orderLineItem.setSubServiceName(BPMConstants.PORT_IN_SUB_SERVICE);
			}
			orderLineItem.setLineItemNo(workFlowRequest.getLineItemNumber());
			
			orderLineItem.setLineItemTypCd("M"); //default to A in case line level info is missing
			List<KeyValueObject> lstlineLevelInfo = workFlowRequest.getLineLevelInfo();
			if (lstlineLevelInfo != null && lstlineLevelInfo.size() > 0) {
				for (KeyValueObject keyValueObject : lstlineLevelInfo) {
					if (!StringUtils.isEmpty(keyValueObject.getKey())
							&& !StringUtils.isEmpty(keyValueObject.getValue())) {
						if (keyValueObject.getKey().equalsIgnoreCase("lnItmTypCd")) {
							orderLineItem.setLineItemTypCd(keyValueObject.getValue());
						} 
					}
				}
			}
				
			orderLineItem.setCorrelationId(workFlowRequest.getCorrelationId());
			orderLineItem.setMdn(workFlowRequest.getMtn());
			if(null!=kcontext.getVariable("triggerCaseManagement") && true == (Boolean)kcontext.getVariable("triggerCaseManagement")) {
				//don't set OSP for Calling Plan change Pega Notification
				//set line status code instead
				LOGGER.info("CPC Status Code : " + (String)kcontext.getVariable(BPMConstants.STATUS_CODE));
				orderLineItem.setLnItmStatCd((String)kcontext.getVariable(BPMConstants.STATUS_CODE)); //use the correct setter from Case Mgmt

			} else {
				orderLineItem.setOsp(""); // Need to send Default Value For Actual Value Case Management will make the PNO Call
			}
			requestLineItems.add(orderLineItem);
			
	        fiveGDisconnect.setLineItems(requestLineItems);
	        notification5GRequest.setFiveGDisconnect(fiveGDisconnect);
	        notification5GRequest.setProductionParallelInd(workFlowRequest.getProductionParallelInd());
			kcontext.setVariable(BPMConstants.CONTENT, notification5GRequest);
			ObjectMapper mapper = new ObjectMapper();
			LOGGER.info("Port In Case Mgmt Request Content is : " + mapper.writeValueAsString(notification5GRequest));
		} catch (Exception e) {
			LOGGER.error("Error in initiateCaseMgmtRequest method for Port In. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from Case Mgmt for Port In
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validatePortInCaseMgmtResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validatePortInCaseMgmtResponse() method ...");

		boolean flag = false;

		try {
			Notification5GResponseDomain notification5GResponse = (Notification5GResponseDomain) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != notification5GResponse) {
				LOGGER.info("Port in Case Mgmt Pega Notification Response is : " + notification5GResponse + " for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId()
						);
				String status = notification5GResponse.getStatusDescription();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				} else {
				
					// Call Compensation
					LOGGER.info("ErrorCode: " + notification5GResponse.getStatusCode()
							+ ", Error Source: Port in CASE-MGMT , Error Details: " + notification5GResponse.getStatusDescription());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, notification5GResponse.getStatusCode(), "Visible",
							notification5GResponse.getStatusDescription(), null, BPMConstants.COMP_COUNT);
				}
			} else {
				
				// Call Compensation
				LOGGER.info("Response object from Port in Case-Mgmt MS is NULL for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
						"Response Object from Micro Service is NULL", null, BPMConstants.COMP_COUNT);
			}
		} catch (Exception e) {
			flag = false;
			kcontext.setVariable(BPMConstants.FLAG, flag);
			LOGGER.error("Exception while validating response from Port In CASE-MGMT MS call. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
}
