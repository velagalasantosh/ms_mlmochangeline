package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.provisioning.domain.common.ProvisioningBPMWorkFlow;
import com.vzw.orm.provisioning.domain.common.ProvisioningExceptionInfo;
import com.vzw.orm.provisioning.domain.common.MtasConsumerRequest;
import com.vzw.orm.provisioning.domain.common.MtasProducerRequest;
import com.vzw.orm.provisioning.domain.common.MtasProducerResponse;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.provisioning.domain.common.ProvisioningWorkFlowRequest;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class GlobalProvisioningUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalProvisioningUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * Global Provisioning
	 * 
	 * @param kcontext
	 * @throws Exception 
	 */
	public void initiateGlobalProvRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateGlobalProvRequest() method ...");
		try
		{
			
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			ProvisioningWorkFlowRequest proWorkFlowRequest = new ProvisioningWorkFlowRequest();
			//converting workflow object to provisioning workflow object
			BeanUtils.copyProperties(proWorkFlowRequest, workFlowRequest);
			proWorkFlowRequest.setFeatures((String)kcontext.getVariable("features"));
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String gpUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.GP_URL.toString(), BPMConstants.SERVICE_ID_PROV);
			LOGGER.info("GP URL is : " + gpUrl);
			kcontext.setVariable(BPMConstants.URL, gpUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			
			ProvisioningBPMWorkFlow bpmWorkflow = new ProvisioningBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setDeploymentId((String)kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_GP_RESPONSE);
			
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			MtasProducerRequest mtasRequest = new MtasProducerRequest();
			mtasRequest.setBpmWorkFlowObject(bpmWorkflow);
			mtasRequest.setProvisioningRequestType("GP");
			mtasRequest.setWorkFlowRequestObject(proWorkFlowRequest);
			kcontext.setVariable(BPMConstants.CONTENT, mtasRequest);
			//Jay start to print actual JSON sending to MS
			ObjectMapper obj = new ObjectMapper();
			LOGGER.info("Global Provisioning Request Content is : " + obj.writeValueAsString(mtasRequest));
			//Jay end
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateGlobalProvRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from Global
	 * Provisioning
	 * 
	 * @param kcontext
	 * @throws Exception 
	 */
	public void validateGlobalProvResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateGlobalProvResponse() method ...");

		boolean flag = false;

		try
		{
			MtasProducerResponse gpResponse = (MtasProducerResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != gpResponse)
			{
				LOGGER.info("Global Provisioning Response is : " + gpResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String deviceProreqStatus = gpResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(deviceProreqStatus))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					ProvisioningExceptionInfo expInfo = gpResponse.getExceptionInfo();
					LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : " + expInfo.getErrorSource() + ", Error Details : " + expInfo.getErrorDeatils());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), expInfo.getErrorSource(), expInfo.getErrorDeatils(), 
							null, BPMConstants.COMP_COUNT);
				}
			}
			else{
				
				LOGGER.info("Response Object from Global Provisioning Service is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL", 
						null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from Global Provisioning. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
	
	/**
	 * This method is used to validate the signal response received from global provisioning
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateGPSignalResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateGPSignalResponse() method ...");

		boolean flag = false;

		try
		{
			MtasConsumerRequest gpSignalResponse = (MtasConsumerRequest) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != gpSignalResponse)
			{
				LOGGER.info("Global Provisioning Signal Response is : " + gpSignalResponse + " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = gpSignalResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					LOGGER.info("ErrorCode : " + gpSignalResponse.getStatusCode() + ", Error Source : MTAS, Error Details : " + gpSignalResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, gpSignalResponse.getStatusCode(), "MTAS", gpSignalResponse.getStatusDesc(),
							null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object MtasConsumerRequest from Global Provisioning is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from Global Provisioning Signal. Exception : " + e);
			CompensationUtil compUtil = new CompensationUtil();
			compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Onject from Micro Service is either NULL or unexpected",
					null, BPMConstants.COMP_COUNT);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

}