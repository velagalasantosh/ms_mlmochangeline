/**
 * 
 */
package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.KeyValueObject;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;


/**
 * @author sankuvi
 *
 */
public class CloseOutProcessUtil implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(CloseOutProcessUtil.class);
	
	@SuppressWarnings("unchecked")
	public void validateLCDSignalResponse(ProcessContext kcontext) throws Exception 
	{
		LOGGER.info("Entering validateLCDSignalResponse() method for processInstanceId:"
				+ kcontext.getProcessInstance().getId());
		try {
			
			Map<String, Object> resp = (Map<String, Object>) kcontext.getVariable("result");
			LOGGER.info("Signal Data for LCD_RESPONSE signal is:" + resp);
			boolean isTNIP = false;
			if (resp != null && resp.size() > 0 && resp.get("action") != null) {
				if ("COMPLETE".equalsIgnoreCase(resp.get("action").toString())) {
					LOGGER.info("Received Complete response");
					kcontext.setVariable("statusCode", "LC");
					kcontext.setVariable("statusDesc", "LINE COMPLETE");
					kcontext.setVariable("flag", true);
				} else if ("CANCEL".equalsIgnoreCase(resp.get("action").toString())) {
					LOGGER.info("Received Cancel response");
					kcontext.setVariable("statusCode", "LD");
					kcontext.setVariable("statusDesc", "LINE DELETE");
					if (resp.get("workflowRequest") != null) {
						WorkFlowRequest wfrOld = (WorkFlowRequest) kcontext.getVariable("workflowRequest");
						WorkFlowRequest wfr = (WorkFlowRequest) resp.get("workflowRequest");
						List<KeyValueObject> lineLvlInfo = wfr.getLineLevelInfo();
						List<WorkFlowRequest> tnipWorkflowReqList = new ArrayList<>();
						for (KeyValueObject info : lineLvlInfo) {
							LOGGER.info("validating key:" + info.getKey() + " and value:" +info.getValue());
							if (info.getKey() != null && info.getKey().toUpperCase().contains("CANCEL") && "true".equalsIgnoreCase(info.getValue())) {
								WorkFlowRequest wfrToList = new WorkFlowRequest();
								wfr.setCancelContext(info.getKey());
								BeanUtils.copyProperties(wfrToList, wfr);
								wfrToList.setCancelContext(info.getKey());
								tnipWorkflowReqList.add(wfrToList);
								LOGGER.info("Added key to wfrList:" + info.getKey());
								isTNIP = true;
							}
						}
						if(isTNIP){
							kcontext.setVariable("tnipWorkflowReqList", tnipWorkflowReqList);
							LOGGER.info("tnipWorkflowReqList-->" +tnipWorkflowReqList);
						}
						wfr.setParentProcessInstanceId(wfrOld.getParentProcessInstanceId());
						kcontext.setVariable("workflowRequest", wfr);
						LOGGER.info("updated wfr for cancel-->" + wfr);
					}
					kcontext.setVariable("flag", true);
				} else if ("CLOSE".equalsIgnoreCase(resp.get("action").toString())) {
					LOGGER.info("Received Close response, hence the closeout process will be closed");
					kcontext.setVariable("close", true);
				} else {
					LOGGER.info("Received undesired response");
					throw new Exception("Received undesired action : "+resp.get("action").toString());
				}
			} else{
				throw new Exception("Received undesired response : "+resp);
			}
			kcontext.setVariable("isTNIP", isTNIP);
		}catch(Exception e){
			throw new Exception("Exception inside validateLCDSignalResponse method. Exception:"+e);
		}
	}
	
	public void abortAllActiveChildProcesses(ProcessContext kcontext){
		LOGGER.info("Entering into abortAllActiveChildProcesses() method..");
		try
		{
			ServiceProvider bpmUtil = new ServiceProvider();
			String URL = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BASE_URL.toString(),
					BPMConstants.SERVICE_ID_BPM);
			LOGGER.info("Aborting PID:"+kcontext.getVariable("pid").toString());
			URL = URL + "/" + kcontext.getKieRuntime().getEnvironment(
					).get(BPMConstants.DEPLOYMENT_ID) +"/processes/instances/"
										+kcontext.getVariable("pid").toString();
			LOGGER.info("Process Abort URL : " + URL);

			kcontext.setVariable("url", URL);
			if(kcontext.getVariable("bpmAccessKey")==null){
				kcontext.setVariable("bpmAccessKey",
						PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
			}
			if(kcontext.getVariable("bpmAccessCode")==null){
				kcontext.setVariable("bpmAccessCode",
						PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));
			}
		}catch (Exception e){
			LOGGER.error("Exception while Aborting process. Exception : "+e);
		}
	}
	
	public void getChildProcessesToAbort(ProcessContext kcontext, String processType) throws Exception 
	{
		LOGGER.info("inside getChildProcessesToAbort() method for " +processType);
		Connection connection = null;
		ProcessInstanceLogModel pilModel = new ProcessInstanceLogModel();
		try {

			pilModel.setProcessId("MLMOChangeLine.ChangeLine");
			if("child".equalsIgnoreCase(processType)){
				pilModel.setProcessInstanceId(kcontext.getProcessInstance().getParentProcessInstanceId());
			}else{
				pilModel.setProcessInstanceId(kcontext.getProcessInstance().getId());
			}

			Properties projectProps = PropertyFile.getProjectProperties();
			String url = PropertyFile.getProjectProperties().get("resource.ds1.driverProperties.url")
					.toString();
			Properties props = new Properties();
			props.setProperty("user", projectProps.get("resource.ds1.driverProperties.accessid").toString());
			props.setProperty("password", projectProps.get("resource.ds1.driverProperties.accesscode").toString());
			connection = DriverManager.getConnection(url, props);

			setChildrenForProcess(pilModel, connection, processType);

			ObjectMapper obj = new ObjectMapper();
			LOGGER.info("Final PILModel is:" + obj.writeValueAsString(pilModel));
			List<String> pidList = new ArrayList<String>();
			pidList = consolidateChilds(pilModel, pidList, processType);
			
			Collections.sort(pidList);
			LOGGER.info("Final PIL: " + pidList);

			kcontext.setVariable("pidList", pidList);
			kcontext.setVariable("flag", true);

		} catch (final Exception e) {
			LOGGER.error("Exception inside getChildProcessesToAbort method for " +processType +":" + e);
			e.printStackTrace();
			if("child".equalsIgnoreCase(processType)){
				kcontext.setVariable("flag", false);
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMDBERR01", "BPM", "Exception while finding the chld processes",
						"LCD_SIGNAL", "compCount");
			}else{
				throw new Exception(e);
			}
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (final SQLException e) {
					e.printStackTrace();
					if ("child".equalsIgnoreCase(processType)) {
						kcontext.setVariable("flag", false);
						CompensationUtil compUtil = new CompensationUtil();
						compUtil.setCompensationObject(kcontext, "BPMDBERR02", "BPM",
								"Exception while closing connections", "LCD_SIGNAL", "compCount");
					} else {
						throw new Exception(e);
					}
				}
			}
		}
	}
	
	public void setChildrenForProcess(ProcessInstanceLogModel processModel, Connection connection, String processType) throws Exception 
	{
		LOGGER.info("inside setChildrenForProcess() method..");
		PreparedStatement pstmt = null;
		List<ProcessInstanceLogModel> results = new ArrayList<ProcessInstanceLogModel>();
		try {
			pstmt = connection.prepareStatement("select * from ProcessInstanceLog where parentProcessInstanceId = ?");
			pstmt.setLong(1, processModel.getProcessInstanceId());
			ResultSet rs = null;
			pstmt.setFetchSize(100);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				results.add(mapProcessModel(rs));
			}
			rs.close();

			processModel.setChildren(results);

			LOGGER.debug("processModel in setChidren..." + processModel);
			if (results.size() > 0 && "child".equalsIgnoreCase(processType)) 
			{
				setChildrenForProcessList(results, connection, processType);
			}
			
		} catch (final Exception e) {
			LOGGER.error("Exception inside setChildrenForProcess method. Exception: " + e);
			e.printStackTrace();
			throw new Exception("Exception inside setChildrenForProcess method. Exception: " + e);
			
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (final SQLException e) {
				e.printStackTrace();
				LOGGER.error("Exception while closing prepared statement: "+e);
				throw new Exception("Exception: " + e);
			}
		}
	}
	
	private ProcessInstanceLogModel mapProcessModel(final ResultSet resultSet) throws SQLException 
	{
		final ProcessInstanceLogModel processInstanceLogModel = new ProcessInstanceLogModel();
		LOGGER.debug("Adding to List as the " + resultSet.getString("processId") + " status : "
				+ resultSet.getInt("status"));
		processInstanceLogModel.setProcessId(resultSet.getString("processId"));
		processInstanceLogModel.setStatus(resultSet.getInt("status"));
		processInstanceLogModel.setProcessInstanceId(resultSet.getLong("processInstanceId"));
		processInstanceLogModel.setParentProcessInstanceId(resultSet.getLong("parentProcessInstanceId"));
		LOGGER.debug("Not Adding to List as the " + resultSet.getString("processId") + " status : "
				+ resultSet.getInt("status"));
		return processInstanceLogModel;
	}
	
	private void setChildrenForProcessList(final List<ProcessInstanceLogModel> processInstList,
			final Connection connection, String processType) throws Exception 
	{
		if (processInstList != null && processInstList.size() > 0) 
		{
			for (final ProcessInstanceLogModel processInstModel : processInstList) {
				setChildrenForProcess(processInstModel, connection, processType);
			}
		}
	}
	
	private static List<String> consolidateChilds(ProcessInstanceLogModel pilModel, List<String> pidList, String processType) 
	{
		if (pilModel.getChildren() != null && pilModel.getChildren().size() > 0) 
		{
			for (ProcessInstanceLogModel pil : pilModel.getChildren()) {
				
				if("child".equalsIgnoreCase(processType)){
					if (pil.getStatus() == 1 && !pil.getProcessId().contains("CloseOutProcess")
							&& !pil.getProcessId().contains("HybridCompensation")) 
					{
						pidList.add(String.valueOf(pil.getProcessInstanceId()));
					}
				} else if("parent".equalsIgnoreCase(processType)){
					if (pil.getStatus() == 1 && pil.getProcessId().contains("CloseOutProcess"))
					{
						pidList.add(String.valueOf(pil.getProcessInstanceId()));
						break;
					}
				}
				
				
				if (pil.getChildren() != null && pil.getChildren().size() > 0) 
				{
					consolidateChilds(pil, pidList, processType);
				}
			}
		}
		LOGGER.info("pidList inside consolidateChilds method:" + pidList);
		return pidList;
	}
	
	public void getParentProcessInstanceId(ProcessContext kcontext) throws Exception 
	{
		LOGGER.info("inside getParentProcessInstanceId() method for PID:" +kcontext.getProcessInstance().getId());
		Connection connection = null;
		PreparedStatement pstmt = null;
		ProcessInstanceLogModel results = new ProcessInstanceLogModel();
		try {
			connection = PropertyFile.getPostgresConnection();
			pstmt = connection.prepareStatement("select * from ProcessInstanceLog where processInstanceId = ?");
			pstmt.setLong(1, kcontext.getProcessInstance().getId());
			ResultSet rs = null;
			pstmt.setFetchSize(1);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				results = mapProcessModel(rs);
			}
			rs.close();
			LOGGER.info("Parent Process Instance Id of the Async Process to Signal: "+results.getParentProcessInstanceId());
			kcontext.setVariable("asyncpid" , String.valueOf(results.getParentProcessInstanceId()));
			
		} catch (final Exception e) {
			LOGGER.error("Exception inside getParentProcessInstanceId method. Exception: " + e);
			e.printStackTrace();
			
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (final SQLException e) {
				e.printStackTrace();
				LOGGER.error("Exception while closing prepared statement while getParentProcessInstanceId: "+e);
			}
		}
		
	}
	
	public class ProcessInstanceLogModel {

		private String processId;
		private long processInstanceId;
		private long parentProcessInstanceId;
		private int status;
		private List<ProcessInstanceLogModel> children;

		public String getProcessId() {
			return processId;
		}

		public void setProcessId(String processId) {
			this.processId = processId;
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public long getProcessInstanceId() {
			return processInstanceId;
		}

		public void setProcessInstanceId(long processInstanceId) {
			this.processInstanceId = processInstanceId;
		}

		public long getParentProcessInstanceId() {
			return parentProcessInstanceId;
		}

		public void setParentProcessInstanceId(long parentProcessInstanceId) {
			this.parentProcessInstanceId = parentProcessInstanceId;
		}

		public List<ProcessInstanceLogModel> getChildren() {
			return children;
		}

		public void setChildren(List<ProcessInstanceLogModel> children) {
			this.children = children;
		}

		@Override
		public String toString() {
			return "ProcessInstanceLogModel [processId=" + processId + ", processInstanceId=" + processInstanceId
					+ ", parentProcessInstanceId=" + parentProcessInstanceId + ", status=" + status + ", children="
					+ children + "]";
		}
	}
}
