package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.beanutils.BeanUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.peganotifier.domain.common.PegaNotifRequest;
import com.vzw.orm.peganotifier.domain.common.PegaNotiflResponseDomain;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Pratyusha Pooskuru
 *
 */
public class CaseManagementUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(CaseManagementUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * the Visible Disconnect Request
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateCaseMgmtRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiateCaseMgmtRequest() method ...");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);

			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String dvsAccessUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.CASE_MGMT_URL.toString(),
					BPMConstants.SERVICE_ID_CASE_MGMT);
			LOGGER.info("CASE MGMT MS URL is : " + dvsAccessUrl);
			kcontext.setVariable(BPMConstants.URL, dvsAccessUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			PegaNotifRequest pegaNotifRequest = new PegaNotifRequest();

			BeanUtils.copyProperties(pegaNotifRequest, workFlowRequest);
			pegaNotifRequest.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			pegaNotifRequest.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			
			kcontext.setVariable(BPMConstants.CONTENT, pegaNotifRequest);
			ObjectMapper mapper = new ObjectMapper();
			LOGGER.info("Case Mgmt Pega Notifier Request Content is : " + mapper.writeValueAsString(pegaNotifRequest));
		} catch (Exception e) {
			LOGGER.error("Error in initiateCaseMgmtRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from Case Mgmt
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateCaseMgmtResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validateCaseMgmtResponse() method ...");

		boolean flag = false;

		try {
			PegaNotiflResponseDomain pegaNotiflResponse = (PegaNotiflResponseDomain) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != pegaNotiflResponse) {
				LOGGER.info("Case Mgmt Pega Notification Response is : " + pegaNotiflResponse + " for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = pegaNotiflResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				} else {
					// Call Compensation
					LOGGER.info("ErrorCode : " + pegaNotiflResponse.getStatusCode()
							+ ", Error Source : CASE-MGMT , Error Details : " + pegaNotiflResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, pegaNotiflResponse.getStatusCode(), "Visible",
							pegaNotiflResponse.getStatusDesc(), null, BPMConstants.COMP_COUNT);
				}
			} else {
				// Call Compensation
				LOGGER.info("Response object from Case-Mgmt MS is NULL for ProcessInstanceId: "
						+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
						"Response Object from Micro Service is NULL", null, BPMConstants.COMP_COUNT);
			}
		} catch (Exception e) {
			flag = false;
			kcontext.setVariable(BPMConstants.FLAG, flag);
			LOGGER.error("Exception while validating response from CASE-MGMT MS call. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
}
