/**
 * 
 */
package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpm.domain.objects.identifier.rules.WorkFlowRulesResponse;
import com.vzw.orm.bpmi.domain.objects.consumer.ConsumerWorkflowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.KeyValueObject;
import com.vzw.orm.bpmi.domain.objects.consumer.OMSLineItemDomain;
import com.vzw.orm.bpmi.domain.objects.consumer.OMSResponse;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.bpmi.domain.objects.subscriptions.LineItem;
import com.vzw.orm.bpmi.domain.objects.subscriptions.Spo;
import com.vzw.orm.bpmi.domain.objects.subscriptions.VFWorkflowRequest;
import com.vzw.orm.business.domain.objects.BPMIOrderRequest;
import com.vzw.orm.bpmi.domain.objects.subscriptions.SubscriptionKieRequestObject;
import com.vzw.orm.consumer.domain.objects.ConsumerFeatureListResponse;
import com.vzw.orm.consumer.domain.objects.LineItems;
import com.vzw.orm.consumer.domain.objects.SPOArray;

/**
 * @author Pratyusha Pooskuru
 *
 */
public class AddLineUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(AddLineUtil.class);

	public void initiateBpmDeploymentIdentifier(ProcessContext kcontext) throws Exception {

		LOGGER.info("Entering initiateBpmDeploymentIdentifier() method..");

		try {
			ServiceProvider bpmUtil = new ServiceProvider();
			String url = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BPM_DEP_ID_URL.toString(),
					BPMConstants.SERVICE_ID_BPM_BRMS);
			LOGGER.info("BRMS BpmDeploymentIdentifier URL is : " + url);
			kcontext.setVariable("url", url);
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			kcontext.setVariable("bpmAccessId",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
			kcontext.setVariable("bpmAccessCode",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));

			String request = "{ \"commands\": [{ \"insert\": { \"object\": { \"com.vzw.orm.bpm.domain.objects.identifier.rules.WorkFlowRulesResponse\": { \"orderType\": \"ConsumerAddLine\", \"orderSource\": \"Activation\" } }, \"out-identifier\": \"workflowResponseObject\" } }, { \"fire-all-rules\": \"\" }] }";

			LOGGER.info("BpmDeploymentIdentifier Request --->" + request);
			kcontext.setVariable("content", request);

		} catch (Exception e) {
			kcontext.setVariable("flag", false);
			LOGGER.error("Error in initiateBpmDeploymentIdentifier method. Exception : ", e);
			throw new Exception(e);
		}
	}

	public void validateBpmDeploymentIdentifierResponse(ProcessContext kcontext) throws Exception {

		LOGGER.info("Entering validateBpmDeploymentIdentifierResponse() method..");
		kcontext.setVariable("flag", false);
		try {
			WorkFlowRulesResponse wflwRulesResp = (WorkFlowRulesResponse) kcontext
					.getVariable("workflowResponseObject");
			LOGGER.info("BpmDeploymentIdentifier Response --->" + wflwRulesResp);
			//Added By Gazala for OC Signal Miss Fire Issue - 12/05/2019 : Begin
			//Setting the Root Deployment ID with the original deployment ID 
			//before it is updated from the Workflow Rules Response 
			
			kcontext.setVariable(BPMConstants.ROOT_DEPLOYMENT_ID, (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			LOGGER.info("Setting rootDeploymentId in AddLineUtil validateBpmDeploymentIdentifierResponse: "  + kcontext.getVariable(BPMConstants.ROOT_DEPLOYMENT_ID));
			//Added By Gazala for OC Signal Miss Fire Issue - 12/05/2019 : End
			if (wflwRulesResp != null) {
				if (wflwRulesResp.getDeploymentId() != null && wflwRulesResp.getWorkFlowId() != null) {
					LOGGER.info("About to initiate ConsumerAddLine.ConsumerAddLine process under "
							+ wflwRulesResp.getDeploymentId());
					kcontext.setVariable(BPMConstants.DEPLOYMENT_ID, wflwRulesResp.getDeploymentId());
					kcontext.setVariable("flag", true);
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				} else {
					LOGGER.info("DeploymentId and workflowId are null for the given orderType and orderSource");
					throw new Exception("DeploymentId and workflowId are null for the given orderType and orderSource");
				}
			} else {
				LOGGER.info("BpmDeploymentIdentifier Response is null or unexpected");
				throw new Exception("BpmDeploymentIdentifier Response is null or unexpected");
			}

		} catch (Exception e) {
			LOGGER.error("Error in validateBpmDeploymentIdentifierResponse method. Exception : ", e);
			throw new Exception(e);
		}
	}

	public void initiateAddLineProcess(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering into initiateAddLineProcess() method..");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " RootParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String URL = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BASE_URL.toString(),
					BPMConstants.SERVICE_ID_BPM);

			//Added By Gazala for OC Signal Miss Fire Issue - 12/05/2019 : Begin
			//Set the Root Deployment ID from kcontext in the Workflow Request Object here
			workFlowRequest.setRootDeploymentId((String) kcontext.getVariable(BPMConstants.ROOT_DEPLOYMENT_ID));
			LOGGER.info("Setting WorkFlow object rootDeploymentId in AddLineUtil initiateAddLineProcess: "  + workFlowRequest.getRootDeploymentId());
			//Added By Gazala for OC Signal Miss Fire Issue - 12/05/2019 : End
						
			String deploymentId = (String) kcontext.getVariable(BPMConstants.DEPLOYMENT_ID);
			String processId = "ConsumerAddLine.ConsumerAddLine";
			URL = URL + "/" + deploymentId + "/processes/" + processId + "/instances";
			LOGGER.info("ConsumerAddLine Process Initiation URL : " + URL);

			kcontext.setVariable("url", URL);

			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT,
					PropertyFile.getProjectProperties().get(BPMConstants.CAL_CONNECTIONTIMEOUT));
			kcontext.setVariable(BPMConstants.READ_TIME_OUT,
					PropertyFile.getProjectProperties().get(BPMConstants.CAL_READTIMEOUT));
			kcontext.setVariable("bpmAccessKey",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
			kcontext.setVariable("bpmAccessCode",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));
			/*
			 * KieRequestObject kieReqObj = new KieRequestObject();
			 * kieReqObj.setWorkflowRequest(workFlowRequest);
			 */

			ObjectMapper obj = new ObjectMapper();
			String request = "{\"workflowRequest\":{\"com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest\": "
					+ obj.writeValueAsString(workFlowRequest) + "}}";
			kcontext.setVariable(BPMConstants.CONTENT, request);
			LOGGER.info("ConsumerAddLine Content is : " + request);

		} catch (Exception e) {
			LOGGER.error("Exception while intiating ConsumerAddLine Process. Exception:" + e);
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	public void validateAddLineResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering into validateAddLineResponse() method..");
		kcontext.setVariable("flag", false);
		try {
			String processinstanceid = (String) kcontext.getVariable("result");
			kcontext.setVariable("calpid", processinstanceid);
			if (processinstanceid != null) {
				LOGGER.info("ConsumerAddLine Process Instance id ->" + processinstanceid);
				kcontext.setVariable("flag", true);
			} else {
				LOGGER.info("Unable to initiate ConsumerAddLine process");
				throw new Exception("Unable to initiate ConsumerAddLine process");
			}

		} catch (Exception e) {
			LOGGER.error("Exceptiom inside validateAddLineResponse() method.." + e.getMessage());
			throw new Exception("Exceptiom inside validateAddLineResponse() method.." + e.getMessage());
		}
	}
	
	public void initiateBPMIAddLineRequest(ProcessContext kcontext) throws Exception  
	  {
	    LOGGER.info("Entering initiateBPMIAddLineRequest() method" + kcontext);
	    try
	    {
	      ServiceProvider bpmUtil = new ServiceProvider();
	      String BPMIUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.CAL_BPMI_URL.toString(), BPMConstants.SERVICE_ID_BPMI);
	      String ppid = String.valueOf(kcontext.getProcessInstance().getParentProcessInstanceId());
	      LOGGER.info("-----------------ParentProcessId:--------------------- " + ppid);
	      LOGGER.info("-------------------InitiateBPMIAddLineRequest URL is:--------------------- " + BPMIUrl);
	      kcontext.setVariable("url", BPMIUrl);
	      kcontext.setVariable("msAccessId", PropertyFile.getInstance().getMSAccessId());
	      kcontext.setVariable("msAccessCode", PropertyFile.getInstance().getMSAccessCode());
	      kcontext.setVariable("connectionTimeOut", PropertyFile.getInstance().getConnectionTimeOut());
	      kcontext.setVariable("readTimeOut", PropertyFile.getInstance().getReadTimeOut());
	      
	      WorkFlowRequest workFlowRequest = new WorkFlowRequest();
	      BPMIOrderRequest bpmiOrderRequest = new BPMIOrderRequest();
	      List<OMSLineItemDomain> omsLineItemDomainList = new ArrayList<OMSLineItemDomain>();
	      
	      Set<String> filteredMtnList = (Set<String>) kcontext.getVariable("filteredMtnList");
	      List<KeyValueObject> keyValueObjectList = new ArrayList<KeyValueObject>();
	     
	      KeyValueObject keyValueObject = new KeyValueObject();
	      List<WorkFlowRequest> filteredLstOfAddLineWorkflow = (List<WorkFlowRequest>)kcontext.getVariable("filteredLstOfAddLineWorkflow");
	      workFlowRequest = (WorkFlowRequest)filteredLstOfAddLineWorkflow.get(0);
	      kcontext.setVariable("workflowRequest", workFlowRequest);
	      LOGGER.info("-----------------workFlowRequest:--------------------- " + workFlowRequest);
	      BeanUtils.copyProperties(bpmiOrderRequest, workFlowRequest);
	      bpmiOrderRequest.setOrderSource("Activation");	      
	     
	      for (WorkFlowRequest wfr : filteredLstOfAddLineWorkflow)
	      {
	    	  filteredMtnList.remove(wfr.getMtn());
		      if (Integer.parseInt(wfr.getLineItemNumber()) < 101) {
		    	  bpmiOrderRequest.setOrderType("ConsumerOrderAddLine");
				} else {
					bpmiOrderRequest.setOrderType("ConsumerOrderAddLineWithAccount");
				}
	      }	      
	      LOGGER.info("FilteredMtnList after remove mtn : " + filteredMtnList);

	      bpmiOrderRequest.setOrderTimestamp(workFlowRequest.getOrderDateTime());
	      bpmiOrderRequest.setOrderFulfillmentDate(workFlowRequest.getOrderDueDate());
	      bpmiOrderRequest.setVisionLess("Y");
	      LOGGER.info("-----------------bpmiOrderRequest:--------------------- " + bpmiOrderRequest);
	      for (WorkFlowRequest wfr : filteredLstOfAddLineWorkflow)
	      {	    	  
	        OMSLineItemDomain omsLineItemDomain = new OMSLineItemDomain();
	        BeanUtils.copyProperties(omsLineItemDomain, wfr);
	        
	        LOGGER.info("----------------omsLineItemDomain Request:------------------- " + omsLineItemDomain);
	        if ((wfr.getSpoCategoryCodes() != null) && (wfr.getSpoCategoryCodes().length > 0))
	        {
	          keyValueObject = new KeyValueObject();
	          keyValueObjectList = wfr.getLineLevelInfo();
	          LOGGER.info("-----------------keyValueObject Before:--------------------- " + keyValueObject);
	          keyValueObject.setKey("smSkip");
	          keyValueObject.setValue("true");
	          LOGGER.info("-----------------keyValueObject After:---------------------- " + keyValueObject);
	          keyValueObjectList.add(keyValueObject);
	          
	        }
	        keyValueObject = new KeyValueObject();
	        keyValueObject.setKey("CALviaMLMO");
	        keyValueObject.setValue("true");
	        keyValueObjectList.add(keyValueObject);
	        omsLineItemDomain.setLineLevelInfo(keyValueObjectList);
	        omsLineItemDomainList.add(omsLineItemDomain);
	      }
	      bpmiOrderRequest.setLineItems(omsLineItemDomainList);
	      LOGGER.info("-----------------omsLineItemDomainList:--------------------- " + omsLineItemDomainList);
	      kcontext.setVariable(BPMConstants.CONTENT, bpmiOrderRequest);
	      kcontext.setVariable("filteredMtnList", filteredMtnList);
	      ObjectMapper obj = new ObjectMapper();
	      LOGGER.info("FilteredMtnList : " + obj.writeValueAsString(filteredMtnList));
	      LOGGER.info("BPMIAddLine Request Content for  : " + obj.writeValueAsString(bpmiOrderRequest));
	    }
	    catch (Exception e)
	    {
	      LOGGER.error("Error in initiateBPMIAddLineRequest method. Exception : " + e);
	      throw new Exception(e);
	    }
	  }
		  
	  public void validateBPMIAddLineResponse(ProcessContext kcontext)  throws Exception 
	  {
	    LOGGER.info("Entering ValidateBPMIAddLineResponse() method");
	    kcontext.setVariable("flag", false);
	    try
	    {
	      OMSResponse omsResponse = (OMSResponse)kcontext.getVariable(BPMConstants.RESULT);
	      if (null != omsResponse)
	      {
	        LOGGER.info("BPMIAddLineResponse Response is : " + omsResponse + " for ProcessInstanceId: " + kcontext
	          .getProcessInstance().getId() + " and ParentProcessInstanceId: " + kcontext
	          .getProcessInstance().getParentProcessInstanceId());
	        String status = omsResponse.getStatus();
	        LOGGER.info("Status is:" + status);
	        if ("success".equalsIgnoreCase(status))
	        {
	          LOGGER.info("BPMIAddLineResponse responded with Success");
	          kcontext.setVariable("flag", true);
	        }
	        else
	        {
	          LOGGER.debug("ErrorCode : " + omsResponse.getStatus() + ", Error Source : BPMI , Error Details : " + omsResponse
	            .getStatus());
	          CompensationUtil compUtil = new CompensationUtil();
	          compUtil.setCompensationObject(kcontext, omsResponse.getStatus(), "BPMI", omsResponse
	            .getStatusMessage(), null, "compCount");
	        }
	      }
	      else
	      {
	        LOGGER.error("Response object from BPMIAddLineResponse is NULL or unexpected");
	        throw new Exception("Response object from BPMIAddLineResponse is NULL or unexpected");
	      }
	    }
	    catch (Exception e)
	    {
	      LOGGER.error("Exception while validating response received from BPMIAddLineResponse. Exception : " + e);
	      throw new Exception(e);
	    }
	  }
}
