package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.common.core.util.DateTimeUtils;
import com.vzw.orm.tnip.domain.TnipBPMWorkFlow;
import com.vzw.orm.tnip.domain.EtniConsumerResponse;
import com.vzw.orm.tnip.domain.EtniRequest;
import com.vzw.orm.tnip.domain.ExceptionInfo;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.tnip.domain.EtniResponse;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Vinodh Sankuru
 *
 */
public class PTINUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(PTINUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * in PTIN
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiatePTINRequest(ProcessContext kcontext, String type) throws Exception
	{
		LOGGER.info("Entering initiate"+type+"Request() method ...");
		try
		{
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);

			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
					+ kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			Boolean isSync=false;			
			if("PR".equalsIgnoreCase(workFlowRequest.getPortIn())  || "PIR".equalsIgnoreCase(workFlowRequest.getPortIn()) || "PIT".equalsIgnoreCase(workFlowRequest.getPortIn())){
				isSync=true;				
			}			
			kcontext.setVariable("isSync",isSync);

			ServiceProvider bpmUtil = new ServiceProvider();
			String portInUrl = null; 
					
			TnipBPMWorkFlow bpmWorkflow = new TnipBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			if("PREQ".equalsIgnoreCase(type)) {
				portInUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.PREQ_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
				bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_PREQ_RESPONSE);
				if(kcontext.getVariable("remarksAlreadyInvoked") == null){
					kcontext.setVariable("remarksAlreadyInvoked", false);
				}
			}else if("PTIN".equalsIgnoreCase(type)) {
				portInUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.PTIN_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
				bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_PTIN_RESPONSE);
			}else if("PRQ2".equalsIgnoreCase(type)){
				portInUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.PRQ2_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
				bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_PRQ2_RESPONSE);
				if(kcontext.getVariable("remarksAlreadyInvoked") == null){
					kcontext.setVariable("remarksAlreadyInvoked", false);
				}
			}
			LOGGER.info(type+" URL is : " + portInUrl);
			
			kcontext.setVariable(BPMConstants.URL, portInUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			
			EtniRequest ptinRequest = new EtniRequest();
			BeanUtils.copyProperties(ptinRequest, workFlowRequest);
			ptinRequest.setOrderDueDate(DateTimeUtils.formatCurrentTsBasedOnFormat("yyyy-MM-dd-HH.mm.ss.SSS"));
			ptinRequest.setApiLookupName(BPMConstants.SIGNAL_QBLR);
			ptinRequest.setWfReqid(kcontext.getProcessInstance().getProcessId());
			ptinRequest.setProcessInsId(String.valueOf(kcontext.getProcessInstance().getId()));
			ptinRequest.setBpmWorkFlowObject(bpmWorkflow);
			
			kcontext.setVariable(BPMConstants.CONTENT, ptinRequest);
			ObjectMapper obj = new ObjectMapper();
			LOGGER.info(type + " Request Content is : " + obj.writeValueAsString(ptinRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiate"+type+"Request method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from PTIN
	 * Activation
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validatePTINResponse(ProcessContext kcontext, String type) throws Exception
	{
		LOGGER.info("Entering validate"+type+"Response() method ...");

		boolean flag = false;

		try
		{
			EtniResponse ptinResponse = (EtniResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != ptinResponse)
			{
				LOGGER.info(type+" Response is : " + ptinResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = ptinResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					//kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					/*if(kcontext.getVariable("lineStatus") != null) {
						kcontext.setVariable("preStatus", (String)kcontext.getVariable("lineStatus"));
					}else{
						if((Boolean)kcontext.getVariable("isSync")){
							kcontext.setVariable("preStatus", "LN");
						}else{
							kcontext.setVariable("preStatus", "OK");
						}
					}*/
					if((Boolean)kcontext.getVariable("isSync")){
						kcontext.setVariable("preStatus", "LN");
					}else{
						kcontext.setVariable("preStatus", "OK");
					}
				}
				else
				{
					// Call Compensation
					LOGGER.info("ErrorCode : " + ptinResponse.getStatusCode()
							+ ", Error Source : ETNI , Error Details : " + ptinResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, ptinResponse.getStatusCode(), "ETNI",
							ptinResponse.getStatusDesc(), null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object EtniResponse Object from "+type+" is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
						"Response Object from Micro Service is NULL", null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from "+type+ ". Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

	/**
	 * This method is used to validate the response received from PTIN
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validatePTINSignalResponse(ProcessContext kcontext, String type) throws Exception
	{
		LOGGER.info("Entering validate"+type+"SignalResponse() method ...");

		boolean flag = false;

		try
		{
			EtniConsumerResponse ptinSignalResponse = (EtniConsumerResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != ptinSignalResponse)
			{
				LOGGER.info(type+" Signal Response is : " + ptinSignalResponse);
				String status = ptinSignalResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					
					if((Boolean)kcontext.getVariable("isSync")){
						kcontext.setVariable("preStatus", "LN");
					}else{
						kcontext.setVariable("preStatus", "OK");
					}
				}
				else
				{
					ExceptionInfo expInfo = ptinSignalResponse.getExceptionInfo();
					// Call Compensation
					CompensationUtil compUtil = new CompensationUtil();
					if(BPMConstants.GUI_ERR.equalsIgnoreCase(expInfo.getErrorCode()))
					{
						kcontext.setVariable("isGuiErr", true);
						LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Errolor Source : GUI , Error Details : "
								+ expInfo.getErrorDeatils());
						compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), BPMConstants.GUI, expInfo.getErrorDeatils(),
							null, BPMConstants.COMP_COUNT);
					}else{
						LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : ETNI , Error Details : "
								+ expInfo.getErrorDeatils());
						compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), "ETNI", expInfo.getErrorDeatils(),
								null, BPMConstants.COMP_COUNT);
					}
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object EtniConsumerResponse from "+type+" signal is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
						"Response Object from Micro Service is NULL", null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from "+type+" signal. Exception : " + e);
			CompensationUtil compUtil = new CompensationUtil();
			compUtil.setCompensationObject(kcontext, "BPMERR03", "BPM",
					"Exception while validating response received from "+type, null, BPMConstants.COMP_COUNT);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
	
	public void validateConsentResponse(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering validateConsentResponse() method ...");
		try {
			@SuppressWarnings("unchecked")
			Map<String, Object> resp = (Map<String, Object>) kcontext.getVariable(BPMConstants.RESULT);
			LOGGER.info("Consent Signal Response:" + resp);
			if (resp.get("consent") != null) {
				if ("Y".equalsIgnoreCase(resp.get("consent").toString())) {
					LOGGER.info("Consent has been sent");
					kcontext.setVariable("consent", true);
				} else if ("N".equalsIgnoreCase(resp.get("consent").toString())) {
					LOGGER.info("Consent has been sent false");
					kcontext.setVariable("consent", false);
				} else {
					throw new Exception("Invalid Consent sent:" + resp.get("consent").toString());
				}
			} else {
				throw new Exception("Consent value not present in the response");
			}
		} catch (Exception e) {
			LOGGER.error("Exception while validating Consent. Exception:" + e);
			throw new Exception("Exception while validating Consent. Exception:" + e);
		}
	}

}