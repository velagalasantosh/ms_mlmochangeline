package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vzw.orm.provisioning.domain.common.ProvisioningBPMWorkFlow;
import com.vzw.orm.provisioning.domain.common.MtasConsumerRequest;
import com.vzw.orm.provisioning.domain.common.MtasProducerRequest;
import com.vzw.orm.provisioning.domain.common.MtasProducerResponse;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.provisioning.domain.common.ProvisioningWorkFlowRequest;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class OrderProvisioningUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderProvisioningUtil.class);

	/**
	 * This method is used to set all the variable values required to call MTAS
	 * in Consumer Workflow
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateMTASRequest(ProcessContext kcontext) throws Exception
	{
		String type = "MLMOChangeLine.OrderProvisioningNC".equalsIgnoreCase(kcontext.getProcessInstance().getProcessId()) ? "Non-Critical" : "Critical";
		LOGGER.info("Entering initiateMTASRequest() method for "+type);

		try
		{
            WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
            
            ProvisioningWorkFlowRequest proWorkFlowRequest = new ProvisioningWorkFlowRequest();
			
			BeanUtils.copyProperties(proWorkFlowRequest, workFlowRequest);
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			
			kcontext.setVariable(BPMConstants.FLAG, false);
			ServiceProvider bpmUtil = new ServiceProvider();
			String mtasUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.MTAS_URL.toString(), BPMConstants.SERVICE_ID_PROV);
			LOGGER.info("MTAS URL for "+type+ " is : " + mtasUrl);
			kcontext.setVariable(BPMConstants.URL, mtasUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			ProvisioningBPMWorkFlow bpmWorkflow = new ProvisioningBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			if ("Critical".equalsIgnoreCase(type)) {
				bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_MTAS);
			}else{
				bpmWorkflow.setSignalEventId("MTAS_NC");
			}
			LOGGER.info("deploymentId-->" + (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			MtasProducerRequest mtasRequest = new MtasProducerRequest();
			
			mtasRequest.setBpmWorkFlowObject(bpmWorkflow);
			mtasRequest.setProvisioningRequestType("OP");
			mtasRequest.setWorkFlowRequestObject(proWorkFlowRequest);

			kcontext.setVariable(BPMConstants.CALL_GLOB_PROV, workFlowRequest.getGlobalTechnology()!= null ? (workFlowRequest.getGlobalTechnology().matches(("VZ|VD")) ? true:false) : false);
			kcontext.setVariable(BPMConstants.NE_TYPE_SET, new HashSet<String>());

			kcontext.setVariable(BPMConstants.CONTENT, mtasRequest);
			
			ObjectMapper obj = new ObjectMapper();
			LOGGER.info("MTAS Content for "+type+ " is : " + obj.writeValueAsString(mtasRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateMTASRequest method for "+type+ ". Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate if the MTAS service was invoked properly
	 * without any failures in Consumer Workflow
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateMTASRequestsResponse(ProcessContext kcontext) throws Exception
	{
		String type = "MLMOChangeLine.OrderProvisioningNC".equalsIgnoreCase(kcontext.getProcessInstance().getProcessId()) ? "Non-Critical" : "Critical";
		LOGGER.info("Entering validateMTASRequestsResponse() method for "+type);
		kcontext.setVariable("skipMTASSignal", false);
		try
		{
			if (null != kcontext.getVariable(BPMConstants.RESULT))
			{
				MtasProducerResponse mtasProducerResp = (MtasProducerResponse) (kcontext.getVariable(BPMConstants.RESULT));
				LOGGER.info("MtasProducerResponse for "+type+ ": " + mtasProducerResp+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());

				if (null != mtasProducerResp) {
					if (BPMConstants.SUCCESS.equalsIgnoreCase(mtasProducerResp.getStatus())) {
						kcontext.setVariable(BPMConstants.FLAG, true);
						if ("Y".equalsIgnoreCase(mtasProducerResp.getSwitchBypassInd())) {
							kcontext.setVariable("skipMTASSignal", true);
						}
						kcontext.setVariable("features", mtasProducerResp.getFeatures());
						LOGGER.info("features list from MTAS :" + mtasProducerResp.getFeatures());
					} else {
						kcontext.setVariable(BPMConstants.FLAG, false);
						CompensationUtil compUtil = new CompensationUtil();
						if (mtasProducerResp.getExceptionInfo() != null) {
							compUtil.setCompensationObject(kcontext, mtasProducerResp.getExceptionInfo().getErrorCode(),
									mtasProducerResp.getExceptionInfo().getErrorSource(),
									mtasProducerResp.getExceptionInfo().getErrorDeatils(), "MTAS_RES", "compCount");
						} else {
							compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
									"Response Object from Micro Service is either NULL or unexpected", "MTAS_RES",
									"compCount");
						}
					}
				}
				else
				{
					LOGGER.info("Mtas responded with NULL or Error response");
					kcontext.setVariable(BPMConstants.FLAG, false);
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is either NULL or unexpected", "MTAS_RES", "compCount");
				}
			}
			else
			{
				LOGGER.error("Exception while Invoking MTAS Service");
				kcontext.setVariable(BPMConstants.FLAG, false);
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Exception while Invoking Micro Service", "MTAS_RES", "compCount");
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Error in validateMTASRequestsResponse method for "+type+ ". Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from MTAS and to
	 * check whether all the critical elements are received or not
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateMTASResponse(ProcessContext kcontext) throws Exception
	{
		String type = "MLMOChangeLine.OrderProvisioningNC".equalsIgnoreCase(kcontext.getProcessInstance().getProcessId()) ? "Non-Critical" : "Critical";
		LOGGER.info("Entering validateMTASResponse() method for "+type);

		boolean isAllCriticalCompleted = false;

		try
		{
			kcontext.setVariable(BPMConstants.MTAS_SUCCESS, false);

			if (null != kcontext.getVariable(BPMConstants.RESULT))
			{
				MtasConsumerRequest mtasRespData = (MtasConsumerRequest) (kcontext.getVariable(BPMConstants.RESULT));
				LOGGER.info("MtasConsumerRequest for "+type+ ": " + mtasRespData);

				String status = mtasRespData.getStatus();

				String neType = mtasRespData.getNeType();
				if (null != status && !status.isEmpty() && BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					String splitListStr = mtasRespData.getSplitList();
					LOGGER.info("Inputs received from MTAS Response payload for "+type+ " is splitList : " + splitListStr + ", neType : " + neType);

					if (null != neType && !neType.isEmpty() && null != splitListStr && !splitListStr.isEmpty())
					{
						boolean isGuiNeType = false;
						if(BPMConstants.GUI.equalsIgnoreCase(neType)) {
							isGuiNeType = true;
							LOGGER.info("isGuiNeType : " +isGuiNeType +". Hence proceeding with Fake Completing the process");
						}
						
						Set<String> splitListSet = new HashSet<String>(Arrays.asList(splitListStr.split(",")));
						kcontext.setVariable(BPMConstants.SPLIT_LIST, splitListSet);

						@SuppressWarnings("unchecked")
						Set<String> neTypeSet = (Set<String>) kcontext.getVariable(BPMConstants.NE_TYPE_SET);

						if (splitListSet.contains(neType))
						{
							neTypeSet.add(neType);
						}
						else
						{
							LOGGER.info("Received element which is not listed in splitList from MTAS : " + neType);
						}
						LOGGER.info("Inputs received from Consumer Workflow for "+type+ " is neTypeSet : " + neTypeSet);
						kcontext.setVariable(BPMConstants.NE_TYPE_SET, neTypeSet);

						if (splitListSet.size() == neTypeSet.size() || isGuiNeType)
						{
							isAllCriticalCompleted = true;
							if ("Critical".equalsIgnoreCase(type)) {
								if (kcontext.getVariable("lineStatus") != null) {
									if("GUIOPRFL"
											.equalsIgnoreCase(kcontext.getVariable("lineStatus").toString())){
										LOGGER.info("Prestatus when GUIERR01: "+(String) kcontext.getVariable("preStatus"));
										kcontext.setVariable("preStatus", (String) kcontext.getVariable("preStatus"));
									} else{
										kcontext.setVariable("preStatus", (String) kcontext.getVariable("lineStatus"));
										LOGGER.info("Prestatus: "+(String) kcontext.getVariable("preStatus"));
									}
								} else {
									kcontext.setVariable("preStatus", "SS");
								}
								LOGGER.info("Call Global Provisioning : "
										+ kcontext.getVariable(BPMConstants.CALL_GLOB_PROV));
							}
							discardLine(kcontext, BPMConstants.SIGNAL_KEY_MTAS);
						}
					}
					kcontext.setVariable(BPMConstants.MTAS_SUCCESS, true);
				}
				else
				{
					LOGGER.info("Error response received from MTAS for " + type + " with errorCode : ["
							+ mtasRespData.getStatusCode() + "] and errorDesc : [" + mtasRespData.getStatusDesc()
							+ "]");
						discardLine(kcontext, BPMConstants.SIGNAL_KEY_MTAS);

						CompensationUtil compUtil = new CompensationUtil();
						if (BPMConstants.GUI_ERR.equalsIgnoreCase(mtasRespData.getStatusCode())) {
							kcontext.setVariable("isGuiErr", true);
							compUtil.setCompensationObject(kcontext, mtasRespData.getStatusCode(), BPMConstants.GUI,
									mtasRespData.getStatusDesc(), null, BPMConstants.COMP_COUNT);
						} else {
							compUtil.setCompensationObject(kcontext, mtasRespData.getStatusCode(), "MTAS_RES",
									mtasRespData.getStatusDesc(), null, BPMConstants.COMP_COUNT);
					}
				}
			}
			else
			{
				LOGGER.error("Received NULL response from MTAS for "+type+ ". Deactivating safe point and calling compensation.");
				discardLine(kcontext, BPMConstants.SIGNAL_KEY_MTAS);
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL for "+type, null, BPMConstants.COMP_COUNT);
			}
			LOGGER.info("All "+type+ " Items Received : " + isAllCriticalCompleted);
		}
		catch (Exception e)
		{
			LOGGER.error("Exception while validating response received from MTAS for "+type+ ". Exception : " + e);
			discardLine(kcontext, BPMConstants.SIGNAL_KEY_MTAS);
			CompensationUtil compUtil = new CompensationUtil();
			compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Exception while validating response received from MTAS for "+type, null, BPMConstants.COMP_COUNT);
			kcontext.setVariable(BPMConstants.MTAS_SUCCESS, false);
		}
		kcontext.setVariable(BPMConstants.FLAG, isAllCriticalCompleted);
	}

	/**
	 * This method is used to validate the response received from MTAS and to
	 * check whether all the non-critical elements are received or not
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateNonCriticalMTASResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateNonCriticalMTASResponse() method for Non-Critical");

		boolean isAllCriticalCompleted = false;

		try
		{
			if (null != kcontext.getVariable(BPMConstants.RESULT))
			{
				MtasConsumerRequest mtasRespData = (MtasConsumerRequest) (kcontext.getVariable(BPMConstants.RESULT));
				LOGGER.info("MtasConsumerRequest for Non-Critical : " + mtasRespData);

				String status = mtasRespData.getStatus();

				if (null != status && !status.isEmpty())
				{
					String neType = mtasRespData.getNeType();
					String splitListStr = mtasRespData.getSplitList();
					LOGGER.info("Inputs received from MTAS Response payload for Non-Critical is splitList : " + splitListStr + ", neType : " + neType);

					if (null != neType && !neType.isEmpty() && null != splitListStr && !splitListStr.isEmpty())
					{
						Set<String> splitListSet = new HashSet<String>(Arrays.asList(splitListStr.split(",")));
						kcontext.setVariable(BPMConstants.SPLIT_LIST, splitListSet);

						@SuppressWarnings("unchecked")
						Set<String> neTypeSet = (Set<String>) kcontext.getVariable(BPMConstants.NE_TYPE_SET);

						if (splitListSet.contains(neType))
						{
							neTypeSet.add(neType);
						}
						else
						{
							LOGGER.info("Received element which is not listed in splitList from MTAS : " + neType);
						}
						LOGGER.info("Inputs received from Consumer Workflow for Non-Critical is neTypeSet : " + neTypeSet);
						kcontext.setVariable(BPMConstants.NE_TYPE_SET, neTypeSet);

						if (splitListSet.size() == neTypeSet.size())
						{
							isAllCriticalCompleted = true;
							discardLine(kcontext, BPMConstants.SIGNAL_KEY_MTAS);
						}
					}
				}
				else
				{
					LOGGER.info("Error response received from MTAS for Non-Critical with errorCode : [" + mtasRespData.getStatusCode() + "] and errorDesc : ["
							+ mtasRespData.getStatusDesc() + "]");
				}
			}
			else
			{
				LOGGER.error("Received NULL response from MTAS for Non-Critical. Deactivating safe point and calling compensation.");
				
			}
			LOGGER.info("All Non-Critical Items Received : " + isAllCriticalCompleted);
		}
		catch (Exception e)
		{
			LOGGER.error("Exception while validating response received from MTAS for Non-Critical. Exception : " + e);
		}
		kcontext.setVariable(BPMConstants.MTAS_SUCCESS, true);
		kcontext.setVariable(BPMConstants.FLAG, isAllCriticalCompleted);
	}
	
	public void discardLine(ProcessContext kcontext, String signalKey)
	{
		LOGGER.info("Entering discardLine() method ...");
		try
		{
			kcontext.setVariable(BPMConstants.OPERATION_TYPE, "REFLOW");
			kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
			LOGGER.info("De-activating safe point of signalKey : " + signalKey + " and signalId : " + kcontext.getVariable(signalKey));

			if (signalKey != null && kcontext.getVariable(signalKey) !=null) 
			{
				kcontext.getKieRuntime().getWorkItemManager().completeWorkItem(Long.parseLong(String.valueOf(kcontext.getVariable(signalKey))), null);
			}

		}
		catch (Exception e)
		{
			LOGGER.error("Exception while validating response received from Line Item Order Status Update. Exception : " + e);
			e.printStackTrace();
		}
	}
	
	public void deriveLineStatus(ProcessContext kcontext) {

		LOGGER.info("Entering deriveLineStatus() method ...");
		try
		{
			if (kcontext.getVariable(BPMConstants.RESULT) != null) {
				if (kcontext.getVariable(BPMConstants.RESULT) instanceof MtasConsumerRequest) {
					MtasConsumerRequest mtasRespData = (MtasConsumerRequest) (kcontext.getVariable(BPMConstants.RESULT));
					if (mtasRespData != null && mtasRespData.getNeType() != null) {
	
						String neType = mtasRespData.getNeType();
						if (neType.equalsIgnoreCase("AUTO1000")) {
							kcontext.setVariable("lineStatus", "EC");
						} else if (neType.equalsIgnoreCase("SIMOTA")) {
							kcontext.setVariable("lineStatus", "ES");
						} else if (neType.equalsIgnoreCase("WIS")) {
							kcontext.setVariable("lineStatus", "E9");
						} else if (neType.equalsIgnoreCase("TASPI")) {
							kcontext.setVariable("lineStatus", "EN");
						} else {
							if (kcontext.getVariable("lineStatus") != null) {

								String lineStatus = kcontext.getVariable("lineStatus").toString();
								LOGGER.info("lineStatus-->" + lineStatus);
								if (lineStatus.matches("EC|EN|ES|E9|EW")) {
									LOGGER.info("the previous line status is: " + lineStatus);
								} else {
									LOGGER.info(
											"Previous line status does not match with any of the list: " + lineStatus);
									kcontext.setVariable("lineStatus", "EB");
								}
							} else {
								LOGGER.info("lineStatus is null, hence setting to EB");
								kcontext.setVariable("lineStatus", "EB");
							}

						}
					}else {
						LOGGER.info("MtasConsumerRequest is null or neType is null");
						kcontext.setVariable("lineStatus", "EB");
					}
				}
				else if(kcontext.getVariable(BPMConstants.RESULT) instanceof MtasProducerRequest){
					
					LOGGER.info("Setting lineStatus to EB as this is mtas sync failure");
					kcontext.setVariable("lineStatus", "EB");
				}
				else {
					
					LOGGER.info("Response is null or unexpected. Hence setting line_status to EB");
					kcontext.setVariable("lineStatus", "EB");
				}
			} else {
				
				LOGGER.info("Result is null or unexpected. Hence setting line_status to EB");
				kcontext.setVariable("lineStatus", "EB");
			}
		}
		catch (Exception e)
		{
			kcontext.setVariable("lineStatus", "EB");
			LOGGER.error("Exception inside deriveLineStatus() method. Exception : " + e);
			e.printStackTrace();
		}
		
		LOGGER.info("final lineStatus for MTAS failure:" + kcontext.getVariable("lineStatus"));
	}

}