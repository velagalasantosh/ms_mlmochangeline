package com.vzw.orm.wflw.process.workitem;

import org.drools.core.spi.ProcessContext;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.kie.api.runtime.process.WorkflowProcessInstance;
import org.kie.internal.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vzw.orm.wflw.util.BPMConstants;

public class KeepSignalAliveWorkItemHandler implements WorkItemHandler
{

	private static final Logger LOGGER = LoggerFactory.getLogger(KeepSignalAliveWorkItemHandler.class);

	private StatefulKnowledgeSession ksession;

	/**
	 * Used when no authentication is required and requires a session
	 */
	public KeepSignalAliveWorkItemHandler(StatefulKnowledgeSession ksession)
	{
		this.ksession = ksession;
	}

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager)
	{
		LOGGER.info("Entering executeWorkItem() method ...");
		try
		{
			ProcessContext kcontext = new ProcessContext(ksession);
			WorkflowProcessInstance processInstance = (WorkflowProcessInstance) ksession.getProcessInstance(workItem.getProcessInstanceId());
			kcontext.setProcessInstance(processInstance);  
			kcontext.setVariable(String.valueOf(workItem.getParameter("key")), String.valueOf(workItem.getId()));
			LOGGER.info("Activating Signal for Signal Key : " + workItem.getParameter("key") + ", Value : " + workItem.getId());
			
			boolean signalProcess = Boolean.parseBoolean(String.valueOf(workItem.getParameter("signalProcess")));
			LOGGER.info("Signal Process : "+signalProcess );
			if(signalProcess)
			{
				String signalName = String.valueOf(workItem.getParameter("signalName"));
				LOGGER.info("Calling process model by signaling : "+signalName + "operationType : " +kcontext.getVariable(BPMConstants.OPERATION_TYPE));
				ksession.signalEvent(signalName, kcontext.getVariable(BPMConstants.OPERATION_TYPE), workItem.getProcessInstanceId());
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Error in executeWorkItem() method of KeepSignalAliveWorkItemHandler.");
			e.printStackTrace();
		}

	}

	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1)
	{
		// TODO Auto-generated method stub

	}

	public enum AuthenticationType
	{
		NONE, BASIC, FORM_BASED
	}

}
