package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.billing.datacontract.VisionRequest;
import com.vzw.orm.billing.datacontract.VisionSaveRecordResponse;
import com.vzw.orm.billing.datacontract.VisionSaveRecordSignalResponse;
import com.vzw.orm.billing.datacontract.VisionSyncRequest;
import com.vzw.orm.billing.domain.BillingBPMWorkFlow;
import com.vzw.orm.billing.domain.BillingWorkFlowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.ConsumerWorkflowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;

/**
 * This class is used to perform basic validations and utility operations
 *
 * @author Rahul Kumar Agarwal
 */
public class VisionRecordOrderUtil implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(VisionRecordOrderUtil.class);

    /**
     * This method is used to set all the variable values required to initiate
     * Vision Record Order Micro Service
     *
     * @param kcontext
     * @throws Exception
     */
    public void initiateVisionRecordOrderRequest(ProcessContext kcontext) throws Exception {
        LOGGER.info("Entering initiateVisionRecordOrderRequest() method ...");
        try {
            ServiceProvider bpmUtil = new ServiceProvider();
            boolean line = null != kcontext.getVariable("vroLine") ? (Boolean) kcontext.getVariable("vroLine") : false;
            LOGGER.info("Line Level: " + line);
            BillingBPMWorkFlow billingWorkflow = new BillingBPMWorkFlow();
            billingWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
            billingWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
            billingWorkflow.setSignalEventId(BPMConstants.SIGNAL_VRO_RESPONSE);
            billingWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));

            if (PropertyFile.getInstance().isConsumerClustered()) {
                billingWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
                        + PropertyFile.getInstance().getDataCenter());
            } else {
                billingWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
            }

            ObjectMapper obj = new ObjectMapper();
            String visionRecordOrderUrl = null;
            if (line) {
                visionRecordOrderUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VISION_LN_REC_URL.toString(), BPMConstants.SERVICE_ID_BILLING);

                WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
                ///Jay to make it null as Vision billing was failing for featureList
                //Jay end
                BillingWorkFlowRequest billingWorkFlowRequest = new BillingWorkFlowRequest();
                //converting workflow object to provisioning workflow object
                BeanUtils.copyProperties(billingWorkFlowRequest, workFlowRequest);
                billingWorkFlowRequest.setFeatures(null);
                LOGGER.info("workFlowRequest for LINE level record is.." + workFlowRequest);

                LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId()
                        + " ParentProcessInstanceId:" + kcontext.getProcessInstance().getParentProcessInstanceId());

                VisionRequest visionRecordRequest = new VisionRequest();
                visionRecordRequest.setBpmWorkFlowObject(billingWorkflow);

                visionRecordRequest.setWorkFlowRequestObject(billingWorkFlowRequest);
                kcontext.setVariable(BPMConstants.CONTENT, visionRecordRequest);
                LOGGER.info("Vision Record Order Request Content for LINE level is : " + obj.writeValueAsString(visionRecordRequest));

            } else {
                kcontext.setVariable("vroLine", line);
                boolean isAccVP = Boolean.parseBoolean(PropertyFile.getProjectProperties().get("ACC_LEVEL_VP_REQ").toString());
                LOGGER.info("Account Level VP Enabled-->" + isAccVP);
                kcontext.setVariable("isAccVP", isAccVP);

                visionRecordOrderUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VISION_ORD_REC_URL.toString(), BPMConstants.SERVICE_ID_BILLING);
                VisionSyncRequest visionSyncRequest = new VisionSyncRequest();
                ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext.getVariable(BPMConstants.CONSUMERWFLREQUEST);
                LOGGER.info("consumerWFRequest for ACCOUNT level record is.." + consumerWFRequest);

                LOGGER.info("OrderID:" + consumerWFRequest.getLstOfWorkflowRequest().get(0).getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
                        + kcontext.getProcessInstance().getParentProcessInstanceId());

                //workflowRequest object construction is for ORDER LEVEL record failures
                WorkFlowRequest workFlowRequest = new WorkFlowRequest();
                BeanUtils.copyProperties(workFlowRequest, consumerWFRequest.getLstOfWorkflowRequest().get(0));
                String lineItemNumbers = consumerWFRequest.getLstOfWorkflowRequest().stream()
                        .filter(wrkflw -> Integer.parseInt((wrkflw.getLineItemNumber())) > 100)
                        .map(WorkFlowRequest::getLineItemNumber)
                        .collect(Collectors.joining(","));
                workFlowRequest.setLineItemNumber(lineItemNumbers);

                kcontext.setVariable(BPMConstants.WORKFLOW_REQ, workFlowRequest);

                List<BillingWorkFlowRequest> billingWorkFlowRequestObjectList = new ArrayList<BillingWorkFlowRequest>();
                for (WorkFlowRequest workFlowRequestTemp : consumerWFRequest.getLstOfWorkflowRequest()) {
                    BillingWorkFlowRequest billingWorkFlowRequest = new BillingWorkFlowRequest();
                    //converting workflow object to provisioning workflow object
                    BeanUtils.copyProperties(billingWorkFlowRequest, workFlowRequestTemp);
                    billingWorkFlowRequestObjectList.add(billingWorkFlowRequest);
                }

                visionSyncRequest.setWorkFlowRequestObject(billingWorkFlowRequestObjectList);
                visionSyncRequest.setBpmWorkFlowObject(billingWorkflow);
                kcontext.setVariable(BPMConstants.CONTENT, visionSyncRequest);

                LOGGER.info("Vision Record Order Request Content for ACCOUNT Level is : " + obj.writeValueAsString(visionSyncRequest));

            }

            LOGGER.info("Vision Record Order URL is : " + visionRecordOrderUrl);
            kcontext.setVariable(BPMConstants.URL, visionRecordOrderUrl);
            kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
            kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
            kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
            kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());


        } catch (Exception e) {
            LOGGER.error("Error in initiateVisionRecordOrderRequest method. Exception : " + e);
            throw new Exception(e);
        }
    }

    /**
     * This method is used to validate the asyncSaveOrderInd
     *
     * @param kcontext
     */
    public void validateAsyncVisionSaveInd(ProcessContext kcontext) {

        LOGGER.info("Entering validateAsyncVisionSaveInd() method ...");
        boolean line = null != kcontext.getVariable("vroLine") ? (Boolean) kcontext.getVariable("vroLine") : false;
        LOGGER.info("Vision Record Order Line level flag :" + line);

        Boolean aysncVisionSaveInd = (Boolean) kcontext.getVariable("asyncSaveOrderInd");

        if (!line) {
            ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest)kcontext.getVariable(BPMConstants.CONSUMERWFLREQUEST);
//			WorkFlowRequest workFlowRequest = consumerWFRequest.getLstOfWorkflowRequest().stream().filter(p-> "101".equalsIgnoreCase(p.getLineItemNumber())).findAny().orElse(null);
            WorkFlowRequest workFlowRequest = consumerWFRequest.getLstOfWorkflowRequest().stream().filter(p-> Integer.valueOf(p.getLineItemNumber()) > 100).findAny().orElse(null);
            aysncVisionSaveInd = workFlowRequest.getAsyncSaveOrderInd() !=null ? Boolean.valueOf(workFlowRequest.getAsyncSaveOrderInd()) : false;

            kcontext.setVariable(BPMConstants.WORKFLOW_REQ, workFlowRequest);
            if(aysncVisionSaveInd!=null){
                kcontext.setVariable("asyncSaveOrderInd",aysncVisionSaveInd);
            }
        } else {

            if(aysncVisionSaveInd!=null){
                kcontext.setVariable("asyncSaveOrderInd",aysncVisionSaveInd);
            } else {
                WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
                aysncVisionSaveInd = workFlowRequest.getAsyncSaveOrderInd() !=null ? Boolean.valueOf(workFlowRequest.getAsyncSaveOrderInd()) : false;
                kcontext.setVariable("asyncSaveOrderInd",aysncVisionSaveInd);

            }

        }

        LOGGER.info("Async save order flag :" + kcontext.getVariable("asyncSaveOrderInd"));
        LOGGER.info("Exit validateAsyncVisionSaveInd() method ...");

    }

    /**
     * This method is used to validate the response received from Vision Record
     * Order
     *
     * @param kcontext
     * @throws Exception
     */
    public void validateVisionRecordOrderResponse(ProcessContext kcontext) throws Exception {
        LOGGER.info("Entering validateVisionRecordOrderResponse() method ...");
        Object obj = kcontext.getVariable(BPMConstants.RESULT);
        LOGGER.info("validateVisionRecordOrderResponse() kcontext RESULT object type is >> " + obj.getClass() + " and value is " + obj);

        boolean flag = false;

        try {
            ObjectMapper mapper = new ObjectMapper();
            VisionSaveRecordResponse visionSaveResponse = mapper.readValue((String) obj, VisionSaveRecordResponse.class);
            //(VisionSaveRecordResponse) (kcontext.getVariable(BPMConstants.RESULT));

            if (null != visionSaveResponse) {
                LOGGER.info("Vision Record Order Response is : " + visionSaveResponse + " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
                        + kcontext.getProcessInstance().getParentProcessInstanceId());
                String status = visionSaveResponse.getStatus();
                if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
                    flag = true;
                    //kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
                } else {
                    // Call Compensation
                    LOGGER.info("ErrorCode : " + visionSaveResponse.getStatusCode() + ", Error Source : VISION , Error Details : " + visionSaveResponse.getStatusDesc());
                    CompensationUtil compUtil = new CompensationUtil();
                    compUtil.setCompensationObject(kcontext, visionSaveResponse.getStatusCode(), "VISION", visionSaveResponse.getStatusDesc(),
                            null, BPMConstants.COMP_COUNT);
                }
            } else {
                // Call Compensation
                LOGGER.info("Response object VisionSaveRecordResponse from Vision Record Order is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
                        + kcontext.getProcessInstance().getParentProcessInstanceId());
                CompensationUtil compUtil = new CompensationUtil();
                compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
                        null, BPMConstants.COMP_COUNT);
            }
        } catch (Exception e) {
            flag = false;
            LOGGER.error("Exception while validating response received from Vision Record Order. Exception : " + e);
            throw new Exception(e);
        }
        kcontext.setVariable(BPMConstants.FLAG, flag);
    }

    /**
     * This method is used to validate the response received from Vision Save
     * Order Signal
     *
     * @param kcontext
     */
    public void validateVisionRecordOrderSignalResponse(ProcessContext kcontext) {
        LOGGER.info("Entering validateVisionRecordOrderSignalResponse() method ...");

        boolean flag = false;

        try {
            VisionSaveRecordSignalResponse visionSaveResponse = (VisionSaveRecordSignalResponse) (kcontext.getVariable(BPMConstants.RESULT));
            WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
            if (null != visionSaveResponse) {
                LOGGER.info("Vision Record Order Signal Response is : " + visionSaveResponse + " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
                        + kcontext.getProcessInstance().getParentProcessInstanceId());
                String status = visionSaveResponse.getStatus();
                if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
                    flag = true;
                    kcontext.setVariable(BPMConstants.COMP_COUNT, 0);

                    if (kcontext.getVariable("lineStatus") != null) {
                        if ("GUIVRORFL"
                                .equalsIgnoreCase(kcontext.getVariable("lineStatus").toString())) {
                            LOGGER.info("Prestatus when GUIERR01: " + (String) kcontext.getVariable("preStatus"));
                            kcontext.setVariable("preStatus", (String) kcontext.getVariable("preStatus"));
                        } else {
                            kcontext.setVariable("preStatus", (String) kcontext.getVariable("lineStatus"));
                            LOGGER.info("Prestatus: " + (String) kcontext.getVariable("preStatus"));
                        }
                    }

                } else {
                    if (workFlowRequest != null && workFlowRequest.getProductionParallelInd() != null
                            && "Y".equalsIgnoreCase(workFlowRequest.getProductionParallelInd())) {
                        flag = true;
                    } else {
                        CompensationUtil compUtil = new CompensationUtil();
                        if (BPMConstants.GUI_ERR.equalsIgnoreCase(visionSaveResponse.getStatusCode())) {
                            LOGGER.info("ErrorCode : " + visionSaveResponse.getStatusCode() + ", Error Source : GUI , Error Details : " + visionSaveResponse.getStatusDesc());
                            kcontext.setVariable("isGuiErr", true);
                            compUtil.setCompensationObject(kcontext, visionSaveResponse.getStatusCode(), BPMConstants.GUI, visionSaveResponse.getStatusDesc(),
                                    null, BPMConstants.COMP_COUNT);
                        } else {
                            LOGGER.info("ErrorCode : " + visionSaveResponse.getStatusCode() + ", Error Source : VISION , Error Details : " + visionSaveResponse.getStatusDesc());
                            compUtil.setCompensationObject(kcontext, visionSaveResponse.getStatusCode(), "VISION", visionSaveResponse.getStatusDesc(),
                                    null, BPMConstants.COMP_COUNT);
                        }
                    }
                }
            } else {
                // Call Compensation
                LOGGER.info("Response object VisionSaveRecordSignalResponse from Vision Record Order is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
                        + kcontext.getProcessInstance().getParentProcessInstanceId());
                CompensationUtil compUtil = new CompensationUtil();
                compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
                        null, BPMConstants.COMP_COUNT);
            }
        } catch (Exception e) {
            flag = false;
            LOGGER.error("Exception while validating response received from Vision Record Order Signal. Exception : " + e);
            CompensationUtil compUtil = new CompensationUtil();
            compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Onject from Micro Service is either NULL or unexpected",
                    null, BPMConstants.COMP_COUNT);
        }
        kcontext.setVariable(BPMConstants.FLAG, flag);
    }


    /**
     * This method is used to set all the variable values required to update
     * Vision Save Order status
     *
     * @param kcontext
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public void initiateVisionSaveReflowRequest(ProcessContext kcontext, String processName) throws Exception {
        LOGGER.info("Entering initiateVisionSaveReflowRequest() method for " + processName);
        try {
            long parentProcessInstanceId = kcontext.getProcessInstance().getParentProcessInstanceId();
            long processInstanceId = kcontext.getProcessInstance().getId();

            boolean accLevel = false;
            WorkFlowRequest workFlowRequest = null;
            if (kcontext.getVariable(BPMConstants.CONSUMERWFLREQUEST) != null) {
                ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext.getVariable(BPMConstants.CONSUMERWFLREQUEST);
                workFlowRequest = consumerWFRequest.getLstOfWorkflowRequest().stream().filter(p -> Integer.valueOf(p.getLineItemNumber()) > 100).findAny().orElse(null);
                if (workFlowRequest != null) {
                    accLevel = true;
                }
                kcontext.setVariable(BPMConstants.WORKFLOW_REQ, workFlowRequest);
            } else {
                workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
            }

            LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:" + processInstanceId + " ParentProcessInstanceId:" + parentProcessInstanceId);

            ServiceProvider bpmUtil = new ServiceProvider();

            String visionSaveOrderUrl = null;
            if (accLevel) {
                visionSaveOrderUrl = bpmUtil.getServiceURL(
                        PropertyFile.PROP_CONST.VISION_SAV_ACC_LVL_RFLW_URL.toString(),
                        BPMConstants.SERVICE_ID_BILLING);
                LOGGER.info("Vision Save Order account level Reflow URL for " + processName + " is : "
                        + visionSaveOrderUrl);
            } else {
                visionSaveOrderUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VISION_SAV_RFLW_URL.toString(),
                        BPMConstants.SERVICE_ID_BILLING);
                LOGGER.info("Vision Save Order Reflow URL for " + processName + " is : " + visionSaveOrderUrl);
            }
            kcontext.setVariable(BPMConstants.URL, visionSaveOrderUrl);
            kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
            kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
            kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
            kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

            BillingBPMWorkFlow bpmWorkflow = new BillingBPMWorkFlow();
            bpmWorkflow.setProcessInstanceId(String.valueOf(processInstanceId));
            bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
            bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_VSO_RESPONSE);
            bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
            if (PropertyFile.getInstance().isConsumerClustered()) {
                bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
                        + PropertyFile.getInstance().getDataCenter());
            } else {
                bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
            }

            VisionSyncRequest visionSyncRequest = new VisionSyncRequest();

            List<BillingWorkFlowRequest> wfList = new ArrayList<BillingWorkFlowRequest>();

            if (accLevel) {
                //Account Level Vision Save Reflow
                List<WorkFlowRequest> lstOfAccLevelLines = (List<WorkFlowRequest>) kcontext.getVariable("lstOfAccLevelLines");
                if (lstOfAccLevelLines != null && lstOfAccLevelLines.size() > 0) {
                    for (WorkFlowRequest workflowRequest : lstOfAccLevelLines) {
                        BillingWorkFlowRequest billingWorkFlowRequest = new BillingWorkFlowRequest();
                        BeanUtils.copyProperties(billingWorkFlowRequest, workflowRequest);
                        if (null != processName && !processName.equalsIgnoreCase(BPMConstants.COMPENSATION)) {
                            billingWorkFlowRequest.setReleasePendingSaveOrder(BPMConstants.IS_RELEASE_PENDING_SAVE_ORDER);
                        }
                        wfList.add(billingWorkFlowRequest);
                    }
                }
            } else {
                //Line Level Vision Save Reflow for FD Order.
                //converting workflow object to billing workflow object
                BillingWorkFlowRequest billingWorkFlowRequest = new BillingWorkFlowRequest();
                BeanUtils.copyProperties(billingWorkFlowRequest, workFlowRequest);
                if (null != processName && !processName.equalsIgnoreCase(BPMConstants.COMPENSATION)) {
                    billingWorkFlowRequest.setReleasePendingSaveOrder(BPMConstants.IS_RELEASE_PENDING_SAVE_ORDER);
                }
                wfList.add(billingWorkFlowRequest);
            }
            visionSyncRequest.setWorkFlowRequestObject(wfList);
            visionSyncRequest.setBpmWorkFlowObject(bpmWorkflow);
            kcontext.setVariable(BPMConstants.CONTENT, visionSyncRequest);

            ObjectMapper obj = new ObjectMapper();
            LOGGER.info("Vision Save Reflow Request Content for " + processName + " is : " + obj.writeValueAsString(visionSyncRequest));
        } catch (Exception e) {
            LOGGER.error("Error in initiateVisionSaveReflowRequest method for " + processName + ". Exception : " + e);
            throw new Exception(e);
        }
    }


    /**
     * This method is used to validate the response received from Vision Save
     * Order
     *
     * @param kcontext
     * @throws Exception
     */
    public void validateVisionSaveReflowResponse(ProcessContext kcontext, String processName) throws Exception {
        LOGGER.debug("Entering validateVisionSaveReflowResponse() method for " + processName);
        kcontext.setVariable("flag", false);
        try {
            VisionSaveRecordResponse visionSaveResponse = (VisionSaveRecordResponse) (kcontext.getVariable(BPMConstants.RESULT));

            if (null != visionSaveResponse) {
                LOGGER.info("Vision Save Reflow Response is : " + visionSaveResponse + " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
                        + kcontext.getProcessInstance().getParentProcessInstanceId());
                String status = visionSaveResponse.getStatus();
                if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
                    LOGGER.info("Vision Save Reflow responded with Success for " + processName);
                    kcontext.setVariable("flag", true);
                } else {
                    LOGGER.debug("ErrorCode : " + visionSaveResponse.getStatusCode() + ", Error Source : VISION , Error Details : " + visionSaveResponse.getStatusDesc());
                    if (!"Compensation".equalsIgnoreCase(processName)) {
                        CompensationUtil compUtil = new CompensationUtil();
                        compUtil.setCompensationObject(kcontext, visionSaveResponse.getStatusCode(), "VISION", visionSaveResponse.getStatusDesc(),
                                null, BPMConstants.COMP_COUNT);
                    } else {
                        ObjectMapper obj = new ObjectMapper();
                        throw new Exception("Received failure response from Vision Save Reflow : " + obj.writeValueAsString(visionSaveResponse));
                    }
                }
            } else {
                LOGGER.error("Response object VisionSaveRecordResponse from Vision Save Reflow is NULL or unexpected");
                throw new Exception("Response object VisionSaveRecordResponse from Vision Save Reflow is NULL or unexpected");
            }
        } catch (Exception e) {
            LOGGER.error("Exception while validating response received from Vision Save Reflow for " + processName + ". Exception : " + e);
            throw new Exception(e);
        }
    }

    /**
     * This method is used to set all the variable values required to update
     * Vision Seeding Data
     *
     * @param kcontext
     * @throws Exception
     */
    public void initiateVisionSeedDataRequest(ProcessContext kcontext) throws Exception {
        LOGGER.info("Entering initiateVisionSeedDataRequest() method");
        try {
            WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
            long parentProcessInstanceId = kcontext.getProcessInstance().getParentProcessInstanceId();
            long processInstanceId = kcontext.getProcessInstance().getId();
            LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:" + processInstanceId
                    + " ParentProcessInstanceId:" + parentProcessInstanceId);
            ServiceProvider bpmUtil = new ServiceProvider();
            String visionSeedDataUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VISION_SEED_DATA_URL.toString(),
                    BPMConstants.SERVICE_ID_BILLING);

            LOGGER.info("Vision Seed Data URL is : " + visionSeedDataUrl);
            kcontext.setVariable(BPMConstants.URL, visionSeedDataUrl);
            kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
            kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
            kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
            kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

            BillingBPMWorkFlow bpmWorkflow = new BillingBPMWorkFlow();
            bpmWorkflow.setProcessInstanceId(String.valueOf(processInstanceId));
            bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
            bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_VSO_RESPONSE);
            bpmWorkflow.setDeploymentId(
                    (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
            if (PropertyFile.getInstance().isConsumerClustered()) {
                bpmWorkflow.setSite(
                        PropertyFile.getInstance().getClusterId() + "-" + PropertyFile.getInstance().getDataCenter());
            } else {
                bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
            }

            VisionSyncRequest visionSyncRequest = new VisionSyncRequest();

            BillingWorkFlowRequest billingWorkFlowRequest = new BillingWorkFlowRequest();
            BeanUtils.copyProperties(billingWorkFlowRequest, workFlowRequest);

            List<BillingWorkFlowRequest> wfList = new ArrayList<BillingWorkFlowRequest>();
            wfList.add(billingWorkFlowRequest);
            visionSyncRequest.setWorkFlowRequestObject(wfList);

            visionSyncRequest.setBpmWorkFlowObject(bpmWorkflow);
            kcontext.setVariable(BPMConstants.CONTENT, visionSyncRequest);

            ObjectMapper obj = new ObjectMapper();
            LOGGER.info("Vision Seed Data Request Content is : " + obj.writeValueAsString(visionSyncRequest));
        } catch (Exception e) {
            LOGGER.error("Error in initiateVisionSeedDataRequest method. Exception : " + e);
            throw new Exception(e);
        }
    }

    /**
     * This method is used to validate the response received from Billing for
     * Vision Seed Data Request
     *
     * @param kcontext
     * @throws Exception
     */
    public void validateVisionSeedDataResponse(ProcessContext kcontext) throws Exception {
        LOGGER.debug("Entering validateVisionSeedDataResponse() method ");
        kcontext.setVariable("flag", false);
        try {
            VisionSaveRecordResponse visionSeedDataResponse = (VisionSaveRecordResponse) (kcontext
                    .getVariable(BPMConstants.RESULT));

            if (null != visionSeedDataResponse) {
                LOGGER.info("Vision Seed Data Response is : " + visionSeedDataResponse + " for ProcessInstanceId: "
                        + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
                        + kcontext.getProcessInstance().getParentProcessInstanceId());
                String status = visionSeedDataResponse.getStatus();
                if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
                    LOGGER.info("Vision Seed Data Response was successful");
                    kcontext.setVariable("flag", true);
                } else {
                    LOGGER.debug("ErrorCode : " + visionSeedDataResponse.getStatusCode()
                            + ", Error Source : VISION , Error Details : " + visionSeedDataResponse.getStatusDesc());
                    CompensationUtil compUtil = new CompensationUtil();
                    compUtil.setCompensationObject(kcontext, visionSeedDataResponse.getStatusCode(), "VISION",
                            visionSeedDataResponse.getStatusDesc(), null, BPMConstants.COMP_COUNT);
                }
            } else {
                LOGGER.error("Response object VisionSaveRecordResponse for Vision Seed Data request is NULL or unexpected");
                throw new Exception(
                        "Response object VisionSaveRecordResponse for Vision Seed Data request is NULL or unexpected");
            }
        } catch (Exception e) {
            LOGGER.error("Exception while validating response received for Vision Seed Data request. Exception : " + e);
            throw new Exception(e);
        }
    }
}