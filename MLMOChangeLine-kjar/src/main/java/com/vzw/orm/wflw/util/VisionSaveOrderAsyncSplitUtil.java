package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vzw.orm.bpmi.domain.objects.consumer.ConsumerWorkflowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;

public class VisionSaveOrderAsyncSplitUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(VisionSaveOrderAsyncSplitUtil.class);

	/**
	 * This method is used to split the request as a set of 15 Vision Save Order
	 * Async status
	 *
	 * @param kcontext
	 * @throws Exception
	 */

	public void initiateVisionSaveAsyncSplitRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiateVisionSaveAsyncSplitRequest() method ...");
		try {

			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
					.getVariable(BPMConstants.CONSUMERWFLREQUEST);

			int totalCountOfWorkFlowRequests = consumerWFRequest.getLstOfWorkflowRequest().size();
			LOGGER.info("Total Count of Work Flow Request found : " + totalCountOfWorkFlowRequests);

			// split workflow based on the mtn and account info
			workFLowRequestSplit(consumerWFRequest, kcontext);
		} catch (Exception ex) {
			LOGGER.error("Error in initiateVisionSaveAsyncSplitRequest method. Exception : " + ex);
			throw new Exception(ex);
		}
	}

	private void workFLowRequestSplit(ConsumerWorkflowRequest consumerWFRequest, ProcessContext kcontext) {

		try {
			LOGGER.info("Req Details List of WorkflowReq Cont :{} ",
					consumerWFRequest.getLstOfWorkflowRequest().size());
			ServiceProvider bpmUtil = new ServiceProvider();
			Boolean value = consumerWFRequest.isOrderLevel();
			Boolean isAccountLineExist = false;
			LOGGER.info("consumer Work Flow Request order Level Value: {}", value.toString());

			List<WorkFlowRequest> lstFirstSynWorkflowRequetWithAccount = new ArrayList<>();
			Map<String, List<WorkFlowRequest>> groupByMtns = null;
			List<List<WorkFlowRequest>> lstRestOfAllWorkFlowRequest = new ArrayList();

			if (consumerWFRequest.getLstOfWorkflowRequest().size() > 0) {

				lstFirstSynWorkflowRequetWithAccount = consumerWFRequest.getLstOfWorkflowRequest().stream()
						.filter(e -> e.getLineItemNumber() != null && Integer.parseInt(e.getLineItemNumber()) >= 100)
						.collect(Collectors.toList());

				groupByMtns = consumerWFRequest.getLstOfWorkflowRequest().stream()
						.filter(e -> Integer.parseInt(e.getLineItemNumber()) < 100)
						.collect(Collectors.groupingBy(WorkFlowRequest::getMtn));
			}

			if (lstFirstSynWorkflowRequetWithAccount.size() > 0) {
				isAccountLineExist = true;
			}

			LOGGER.info("After split the separate list of WorkFlowRequests  account :{} , mtnMAp :{} ,totla :{}",
					lstFirstSynWorkflowRequetWithAccount.size(), groupByMtns.size(),
					consumerWFRequest.getLstOfWorkflowRequest().size());

			int chunkSize = BPMConstants.DEFAULT_CHUNK_SIZE;
			String chunkSizeValue = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.CHUNK_SIZE_VALUE_VISION.toString(),
					BPMConstants.CHUNK_SIZE);
			try {
				if (chunkSizeValue != null) {
					chunkSize = Integer.parseInt(chunkSizeValue);
				}
			} catch (NumberFormatException ex) {
				LOGGER.info("Number Format exception for the input:" + ex.getMessage());
			}

			if (groupByMtns != null && groupByMtns.size() > 0) {

				// try to together lines with account in the first list
				List<String> addListIntoAccount = new ArrayList<String>();
				for (Map.Entry<String, List<WorkFlowRequest>> groupByMtn : groupByMtns.entrySet()) {

					if (lstFirstSynWorkflowRequetWithAccount.size() <= chunkSize) {

						int remainToFile = (chunkSize - lstFirstSynWorkflowRequetWithAccount.size());
						if (remainToFile >= groupByMtn.getValue().size()) {
							lstFirstSynWorkflowRequetWithAccount.addAll(groupByMtn.getValue());
							addListIntoAccount.add(groupByMtn.getKey());
							LOGGER.info(
									"loop firstListwithAccount   total size : {} - each time add into list   : {}  ",
									lstFirstSynWorkflowRequetWithAccount.size(), groupByMtn.getValue().size());
						}
					}
				}

				if (addListIntoAccount.size() > 0) {

					for (String key : addListIntoAccount) {
						groupByMtns.remove(key);
					}
				}
				LOGGER.info("loop firstListwithAccount total  size    {} ",
						lstFirstSynWorkflowRequetWithAccount.size());

				// total size
				boolean isGroupAsList = true;
				while (!groupByMtns.isEmpty()) {
					List<WorkFlowRequest> listOfWFReqFinalSplit = new ArrayList();

					List<String> addListKey = new ArrayList<String>();

					for (Map.Entry<String, List<WorkFlowRequest>> groupByMtn : groupByMtns.entrySet()) {
						LOGGER.info("MTN size" + Integer.toString(groupByMtn.getValue().size()) + "Listed "
								+ groupByMtn.toString());
						if (listOfWFReqFinalSplit.size() <= chunkSize) {
							int remainToFile = (chunkSize - listOfWFReqFinalSplit.size());
							if (remainToFile >= groupByMtn.getValue().size()) {
								listOfWFReqFinalSplit.addAll(groupByMtn.getValue());
								addListKey.add(groupByMtn.getKey());
								LOGGER.info(
										"loop listOfWFReqFinalSplit   total size : {} 	each time add into list   : {} ",
										listOfWFReqFinalSplit.size(), groupByMtn.getValue().size());
							}
							// when remainToFile is less than the groupByMTN
							// We need to add the sublist of the values into the chunk size of
							// listOfWFReqFinalSplit
							else {
								int count = 0;
								addListKey.add(groupByMtn.getKey());
								LOGGER.info("RemaintoFile less than groupByMtn, Creating Sublists");
								if(listOfWFReqFinalSplit.size() !=0){

									lstRestOfAllWorkFlowRequest.add(listOfWFReqFinalSplit);
								}
								while (count <= groupByMtn.getValue().size()) {
									listOfWFReqFinalSplit = new ArrayList();
									if (count + chunkSize <= groupByMtn.getValue().size()) {
										listOfWFReqFinalSplit
												.addAll(groupByMtn.getValue().subList(count, count + chunkSize));
										lstRestOfAllWorkFlowRequest.add(listOfWFReqFinalSplit);
										count += chunkSize;
									} else if (count >= groupByMtn.getValue().size() - chunkSize) {
										listOfWFReqFinalSplit.addAll(
												groupByMtn.getValue().subList(count, groupByMtn.getValue().size()));
										count += chunkSize;
										LOGGER.info("Final Split");
									}
									LOGGER.info(
											"inner loop for bigger groupByMtn listOfWFReqFinalSplit   total size : {} 	each time add into list   : {} ",
											listOfWFReqFinalSplit.size(), groupByMtn.getValue().size());
								}
							}
						}else {
						    break;
                        }
					}

					if (listOfWFReqFinalSplit.size() > 0) {
						lstRestOfAllWorkFlowRequest.add(listOfWFReqFinalSplit);
						for (String key : addListKey) {
							groupByMtns.remove(key);
						}
					}
					LOGGER.info("loop listOfWFReqFinalSplit each time size 2    {} - final list {} ",
							listOfWFReqFinalSplit.size(), lstRestOfAllWorkFlowRequest.size());
				}

			}

			ConsumerWorkflowRequest firstFifttenConsumerWFRPayload = new ConsumerWorkflowRequest();
			if (lstFirstSynWorkflowRequetWithAccount.size()==0) {
				firstFifttenConsumerWFRPayload.setLstOfWorkflowRequest(lstRestOfAllWorkFlowRequest.get(0));
				firstFifttenConsumerWFRPayload.setOrderLevel(false);
				lstRestOfAllWorkFlowRequest.remove(0);
			} else {

				firstFifttenConsumerWFRPayload.setLstOfWorkflowRequest(lstFirstSynWorkflowRequetWithAccount);
				firstFifttenConsumerWFRPayload.setOrderLevel(isAccountLineExist);

			}

			kcontext.setVariable("SyncCallListConsumerWorkflowRequest", firstFifttenConsumerWFRPayload);
			LOGGER.info(" first syc call workflow request (firstSyncCallListForconsumerWorkflowRequest)   size : {} ",
					lstFirstSynWorkflowRequetWithAccount.size());
			LOGGER.debug("Consumer Work Flow Content For First fifteen list are: "
					+ kcontext.getVariable("firstSyncCallListForconsumerWorkflowRequest"));

			List<ConsumerWorkflowRequest> consumerWFListFinal = new ArrayList();
			int i = 1;
			for (List<WorkFlowRequest> wfrLst : lstRestOfAllWorkFlowRequest) {
				i = i + 1;
				ConsumerWorkflowRequest consumerPayload = new ConsumerWorkflowRequest();
				consumerPayload.setLstOfWorkflowRequest(wfrLst);
				consumerPayload.setOrderLevel(false);
				consumerWFListFinal.add(consumerPayload);

				LOGGER.info(
						" rest of the asych call workflow request split  count  {} (ConsumerWorkflowRequest)   size : {} ",
						i, wfrLst.size());
			}
			kcontext.setVariable("AsyncCallListConsumerWorkflowRequest", consumerWFListFinal);

			LOGGER.debug("rest of the asych call workflow request: "
					+ kcontext.getVariable("firstSyncCallListForconsumerWorkflowRequest"));

		} catch (Exception ex) {
			LOGGER.error("Error in workFLowRequestSplit method. Exception : " + ex);

		}
	}

	public void initiateSplitRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiateSplitRequest() method ...");
		// boolean countFlag = true;
		boolean isDefaultLineSizeExit = false; // default size 15 line as per DVS , which is allow us to save upto 15
		// line to vision at one request
		try {

			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
					.getVariable(BPMConstants.CONSUMERWFLREQUEST);

			int totalCountOfWorkFlowRequests = consumerWFRequest.getLstOfWorkflowRequest().size();
			LOGGER.info("Total Count of Work Flow Request found : " + totalCountOfWorkFlowRequests);
			if (totalCountOfWorkFlowRequests <= 15) {
				// countFlag = false;
				isDefaultLineSizeExit = true;
			}
			kcontext.setVariable("isDefaultLineSizeExit", isDefaultLineSizeExit);

		} catch (Exception ex) {
			LOGGER.error("Error in initiateSplitRequest method. Exception : " + ex);
			kcontext.setVariable("isDefaultLineSizeExit", isDefaultLineSizeExit);
		}
	}
}