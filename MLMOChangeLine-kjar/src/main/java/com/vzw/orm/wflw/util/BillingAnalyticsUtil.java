package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.billinganalytics.domaininteg.BillingAnalyticsRequest;
import com.vzw.orm.billinganalytics.domaininteg.BillingAnalyticsResponse;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;

public class BillingAnalyticsUtil implements Serializable
{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(BillingAnalyticsUtil.class);

	/**
	 * This method is used to set all the variable values required to call
	 * Vendor Provisioning in Consumer Workflow
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateBARequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiate"+kcontext.getVariable("component") +"Request() method ...");
		try
		{
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);

			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			
			ServiceProvider bpmUtil = new ServiceProvider();
			String url = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.BILLING_ANA_URL.toString(), BPMConstants.BILLING_ANA);
			LOGGER.info(kcontext.getVariable("component") + "BA URL is : " + url);
			kcontext.setVariable(BPMConstants.URL, url);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			BillingAnalyticsRequest billingAnalyticsRequest = new BillingAnalyticsRequest();
			billingAnalyticsRequest.setAccountNumber(workFlowRequest.getAccountNumber());
			billingAnalyticsRequest.setBillSysId(workFlowRequest.getBillingInstance());
			billingAnalyticsRequest.setCustomerId(workFlowRequest.getCustomerId());
			billingAnalyticsRequest.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			billingAnalyticsRequest.setLineItemNumber(workFlowRequest.getLineItemNumber());
			billingAnalyticsRequest.setMtn(workFlowRequest.getMtn());
			billingAnalyticsRequest.setOrderId(workFlowRequest.getOrderId());
			billingAnalyticsRequest.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			billingAnalyticsRequest.setCorrelationId(workFlowRequest.getCorrelationId());
			billingAnalyticsRequest.setProductionParallelInd(workFlowRequest.getProductionParallelInd());
			
			kcontext.setVariable(BPMConstants.CONTENT, billingAnalyticsRequest);
			ObjectMapper obj = new ObjectMapper();
			
			LOGGER.info(kcontext.getVariable("component")+" Request Content is : " + obj.writeValueAsString(billingAnalyticsRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiate"+kcontext.getVariable("component")+"Request method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from Vendor
	 * Provisioning
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateBAResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validate"+kcontext.getVariable("component")+"Response() method ...");

		boolean flag = false;
		try
		{
			BillingAnalyticsResponse baResponse = (BillingAnalyticsResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != baResponse)
			{
				LOGGER.info("BA Response is : " + baResponse + " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String deviceProreqStatus = baResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(deviceProreqStatus))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					LOGGER.info(
							"ErrorCode : " + baResponse.getStatusCode() + ", Error Details : " + baResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, baResponse.getStatusCode(), "BA", baResponse.getStatusDesc(),
							null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				LOGGER.info("Response Object from "+kcontext.getVariable("component")+" is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPM_RESP_NULL_MS", "BPM", "Response Object from BA Micro Service is NULL", null,
						BPMConstants.COMP_COUNT);
			}
			

		}
		catch (Exception e)
		{
			LOGGER.error("Exception while validating response received from "+kcontext.getVariable("component")+". Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

}