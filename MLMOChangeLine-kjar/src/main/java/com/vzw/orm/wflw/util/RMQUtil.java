/**
 * 
 */
package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.ConsumerWorkflowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;


/**
 * @author sankuvi
 *
 */
public class RMQUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(RMQUtil.class);

	
	public void initializeRMQForOrderLevel(ProcessContext kcontext) throws Exception{
		LOGGER.info("inside initializeRMQForOrderLevel method");
		long starttime = System.currentTimeMillis();
		try{
			kcontext.setVariable(BPMConstants.FLAG, false);
			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext.getVariable(BPMConstants.CONSUMERWFLREQUEST);
			Properties props = PropertyFile.getProjectProperties();
			LOGGER.info("loaded props from PropertyFile");
			kcontext.setVariable(BPMConstants.RMQ_HOST, props.getProperty("rabbitmq.host"));
			kcontext.setVariable(BPMConstants.RMQ_VIRTUAL_HOST, props.getProperty("rabbitmq.virtualhost"));
			kcontext.setVariable(BPMConstants.RMQ_PORT, props.getProperty("rabbitmq.port"));
			kcontext.setVariable(BPMConstants.RMQ_ACCESS_ID, props.getProperty("rabbitmq.accessid"));
			kcontext.setVariable(BPMConstants.RMQ_ACCESS_CODE, props.getProperty("rabbitmq.accesscode"));
			kcontext.setVariable(BPMConstants.RMQ_EXCHANGE, props.getProperty("rabbitmq.exchange"));
			kcontext.setVariable(BPMConstants.RMQ_TYPE, props.getProperty("rabbitmq.type"));
			kcontext.setVariable(BPMConstants.RMQ_ROUTING_KEY, props.getProperty("rabbitmq.async.routing.key"));
			LOGGER.info("RMQ_ASYNC_ROUTING_KEY:"+kcontext.getVariable(BPMConstants.RMQ_ROUTING_KEY));
			ObjectMapper obj = new ObjectMapper();
			Map<String, String> request = new HashMap<String, String>();
			request.put("deploymentId", (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			request.put("parentprocessinstanceid", String.valueOf(kcontext.getProcessInstance().getId()));
			if (PropertyFile.getInstance().isConsumerClustered()) {
				request.put("bpmClusterId", "BPM-"+PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				request.put("bpmClusterId", "BPM-"+PropertyFile.getInstance().getDataCenter());
			}
			request.put("processDefId", props.getProperty("MLMO_ORDER_LINE_LEVEL_PROCESS_DEF_ID"));
			
			String payload = "{\"consumerWorkflowRequest\": " +obj.writeValueAsString(consumerWFRequest) + "}";
			request.put("payload", "PAYLOAD");
			String requestBefore = obj.writeValueAsString(request);
			String requestAfter = requestBefore.replace("\"PAYLOAD\"", payload);
			kcontext.setVariable(BPMConstants.MESSAGE, requestAfter);
			LOGGER.info("MESSAGE:"+kcontext.getVariable(BPMConstants.MESSAGE));
			kcontext.setVariable(BPMConstants.FLAG, true);
			long endtime = System.currentTimeMillis();
			LOGGER.info("Time taken to execute initializeRMQForOrderLevel:" +(endtime-starttime));
		}catch (Exception e){
			LOGGER.error("Exception inside initializeRMQForOrderLevel method. Exception:"+e.getMessage());
			e.printStackTrace();
			throw new Exception("Exception inside initializeRMQForOrderLevel method. Exception:"+e.getMessage());
		}
	}
	
	public void initializeRMQForLineLevel(ProcessContext kcontext) throws Exception{
		LOGGER.info("inside initializeRMQForLineLevel method");
		long starttime = System.currentTimeMillis();
		try{
			kcontext.setVariable(BPMConstants.FLAG, false);
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			Properties props = PropertyFile.getProjectProperties();
			LOGGER.info("loaded props from PropertyFile");
			kcontext.setVariable(BPMConstants.RMQ_HOST, props.getProperty("rabbitmq.host"));
			kcontext.setVariable(BPMConstants.RMQ_VIRTUAL_HOST, props.getProperty("rabbitmq.virtualhost"));
			kcontext.setVariable(BPMConstants.RMQ_PORT, props.getProperty("rabbitmq.port"));
			kcontext.setVariable(BPMConstants.RMQ_ACCESS_ID, props.getProperty("rabbitmq.accessid"));
			kcontext.setVariable(BPMConstants.RMQ_ACCESS_CODE, props.getProperty("rabbitmq.accesscode"));
			kcontext.setVariable(BPMConstants.RMQ_EXCHANGE, props.getProperty("rabbitmq.exchange"));
			kcontext.setVariable(BPMConstants.RMQ_TYPE, props.getProperty("rabbitmq.type"));
			kcontext.setVariable(BPMConstants.RMQ_ROUTING_KEY, props.getProperty("rabbitmq.addline.routing.key"));
			LOGGER.info("RMQ_ADDLINE_ROUTING_KEY:"+kcontext.getVariable(BPMConstants.RMQ_ROUTING_KEY));
			
			ObjectMapper obj = new ObjectMapper();
			Map<String, String> request = new HashMap<String, String>();
			request.put("deploymentId", (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			request.put("parentprocessinstanceid", String.valueOf(kcontext.getProcessInstance().getId()));
			if (PropertyFile.getInstance().isConsumerClustered()) {
				request.put("bpmClusterId", "BPM-"+PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				request.put("bpmClusterId", "BPM-"+PropertyFile.getInstance().getDataCenter());
			}
			
			if("true".equalsIgnoreCase(workFlowRequest.getActivateMtn())){
				request.put("processDefId", "MLMOChangeLine.AddLine");
			} else{
				request.put("processDefId", "MLMOChangeLine.ChangeLine");
			}

			String payload = "{\"workflowRequest\":{\"com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest\": " +obj.writeValueAsString(workFlowRequest) + "}}";
			request.put("payload", "PAYLOAD");
			String requestBefore = obj.writeValueAsString(request);
			String requestAfter = requestBefore.replace("\"PAYLOAD\"", payload);
			kcontext.setVariable(BPMConstants.MESSAGE, requestAfter);
			
			LOGGER.info("MESSAGE:"+kcontext.getVariable(BPMConstants.MESSAGE));
			kcontext.setVariable(BPMConstants.FLAG, true);
			long endtime = System.currentTimeMillis();
			LOGGER.info("Time taken to execute initializeRMQForLineLevel:" +(endtime-starttime));
		}catch (Exception e){
			LOGGER.error("Exception inside initializeRMQForLineLevel method. Exception:"+e.getMessage());
			e.printStackTrace();
			throw new Exception("Exception inside initializeRMQForOrderLevel method. Exception:"+e.getMessage());
		}
	}
	
	public void initializeRMQForPostLCProcessing(ProcessContext kcontext) throws Exception{
		LOGGER.info("inside initializeRMQForPostLCProcessing method");
		long starttime = System.currentTimeMillis();
		try{
			kcontext.setVariable(BPMConstants.FLAG, false);
			Properties props = PropertyFile.getProjectProperties();
			LOGGER.info("loaded props from PropertyFile");
			kcontext.setVariable(BPMConstants.RMQ_HOST, props.getProperty("rabbitmq.host"));
			kcontext.setVariable(BPMConstants.RMQ_VIRTUAL_HOST, props.getProperty("rabbitmq.virtualhost"));
			kcontext.setVariable(BPMConstants.RMQ_PORT, props.getProperty("rabbitmq.port"));
			kcontext.setVariable(BPMConstants.RMQ_ACCESS_ID, props.getProperty("rabbitmq.accessid"));
			kcontext.setVariable(BPMConstants.RMQ_ACCESS_CODE, props.getProperty("rabbitmq.accesscode"));
			kcontext.setVariable(BPMConstants.RMQ_EXCHANGE, props.getProperty("rabbitmq.exchange"));
			kcontext.setVariable(BPMConstants.RMQ_TYPE, props.getProperty("rabbitmq.type"));
			kcontext.setVariable(BPMConstants.RMQ_ROUTING_KEY, props.getProperty("rabbitmq.misc.routing.key"));
			LOGGER.info("RMQ_MISC_ROUTING_KEY:" + kcontext.getVariable(BPMConstants.RMQ_ROUTING_KEY));
			
			ObjectMapper obj = new ObjectMapper();
			Map<String, String> request = new HashMap<String, String>();
			request.put("deploymentId", (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			request.put("processInstanceId", String.valueOf(kcontext.getProcessInstance().getParentProcessInstanceId()));
			if (PropertyFile.getInstance().isConsumerClustered()) {
				request.put("bpmClusterId", "BPM-"+PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				request.put("bpmClusterId", "BPM-"+PropertyFile.getInstance().getDataCenter());
			}			
			request.put("signalId", "POSTLC");

			String requestString = obj.writeValueAsString(request);
			kcontext.setVariable(BPMConstants.MESSAGE, requestString);
			
			LOGGER.info("MESSAGE: " + kcontext.getVariable(BPMConstants.MESSAGE));
			kcontext.setVariable(BPMConstants.FLAG, true);
			long endtime = System.currentTimeMillis();
			LOGGER.info("Time taken to execute initializeRMQForPostLCProcessing:" +(endtime-starttime));
		}catch (Exception e){
			LOGGER.error("Exception inside initializeRMQForPostLCProcessing method. Exception:"+e.getMessage());
			e.printStackTrace();
			throw new Exception("Exception inside initializeRMQForPostLCProcessing method. Exception:"+e.getMessage());
		}
	}
}
