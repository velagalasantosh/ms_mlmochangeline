package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vzw.orm.provisioning.domain.common.ProvisioningBPMWorkFlow;
import com.vzw.orm.provisioning.domain.common.ProvisioningExceptionInfo;
import com.vzw.orm.provisioning.domain.common.MtasProducerRequest;
import com.vzw.orm.provisioning.domain.common.MtasProducerResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.provisioning.domain.common.ProvisioningWorkFlowRequest;
import com.vzw.orm.vendorprovisioning.domain.common.VpBPMWorkFlow;
import com.vzw.orm.vendorprovisioning.domain.common.VpMtasProducerRequest;
import com.vzw.orm.vendorprovisioning.domain.common.VpWorkFlowRequest;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class VendorProvisioningUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(VendorProvisioningUtil.class);

	/**
	 * This method is used to set all the variable values required to call
	 * Vendor Provisioning in Consumer Workflow
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateVPRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateVPRequest() method ...");
		try
		{
			
			boolean account = null != kcontext.getVariable("vpAccTriggered") ? (Boolean) kcontext.getVariable("vpAccTriggered") : false;
			
            LOGGER.info("Is Account Level VP: "+account);
			
			if (!account) {
				
				WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
				//setting features to workflowRequest object as part of VP
				ProvisioningWorkFlowRequest proWorkFlowRequest = new ProvisioningWorkFlowRequest();
				//converting workflow object to provisioning workflow object
				BeanUtils.copyProperties(proWorkFlowRequest, workFlowRequest);
				proWorkFlowRequest.setFeatures((String)kcontext.getVariable("features"));
				LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				
				kcontext.setVariable(BPMConstants.FLAG, false);

				ServiceProvider bpmUtil = new ServiceProvider();
				String url = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VP_URL.toString(), BPMConstants.SERVICE_ID_PROV);
				LOGGER.debug("VP URL is : " + url);
				kcontext.setVariable(BPMConstants.URL, url);
				kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
				kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
				kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
				kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

				ProvisioningBPMWorkFlow bpmWorkflow = new ProvisioningBPMWorkFlow();
				bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
				bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
				bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
				if (PropertyFile.getInstance().isConsumerClustered()) {
					bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
							+ PropertyFile.getInstance().getDataCenter());
				} else {
					bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
				}
				
				MtasProducerRequest mtasRequest = new MtasProducerRequest();
				mtasRequest.setBpmWorkFlowObject(bpmWorkflow);
				mtasRequest.setProvisioningRequestType("Vendor Provisioning");
				mtasRequest.setWorkFlowRequestObject(proWorkFlowRequest);
				kcontext.setVariable(BPMConstants.CONTENT, mtasRequest);
				ObjectMapper obj = new ObjectMapper();
				LOGGER.info("VP Request Content is : " + obj.writeValueAsString(mtasRequest));
			} else {
				LOGGER.info("Inside Account Level VP...");
				WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);

				ServiceProvider bpmUtil = new ServiceProvider();
				String url = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VP_ACC_URL.toString(),
						BPMConstants.SERVICE_ID_VENDOR_PROV);
				LOGGER.info("VP Account URL is : " + url);
				kcontext.setVariable(BPMConstants.URL, url);
				kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
				kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
				kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT,
						PropertyFile.getInstance().getConnectionTimeOut());
				kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

				VpBPMWorkFlow bpmWorkflow = new VpBPMWorkFlow();
				bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
				bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
				bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
				if (PropertyFile.getInstance().isConsumerClustered()) {
					bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
							+ PropertyFile.getInstance().getDataCenter());
				} else {
					bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
				}
				VpWorkFlowRequest vpWorkFlowRequest = new VpWorkFlowRequest();
				// converting workflow object to vpWorkFlowRequest object
				BeanUtils.copyProperties(vpWorkFlowRequest, workFlowRequest);
				VpMtasProducerRequest vpMtasProducerRequest = new VpMtasProducerRequest();
				vpMtasProducerRequest.setProvisioningRequestType("Vendor Provisioning");
				vpMtasProducerRequest.setBpmWorkFlowObject(bpmWorkflow);
				vpMtasProducerRequest.setWorkFlowRequestObject(vpWorkFlowRequest);
				kcontext.setVariable(BPMConstants.CONTENT, vpMtasProducerRequest);
				ObjectMapper obj = new ObjectMapper();
				LOGGER.info("Account Level VP Request Content is : " + obj.writeValueAsString(vpMtasProducerRequest));

			}
			
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateVPRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received from Vendor
	 * Provisioning
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateVPResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateVPResponse() method ...");

		boolean flag = false;
		try
		{
			MtasProducerResponse vpResponse = (MtasProducerResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != vpResponse)
			{
				LOGGER.info("VP Response is : " + vpResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String deviceProreqStatus = vpResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(deviceProreqStatus))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					
				}
				else
				{
					ProvisioningExceptionInfo expInfo = vpResponse.getExceptionInfo();
					LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : " + expInfo.getErrorSource() + ", Error Details : " + expInfo.getErrorDeatils());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), expInfo.getErrorSource(), expInfo.getErrorDeatils(),
							null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				LOGGER.info("Response Object from Vendor Provisioing is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL", null,
						BPMConstants.COMP_COUNT);
			}
			

		}
		catch (Exception e)
		{
			LOGGER.error("Exception while validating response received from VP. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

}
