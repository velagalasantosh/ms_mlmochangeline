package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.ConsumerWorkflowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.saveorder.vision.datacontract.VisionSaveRecordResponse;
import com.vzw.orm.saveorder.vision.datacontract.VisionSaveRecordSignalResponse;
import com.vzw.orm.saveorder.vision.datacontract.VisionSyncRequest;
import com.vzw.orm.saveorder.vision.domain.SovBPMWorkFlow;
import com.vzw.orm.saveorder.vision.domain.SovWorkFlowRequest;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class VisionSaveOrderUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(VisionSaveOrderUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * Vision Save Order Call
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateVisionSaveOrderRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.debug("Entering initiateVisionSaveOrderRequest() method ...");
		try
		{
			ObjectMapper mapper = new ObjectMapper();
			ServiceProvider bpmUtil = new ServiceProvider();
			String visionSaveOrderUrl = null;
			String sovType = "";
			if(kcontext.getVariable("sovCancelReq")!=null) {
				LOGGER.info("Calling Vision Save Order Cancel");
				sovType = "Cancel ";
				visionSaveOrderUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VISION_SAV_CANCEL_URL.toString(),BPMConstants.SAVEORDER_VISION);
			} else{
				visionSaveOrderUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VISION_SAVORDER_URL.toString(),BPMConstants.SAVEORDER_VISION);
			}
			LOGGER.info("Vision Save Order " +sovType +"URL is : " + visionSaveOrderUrl);
			kcontext.setVariable(BPMConstants.URL, visionSaveOrderUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			
			long parentProcessInstanceId = kcontext.getProcessInstance().getParentProcessInstanceId();
			long processInstanceId = kcontext.getProcessInstance().getId();
			
			SovBPMWorkFlow bpmWorkflow = new SovBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(processInstanceId));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_VSO_RESPONSE);
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext.getVariable(BPMConstants.CONSUMERWFLREQUEST);
//			LOGGER.info("Vision Save Order Request content is : " +consumerWFRequest);
			WorkFlowRequest workFlowRequest = consumerWFRequest.getLstOfWorkflowRequest().get(0);
			kcontext.setVariable(BPMConstants.WORKFLOW_REQ, workFlowRequest);
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:"
							+ processInstanceId + " ParentProcessInstanceId:" + parentProcessInstanceId);
			kcontext.setVariable("orderKey", "orderId,"+workFlowRequest.getOrderId());
			kcontext.setVariable("dueDate", workFlowRequest.getOrderDueDate());
			kcontext.setVariable("productionParallelInd",
					"Y".equalsIgnoreCase(workFlowRequest.getProductionParallelInd())
							? true : false);
			
			String outageDC = PropertyFile.getProjectProperties().get("VISION_OUTAGE_DC")!= null ?
					PropertyFile.getProjectProperties().get("VISION_OUTAGE_DC").toString() : "";
			LOGGER.info("outageDC:"+outageDC);
			if(workFlowRequest.getBillingInstance().matches(outageDC)){
				kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getProjectProperties().get(BPMConstants.VSO_OUTAGE_CONNECTIONTIMEOUT));
				kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getProjectProperties().get(BPMConstants.VSO_OUTAGE_READTIMEOUT));
			} else{
				kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getProjectProperties().get(BPMConstants.VSO_CONNECTIONTIMEOUT));
				kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getProjectProperties().get(BPMConstants.VSO_READTIMEOUT));	
			}
			LOGGER.info("VSO_CONNECTIONTIMEOUT: " + kcontext.getVariable(BPMConstants.CONNECTION_TIME_OUT));
			LOGGER.info("VSO_READTIMEOUT: " + kcontext.getVariable(BPMConstants.READ_TIME_OUT));
			
			
			List<SovWorkFlowRequest> sovWorkFlowRequestObjectList = new ArrayList<SovWorkFlowRequest>();
			for(WorkFlowRequest workFlowRequestTemp : consumerWFRequest.getLstOfWorkflowRequest())
			{
				SovWorkFlowRequest sovWorkFlowRequest = new SovWorkFlowRequest();
				//converting workflow object to provisioning workflow object
				BeanUtils.copyProperties(sovWorkFlowRequest, workFlowRequestTemp);
				sovWorkFlowRequestObjectList.add(sovWorkFlowRequest);
			}
			
			VisionSyncRequest visionSyncRequest = new VisionSyncRequest();
			visionSyncRequest.setWorkFlowRequestObject(sovWorkFlowRequestObjectList);
			visionSyncRequest.setBpmWorkFlowObject(bpmWorkflow);
			kcontext.setVariable(BPMConstants.CONTENT, visionSyncRequest);
			LOGGER.info("Vision Save Order " +sovType +"Request content is : " + mapper.writeValueAsString(visionSyncRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateVisionSaveOrderRequest method. Exception : " + e);
			kcontext.setVariable("error", 
					"Error in initiateVisionSaveOrderRequest method. Exception : "+e);
			throw new Exception((String)kcontext.getVariable("error"));
		}
	}

	/**
	 * This method is used to validate the response received from Vision Save
	 * Order
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateVisionSaveOrderResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.debug("Entering validateVisionSaveOrderResponse() method ...");
		String sovType = "";
		if(kcontext.getVariable("sovCancelReq")!=null && (Boolean)kcontext.getVariable("sovCancelReq")){
			sovType = "Cancel ";
		}
		
		try
		{
			int statusCode = (Integer) kcontext.getVariable("status");
			String statusMsg = (String) kcontext.getVariable("statusMsg");
			LOGGER.info("Status : " + statusCode + ". Status Message : " + statusMsg);
			
			if(statusCode >= 200 && statusCode < 300){
				
				VisionSaveRecordResponse visionSaveResponse = (VisionSaveRecordResponse) (kcontext.getVariable(BPMConstants.RESULT));
				ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext.getVariable(BPMConstants.CONSUMERWFLREQUEST);
				if (null != visionSaveResponse)
				{
					LOGGER.info("Vision Save Order "+sovType+"Response is : " + visionSaveResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
							+ kcontext.getProcessInstance().getParentProcessInstanceId());
					String status = visionSaveResponse.getStatus();
					if ("true".equalsIgnoreCase(visionSaveResponse.getAsyncSaveOrderInd())) {
						consumerWFRequest.getLstOfWorkflowRequest().forEach(wrkflw -> wrkflw.setAsyncSaveOrderInd("true"));
						kcontext.setVariable("asyncSaveOrderInd",true);
					} else {
						consumerWFRequest.getLstOfWorkflowRequest().forEach(wrkflw -> wrkflw.setAsyncSaveOrderInd("false"));
						kcontext.setVariable("asyncSaveOrderInd",false);
					}
					kcontext.setVariable(BPMConstants.CONSUMERWFLREQUEST,consumerWFRequest);
					if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
						LOGGER.info("Vision Save Order "+sovType+"responded with Success");
						kcontext.setVariable("flag", true);
					}
					else
					{
						LOGGER.info("ErrorCode : " + visionSaveResponse.getStatusCode()
								+ ", Error Source : VISION , Error Details : " + visionSaveResponse.getStatusDesc());
						ObjectMapper obj = new ObjectMapper();
						if (visionSaveResponse.getStatusCode() != null && visionSaveResponse.getStatusDesc() != null) {
							if (visionSaveResponse.getStatusCode().equalsIgnoreCase("VISORSERR04_8086_DVS")
									&& visionSaveResponse.getStatusDesc()
											.contains("DUPLICATE ORDER/LINE ITEM NUMBER")) 
							{
								if(kcontext.getVariable("vsoRetryCount") == null){
									throw new Exception("Received failure response from Vision : "
											+ obj.writeValueAsString(visionSaveResponse));
								}else{
									LOGGER.info("Order Save Order "+sovType+" successfull after Timeout Retry");
									kcontext.setVariable("flag", true);
								}
								
							} else if (visionSaveResponse.getStatusCode().equalsIgnoreCase("VISORSERR08_8092_DVS")
									&& visionSaveResponse.getStatusDesc()
											.contains("Timeout while connect to DVS end point")) 
							{
								LOGGER.info("Order Save Order was timedout, hence proceeding to retry");
								if(kcontext.getVariable("workflowRequest")==null){
									consumerWFRequest = (ConsumerWorkflowRequest) kcontext
											.getVariable(BPMConstants.CONSUMERWFLREQUEST);
									kcontext.setVariable("workflowRequest", consumerWFRequest.getLstOfWorkflowRequest().get(0));
								}
								int count = 0;
								if(kcontext.getVariable("vsoRetryCount")==null){
									kcontext.setVariable("vsoRetryCount", 1);
								}else{
									count = Integer.valueOf(String.valueOf(kcontext.getVariable("vsoRetryCount")));
									kcontext.setVariable("vsoRetryCount", count+1);
								}
								kcontext.setVariable("flag", false);
								CompensationUtil compUtil = new CompensationUtil();
								compUtil.setCompensationObject(kcontext, visionSaveResponse.getStatusCode(), "VISION", visionSaveResponse.getStatusDesc(),
										"VISION", "compCount");
	
							} else {
								LOGGER.info("inside else of err check");
								if(kcontext.getVariable("vsoRetryCount")!=null){
									CompensationUtil compUtil = new CompensationUtil();
									compUtil.setCompensationObject(kcontext, visionSaveResponse.getStatusCode(), "VISION", visionSaveResponse.getStatusDesc(),
											"VISION", "compCount");
								} else{
									throw new Exception("Received failure response from Vision : "
											+ obj.writeValueAsString(visionSaveResponse));
								}
							}
							
						} else {
							LOGGER.info("inside else of errcd and desc null");
							
							if(kcontext.getVariable("vsoRetryCount")!=null){
								CompensationUtil compUtil = new CompensationUtil();
								compUtil.setCompensationObject(kcontext, "VSOERR03", "VISION", "Vision Save Order Failed",
										"VISION", "compCount");
							} else{
								kcontext.setVariable("error", "Received failure response from Vision : "
										+ obj.writeValueAsString(visionSaveResponse));
								throw new Exception("Received failure response from Vision : "
										+ obj.writeValueAsString(visionSaveResponse));
							}
						}
					}
				}
				else
				{
					LOGGER.error("Response object VisionSaveRecordResponse from Vision Save Order "+sovType+"is NULL or unexpected for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
							+ kcontext.getProcessInstance().getParentProcessInstanceId());
					kcontext.setVariable("error", 
							"Response object VisionSaveRecordResponse from Vision Save Order "+sovType+"is NULL or unexpected");
					
					if(kcontext.getVariable("vsoRetryCount")!=null){
						CompensationUtil compUtil = new CompensationUtil();
						compUtil.setCompensationObject(kcontext, "VSOERR01", "VISION", "Vision Save Order "+sovType+"Failed",
								"VISION", "compCount");
					} else{
						throw new Exception(
							"Response object VisionSaveRecordResponse from Vision Save Order "+sovType+"is NULL or unexpected");
					}
				}
			} else if(statusCode==408) {
				
				LOGGER.info("BPM was timedout for vision");
				if(kcontext.getVariable("workflowRequest")==null){
					ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext
							.getVariable(BPMConstants.CONSUMERWFLREQUEST);
					kcontext.setVariable("workflowRequest", consumerWFRequest.getLstOfWorkflowRequest().get(0));
				}
				int count = 0;
				if(kcontext.getVariable("vsoRetryCount")==null){
					kcontext.setVariable("vsoRetryCount", 1);
				}else{
					count = Integer.valueOf(String.valueOf(kcontext.getVariable("vsoRetryCount")));
					kcontext.setVariable("vsoRetryCount", count+1);
				}
				
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "VISORSERR08_8092_MS", "VISION", "Read Time Out Exception while invoking VSO MS",
						"VISION", "compCount");
				
				kcontext.setVariable("flag", false);
			}  else {
				LOGGER.error("undefined status code : " +statusCode);
				kcontext.setVariable("error", 
						"Exception invoking Vision Save Order.");
				throw new Exception((String)kcontext.getVariable("error"));
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception while validating response received from Vision Save Order. Exception : " + e);
			kcontext.setVariable("error", 
					"Exception while validating response received from Vision Save Order. Exception : "+e);
			throw new Exception((String)kcontext.getVariable("error"));
		}

	}

	public static void validateFailureResponse(ProcessContext kcontext) throws Exception{
		try {
			CompensationUtil compUtil = new CompensationUtil();
			if (kcontext.getVariable("wsError") != null) {
				LOGGER.info("wsError:" +kcontext.getVariable("wsError").toString());
				compUtil.setCompensationObject(kcontext, "VSOERR01", "BPM", kcontext.getVariable("wsError").toString(),
						"VISION", "compCount");
				kcontext.setVariable("wsError", null);
			} else if(kcontext.getVariable("error") != null) {
				LOGGER.info("error:" +kcontext.getVariable("error").toString());
				compUtil.setCompensationObject(kcontext, "VSOERR02", "BPM", kcontext.getVariable("error").toString(),
						"VISION", "compCount");
				kcontext.setVariable("error", null);
			} else{
				compUtil.setCompensationObject(kcontext, "BPMERR01", "BPM", "Exception while validating failure response",
						"VISION", "compCount");
			}

		} catch (Exception e) {
			LOGGER.info("Exception while validating failure response : " +e);
			CompensationUtil compUtil = new CompensationUtil();
			compUtil.setCompensationObject(kcontext, "BPMERR01", "BPM", "Exception while validating response",
					"VISION", "compCount");
		}
	}
	
	/**
	 * This method is used to validate the response received from Vision Save
	 * Order Signal
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateVisionSaveOrderSignalResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.debug("Entering validateVisionSignalResponse() method ...");

		boolean flag = false;

		try
		{
			VisionSaveRecordSignalResponse visionSaveResponse = (VisionSaveRecordSignalResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != visionSaveResponse)
			{
				LOGGER.info("Vision Save Order Signal Response is : " + visionSaveResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = visionSaveResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					// Call Compensation
					LOGGER.info("ErrorCode : " + visionSaveResponse.getStatusCode()
							+ ", Error Source : VISION , Error Details : " + visionSaveResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, visionSaveResponse.getStatusCode(), "VISION",
							visionSaveResponse.getStatusDesc(), null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object VisionSaveRecordSignalResponse from Vision Save Order is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
						"Response Object from Micro Service is NULL", null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error(
					"Exception while validating response received from Vision Save Order Signal. Exception : " + e);
			LOGGER.info("Response object VisionSaveRecordSignalResponse from Vision Save Order is NULL.");
			CompensationUtil compUtil = new CompensationUtil();
			compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM",
					"Response Onject from Micro Service is either NULL or unexpected", null, BPMConstants.COMP_COUNT);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
}