package com.vzw.orm.wflw.util;

public class BPMConstants
{
	// Property File Constants
	public static final String PROP_FILE_PATH = "prop.path";
	public static final String FILE_NAME = "consumer.properties";
	public static final String EUREKA_FLAG = "get.registry.from.code.hack";
	public static final String IS_CLUSTERED = "is.consumer.clustered";

	// Process Variables common to all process models 
	public static final String URL = "url";
	public static final String KIE_SERVER_INVOKE_ID = "KIE_SERVER_INVOKE_ID";
	public static final String KIE_SERVER_INVOKE_TOKEN = "KIE_SERVER_INVOKE_TOKEN";
	public static final String CONNECTION_TIME_OUT = "connectionTimeOut";
	public static final String VSO_CONNECTIONTIMEOUT = "VSO_CONNECTIONTIMEOUT";
	public static final String MS_ACCESS_ID = "msAccessId";
	public static final String MS_ACCESS_CODE = "msAccessCode";
	public static final String READ_TIME_OUT = "readTimeOut";
	public static final String VSO_READTIMEOUT = "VSO_READTIMEOUT";
	public static final String VSO_OUTAGE_CONNECTIONTIMEOUT = "VSO_OUTAGE_CONNECTIONTIMEOUT";
	public static final String VSO_OUTAGE_READTIMEOUT = "VSO_OUTAGE_READTIMEOUT";
	public static final String CAL_CONNECTIONTIMEOUT = "CAL_CONNECTIONTIMEOUT";
	public static final String CAL_READTIMEOUT = "CAL_READTIMEOUT";
	public static final String CONTENT = "content";
	public static final String RESULT = "result";
	public static final String WORKFLOW_REQ = "workflowRequest";
	public static final String FLAG = "flag";
	public static final String BUCKET_DT = "bucketDate";
	public static final String COMP_COUNT = "compCount";
	public static final String ERROR = "error";
	public static final String STATUS_CODE_SUCCESS = "0000";
	public static final String SUCCESS = "Success";
	public static final String LINE_SATATUS = "lineStatus";
	public static final String GUI = "GUI";
	public static final String GUI_ERR = "GUIERR01";
	public static final String REST_METHOD = "method";
	public static final String BPMI_STATUS_CODE = "00";

	public static final String CONSUMERWFLREQUEST = "consumerWorkflowRequest";
	public static final String CHUNK_SIZE = "chunkSizeValue";
	public static final int DEFAULT_CHUNK_SIZE = 15;
	
	// Order Provisioning Process Variables
	public static final String SPLIT_LIST = "splitList";
	public static final String NE_TYPE_SET = "rcvdNeTypeSet";
	public static final String CALL_GLOB_PROV = "callGlobalProv";
	public static final String MTAS_SUCCESS = "mtasSuccess";
	
	// ETNI Process Variables
	public static final String STATUS_CODE = "statusCode";
	public static final String STATUS_DESC = "statusDesc";
	
	public static final String SUPP_YES = "Y";
	public static final String SUPP_NO = "N";
	public static final String ACTION_TYPE_CANCEL = "Cancel";
	public static final String ACTION_TYPE_CANCEL_RESUBMIT = "CancelResubmit";
	public static final String ACTION_TYPE_MODIFY = "Modify";
	public static final String SUPP_TYPE_CANCEL = "SC";
	public static final String SUPP_TYPE_CANCEL_RESUBMIT = "SR";
	public static final String SUPP_TYPE_MODIFY = "PD";
	// ETNI Process Variables
	public static final String CALL_API = "callAPI";

	// Compensation Process Variables
	public static final String COMP_REQ = "compRequest";
	public static final String COMP_RES = "compensationRuleResponseObject";
	public static final String AUTO_RETRY = "isAutoAction";
	public static final String ACTION = "action";

	// Signal ID Variables
	public static final String SIGNAL_MTAS = "MTAS";
	public static final String SIGNAL_OTA_RESPONSE = "OTA_RESPONSE";
	public static final String SIGNAL_GP_RESPONSE = "GP_RESPONSE";
	public static final String SIGNAL_VSO_RESPONSE = "VSO_RESPONSE";
	public static final String SIGNAL_VSA_RESPONSE = "VSA_RESPONSE";
	public static final String SIGNAL_VRO_RESPONSE = "VRO_RESPONSE";
	public static final String SIGNAL_VGP_RESPONSE = "VGP_RESPONSE";
	public static final String SIGNAL_ETNI_RESPONSE = "ETNI_RESPONSE";
	public static final String SIGNAL_VISIBLE_RESPONSE = "VISIBLE_RESPONSE";
	public static final String SIGNAL_PREQ_RESPONSE = "PREQ_RESPONSE";
	public static final String SIGNAL_PTIN_RESPONSE = "PTIN_RESPONSE";
	public static final String SIGNAL_PRQ2_RESPONSE = "PRQ2_RESPONSE";
	public static final String SIGNAL_IP_RESPONSE = "IP_RESPONSE";
	public static final String SIGNAL_QBLR = "QBLR";
	public static final String SIGNAL_CHPP = "CHPP";
	public static final String SIGNAL_VISIBLE = "VISIBLE";
	public static final String SIGNAL_TNIP_CANCEL = "TNIP_CANCEL";
	public static final String SIGNAL_RPA = "RPA_RESPONSE";
	public static final String SIGNAL_UNGRAFT_DEVICE="MTAS_UNGRAFT";
	public static final String SIGNAL_TRUNK_SIGNAL="TRUNK_RESPONSE";

	
	// Signal Key Values
	public static final String SIGNAL_KEY_MTAS = "mtasResponseSignal";
	public static final String OPERATION_TYPE = "operationType";
	public static final String DEPLOYMENT_ID = "deploymentId";
	//Added By Gazala for OC Signal Miss Fire Issue - 12/05/2019 : Below
	public static final String ROOT_DEPLOYMENT_ID = "rootDeploymentId"; 
	
	// Service ID Values used for eureka integration
	public static final String SERVICE_ID_PROV = "PROVISIONING";
	public static final String SERVICE_ID_VENDOR_PROV = "VENDOR-PROVISIONING";
	public static final String SERVICE_ID_BILLING = "BILLING";
	public static final String SERVICE_ID_DEVICE_MANAGER = "DEVICE-MANAGER";
	public static final String SERVICE_ID_BPM_DATA_UPD = "BPM-DATA-UPDATE";
	public static final String SERVICE_ID_BPM_ORD_STATUS_MCS = "ORDER-STATUS-UPDATER";
	public static final String SERVICE_ID_MIGRATION = "DATAMIGRATION";
	public static final String SAVEORDER_VISION = "SAVEORDER-VISION";
	public static final String SERVICE_ID_CASE_MGMT = "CASE-MGMT";
    public static final String SERVICE_ID_PORT_IN_CASE_MGMT = "CASE-MGMT";
    public static final String PORT_IN_ORDER_TYPE = "Chg";
    public static final String PORT_IN_SUB_SERVICE = "PortIn";
    public static final String RESULT_CLASS = "resultClass";
	public static final String BILLING_ANA = "BILLINGANALYTICS";
	public static final String QUARTZ = "QUARTZ";
	public static final String SERVICE_ID_SPRING_BATCH = "SPRING-BATCH";
	
	public static final String SERVICE_ID_BPMI = "BPMI";
	public static final String SERVICE_ID_TNIP = "TN-IP-MANAGER";
	public static final String SERVICE_ID_VISIBLE = "VISIBLE";
	public static final String SERVICE_ID_DVS_ACCESS = "DVS-ACCESS";
	public static final String SERVICE_ID_AM = "ORBPM-ACCESS-MANAGER";
	public static final String SERVICE_ID_BPM_EH = "BPM-EH";
	public static final String SERVICE_ID_BPM_BRMS = "BRMS";
	public static final String SERVICE_ID_BPM = "BPM";
	public static final String SERVICE_ID_BPM_SYNC = "BPM-SYNC";
	public static final String SERVICE_ID_RPA = "RPA";
	public static final String SM_BPMI_FLOW = "TRUE";
	
	//Order Status update MCS
	public static final String MCS_URL = "mcs_url";
	/*public static final String MCS_METHOD = "mcs_method";
	public static final String MCS_USERNAME = "mcs_username";
	public static final String MCS_PWD = "mcs_password";
	public static final String MCS_READTIMEOUT = "mcs_readTimeOut";
	public static final String MCS_CONNECTTIMEOUT = "mcs_connectionTimeOut";*/
	public static final String MCS_RESULT = "mcs_result";
	public static final String MCS_CONTENT = "mcs_content";
	
	//Subscription Manager
	public static final String SERVICE_HEADER_PROP_KEY_CLIENTID = "sm.service.header.clientId";
	
	//RMQ Properties
	public static final String RMQ_JKS_FILE = "rmq.jks";
	public static final String RMQ_PROP_FILE = "rmq.properties";
	public static final String RMQ_HOST = "rmqHost";
	public static final String RMQ_VIRTUAL_HOST = "rmqVirtualHost";
	public static final String RMQ_PORT = "rmqPort";
	public static final String RMQ_ACCESS_ID = "rmqAccessId";
	public static final String RMQ_ACCESS_CODE = "rmqAccessCode";
	public static final String MESSAGE = "message";
	public static final String RMQ_EXCHANGE = "exchange";
	public static final String RMQ_TYPE = "type";
	public static final String RMQ_ROUTING_KEY = "routingKey";
	
	public static final String IS_RELEASE_PENDING_SAVE_ORDER = "true";
	public static final String COMPENSATION = "Compensation";
	
}
