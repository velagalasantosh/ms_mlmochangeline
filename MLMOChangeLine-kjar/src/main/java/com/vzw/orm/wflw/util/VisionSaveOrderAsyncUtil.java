package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.business.data.update.domain.objects.VisionSyncRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.business.data.update.domain.BPMDataUpdateWorkFlowRequest;
import com.vzw.orm.business.data.update.domain.BPMDataUpdateWorkFlow;
import com.vzw.orm.business.data.update.domain.objects.SaveVisIndBpmRes;
import com.vzw.orm.saveorder.vision.datacontract.SaveorderBpmSignalResponse;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Pratyusha Pooskuru
 *
 */
public class VisionSaveOrderAsyncUtil implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(VisionSaveOrderAsyncUtil.class);

	/**
	 * This method is used to set all the variable values required to retrieve
	 * Vision Save Order Async status
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void initiateVisionSaveAsyncRequest(ProcessContext kcontext) throws Exception {
		LOGGER.info("Entering initiateVisionSaveAsyncRequest() method");
		try {
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			long parentProcessInstanceId = kcontext.getProcessInstance().getParentProcessInstanceId();
			long processInstanceId = kcontext.getProcessInstance().getId();
			LOGGER.info("OrderID:" + workFlowRequest.getOrderId() + " ProcessInstanceId:" + processInstanceId + " ParentProcessInstanceId:" + parentProcessInstanceId);
			ServiceProvider bpmUtil = new ServiceProvider();
			String visionSaveAsyncUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.VISION_SAV_ASYNC_URL.toString(), BPMConstants.SERVICE_ID_BPM_DATA_UPD);

			LOGGER.info("Vision Save Async URL for is : " + visionSaveAsyncUrl);
			kcontext.setVariable(BPMConstants.URL, visionSaveAsyncUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());

			BPMDataUpdateWorkFlow bpmWorkflow = new BPMDataUpdateWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(processInstanceId));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_VSA_RESPONSE);
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-" + PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}

			VisionSyncRequest visionSyncRequest = new VisionSyncRequest();

			BPMDataUpdateWorkFlowRequest bpmDataUpdateWorkflowRequest = new BPMDataUpdateWorkFlowRequest();
			// converting workflow object to bpmdatupdate workflow object
			BeanUtils.copyProperties(bpmDataUpdateWorkflowRequest, workFlowRequest);

			List<BPMDataUpdateWorkFlowRequest> wfList = new ArrayList<BPMDataUpdateWorkFlowRequest>();
			wfList.add(bpmDataUpdateWorkflowRequest);
			visionSyncRequest.setWorkFlowRequestObject(wfList);

			visionSyncRequest.setBpmWorkFlowObject(bpmWorkflow);
			kcontext.setVariable(BPMConstants.CONTENT, visionSyncRequest);

			ObjectMapper obj = new ObjectMapper();
			LOGGER.info("Vision Save Async Status Request Content for is : " + obj.writeValueAsString(visionSyncRequest));
		} catch (Exception e) {
			LOGGER.error("Error in initiateVisionSaveAsyncRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}

	/**
	 * This method is used to validate the response received for Vision Save
	 * Order Async Status
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateVisionSaveAsyncResponse(ProcessContext kcontext) throws Exception {
		LOGGER.debug("Entering validateVisionSaveReflowResponse() method");
		kcontext.setVariable("flag", false);
		kcontext.setVariable("visoinSaveSuccess", false);
		try {
			SaveVisIndBpmRes visionSaveAsyncIndResponse = (SaveVisIndBpmRes) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != visionSaveAsyncIndResponse) {
				LOGGER.info("Vision Save Aync Ind Response is : " + visionSaveAsyncIndResponse + " for ProcessInstanceId: "
								+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
								+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = visionSaveAsyncIndResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
					LOGGER.info("Vision Save Async Indicator responded with Success");
					kcontext.setVariable("flag", true);
					kcontext.setVariable("visoinSaveSuccess", "Y".equalsIgnoreCase(visionSaveAsyncIndResponse.getSavedToVisionInd()) ? true: false);
				} else {
					LOGGER.debug("ErrorCode : " + visionSaveAsyncIndResponse.getStatusCode()
							+ ", Error Source : VISION , Error Details : " + visionSaveAsyncIndResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, visionSaveAsyncIndResponse.getStatusCode(), "BPM-DATA-UPDATE",
							visionSaveAsyncIndResponse.getStatusDesc(), null, BPMConstants.COMP_COUNT);
				}
			} else {
				LOGGER.error("Response object VisionSaveAsyncIndResponse from BPM Data Update is NULL or unexpected");
				throw new Exception("Response object VisionSaveAsyncIndResponse from BPM Data Update is NULL or unexpected");
			}
		} catch (Exception e) {
			LOGGER.error("Exception while validating response received from Vision Save Async Ind. Exception : " + e);
			throw new Exception(e);
		}
	}
	
	/**
	 * This method is used to validate the response signal received from
	 * Vision Save Order Async status
	 * 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateSaveOrderAsyncSignalResponse(ProcessContext kcontext) {
		LOGGER.debug("Entering validateSaveOrderAsyncSignalResponse() method");
		kcontext.setVariable("flag", false);
		kcontext.setVariable("visoinSaveSuccess", false);
		try {
			SaveorderBpmSignalResponse saveorderBpmSignalResponse = (SaveorderBpmSignalResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != saveorderBpmSignalResponse) {
				LOGGER.info("Vision Save Aync Signal Response is : " + saveorderBpmSignalResponse + " for ProcessInstanceId: "
								+ kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
								+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = saveorderBpmSignalResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status)) {
					LOGGER.info("Vision Save Async Indicator responded with Success");
					kcontext.setVariable("flag", true);
					kcontext.setVariable("visoinSaveSuccess", true);
				} else {
					kcontext.setVariable("visoinSaveSuccess", false);
				}
			} else {
				LOGGER.error("Response object saveorderBpmSignalResponse is NULL or unexpected");
				throw new Exception("Response object saveorderBpmSignalResponse is NULL or unexpected");
			}
		} catch (Exception e) {
			LOGGER.error("Exception while validating signal response received for Vision Save Async Ind. Exception : " + e);
		}
	}
}