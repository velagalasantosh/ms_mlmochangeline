package com.vzw.orm.wflw.util;

import java.io.Serializable;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vzw.orm.provisioning.domain.common.ProvisioningBPMWorkFlow;
import com.vzw.orm.provisioning.domain.common.ProvisioningExceptionInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.provisioning.domain.common.MtasProducerRequest;
import com.vzw.orm.provisioning.domain.common.MtasProducerResponse;
import com.vzw.orm.provisioning.domain.common.ProvisioningWorkFlowRequest;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class AccessManagerVPUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(AccessManagerVPUtil.class);

	public void initiateAccessManagerVPRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateAccessManagerVPRequest() method ...");
		try
		{
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			ProvisioningWorkFlowRequest proWorkFlowRequest = new ProvisioningWorkFlowRequest();
			//converting workflow object to provisioning workflow object
			BeanUtils.copyProperties(proWorkFlowRequest, workFlowRequest);
			proWorkFlowRequest.setFeatures((String)kcontext.getVariable("features"));
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			
			ServiceProvider bpmUtil = new ServiceProvider();
			
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			String accessManagerUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.AM_VP_URL.toString(), BPMConstants.SERVICE_ID_PROV);
			LOGGER.info("Access Manager VP URL is : " + accessManagerUrl);
			
			ProvisioningBPMWorkFlow bpmWorkflow = new ProvisioningBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
					
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			LOGGER.info("Features for AMVP : " +(String)kcontext.getVariable("features"));
			MtasProducerRequest mtasRequest = new MtasProducerRequest();
			mtasRequest.setBpmWorkFlowObject(bpmWorkflow);
			mtasRequest.setProvisioningRequestType("RealTimeVendorProvisioning");
			mtasRequest.setWorkFlowRequestObject(proWorkFlowRequest);
			ObjectMapper obj = new ObjectMapper();
			kcontext.setVariable(BPMConstants.CONTENT, mtasRequest);
			LOGGER.info("Access Manager Request Content For VP AM is : " + obj.writeValueAsString(mtasRequest));
			
			kcontext.setVariable(BPMConstants.URL, accessManagerUrl);
			
			
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateAccessManagerVPRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}
	
	public void validateAccessManagerVPResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateAccessManagerVPResponse() method ...");

		boolean flag = false;

		try
		{
			
			MtasProducerResponse vpResponse = (MtasProducerResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != vpResponse)
			{
				LOGGER.info("AM VP Response is : " + vpResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String vpStatus = vpResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(vpStatus))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					
				}
				else
				{
					ProvisioningExceptionInfo expInfo = vpResponse.getExceptionInfo();
					LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : " + expInfo.getErrorSource() + ", Error Details : " + expInfo.getErrorDeatils());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), expInfo.getErrorSource(), expInfo.getErrorDeatils(),
							null, BPMConstants.COMP_COUNT);
				}
			}else
			{
				LOGGER.info("Response Object from Access Manager VP is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL", null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from Access Manager VP. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
		LOGGER.info("Exiting validateAccessManagerVPResponse() method ...");
	}

}