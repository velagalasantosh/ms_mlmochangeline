/**
 * 
 */
package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpm.domain.objects.identifier.rules.WorkFlowRulesResponse;
import com.vzw.orm.bpmi.domain.objects.consumer.ConsumerWorkflowRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.bpmi.domain.objects.subscriptions.LineItem;
import com.vzw.orm.bpmi.domain.objects.subscriptions.Spo;
import com.vzw.orm.bpmi.domain.objects.subscriptions.VFWorkflowRequest;
import com.vzw.orm.bpmi.domain.objects.subscriptions.SubscriptionKieRequestObject;
import com.vzw.orm.consumer.domain.objects.ConsumerFeatureListRequest;
import com.vzw.orm.consumer.domain.objects.ConsumerFeatureListResponse;
import com.vzw.orm.consumer.domain.objects.LineDetails;
import com.vzw.orm.consumer.domain.objects.LineItems;
import com.vzw.orm.consumer.domain.objects.SPOArray;

/**
 * @author sankuvi
 *
 */
public class SubscriptionManagerUtil implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionManagerUtil.class);

	public void getSPOAttributesRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering getSPOAttributesRequest() method ...");
		try
		{
			ServiceProvider bpmUtil = new ServiceProvider();
			String spoUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.SPO_FEATURES_URL.toString(), BPMConstants.SERVICE_ID_BPM_DATA_UPD);
			LOGGER.info("SPO FEATURES URL is : " + spoUrl);
			kcontext.setVariable(BPMConstants.URL, spoUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			
			if(PropertyFile.getProjectProperties().get("IS_SM_BPMI_FLOW_ENABLED")!=null && "true".equalsIgnoreCase(PropertyFile.getProjectProperties().get("IS_SM_BPMI_FLOW_ENABLED").toString())){
				kcontext.setVariable("isBpmiFlowEnabled", true);
			} else {
				kcontext.setVariable("isBpmiFlowEnabled", false);
			}
			
			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext.getVariable("consumerWorkflowRequest");
			WorkFlowRequest workFlowRequest = new WorkFlowRequest();
			BeanUtils.copyProperties(workFlowRequest, consumerWFRequest.getLstOfWorkflowRequest().get(0));
			/*String lineItemNumbers = consumerWFRequest.getLstOfWorkflowRequest().stream()
					 .map(WorkFlowRequest::getLineItemNumber)
					 .collect(Collectors.joining(","));
			workFlowRequest.setLineItemNumber(lineItemNumbers);*/
			
			kcontext.setVariable(BPMConstants.WORKFLOW_REQ, workFlowRequest);
			
			//CONSTRUCTING REQUEST
			
			ConsumerFeatureListRequest consumerFeatureRequest = new ConsumerFeatureListRequest();
			consumerFeatureRequest.setAccountNumber(workFlowRequest.getAccountNumber());
			consumerFeatureRequest.setBillingInstance(workFlowRequest.getBillingInstance());
			consumerFeatureRequest.setCustomerId(workFlowRequest.getCustomerId());
			consumerFeatureRequest.setOrderId(workFlowRequest.getOrderId());
			List<LineDetails> lineDetails = new ArrayList<LineDetails>();
			for(WorkFlowRequest wfRequest : consumerWFRequest.getLstOfWorkflowRequest()) {
				if(wfRequest.getSpoCategoryCodes()!=null && wfRequest.getSpoCategoryCodes().length > 0) { 
					LineDetails line = new LineDetails();
					line.setLineItemNumber(wfRequest.getLineItemNumber());
					line.setMtn(wfRequest.getMtn());
					line.setSpoCategoryCodes(Arrays.asList(wfRequest.getSpoCategoryCodes()));
					lineDetails.add(line);
				}
			}
			consumerFeatureRequest.setLineDetails(lineDetails);
			
			ObjectMapper obj = new ObjectMapper();		
			kcontext.setVariable(BPMConstants.CONTENT, consumerFeatureRequest);
			LOGGER.info("ConsumerFeatureListRequest Content is :" + obj.writeValueAsString(consumerFeatureRequest));
			
		}catch(Exception e) {
			kcontext.setVariable("flag", false);
			LOGGER.error("Entering getSPOAttributesRequest() method. Exception: " + e);
			throw new Exception("Entering getSPOAttributesRequest() method " +e);
		}
		
	}
	
	public void validateSPOAttributesResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateSPOAttributesResponse() method ...");
		kcontext.setVariable("flag", false);
		try
		{
			ConsumerFeatureListResponse consumerFeatureResponse = (ConsumerFeatureListResponse) kcontext.getVariable("consumerFeatureResp");
			LOGGER.info("ConsumerFeatureListResponse is:" + consumerFeatureResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			//PARSE RESPONSE
			if(consumerFeatureResponse!=null) {
				
				if("Success".equalsIgnoreCase(consumerFeatureResponse.getStatus())) 
				{
					if(consumerFeatureResponse.getLineItems()!=null && consumerFeatureResponse.getLineItems().size() > 0) {
						LOGGER.info("Success response from BDU");
						kcontext.setVariable("flag", true);
						kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					}
					else {
						LOGGER.info("LineItems in ConsumerFeatureListResponse is NULL");
						throw new Exception("LineItems in ConsumerFeatureListResponse is NULL");
					}
					
				}
				else {
					LOGGER.info("ErrorCode : " + consumerFeatureResponse.getStatusCode() + ", Error Details : " + consumerFeatureResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, consumerFeatureResponse.getStatusCode(), "BPM", consumerFeatureResponse.getStatusDesc(),
							null, BPMConstants.COMP_COUNT);
				}
				
			}
			else {
				LOGGER.info("ConsumerFeatureListResponse is NULL or Unexpected");
				throw new Exception("ConsumerFeatureListResponse is NULL or Unexpected");
			}
			
		}catch(Exception e) {
			LOGGER.error("Entering validateSPOAttributesResponse() method. Exception: " + e);
			throw new Exception("Entering validateSPOAttributesResponse() method " +e);
		}
		
	}
	
	public void initiateBpmDeploymentIdentifier(ProcessContext kcontext) throws Exception
	{

		LOGGER.info("Entering initiateBpmDeploymentIdentifier() method..");
		
		try {
			ServiceProvider bpmUtil = new ServiceProvider();
			String url = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_BPM_DEP_ID_URL.toString(),
					BPMConstants.SERVICE_ID_BPM_BRMS);
			LOGGER.info("BRMS BpmDeploymentIdentifier URL is : " + url);
			kcontext.setVariable("url", url);
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			kcontext.setVariable("bpmAccessId",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
			kcontext.setVariable("bpmAccessCode",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));

			String request = "{ \"commands\": [{ \"insert\": { \"object\": { \"com.vzw.orm.bpm.domain.objects.identifier.rules.WorkFlowRulesResponse\": { \"orderType\": \"NEW\", \"orderSource\": \"SUBSCRIPTION\" } }, \"out-identifier\": \"workflowResponseObject\" } }, { \"fire-all-rules\": \"\" }] }";

			LOGGER.info("BpmDeploymentIdentifier Request --->" + request);
			kcontext.setVariable("content", request);

		} catch (Exception e) {
			kcontext.setVariable("flag", false);
			LOGGER.error("Error in initiateBpmDeploymentIdentifier method. Exception : ", e);
			throw new Exception(e);
		}
	}
	
	public void validateBpmDeploymentIdentifierResponse(ProcessContext kcontext) throws Exception {

		LOGGER.info("Entering validateBpmDeploymentIdentifierResponse() method..");
		kcontext.setVariable("flag", false);
		try {
			WorkFlowRulesResponse wflwRulesResp = (WorkFlowRulesResponse) kcontext
					.getVariable("workflowResponseObject");
			LOGGER.info("BpmDeploymentIdentifier Response --->" + wflwRulesResp);
			if (wflwRulesResp != null) 
			{
				if (wflwRulesResp.getDeploymentId() != null && wflwRulesResp.getWorkFlowId() != null) 
				{
					LOGGER.info("About to initiate " + wflwRulesResp.getWorkFlowId() + " process under "
							+ wflwRulesResp.getDeploymentId());
					kcontext.setVariable("flag", true);
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				} 
				else 
				{
					LOGGER.info("DeploymentId and workflowId are null for the given orderType and orderSource");
					throw new Exception("DeploymentId and workflowId are null for the given orderType and orderSource");
				}
			} 
			else 
			{
				LOGGER.info("BpmDeploymentIdentifier Response is null or unexpected");
				throw new Exception("BpmDeploymentIdentifier Response is null or unexpected");
			}

		} catch (Exception e) {
			LOGGER.error("Error in validateBpmDeploymentIdentifierResponse method. Exception : ", e);
			throw new Exception(e);
		}
	}
	
	public void initiateSubscriptionManagerProcess(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering into initiateSubscriptionManagerProcess() method..");
		try
		{
			ServiceProvider bpmUtil = new ServiceProvider();
			String URL = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.KIE_SERVER_SYNC_SM_URL.toString(),
					PropertyFile.getProjectProperties().getProperty("sm.service.id").toString());

			WorkFlowRulesResponse wflwRulesResp = (WorkFlowRulesResponse) kcontext
					.getVariable("workflowResponseObject");

			URL = URL + "/" + wflwRulesResp.getDeploymentId() +"/processes/" + wflwRulesResp.getWorkFlowId() + "/instances";
			LOGGER.info("Subscription Manager Process Initiation URL : " + URL);

			kcontext.setVariable("url", URL);
			kcontext.setVariable("bpmAccessKey",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_ID));
			kcontext.setVariable("bpmAccessCode",
					PropertyFile.getProjectProperties().get(BPMConstants.KIE_SERVER_INVOKE_TOKEN));

			//CONTRUCT THE REQUEST
			ConsumerWorkflowRequest consumerWFRequest = (ConsumerWorkflowRequest) kcontext.getVariable("consumerWorkflowRequest");
			WorkFlowRequest workFlowRequest = new WorkFlowRequest();
			BeanUtils.copyProperties(workFlowRequest, consumerWFRequest.getLstOfWorkflowRequest().get(0));
			SubscriptionKieRequestObject vzCareKieRequest = new SubscriptionKieRequestObject();
			
			VFWorkflowRequest vfWorkflowRequest = new VFWorkflowRequest();
			vfWorkflowRequest.setAccountNumber(workFlowRequest.getAccountNumber());
			vfWorkflowRequest.setCustomerId(workFlowRequest.getCustomerId());
			vfWorkflowRequest.setCorrelationId(workFlowRequest.getCorrelationId());
			vfWorkflowRequest.setVisonOrderNumber(workFlowRequest.getOrderId());
			
			int billSysId;
			switch (workFlowRequest.getBillingInstance()) {
			case "E":
				billSysId = 1;
				break;
			case "N":
				billSysId = 7;
				break;
			case "W":
				billSysId = 2;
				break;
			case "B":
				billSysId = 8;
				break;
			default:
				billSysId = 1;
				break;
			}
			vfWorkflowRequest.setBillSysId(billSysId);
			
			ConsumerFeatureListResponse consumerFeatureResponse = (ConsumerFeatureListResponse) kcontext.getVariable("consumerFeatureResp");
			LOGGER.info("consumerFeatureResponse before initiating SM: "+consumerFeatureResponse);
			List<LineItem> lineItems = new ArrayList<LineItem>();
			if(consumerFeatureResponse!=null&&consumerFeatureResponse.getLineItems()!=null) {
				for(LineItems lineItms: consumerFeatureResponse.getLineItems()) {
					
					LineItem lnItm = new LineItem();
					lnItm.setLineItemNumber(lineItms.getLineItemNumber());

					for(WorkFlowRequest wfr : consumerWFRequest.getLstOfWorkflowRequest()) {
						if(lineItms.getLineItemNumber().equalsIgnoreCase(wfr.getLineItemNumber())){
							lnItm.setIdNoP1(wfr.getLosIdNoP1());
							lnItm.setIdNoP2(wfr.getLosIdNoP2());
							lnItm.setMtn(wfr.getMtn());
						}
					}
					
					List<Spo> spoList = new ArrayList<Spo>();
					if(lineItms.getSpoArray()!=null && lineItms.getSpoArray().size() > 0) {
						for(SPOArray spoArray : lineItms.getSpoArray()) {
							Spo spo = new Spo();
							spo.setSpoAction(spoArray.getSpoAction());
							spo.setSpoId(spoArray.getSpoId());
							spo.setSpoUniqueId(spoArray.getSpoUniqueId());
							spo.setProductLevelCode(spoArray.getProductLevelCode());
							spo.setSpoCategoryCodes(spoArray.getSpoCategoryCodes());
							spoList.add(spo);
						}
					}
					LOGGER.info("spoList-->" +spoList);
					lnItm.setSpoArray(spoList);
					lineItems.add(lnItm);
				}
			}
			
			LOGGER.info("lineItems-->" +lineItems);
			vfWorkflowRequest.setLineItems(lineItems);
			
			if(PropertyFile.getProjectProperties().get(BPMConstants.SERVICE_HEADER_PROP_KEY_CLIENTID)!=null) 
			{
				vfWorkflowRequest.setClientId((String)PropertyFile.getProjectProperties().get(BPMConstants.SERVICE_HEADER_PROP_KEY_CLIENTID));
			}
			else {
				vfWorkflowRequest.setClientId(consumerFeatureResponse.getClientId());
			}
			vzCareKieRequest.setWorkflowRequest(vfWorkflowRequest);
			ObjectMapper obj = new ObjectMapper();
			kcontext.setVariable(BPMConstants.CONTENT, vzCareKieRequest);
			
			LOGGER.info("VFWorkflowRequest Content is : " + obj.writeValueAsString(vzCareKieRequest));

		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateSubscriptionManagerProcess method. Exception : ", e);
			throw new Exception(e);
		}
	}
	
	public void validateSubscriptionManagerResponse(ProcessContext kcontext) throws Exception
	{
      
		LOGGER.info("Entering into validateSubscriptionManagerResponse() method..");
      	kcontext.setVariable("flag",false);
		try
		{
			String processinsatnceid = (String) kcontext.getVariable("result");
			if(processinsatnceid!=null) {
				LOGGER.info("SubscriptionManager Process Instance id ->" + processinsatnceid);
              	kcontext.setVariable("flag",true);
			}
			else {
				LOGGER.info("Unable to initiate Subscription Manager process");
				throw new Exception("Unable to initiate Subscription Manager process");
			}
			
		}catch (Exception e){
			LOGGER.error("Exceptiom inside validateSubscriptionManagerResponse() method.." + e.getMessage());
			throw new Exception("Exceptiom inside validateSubscriptionManagerResponse() method.." + e.getMessage());
		}
	}
}
