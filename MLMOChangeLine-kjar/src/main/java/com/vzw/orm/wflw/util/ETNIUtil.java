package com.vzw.orm.wflw.util;

import java.io.Serializable;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.common.core.util.DateTimeUtils;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.tnip.domain.EtniConsumerResponse;
import com.vzw.orm.tnip.domain.EtniRequest;
import com.vzw.orm.tnip.domain.EtniResponse;
import com.vzw.orm.tnip.domain.ExceptionInfo;
import com.vzw.orm.tnip.domain.TnipBPMWorkFlow;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class ETNIUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ETNIUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * in ETNI
	 * 
	 * @param kcontext
	 * @throws Exception 
	 */
	public void initiateETNIRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateETNIRequest() method ...");
		try
		{
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());
			String etniEventType = (String)kcontext.getVariable("etniEventType");
			String etniUrl;
			ServiceProvider bpmUtil = new ServiceProvider();
			
			etniUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.ETNI_CHANGE_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
				
			kcontext.setVariable("skipSignal", false);
			
			LOGGER.info("ETNI URL is : " + etniUrl);
			kcontext.setVariable(BPMConstants.URL, etniUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			
			TnipBPMWorkFlow bpmWorkflow = new TnipBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_ETNI_RESPONSE);
			LOGGER.info("deploymentId-->" + (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			EtniRequest etniRequest = new EtniRequest();
			BeanUtils.copyProperties(etniRequest, workFlowRequest);
			etniRequest.setOrderDueDate(DateTimeUtils.formatCurrentTsBasedOnFormat("yyyy-MM-dd-HH.mm.ss.SSS"));
						
			if (StringUtils.isNotBlank(etniEventType) && BPMConstants.SIGNAL_CHPP.equalsIgnoreCase(etniEventType)) {
				etniRequest.setApiLookupName(BPMConstants.SIGNAL_CHPP);
			} else {
				etniRequest.setApiLookupName(BPMConstants.SIGNAL_QBLR);
			}
			
			etniRequest.setWfReqid(kcontext.getProcessInstance().getProcessId());
			etniRequest.setProcessInsId(String.valueOf(kcontext.getProcessInstance().getId()));
			etniRequest.setBpmWorkFlowObject(bpmWorkflow);
						
			kcontext.setVariable(BPMConstants.CONTENT, etniRequest);
			ObjectMapper mapper = new ObjectMapper();
			LOGGER.info("ETNI Request Content is : " + mapper.writeValueAsString(etniRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateETNIRRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}
	/**
	 * This method is used to validate the response received from ETNI Activation
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateETNIResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateETNIResponse() method ...");

		boolean flag = false;

		try
		{
			EtniResponse etniResponse = (EtniResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != etniResponse)
			{
				LOGGER.info("ETNI Response is : " + etniResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = etniResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					if("ETNI-CSIM/CDVC Message NOT Posted in Queue".equalsIgnoreCase(etniResponse.getStatusDesc())){
						LOGGER.info("Not Enabling ETNI_RESPONSE Signal as it is not required.");
						kcontext.setVariable("skipSignal", true);
					}
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					if(kcontext.getVariable("lineStatus") != null) 
					{
						kcontext.setVariable("preStatus", (String) kcontext.getVariable("lineStatus"));
						LOGGER.info("Prestatus: "+(String) kcontext.getVariable("preStatus"));

					}else{
						kcontext.setVariable("preStatus", "LP");
					}
				}
				else
				{
					// Call Compensation
					LOGGER.info("ErrorCode : " + etniResponse.getStatusCode() + ", Error Source : ETNI , Error Details : " + etniResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, etniResponse.getStatusCode(), "ETNI", etniResponse.getStatusDesc(),
							null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object EtniResponseObject from ETNI is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from ETNI. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

	/**
	 * This method is used to validate the response received from ETNI 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateETNISignalResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateETNISignalResponse() method ...");

		boolean flag = false;

		try
		{
			EtniConsumerResponse etniSignalResponse = (EtniConsumerResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != etniSignalResponse)
			{
				LOGGER.info("ETNI Signal Response is : " + etniSignalResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = etniSignalResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					
					if(kcontext.getVariable("lineStatus") != null) {
						if("GUIETNIRFL"
								.equalsIgnoreCase(kcontext.getVariable("lineStatus").toString())){
							LOGGER.info("Prestatus when GUIERR01: "+(String) kcontext.getVariable("preStatus"));
							kcontext.setVariable("preStatus", (String) kcontext.getVariable("preStatus"));
						} else{
							kcontext.setVariable("preStatus", (String) kcontext.getVariable("lineStatus"));
							LOGGER.info("Prestatus: "+(String) kcontext.getVariable("preStatus"));
						}
					}else{
						kcontext.setVariable("preStatus", "LP");
					}
					
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					ExceptionInfo expInfo = etniSignalResponse.getExceptionInfo();
					// Call Compensation
					
					CompensationUtil compUtil = new CompensationUtil();
					if(BPMConstants.GUI_ERR.equalsIgnoreCase(expInfo.getErrorCode()))
					{
						kcontext.setVariable("isGuiErr", true);
						LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : GUI , Error Details : " + expInfo.getErrorDeatils());
						compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), BPMConstants.GUI, expInfo.getErrorDeatils(),
								null, BPMConstants.COMP_COUNT);
						
					}else{
						LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : ETNI , Error Details : " + expInfo.getErrorDeatils());
						compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), "ETNI", expInfo.getErrorDeatils(),
							null, BPMConstants.COMP_COUNT);
					}
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object EtniConsumerResponse from ETNI is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from ETNI. Exception : " + e);
			CompensationUtil compUtil = new CompensationUtil();
			compUtil.setCompensationObject(kcontext, "BPMERR03", "BPM", "Exception while validating response received from ETNI",
					null, BPMConstants.COMP_COUNT);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
	
}