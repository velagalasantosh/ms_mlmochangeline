package com.vzw.orm.wflw.util;

import java.io.Serializable;
import java.util.Map;

import org.jbpm.process.instance.command.UpdateTimerCommand;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.process.ProcessContext;
import org.kie.internal.runtime.manager.context.ProcessInstanceIdContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.orm.bpm.domain.objects.compensation.CompensationRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.rpa.domain.RpaErrorFeedRequest;
import com.vzw.orm.rpa.domain.RpaErrorFeedResponse;
import com.vzw.orm.rpa.domain.RpaWorkflowRequest;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Vinodh Sankuru
 *
 */
public class RPAUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(RPAUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * in RPA
	 * 
	 * @param kcontext
	 * @throws Exception 
	 */
	public void initiateRPARequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateRPARequest() method ...");
		try
		{
			kcontext.setVariable("rpaReceived", false);
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String rpaUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.RPA_URL.toString(), BPMConstants.SERVICE_ID_RPA);
			LOGGER.info("RPA URL is : " + rpaUrl);
			kcontext.setVariable(BPMConstants.URL, rpaUrl);
			
			RpaErrorFeedRequest rpaRequest = new RpaErrorFeedRequest();
			rpaRequest.setAcctNo(workFlowRequest.getAccountNumber());
			rpaRequest.setBillSysId(workFlowRequest.getBillingInstance());
			rpaRequest.setCustIdNo(workFlowRequest.getCustomerId());
			rpaRequest.setOrdNo(workFlowRequest.getOrderId());
			rpaRequest.setLnItmNo(workFlowRequest.getLineItemNumber());
			rpaRequest.setLnItmTypCd(workFlowRequest.getLnItmStatCd());
			rpaRequest.setMtn(workFlowRequest.getMtn());
			
			RpaWorkflowRequest bpmWorkflow = new RpaWorkflowRequest();
			bpmWorkflow.setPrcsInstId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setSignlEvntId(BPMConstants.SIGNAL_RPA);
			LOGGER.info("deploymentId-->" + (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			bpmWorkflow.setDeplmntId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
						
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setDataCenter(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setDataCenter(PropertyFile.getInstance().getDataCenter());
			}
			
			
			rpaRequest.setRpaWorkflowRequest(bpmWorkflow);
			
			CompensationRequest compReq = (CompensationRequest) kcontext.getVariable(BPMConstants.COMP_REQ);
			LOGGER.info("compRequest-->" +compReq);
			rpaRequest.setErrCd(compReq.getErrorCode());
			rpaRequest.setErrDetails(compReq.getErrorDesc());
			kcontext.setVariable(BPMConstants.CONTENT, rpaRequest);
			ObjectMapper mapper = new ObjectMapper();
			LOGGER.info("RPA Request Content is : " + mapper.writeValueAsString(rpaRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateRPARequest method. Exception : " + e);
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	
	/**
	 * This method is used to validate the response received from RPA 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateRPASyncResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateRPASyncResponse() method ...");
		
		try
		{
			RpaErrorFeedResponse resp = (RpaErrorFeedResponse) kcontext.getVariable("result");
			LOGGER.info("RPA Response is:" + resp);
			
			if (resp != null&&resp.getStatus()!=null)
			{
				if ("success".equalsIgnoreCase(resp.getStatus()))
				{
					LOGGER.info("RPA is enabled");
				}else {
					LOGGER.info("RPA cannot correct the order as response is " + resp.getStatus());
					throw new Exception("RPA cannot correct the order as response is " +resp.getStatus());
				}
			}
			else
			{
				LOGGER.info("Response from RPA is NULL");
				throw new Exception("Response from RPA is NULL");
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception inside validateRPASyncResponse().. " + e.getMessage());
			throw new Exception("Exception inside validateRPASyncResponse" + e.getMessage());
		}
	}
	
	/**
	 * This method is used to validate the response received from RPA SIGNAL
	 * @param kcontext
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void validateRPASignalResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateRPASignalResponse() method ...");

		boolean flag = false;
		try
		{
			Map<String, Object> resp = (Map<String, Object>) kcontext.getVariable("result");
			LOGGER.info("Signal Data for RPA signal is:" + resp);
			if (resp != null && resp.size() > 0)
			{
				if (resp.get("status") != null)
				{
					flag = "SUCCESS".equalsIgnoreCase((String) resp.get("status")) ? true : false;
				}
				else
				{
					LOGGER.info("status is null. Proceeding to launch Compensation");
				}
			}
			else
			{
				LOGGER.info("Response data for RPA signal is NULL. Proceeding to launch Compensation");
			}
		}
		catch (Exception e)
		{
			LOGGER.info("Exception while validateRPASignalResponse().. " + e.getMessage()
					+ " Hence launching Compensation Process");
		}
		kcontext.setVariable("rpaReceived", true);
		updateRPATimer(kcontext, "RPA_SLA");
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
	
	public void updateRPATimer(ProcessContext kcontext, String timerName) 
	{
		LOGGER.info("Entering updateRPATimer() method for processInstanceId:"
				+ kcontext.getProcessInstance().getId());
		try {
				RuntimeManager rm = (RuntimeManager) kcontext.getKieRuntime().getEnvironment()
						.get("RuntimeManager");
				RuntimeEngine runtime = rm
						.getRuntimeEngine(ProcessInstanceIdContext.get(kcontext.getProcessInstance().getId()));
				KieSession ksession = runtime.getKieSession();
				ksession.execute(
						new UpdateTimerCommand(kcontext.getProcessInstance().getId(), timerName, 1));
				LOGGER.info("RPA_SLA value has been updated");
				rm.disposeRuntimeEngine(runtime);

		} catch (Exception e) {
			LOGGER.error("Exception inside updateRPATimer method: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void closeRPASignal(ProcessContext kcontext, String signalKey)
	{
		LOGGER.info("Entering closeRPASignal() method...");
		try
		{
			LOGGER.info("De-activating safe point of signalKey : " + signalKey + " and signalId : " + kcontext.getVariable(signalKey));

			if (signalKey != null && kcontext.getVariable(signalKey) !=null) 
			{
				kcontext.getKieRuntime().getWorkItemManager().completeWorkItem(Long.parseLong(String.valueOf(kcontext.getVariable(signalKey))), null);
			}

		}
		catch (Exception e)
		{
			LOGGER.error("Exception inside closeRPASignal method. Exception : " + e);
			e.printStackTrace();
		}
	}
}