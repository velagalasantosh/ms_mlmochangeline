package com.vzw.orm.wflw.util;

import java.io.Serializable;
import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vzw.common.core.util.DateTimeUtils;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.tnip.domain.EtniConsumerResponse;
import com.vzw.orm.tnip.domain.EtniRequest;
import com.vzw.orm.tnip.domain.EtniResponse;
import com.vzw.orm.tnip.domain.TnipBPMWorkFlow;
import com.vzw.orm.tnip.domain.ExceptionInfo;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Rahul Kumar Agarwal
 *
 */
public class IPUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(IPUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * IP Activation/Resume request
	 * 
	 * @param kcontext
	 * @throws Exception 
	 */
	public void initiateIPRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateIPRequest() method ...");
		try
		{
            WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String ipUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.IP_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			LOGGER.info("IP Activation/Resume URL is : " + ipUrl);
			kcontext.setVariable(BPMConstants.URL, ipUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			
			TnipBPMWorkFlow bpmWorkflow = new TnipBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setSignalEventId(BPMConstants.SIGNAL_IP_RESPONSE);
			LOGGER.info("deploymentId-->" + (String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			EtniRequest etniRequest = new EtniRequest();
			BeanUtils.copyProperties(etniRequest, workFlowRequest);
			etniRequest.setOrderDueDate(DateTimeUtils.formatCurrentTsBasedOnFormat("yyyy-MM-dd-HH.mm.ss.SSS"));
			etniRequest.setWfReqid(kcontext.getProcessInstance().getProcessId());
			etniRequest.setProcessInsId(String.valueOf(kcontext.getProcessInstance().getId()));
			etniRequest.setBpmWorkFlowObject(bpmWorkflow);
						
			kcontext.setVariable(BPMConstants.CONTENT, etniRequest);
			LOGGER.info("IP Activation/Resume API Request Content is : " + etniRequest);
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateIPRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}
	/**
	 * This method is used to validate the response received from ACIP/RMIP API
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateIPResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateIPResponse() method ...");

		boolean flag = false;

		try
		{
			EtniResponse etniResponse = (EtniResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != etniResponse)
			{
				LOGGER.info("ETNI Response is : " + etniResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = etniResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
					
					//set parameter true if MS is calling any of ACIP/RMIP API or else false
					if("00".equalsIgnoreCase(etniResponse.getStatusCode()))
					{
						kcontext.setVariable(BPMConstants.CALL_API, true);
					}
					else
					{
						kcontext.setVariable(BPMConstants.CALL_API, false);
					}
				}
				else
				{
					// Call Compensation
					LOGGER.info("ErrorCode : " + etniResponse.getStatusCode() + ", Error Source : ETNI , Error Details : " + etniResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, etniResponse.getStatusCode(), "ETNI", etniResponse.getStatusDesc(),
							null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object EtniResponseObject from ETNI is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from IP Activation/Resume API. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

	/**
	 * This method is used to validate the signal response received from IP Activation/Resume API 
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateIPSignalResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateIPSignalResponse() method ...");

		boolean flag = false;

		try
		{
			EtniConsumerResponse etniSignalResponse = (EtniConsumerResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != etniSignalResponse)
			{
				LOGGER.info("IP Activatio/Resume API Signal Response is : " + etniSignalResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = etniSignalResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					ExceptionInfo expInfo = etniSignalResponse.getExceptionInfo();
					// Call Compensation
					LOGGER.info("ErrorCode : " + expInfo.getErrorCode() + ", Error Source : ETNI , Error Details : " + expInfo.getErrorDeatils());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, expInfo.getErrorCode(), "ETNI", expInfo.getErrorDeatils(),
							null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object EtniConsumerResponse from IP Activatio/Resume API is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from IP Activatio/Resume API. Exception : " + e);
			CompensationUtil compUtil = new CompensationUtil();
			compUtil.setCompensationObject(kcontext, "BPMERR03", "BPM", "Exception while validating response received from ETNI",
					null, BPMConstants.COMP_COUNT);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}
	
}