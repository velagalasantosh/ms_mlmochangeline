package com.vzw.orm.wflw.util;

import java.io.Serializable;
import org.apache.commons.beanutils.BeanUtils;
import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vzw.common.core.util.DateTimeUtils;
import com.vzw.orm.tnip.domain.TnipBPMWorkFlow;
import com.vzw.orm.tnip.domain.EtniRequest;
import com.vzw.orm.bpmi.domain.objects.consumer.WorkFlowRequest;
import com.vzw.orm.tnip.domain.EtniResponse;

/**
 * This class is used to perform basic validations and utility operations
 * 
 * @author Vinodh Sankuru
 *
 */
public class LoanCreationUtil implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(LoanCreationUtil.class);

	/**
	 * This method is used to set all the variable values required to initiate
	 * in Loan Creation
	 * 
	 * @param kcontext
	 * @throws Exception 
	 */
	public void initiateLoanCreationRequest(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering initiateLoanCreationRequest() method ...");
		try
		{
			WorkFlowRequest workFlowRequest = (WorkFlowRequest) kcontext.getVariable(BPMConstants.WORKFLOW_REQ);
			
			LOGGER.info("OrderID:" +workFlowRequest.getOrderId() + " ProcessInstanceId:" + kcontext.getProcessInstance().getId() + " ParentProcessInstanceId:"
					+ kcontext.getProcessInstance().getParentProcessInstanceId());

			ServiceProvider bpmUtil = new ServiceProvider();
			String etniUrl = bpmUtil.getServiceURL(PropertyFile.PROP_CONST.LOAN_CREATION_URL.toString(), BPMConstants.SERVICE_ID_TNIP);
			LOGGER.info("Loan Creation URL is : " + etniUrl);
			kcontext.setVariable(BPMConstants.URL, etniUrl);
			kcontext.setVariable(BPMConstants.MS_ACCESS_ID, PropertyFile.getInstance().getMSAccessId());
			kcontext.setVariable(BPMConstants.MS_ACCESS_CODE, PropertyFile.getInstance().getMSAccessCode());
			kcontext.setVariable(BPMConstants.CONNECTION_TIME_OUT, PropertyFile.getInstance().getConnectionTimeOut());
			kcontext.setVariable(BPMConstants.READ_TIME_OUT, PropertyFile.getInstance().getReadTimeOut());
			
			TnipBPMWorkFlow bpmWorkflow = new TnipBPMWorkFlow();
			bpmWorkflow.setProcessInstanceId(String.valueOf(kcontext.getProcessInstance().getId()));
			bpmWorkflow.setProcessWorkFlowId(kcontext.getProcessInstance().getProcessId());
			bpmWorkflow.setSignalEventId("");
			bpmWorkflow.setDeploymentId((String) kcontext.getKieRuntime().getEnvironment().get(BPMConstants.DEPLOYMENT_ID));
			
			if (PropertyFile.getInstance().isConsumerClustered()) {
				bpmWorkflow.setSite(PropertyFile.getInstance().getClusterId() + "-"
						+ PropertyFile.getInstance().getDataCenter());
			} else {
				bpmWorkflow.setSite(PropertyFile.getInstance().getDataCenter());
			}
			
			EtniRequest etniRequest = new EtniRequest();
			BeanUtils.copyProperties(etniRequest, workFlowRequest);
			etniRequest.setOrderDueDate(DateTimeUtils.formatCurrentTsBasedOnFormat("yyyy-MM-dd-HH.mm.ss.SSS"));
			etniRequest.setApiLookupName(BPMConstants.SIGNAL_QBLR);
			etniRequest.setWfReqid(kcontext.getProcessInstance().getProcessId());
			etniRequest.setProcessInsId(String.valueOf(kcontext.getProcessInstance().getId()));
			etniRequest.setBpmWorkFlowObject(bpmWorkflow);
			
			kcontext.setVariable(BPMConstants.CONTENT, etniRequest);
			ObjectMapper mapper = new ObjectMapper();
			LOGGER.info("Loan Creation Request Content is : " + mapper.writeValueAsString(etniRequest));
		}
		catch (Exception e)
		{
			LOGGER.error("Error in initiateLoanCreationRequest method. Exception : " + e);
			throw new Exception(e);
		}
	}
	/**
	 * This method is used to validate the response received from Loan Creation MS
	 * @param kcontext
	 * @throws Exception
	 */
	public void validateLoanCreationResponse(ProcessContext kcontext) throws Exception
	{
		LOGGER.info("Entering validateLoanCreationResponse() method ...");

		boolean flag = false;

		try
		{
			EtniResponse etniResponse = (EtniResponse) (kcontext.getVariable(BPMConstants.RESULT));

			if (null != etniResponse)
			{
				LOGGER.info("Loan Creation Response is : " + etniResponse+ " for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				String status = etniResponse.getStatus();
				if (BPMConstants.SUCCESS.equalsIgnoreCase(status))
				{
					flag = true;
					kcontext.setVariable(BPMConstants.COMP_COUNT, 0);
				}
				else
				{
					// Call Compensation
					LOGGER.info("ErrorCode : " + etniResponse.getStatusCode() + ", Error Source : LoanCreation , Error Details : " + etniResponse.getStatusDesc());
					CompensationUtil compUtil = new CompensationUtil();
					compUtil.setCompensationObject(kcontext, etniResponse.getStatusCode(), "LoanCreation", etniResponse.getStatusDesc(),
							null, BPMConstants.COMP_COUNT);
				}
			}
			else
			{
				// Call Compensation
				LOGGER.info("Response object from Loan Creation is NULL for ProcessInstanceId: " + kcontext.getProcessInstance().getId() + " and ParentProcessInstanceId: "
						+ kcontext.getProcessInstance().getParentProcessInstanceId());
				CompensationUtil compUtil = new CompensationUtil();
				compUtil.setCompensationObject(kcontext, "BPMERR02", "BPM", "Response Object from Micro Service is NULL",
						null, BPMConstants.COMP_COUNT);
			}
		}
		catch (Exception e)
		{
			flag = false;
			LOGGER.error("Exception while validating response received from Loan Creation. Exception : " + e);
			throw new Exception(e);
		}
		kcontext.setVariable(BPMConstants.FLAG, flag);
	}

}